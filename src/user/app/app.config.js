(function () {

    angular.module("crytexUser").config(config);

    config.$inject = ['$resourceProvider','$mdThemingProvider'];

    function config($resourceProvider,$mdThemingProvider) {
        $resourceProvider.defaults.actions.getAllPage = { method: 'GET', isArray: false };
        $resourceProvider.defaults.actions.pagingQuery = { method: 'GET', isArray: false };
        $mdThemingProvider.definePalette('crytex', {
            '50': '5da1dc',
            '100': '5da1dc',
            '200': '5da1dc',
            '300': '5da1dc',
            '400': '5da1dc',
            '500': '5da1dc',
            '600': '5da1dc',
            '700': '5da1dc',
            '800': '5da1dc',
            '900': '5da1dc',
            'A100': '5da1dc',
            'A200': '5da1dc',
            'A400': '5da1dc',
            'A700': '5da1dc',
            'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                                // on this palette should be dark or light
            'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
                '200', '300', '400', 'A100'],
            'contrastLightColors': undefined    // could also specify this if default was 'dark'
        });


        $mdThemingProvider.theme('default')
            .primaryPalette('blue-grey').
        accentPalette('crytex');
    }

})();