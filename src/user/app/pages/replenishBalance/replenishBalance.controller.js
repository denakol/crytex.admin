var user;
(function (user) {
    var ReplenishBalanceController = (function () {
        function ReplenishBalanceController(WebServices, $state, $messageDialog, baseInfo, $filter) {
            this.WebServices = WebServices;
            this.$state = $state;
            this.$messageDialog = $messageDialog;
            this.baseInfo = baseInfo;
            this.$filter = $filter;
            this.paymentSystemService = WebServices.UserPaymentSystemService;
            this.paymentService = WebServices.UserPayment;
            this.getPaymentSystems();
            this.initializePayment();
        }
        ReplenishBalanceController.prototype.replenishBalance = function () {
            var _this = this;
            this.paymentService.save(this.payment).$promise
                .then(function (result) {
                _this.$messageDialog.show({
                    header: "Пополнение баланса",
                    body: "Ваш баланс пополнен"
                }).finally(function () {
                    _this.$state.go('personalAccount.myAccount');
                });
            })
                .catch(function (err) {
                _this.$messageDialog.show({
                    header: "Пополнение баланса",
                    body: "Произошла ошибка"
                });
            });
        };
        ReplenishBalanceController.prototype.selectPaymentSystem = function (paymentSystem) {
            if (this.selectedPaymentSystem) {
                this.selectedPaymentSystem.isSelected = false;
            }
            this.payment.paymentSystemId = paymentSystem.id;
            paymentSystem.isSelected = true;
            this.selectedPaymentSystem = paymentSystem;
        };
        ReplenishBalanceController.prototype.getPaymentSystems = function () {
            var _this = this;
            this.paymentSystemList = this.paymentSystemService.query({ searchEnabled: true });
            this.paymentSystemList.$promise
                .catch(function (err) {
                _this.$messageDialog.show({
                    header: "Платёжные системы",
                    body: "Произошла ошибка при получении доступных платёжных систем"
                });
            });
        };
        ReplenishBalanceController.prototype.initializePayment = function () {
            this.payment = {
                id: "",
                cashAmount: 0,
                paymentSystemId: "",
                date: new Date(),
            };
        };
        ReplenishBalanceController.$inject = ["WebServices", "$state", "$messageDialog", "BASE_INFO", "$filter"];
        return ReplenishBalanceController;
    }());
    user.ReplenishBalanceController = ReplenishBalanceController;
    angular.module('crytex.replenishBalance')
        .controller('ReplenishBalanceController', ReplenishBalanceController);
})(user || (user = {}));
