module user {

    import IResourceClass = angular.resource.IResourceClass;
    import PaymentSystemView = core.WebApi.Models.PaymentSystemView;
    import PaymentView = core.WebApi.Models.PaymentView;
    
    export class ReplenishBalanceController {

        static $inject = ["WebServices", "$state", "$messageDialog", "BASE_INFO", "$filter"];

        public paymentSystemList: Array<PaymentSystemView>;
        public payment: PaymentView;
        public selectedPaymentSystem: PaymentSystemView;

        private paymentSystemService: IResourceClass<PaymentSystemView>;
        private paymentService: IResourceClass<PaymentView>;

        constructor(private WebServices:any, private $state: any,
                    private $messageDialog: any, private baseInfo: any,
                    private $filter: any) {
            this.paymentSystemService = WebServices.UserPaymentSystemService;
            this.paymentService = WebServices.UserPayment;
            this.getPaymentSystems();
            this.initializePayment();
        }

        public replenishBalance () {
            this.paymentService.save(this.payment).$promise
                .then((result: any) => {
                    this.$messageDialog.show({
                        header: "Пополнение баланса",
                        body: "Ваш баланс пополнен"
                    }).finally(() => {
                        this.$state.go('personalAccount.myAccount');
                    });
                })
                .catch((err:any)=>{
                    this.$messageDialog.show({
                        header: "Пополнение баланса",
                        body: "Произошла ошибка"
                    });
                })
        }

        public selectPaymentSystem (paymentSystem: PaymentSystemView) {

            if (this.selectedPaymentSystem) {
                this.selectedPaymentSystem.isSelected = false;
            }
            this.payment.paymentSystemId = paymentSystem.id;
            paymentSystem.isSelected = true;
            this.selectedPaymentSystem = paymentSystem;
        }

        private getPaymentSystems (){
            this.paymentSystemList = this.paymentSystemService.query({searchEnabled: true});

            this.paymentSystemList.$promise
                .catch((err:any)=>{
                    this.$messageDialog.show({
                        header: "Платёжные системы",
                        body: "Произошла ошибка при получении доступных платёжных систем"
                    });
                })
        }

        private initializePayment () {
            this.payment = <PaymentView> {
                id: "",
                cashAmount: 0,
                paymentSystemId: "",
                date: new Date(),
            }
        }
    }

    angular.module('crytex.replenishBalance')
        .controller('ReplenishBalanceController', ReplenishBalanceController);
}