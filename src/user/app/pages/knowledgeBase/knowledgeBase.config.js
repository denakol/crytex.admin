(function () {

    angular.module("crytex.user.knowledgeBase").config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {

        $stateProvider
            .state('knowledgeBase', {
                url: '/knowledgeBase',
                templateUrl: 'app/pages/knowledgeBase/knowledgeBase.html',
                controller: 'KnowledgeBaseController',
                controllerAs: 'vm',
                resolve: {
                    $title: function () {
                        return 'База знаний';
                    }
                }
            }).state('knowledgeBase.controlPanel', {
                url: '/controlPanel',
                templateUrl: 'app/pages/knowledgeBase/controlPanel/controlPanel.html',
                controller: 'ControlPanelKnowledgeController',
                controllerAs: "vm",
                resolve: {
                    $title: function () {
                        return 'Инструкции по панели управления';
                    }
                }
            }).state('knowledgeBase.directoryTemplateUser', {
                url: '/directoryTemplateUser',
                templateUrl: 'app/pages/knowledgeBase/directoryTemplateUser/directoryTemplateUser.html',
                controller: 'DirectoryTemplateUserKnowledgeController',
                controllerAs: "vm",
                resolve: {
                    $title: function () {
                        return 'Каталоги, шаблоны, пользователи';
                    }
                }
            }).state('knowledgeBase.generalQuestion', {
                url: '/generalQuestion',
                templateUrl: 'app/pages/knowledgeBase/generalQuestion/generalQuestion.html',
                controller: 'GeneralQuestionKnowledgeController',
                controllerAs: "vm",
                resolve: {
                    $title: function () {
                        return 'Общие вопросы';
                    }
                }
            }).state('knowledgeBase.manageNet', {
                url: '/manageNet',
                templateUrl: 'app/pages/knowledgeBase/manageNet/manageNet.html',
                controller: 'ManageNetKnowledgeController',
                controllerAs: "vm",
                resolve: {
                    $title: function () {
                        return 'Настройка сети';
                    }
                }
            }).state('knowledgeBase.manageVirtualMachine', {
                url: '/manageVirtualMachine',
                templateUrl: 'app/pages/knowledgeBase/manageVirtualMachine/manageVirtualMachine.html',
                controller: 'ManageVirtualMachineKnowledgeController',
                controllerAs: "vm",
                resolve: {
                    $title: function () {
                        return 'Настройка и управление виртуальными машинами';
                    }
                }
            }).state('knowledgeBase.personalAccount', {
                url: '/personalAccount',
                templateUrl: 'app/pages/knowledgeBase/personalAccount/personalAccount.html',
                controller: 'PersonalAccountKnowledgeController',
                controllerAs: "vm",
                resolve: {
                    $title: function () {
                        return 'Личный кабинет и биллинг';
                    }
                }
            }).state('knowledgeBase.SLA', {
                url: '/SLA',
                templateUrl: 'app/pages/knowledgeBase/SLA/SLA.html',
                controller: 'SLAKnowledgeController',
                controllerAs: "vm",
                resolve: {
                    $title: function () {
                        return 'SLA';
                    }
                }
            }).state('knowledgeBase.techniqueQuestion', {
                url: '/techniqueQuestion',
                templateUrl: 'app/pages/knowledgeBase/techniqueQuestion/techniqueQuestion.html',
                controller: 'TechniqueQuestionKnowledgeController',
                controllerAs: "vm",
                resolve: {
                    $title: function () {
                        return 'Технические вопросы';
                    }
                }
            });

    }

})();
