var user;
(function (user) {
    var ManageVirtualMachineKnowledgeController = (function () {
        function ManageVirtualMachineKnowledgeController() {
        }
        return ManageVirtualMachineKnowledgeController;
    }());
    user.ManageVirtualMachineKnowledgeController = ManageVirtualMachineKnowledgeController;
    angular.module('knowledgeBase.manageVirtualMachine')
        .controller('ManageVirtualMachineKnowledgeController', ManageVirtualMachineKnowledgeController);
})(user || (user = {}));
