var user;
(function (user) {
    var PersonalAccountKnowledgeController = (function () {
        function PersonalAccountKnowledgeController() {
        }
        return PersonalAccountKnowledgeController;
    }());
    user.PersonalAccountKnowledgeController = PersonalAccountKnowledgeController;
    angular.module('knowledgeBase.personalAccount')
        .controller('PersonalAccountKnowledgeController', PersonalAccountKnowledgeController);
})(user || (user = {}));
