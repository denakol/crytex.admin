var user;
(function (user) {
    var ControlPanelKnowledgeController = (function () {
        function ControlPanelKnowledgeController() {
        }
        return ControlPanelKnowledgeController;
    }());
    user.ControlPanelKnowledgeController = ControlPanelKnowledgeController;
    angular.module('knowledgeBase.controlPanel')
        .controller('ControlPanelKnowledgeController', ControlPanelKnowledgeController);
})(user || (user = {}));
