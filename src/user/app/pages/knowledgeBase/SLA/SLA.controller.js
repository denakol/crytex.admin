var user;
(function (user) {
    var SLAKnowledgeController = (function () {
        function SLAKnowledgeController() {
        }
        return SLAKnowledgeController;
    }());
    user.SLAKnowledgeController = SLAKnowledgeController;
    angular.module('knowledgeBase.SLA')
        .controller('SLAKnowledgeController', SLAKnowledgeController);
})(user || (user = {}));
