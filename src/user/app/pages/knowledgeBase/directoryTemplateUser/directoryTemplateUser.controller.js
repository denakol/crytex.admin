var user;
(function (user) {
    var DirectoryTemplateUserKnowledgeController = (function () {
        function DirectoryTemplateUserKnowledgeController() {
        }
        return DirectoryTemplateUserKnowledgeController;
    }());
    user.DirectoryTemplateUserKnowledgeController = DirectoryTemplateUserKnowledgeController;
    angular.module('knowledgeBase.directoryTemplateUser')
        .controller('DirectoryTemplateUserKnowledgeController', DirectoryTemplateUserKnowledgeController);
})(user || (user = {}));
