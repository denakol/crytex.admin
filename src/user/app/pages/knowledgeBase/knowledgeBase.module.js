(function () {

    angular.module("crytex.user.knowledgeBase", [
        'knowledgeBase.controlPanel',
        'knowledgeBase.directoryTemplateUser',
        'knowledgeBase.generalQuestion',
        'knowledgeBase.manageNet',
        'knowledgeBase.manageVirtualMachine',
        'knowledgeBase.personalAccount',
        'knowledgeBase.SLA',
        'knowledgeBase.techniqueQuestion'
    ]);

})();