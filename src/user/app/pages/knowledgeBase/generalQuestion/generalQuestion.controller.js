var user;
(function (user) {
    var GeneralQuestionKnowledgeController = (function () {
        function GeneralQuestionKnowledgeController() {
        }
        return GeneralQuestionKnowledgeController;
    }());
    user.GeneralQuestionKnowledgeController = GeneralQuestionKnowledgeController;
    angular.module('knowledgeBase.generalQuestion')
        .controller('GeneralQuestionKnowledgeController', GeneralQuestionKnowledgeController);
})(user || (user = {}));
