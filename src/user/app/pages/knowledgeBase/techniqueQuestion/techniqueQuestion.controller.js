var user;
(function (user) {
    var TechniqueQuestionKnowledgeController = (function () {
        function TechniqueQuestionKnowledgeController() {
        }
        return TechniqueQuestionKnowledgeController;
    }());
    user.TechniqueQuestionKnowledgeController = TechniqueQuestionKnowledgeController;
    angular.module('knowledgeBase.techniqueQuestion')
        .controller('TechniqueQuestionKnowledgeController', TechniqueQuestionKnowledgeController);
})(user || (user = {}));
