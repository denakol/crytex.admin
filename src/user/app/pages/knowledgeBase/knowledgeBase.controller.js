var user;
(function (user) {
    var KnowledgeBaseController = (function () {
        function KnowledgeBaseController() {
        }
        return KnowledgeBaseController;
    }());
    user.KnowledgeBaseController = KnowledgeBaseController;
    angular.module('crytex.user.knowledgeBase')
        .controller('KnowledgeBaseController', KnowledgeBaseController);
})(user || (user = {}));
