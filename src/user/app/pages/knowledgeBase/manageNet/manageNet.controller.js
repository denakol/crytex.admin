var user;
(function (user) {
    var ManageNetKnowledgeController = (function () {
        function ManageNetKnowledgeController() {
        }
        return ManageNetKnowledgeController;
    }());
    user.ManageNetKnowledgeController = ManageNetKnowledgeController;
    angular.module('knowledgeBase.manageNet')
        .controller('ManageNetKnowledgeController', ManageNetKnowledgeController);
})(user || (user = {}));
