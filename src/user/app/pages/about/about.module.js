/**
 * Created by denak on 19.01.2016.
 */
(function () {

    angular.module("crytex.user.about", [
        'about.news',
        'about.document',
        'about.license',
        'about.summary'
    ]);

})();