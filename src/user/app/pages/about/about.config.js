/**
 * Created by denak on 19.01.2016.
 */
(function () {

    angular.module("crytex.user.about").config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {

        $stateProvider
            .state('about', {
                url: '/about',
                templateUrl: 'app/pages/about/about.html',
                controller: 'AboutController',
                resolve: {
                    $title: function () {
                        return 'О нас';
                    }
                }
            })
            .state('about.summary', {
            url: '/summary',
            templateUrl: 'app/pages/about/summary/summary.html',
            controller: 'SummaryController',
            resolve: {
                $title: function () {
                    return 'О нас';
                }
            }
        }).state('about.news', {
            url: '/news',
            templateUrl: 'app/pages/about/news/news.html',
            controller: 'NewsController',
            controllerAs: 'vm',
            resolve: {
                $title: function () {
                    return 'Новости';
                }
            }
        }).state('about.license', {
            url: '/license',
            templateUrl: 'app/pages/about/license/license.html',
            controller: 'LicenseController',
            resolve: {
                $title: function () {
                    return 'Лицензии';
                }
            }
        }).state('about.document', {
            url: '/document',
            templateUrl: 'app/pages/about/document/document.html',
            controller: 'DocumentController',
            resolve: {
                $title: function () {
                    return 'Документы';
                }
            }
        })

    };

})();