module user {

    import IResourceClass = angular.resource.IResourceClass;
    import PageModel = core.PageModel;
    import PaginationSetting = core.PaginationSetting;
    import NewsViewModel = core.WebApi.Models.NewsViewModel;

    export class NewsController {

        static $inject = ["WebServices", "$mdDialog"];

        public news:PageModel<NewsViewModel>;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        }
        public changePage: boolean = false;

        private NewsService: IResourceClass<PageModel<NewsViewModel>>;

        constructor(private WebServices: any, private messageDialog: any) {
            this.NewsService = WebServices.UserNews;
            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;
            this.changePage = false;
            this.getNews();
        }

        public pageChangeHandler (index: number) {
            this.paginationSettings.pageNumber = index;
            this.changePage = true;
            this.getNews();
        }

        private getNews() {
            this.news = this.NewsService.getAllPage(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize
                });

            this.news.$promise
                .catch ( (response:any) => {
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }


    }

    angular.module('about.news')
        .controller('NewsController', NewsController);
}
