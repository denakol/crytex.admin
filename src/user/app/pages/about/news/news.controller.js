var user;
(function (user) {
    var NewsController = (function () {
        function NewsController(WebServices, messageDialog) {
            this.WebServices = WebServices;
            this.messageDialog = messageDialog;
            this.paginationSettings = {
                pageNumber: 1,
                pageSize: 5
            };
            this.changePage = false;
            this.NewsService = WebServices.UserNews;
            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;
            this.changePage = false;
            this.getNews();
        }
        NewsController.prototype.pageChangeHandler = function (index) {
            this.paginationSettings.pageNumber = index;
            this.changePage = true;
            this.getNews();
        };
        NewsController.prototype.getNews = function () {
            var _this = this;
            this.news = this.NewsService.getAllPage({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize
            });
            this.news.$promise
                .catch(function (response) {
                _this.messageDialog.show({
                    header: "Сервер",
                    body: "Ошибка сервера"
                });
                return;
            });
        };
        NewsController.$inject = ["WebServices", "$mdDialog"];
        return NewsController;
    }());
    user.NewsController = NewsController;
    angular.module('about.news')
        .controller('NewsController', NewsController);
})(user || (user = {}));
