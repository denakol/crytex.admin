var user;
(function (user) {
    var WebHostingController = (function () {
        function WebHostingController(callMeService, dialog) {
            this.callMeService = callMeService;
            this.dialog = dialog;
            this.userPhone = null;
        }
        WebHostingController.prototype.orderCall = function () {
            var _this = this;
            this.callMeService.orderCall(this.userPhone).then(function (result) {
                _this.showMessage(result);
                _this.clearUserPhone();
            }, function (response) {
                var error = {
                    errorCode: 3,
                    errorText: '',
                    messageHeader: 'У нас ошибка',
                    messageText: 'Попробуйте позже'
                };
                _this.showMessage(error);
                _this.clearUserPhone();
                return error;
            });
        };
        WebHostingController.prototype.showMessage = function (data) {
            this.dialog.show(this.dialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title(data.messageHeader)
                .textContent(data.messageText)
                .ok('ОК'));
        };
        WebHostingController.prototype.clearUserPhone = function () {
            this.userPhone = null;
        };
        WebHostingController.$inject = ["callMeService", "$mdDialog"];
        return WebHostingController;
    }());
    user.WebHostingController = WebHostingController;
    angular.module('crytexUser.webHosting')
        .controller('webHostingController', WebHostingController);
})(user || (user = {}));
