/**
 * Created by denak on 25.01.2016.
 */
(function () {

    angular.module("pages.auth").config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('confirmEmail', {
                url: '/account/verify?{userId}&{code}',
                templateUrl: 'app/pages/home/home.html',
                controller: 'VerifyController',
                resolve: {
                    $title: function () {
                        return 'Домашняя';
                    }
                }
            })
            .state('resetPassword', {
                url: '/account/resetPassword?{userId}&{code}',
                templateUrl: 'app/pages/home/home.html',
                controller: 'ResetPasswordController',
                resolve: {
                    $title: function () {
                        return 'Домашняя';
                    }
                }
            });
    }

})();