/**
 * Created by ruslan on 29.01.2016.
 */
declare module user {
    import IAauthService = core.IAauthService;
    import LoginModel = core.LoginModel;
    import IDialogService = angular.material.IDialogService;
    import IStateService = angular.ui.IStateService;
    class ResetPasswordController {
        private $mdDialog;
        private AuthService;
        private $state;
        private $messageDialog;
        private resetPasswordDialogService;
        static $inject: string[];
        user: LoginModel;
        constructor($mdDialog: IDialogService, AuthService: IAauthService, $state: IStateService, $messageDialog: any, resetPasswordDialogService: ResetPasswordDialogService);
        redirectToHome(): void;
    }
}
