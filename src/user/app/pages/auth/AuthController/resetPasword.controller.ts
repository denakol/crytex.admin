/**
 * Created by ruslan on 29.01.2016.
 */
module user {

    import IAauthService = core.IAauthService;
    import LoginModel = core.LoginModel;
    import IDialogService = angular.material.IDialogService;
    import IStateService = angular.ui.IStateService;


    export class ResetPasswordController {
        static $inject = ['$mdDialog', 'AuthService', '$state', '$messageDialog', 'resetPasswordDialogService'];

        public user:LoginModel;

        constructor(private $mdDialog:IDialogService, private AuthService:IAauthService, private $state:IStateService, private $messageDialog:any, private resetPasswordDialogService:ResetPasswordDialogService) {

            resetPasswordDialogService.show().then(function () {

                $messageDialog.show({
                    header: "Восстановление пароля",
                    body: "Ваш пароль изменен. Выполните вход."
                }).then(()=> {
                    this.redirectToHome();
                });
            }, function () {
                $messageDialog.show({
                    header: "Восстановление пароля",
                    body: "Ваш пароль не удалось изменить. Выполните повторную попытку, либо обратитесь в поддержку."
                }).then(()=> {
                    this.redirectToHome();
                });
            })

        }

        redirectToHome() {
            this.$state.go("home");
        }

    }


    angular.module('pages.auth')
        .controller('ResetPasswordController', ResetPasswordController);

}

