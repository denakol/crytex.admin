/**
 * Created by ruslan on 29.01.2016.
 */
var user;
(function (user) {
    var ResetPasswordController = (function () {
        function ResetPasswordController($mdDialog, AuthService, $state, $messageDialog, resetPasswordDialogService) {
            this.$mdDialog = $mdDialog;
            this.AuthService = AuthService;
            this.$state = $state;
            this.$messageDialog = $messageDialog;
            this.resetPasswordDialogService = resetPasswordDialogService;
            resetPasswordDialogService.show().then(function () {
                var _this = this;
                $messageDialog.show({
                    header: "Восстановление пароля",
                    body: "Ваш пароль изменен. Выполните вход."
                }).then(function () {
                    _this.redirectToHome();
                });
            }, function () {
                var _this = this;
                $messageDialog.show({
                    header: "Восстановление пароля",
                    body: "Ваш пароль не удалось изменить. Выполните повторную попытку, либо обратитесь в поддержку."
                }).then(function () {
                    _this.redirectToHome();
                });
            });
        }
        ResetPasswordController.prototype.redirectToHome = function () {
            this.$state.go("home");
        };
        ResetPasswordController.$inject = ['$mdDialog', 'AuthService', '$state', '$messageDialog', 'resetPasswordDialogService'];
        return ResetPasswordController;
    }());
    user.ResetPasswordController = ResetPasswordController;
    angular.module('pages.auth')
        .controller('ResetPasswordController', ResetPasswordController);
})(user || (user = {}));
