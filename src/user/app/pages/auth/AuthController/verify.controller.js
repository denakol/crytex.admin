(function () {

    angular.module('pages.auth')
        .controller('VerifyController', controller);

    controller.$inject = ['$stateParams','AuthService','$messageDialog','$state'];

    function controller($stateParams,AuthService,$messageDialog,$state) {
        AuthService.verify($stateParams)
            .then(function(){
                $messageDialog.show({header:"Подтверждение учетной записи",body:"Ваша учетная запись подтверждена. Выполните вход."}).then(function(){
                    $state.go('home');
                },function(){
                    $state.go('home');
                });
        });


    }

})();
