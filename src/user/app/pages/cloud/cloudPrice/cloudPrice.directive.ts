/**
 * Created by denak on 09.02.2016.
 */
module user {

    import ResourceType = core.WebApi.Models.Enums.ResourceType;
    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    export class CloudCardController {

        public static $inject = ["calculateDiscountService", "$scope"];

        public subscriptionTypes : typeof SubscriptionType = SubscriptionType;
        
        public discountSize:number;

        constructor(private calculateDiscountService:user.CalculateDiscount,
                    private $scope:any){
            $scope.$watch(()=> this.vmOption.countMonth, ()=> {
                calculateDiscountService.getDiscountSizeWithWait(ResourceType.Vm, this.vmOption.countMonth)
                    .then((discountSize:number)=>{
                        if(discountSize!=0){
                            this.discountSize = discountSize/100;
                        } else {
                            this.discountSize = discountSize;
                        }
                    });
            });
        }
    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            vmOption: '<',
            vmBuy:'&',
            buyLoading: '='
        },
        templateUrl: 'app/pages/cloud/cloudPrice/cloudPrice.html',
        controller: CloudCardController,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("cloudPrice",option);
}