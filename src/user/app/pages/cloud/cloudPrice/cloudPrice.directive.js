/**
 * Created by denak on 09.02.2016.
 */
var user;
(function (user) {
    var ResourceType = core.WebApi.Models.Enums.ResourceType;
    var SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    var CloudCardController = (function () {
        function CloudCardController(calculateDiscountService, $scope) {
            var _this = this;
            this.calculateDiscountService = calculateDiscountService;
            this.$scope = $scope;
            this.subscriptionTypes = SubscriptionType;
            $scope.$watch(function () { return _this.vmOption.countMonth; }, function () {
                calculateDiscountService.getDiscountSizeWithWait(ResourceType.Vm, _this.vmOption.countMonth)
                    .then(function (discountSize) {
                    if (discountSize != 0) {
                        _this.discountSize = discountSize / 100;
                    }
                    else {
                        _this.discountSize = discountSize;
                    }
                });
            });
        }
        CloudCardController.$inject = ["calculateDiscountService", "$scope"];
        return CloudCardController;
    }());
    user.CloudCardController = CloudCardController;
    var option = {
        restrict: "EA",
        bindings: {
            vmOption: '<',
            vmBuy: '&',
            buyLoading: '='
        },
        templateUrl: 'app/pages/cloud/cloudPrice/cloudPrice.html',
        controller: CloudCardController,
        controllerAs: 'vm'
    };
    angular.module("user.cloud")
        .component("cloudPrice", option);
})(user || (user = {}));
