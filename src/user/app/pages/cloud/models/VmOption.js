/**
 * Created by denak on 04.02.2016.
 */
var user;
(function (user) {
    var SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    var VmOption = (function () {
        function VmOption() {
            this.ssd = 0;
            this.backupStoring = 1;
            this.subscriptionType = SubscriptionType.Fixed;
        }
        VmOption.GetFromVm = function (userVm, subscriptionType) {
            var vmOption = new VmOption();
            vmOption.virtualization = userVm.virtualizationType;
            vmOption.cpu = userVm.coreCount;
            vmOption.ram = userVm.ramCount;
            vmOption.hdd = userVm.hardDriveSize;
            vmOption.currentCpu = userVm.coreCount;
            vmOption.currentRam = userVm.ramCount;
            vmOption.currenHdd = userVm.hardDriveSize;
            vmOption.userOperatingSystem = userVm.operatingSystem;
            vmOption.subscriptionType = subscriptionType;
            return vmOption;
        };
        return VmOption;
    }());
    user.VmOption = VmOption;
})(user || (user = {}));
