/**
 * Created by denak on 04.02.2016.
 */
module user {


    import TypeVirtualization = core.WebApi.Models.Enums.TypeVirtualization;
    import UserOperatingSystemViewModel = core.WebApi.Models.UserOperatingSystemViewModel;
    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    import UserVmViewModel = core.WebApi.Models.UserVmViewModel;
    export class VmOption {
        name:string;
        virtualization:TypeVirtualization;
        cpu:number;
        ram:number;
        hdd:number;
        ssd:number = 0;
        backupStoring:number = 1;
        price:number;
        countMonth:number;
        userOperatingSystem:UserOperatingSystemViewModel;
        subscriptionType:SubscriptionType = SubscriptionType.Fixed;
        currentCpu:number;
        currentRam:number;
        currenHdd:number;

        public static
        GetFromVm(userVm:UserVmViewModel, subscriptionType:SubscriptionType):VmOption {

            var vmOption = new VmOption();
            vmOption.virtualization = userVm.virtualizationType;
            vmOption.cpu = userVm.coreCount;
            vmOption.ram = userVm.ramCount;
            vmOption.hdd = userVm.hardDriveSize;
            vmOption.currentCpu= userVm.coreCount;
            vmOption.currentRam= userVm.ramCount;
            vmOption.currenHdd= userVm.hardDriveSize;
            vmOption.userOperatingSystem = userVm.operatingSystem;
            vmOption.subscriptionType = subscriptionType;
            return vmOption;
        }


    }
}