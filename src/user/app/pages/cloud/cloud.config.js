(function () {

    angular.module("user.cloud").config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {

        $stateProvider
            .state('cloudHosting', {
                url: '/cloudHosting',
                templateUrl: 'app/pages/cloud/pages/cloudHosting/cloudHosting.html',
                controller: 'CloudHostingController',
                controllerAs: 'vm',
                resolve: {
                    $title: function () {
                        return 'Облачный хостинг';
                    }
                }
            }).state('cloudHostingCalculator', {
            url: '/cloudHostingBuy/:virtualization',
            templateUrl: 'app/pages/cloud/pages/cloudHostingBuy/cloudHostingBuy.html',
            controller: 'CloudHostingBuyController',
            controllerAs:'vm',
            resolve: {
                $title: function () {
                    return 'Облачный хостинг. Калькулятор';
                }
            }
        });
    }

})();
