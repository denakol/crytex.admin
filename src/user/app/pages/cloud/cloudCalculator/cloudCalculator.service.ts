/*
/!**
 * Created by denak on 04.02.2016.
 *!/
module core {


    import IResourceArray = angular.resource.IResourceArray;
    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    import TariffViewModel = core.WebApi.Models.TariffViewModel;
    import IQService = angular.IQService;
    import IPromise = angular.IPromise;

    export class CloudCalculator {

        static $inject = ["WebServices","$q"]

        public userPrice:IResourceArray<TariffViewModel>;

        constructor(WebServices:any, public $q:IQService) {
            this.userPrice = WebServices.UserPriceService.query();

        }


        public CalculatePrice(vmOption:user.VmOption):PriceVm {


            if (this.userPrice.$resolved && vmOption.userOperatingSystem) {
                var price = 0;
                var tariff = this.userPrice.find((elem:TariffViewModel)=> {
                    return elem.virtualization == vmOption.virtualization && elem.operatingSystem == vmOption.userOperatingSystem.family;
                });

                if (!tariff) {
                    return new PriceVm();
                }
                if (vmOption.backupStoring > 0) {
                    vmOption.backupStoring -= 1;
                }

                tariff.backupStoringGb = 0;
                var priceCpu = tariff.processor1 * vmOption.cpu;
                var priceRam = tariff.raM512 * vmOption.ram / 512;
                var priceHdd = tariff.hdD1 * vmOption.hdd;
                price =
                    priceCpu + priceRam + priceHdd +

                    tariff.backupStoringGb * vmOption.backupStoring * (vmOption.hdd + vmOption.ssd);


                //refactor need

                var priceResult =new PriceVm(price, priceCpu, priceRam, priceHdd);
                if (vmOption.subscriptionType == SubscriptionType.Usage) {
                    priceResult. price = priceResult.price / ( 30 * 24);

                }
                priceResult.cpuPrice /= ( 30 * 24);
                priceResult.ramPrice /= ( 30 * 24);
                priceResult.hddPrice /= ( 30 * 24);
                return priceResult;
            }
            return new PriceVm();
        }

        public CalculatePriceWithWait(vmOption:user.VmOption):IPromise<PriceVm> {
            var deferred = this.$q.defer();
            this.userPrice.$promise.then(()=> {
                deferred.resolve( this.CalculatePrice(vmOption));
                }
            );


            return deferred.promise;
        }
    }


    export class PriceVm {
        constructor(public price:number = 0,
                    public cpuPrice:number = 0,
                    public  ramPrice:number = 0,
                    public hddPrice:number = 0) {

            var hourlyKoef = 1/(30 * 24);
            var hourlyDayKoef = 1/30;
            this.priceHourly = new Price(cpuPrice*hourlyKoef,ramPrice*hourlyKoef,hddPrice*hourlyKoef);
            this.priceDay= new Price(cpuPrice*hourlyDayKoef,ramPrice*hourlyDayKoef,hddPrice*hourlyDayKoef);
            this.priceMonth= new Price(cpuPrice,ramPrice,hddPrice);

        }

        public priceHourly:Price;
        public priceDay:Price;
        public priceMonth:Price;


    }

    export class Price {

        public get Sum():number {
            return this.cpu + this.ram + this.hdd;
        }

        constructor(public cpu:number,
                    public ram:number,
                    public hdd:number) {
        }


    }
    angular.module("blocks.services").service("cloudCalculator", CloudCalculator);
}
*/
