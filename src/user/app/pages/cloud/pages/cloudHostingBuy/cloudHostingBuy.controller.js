/**
 * Created by denak on 04.02.2016.
 */
var user;
(function (user_1) {
    var SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    var TypeVirtualization = core.WebApi.Models.Enums.TypeVirtualization;
    var CloudHostingBuyController = (function () {
        function CloudHostingBuyController($stateParams, localStorageService, loginForm, $state) {
            this.localStorageService = localStorageService;
            this.loginForm = loginForm;
            this.$state = $state;
            this.subscriptionTypes = SubscriptionType;
            this.vmOption = new user_1.VmOption();
            this.virtualizations = TypeVirtualization;
            this.vmOption.virtualization = $stateParams.virtualization;
        }
        CloudHostingBuyController.prototype.buy = function () {
            var user = this.localStorageService.get('authorizationData');
            this.localStorageService.set('vmOption', this.vmOption);
            if (!user) {
                this.loginForm.show();
            }
            else {
                this.$state.go('personalAccount.buyService.buyCloud');
            }
        };
        CloudHostingBuyController.$inject = ['$stateParams', 'localStorageService', '$loginForm', '$state'];
        return CloudHostingBuyController;
    }());
    user_1.CloudHostingBuyController = CloudHostingBuyController;
    ;
    angular.module('user.cloud')
        .controller('CloudHostingBuyController', CloudHostingBuyController);
})(user || (user = {}));
