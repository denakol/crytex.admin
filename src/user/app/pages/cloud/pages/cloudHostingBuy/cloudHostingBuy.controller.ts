/**
 * Created by denak on 04.02.2016.
 */
module user {

    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    import TypeVirtualization = core.WebApi.Models.Enums.TypeVirtualization;
    export class CloudHostingBuyController {

        static $inject = ['$stateParams','localStorageService','$loginForm','$state']
        public subscriptionTypes : typeof SubscriptionType = SubscriptionType;
        public vmOption:VmOption = new VmOption();
        public virtualizations : typeof TypeVirtualization = TypeVirtualization;


        buy(){
            var user = this.localStorageService.get('authorizationData');
            this.localStorageService.set('vmOption', this.vmOption);

            if(!user){
                this.loginForm.show();
            } else {
                this.$state.go('personalAccount.buyService.buyCloud');
            }
        }

        constructor($stateParams:any,private localStorageService:any, private loginForm:any, private $state:any) {

            this.vmOption.virtualization = $stateParams.virtualization;
        }
    };

    angular.module('user.cloud')
        .controller('CloudHostingBuyController', CloudHostingBuyController);
}

