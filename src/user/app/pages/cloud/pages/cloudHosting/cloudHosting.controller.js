var user;
(function (user) {
    var CloudHostingController = (function () {
        function CloudHostingController(callMeService, dialog) {
            this.callMeService = callMeService;
            this.dialog = dialog;
            this.userPhone = null;
            this.choosedType = [true, false];
            this.defaultChoosedType = null;
        }
        CloudHostingController.prototype.chooseType = function (index) {
            if (this.defaultChoosedType != null) {
                if (this.defaultChoosedType != index) {
                    this.choosedType[index] = true;
                    this.choosedType[this.defaultChoosedType] = false;
                    this.defaultChoosedType = index;
                }
            }
            else {
                this.choosedType[index] = true;
                this.defaultChoosedType = index;
            }
        };
        CloudHostingController.prototype.orderCall = function () {
            var _this = this;
            this.callMeService.orderCall(this.userPhone).then(function (result) {
                _this.showMessage(result);
                _this.clearUserPhone();
            }, function (response) {
                var error = {
                    errorCode: 3,
                    errorText: '',
                    messageHeader: 'У нас ошибка',
                    messageText: 'Попробуйте позже'
                };
                _this.showMessage(error);
                _this.clearUserPhone();
                return error;
            });
        };
        CloudHostingController.prototype.showMessage = function (data) {
            this.dialog.show(this.dialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title(data.messageHeader)
                .textContent(data.messageText)
                .ok('ОК'));
        };
        CloudHostingController.prototype.clearUserPhone = function () {
            this.userPhone = null;
        };
        CloudHostingController.$inject = ["callMeService", "$mdDialog"];
        return CloudHostingController;
    }());
    user.CloudHostingController = CloudHostingController;
    angular.module('user.cloud')
        .controller('CloudHostingController', CloudHostingController);
})(user || (user = {}));
