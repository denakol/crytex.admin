declare module user {
    class CloudHostingController {
        private callMeService;
        private dialog;
        static $inject: string[];
        userPhone: string;
        choosedType: Array<boolean>;
        defaultChoosedType: number;
        constructor(callMeService: any, dialog: any);
        chooseType(index: number): void;
        orderCall(): void;
        showMessage(data: ICallMeResult): void;
        clearUserPhone(): void;
    }
}
