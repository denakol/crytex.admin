module user {

    export class CloudHostingController {
        static $inject = ["callMeService", "$mdDialog"];

        userPhone: string = null;
        choosedType: Array<boolean> = [ true, false];
        defaultChoosedType: number = null;

        constructor (private callMeService: any, private  dialog: any){
        }

        chooseType (index: number) {
            if (this.defaultChoosedType != null) {
                if (this.defaultChoosedType != index) {
                    this.choosedType[index] = true;
                    this.choosedType[this.defaultChoosedType] = false;
                    this.defaultChoosedType = index;
                }
            } else {
                this.choosedType[index] = true;
                this.defaultChoosedType = index;
            }
        }

        orderCall() {
            this.callMeService.orderCall(this.userPhone).then( (result: ICallMeResult) => {
                    this.showMessage(result);
                    this.clearUserPhone();
                },
                (response:any) => {
                    var error : ICallMeResult = {
                        errorCode: 3,
                        errorText: '',
                        messageHeader: 'У нас ошибка',
                        messageText: 'Попробуйте позже'
                    }
                    this.showMessage(error);
                    this.clearUserPhone();
                    return error;
                }
            );
        }

        showMessage(data: ICallMeResult) {
            this.dialog.show(
                this.dialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title(data.messageHeader)
                    .textContent(data.messageText)
                    .ok('ОК')
            );
        }

        clearUserPhone() {
            this.userPhone = null;
        }


    }

    angular.module('user.cloud')
        .controller('CloudHostingController', CloudHostingController);
}
