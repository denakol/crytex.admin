/**
 * Created by denak on 04.02.2016.
 */
var user;
(function (user) {
    var SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    var ResourceType = core.WebApi.Models.Enums.ResourceType;
    var CloudHostingCalculatorController = (function () {
        function CloudHostingCalculatorController($scope, cloudCalculator, WebServices, $filter) {
            var _this = this;
            this.cloudCalculator = cloudCalculator;
            this.WebServices = WebServices;
            this.$filter = $filter;
            this.subscriptionTypes = SubscriptionType;
            this.currentPeriod = 2;
            this.getDiscounts();
            this.userOperatingSystem = WebServices.UserOperatingSystem.query();
            this.userOperatingSystem.$promise.then(function () {
                _this.vmOption.userOperatingSystem = _this.userOperatingSystem[0];
            });
            $scope.$watch(function () { return _this.vmOption.userOperatingSystem; }, function () {
                if (_this.vmOption.userOperatingSystem != null) {
                    _this.vmOption.currentCpu = _this.vmOption.userOperatingSystem.minCoreCount;
                    _this.vmOption.currentRam = _this.vmOption.userOperatingSystem.minRamCount;
                    _this.vmOption.currenHdd = _this.vmOption.userOperatingSystem.minHardDriveSize;
                }
            });
            if (!this.vmOption.countMonth) {
                this.vmOption.countMonth = 6;
            }
        }
        CloudHostingCalculatorController.prototype.btnSelect = function (index) {
            if (index != this.currentPeriod) {
                this.periods[index].isSelected = true;
                this.periods[this.currentPeriod].isSelected = false;
                this.currentPeriod = index;
                if (index == 0) {
                    this.vmOption.countMonth = 1;
                }
                else {
                    this.vmOption.countMonth = 3 * Math.pow(2, (index - 1));
                }
            }
        };
        CloudHostingCalculatorController.prototype.getDiscounts = function () {
            var _this = this;
            this.WebServices.UserLongTermDiscountService.query().$promise
                .then(function (data) {
                _this.discounts = data;
                _this.initializePeriods();
            });
        };
        CloudHostingCalculatorController.prototype.initializePeriods = function () {
            this.periods = [
                {
                    month: 1,
                    isSelected: false
                },
                {
                    month: 3,
                    isSelected: false
                },
                {
                    month: 6,
                    isSelected: true
                },
                {
                    month: 12,
                    isSelected: false
                }
            ];
            for (var i = 0; i < this.periods.length; i++) {
                var monthCount = this.periods[i].month;
                var discountItem = this.$filter('filter')(this.discounts, {
                    monthCount: monthCount,
                    resourceType: ResourceType.Vm
                });
                this.periods[i].discount = discountItem.length ? discountItem[0].discountSize : 0;
            }
        };
        CloudHostingCalculatorController.$inject = ['$scope', 'cloudCalculator', 'WebServices', '$filter'];
        return CloudHostingCalculatorController;
    }());
    user.CloudHostingCalculatorController = CloudHostingCalculatorController;
    function cloudHostingCalculator() {
        return {
            restrict: 'AE',
            scope: {
                vmOption: '=vmOption'
            },
            bindToController: true,
            templateUrl: 'app/pages/cloud/cloudHostingCalculator/cloudHostingCalculator.html',
            controller: CloudHostingCalculatorController,
            controllerAs: 'vm'
        };
    }
    angular.module("user.cloud")
        .directive("cloudHostingCalculator", cloudHostingCalculator);
})(user || (user = {}));
