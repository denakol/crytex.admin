/**
 * Created by denak on 04.02.2016.
 */
module user {


    import IScope = angular.IScope;
    import IResourceArray = angular.resource.IResourceArray;
    import IWebServices = core.IWebServices;
    import UserOperatingSystemViewModel = core.WebApi.Models.UserOperatingSystemViewModel;
    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    import SliderModel = user.SliderModel;
    import VmOption = user.VmOption;
    import DiscountViewModel = core.WebApi.Models.DiscountViewModel
    import ResourceType = core.WebApi.Models.Enums.ResourceType;

    export interface VmPeriod {
        month: number;
        discount: number;
        isSelected: boolean;
    }

    export class CloudHostingCalculatorController {
        static $inject = ['$scope', 'cloudCalculator', 'WebServices', '$filter'];
        public vmOption:VmOption;

        //public sliderModel:SliderModel = new SliderModel();
        public sliderModel:SliderModel;
        public userOperatingSystem:IResourceArray<UserOperatingSystemViewModel>;
        public subscriptionTypes:typeof SubscriptionType = SubscriptionType;

        public corePrice:number;
        public ramPrice:number;
        public hddPrice:number;

        public discounts: DiscountViewModel;
        public periods: Array<VmPeriod>;

        private currentPeriod:number = 2;

        constructor($scope:any, public cloudCalculator:core.CloudCalculator, public WebServices:IWebServices,
                    private $filter:any) {

            this.getDiscounts();

            this.userOperatingSystem = WebServices.UserOperatingSystem.query();

            this.userOperatingSystem.$promise.then(()=> {
                this.vmOption.userOperatingSystem = this.userOperatingSystem[0];


            });

            $scope.$watch(()=>this.vmOption.userOperatingSystem, ()=> {
                if (this.vmOption.userOperatingSystem != null) {
                    this.vmOption.currentCpu = this.vmOption.userOperatingSystem.minCoreCount;
                    this.vmOption.currentRam = this.vmOption.userOperatingSystem.minRamCount;
                    this.vmOption.currenHdd = this.vmOption.userOperatingSystem.minHardDriveSize;
                }

            });

            if(!this.vmOption.countMonth){
                this.vmOption.countMonth = 6;
            }
        }

        public btnSelect(index:number){
            if (index != this.currentPeriod) {
                this.periods[index].isSelected = true;
                this.periods[this.currentPeriod].isSelected = false;
                this.currentPeriod = index;

                if (index == 0) {
                    this.vmOption.countMonth = 1;
                } else {
                    this.vmOption.countMonth = 3 * Math.pow(2, (index - 1))
                }
            }
        }

        public getDiscounts(){
            this.WebServices.UserLongTermDiscountService.query(
            ).$promise
                .then((data:any)=> {
                    this.discounts = data;
                    this.initializePeriods();
                })
        }
        private initializePeriods() {
            this.periods = [
                {
                    month: 1,
                    isSelected: false
                },
                {
                    month: 3,
                    isSelected: false
                },
                {
                    month: 6,
                    isSelected: true
                },
                {
                    month: 12,
                    isSelected: false
                }
            ];

            for(var i=0; i<this.periods.length;i++){

                var monthCount = this.periods[i].month;

                var discountItem = this.$filter('filter')(this.discounts, {
                    monthCount: monthCount,
                    resourceType: ResourceType.Vm
                });

                this.periods[i].discount = discountItem.length ? discountItem[0].discountSize : 0;
            }
        }
    }

    function cloudHostingCalculator():ng.IDirective {
        return {
            restrict: 'AE',
            scope: {
                vmOption: '=vmOption'
            },
            bindToController: true,
            templateUrl: 'app/pages/cloud/cloudHostingCalculator/cloudHostingCalculator.html',
            controller: CloudHostingCalculatorController,
            controllerAs: 'vm'
        };
    }

    angular.module("user.cloud")
        .directive("cloudHostingCalculator", cloudHostingCalculator);
}