module user {


    import IScope = angular.IScope;
    import IResourceArray = angular.resource.IResourceArray;
    import IWebServices = core.IWebServices;
    import UserOperatingSystemViewModel = core.WebApi.Models.UserOperatingSystemViewModel;
    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;


    export class CloudParamSlidersController {
        static $inject = ['$scope', 'cloudCalculator', 'WebServices'];
        public vmOption:VmOption;

        public sliderModel:SliderModel = new user.SliderModel();


        public corePrice:number;
        public ramPrice:number;
        public hddPrice:number;


        constructor($scope:any, public cloudCalculator:core.CloudCalculator, public WebServices:IWebServices) {


            $scope.$watch(()=>this.vmOption.userOperatingSystem, ()=> {
                if (this.vmOption.userOperatingSystem != null) {
                    this.sliderModel.minCoreCount = this.vmOption.userOperatingSystem.minCoreCount;
                    this.sliderModel.minRamCount = this.vmOption.userOperatingSystem.minRamCount;
                    this.sliderModel.minHddCount = this.vmOption.userOperatingSystem.minHardDriveSize;
                }

            });


            var calculatePrice = ()=> {
                var price = this.cloudCalculator.CalculatePrice(this.vmOption);
                this.corePrice = price.cpuPrice;
                this.ramPrice = price.ramPrice;
                this.hddPrice = price.hddPrice;
                this.vmOption.price = price.price;
            };


            var init = ()=> {
                this.sliderModel.coreCountValue =  this.vmOption.cpu ||this.vmOption.currentCpu;
                this.sliderModel.coreCount = this.sliderModel.coreCountValue * 100000 / this.sliderModel.maxCoreCount;

                this.sliderModel.ramCountValue =  this.vmOption.ram ||this.vmOption.currentRam;
                this.sliderModel.ramCount = this.sliderModel.ramCountValue * 100000 / this.sliderModel.maxRamCount;

                this.sliderModel.hddCountValue =  this.vmOption.hdd ||this.vmOption.currenHdd;
                this.sliderModel.hddCount = this.sliderModel.hddCountValue * 100000 / this.sliderModel.maxHddCount;
                if (this.vmOption.hdd) {
                    this.sliderModel.minHddCount = this.vmOption.hdd; // Нельзя делать меньше, чем уже есть
                }
            };
            init();
            $scope.$watch(()=>this.vmOption.currentCpu, ()=> {

                init();
            });
            $scope.$watch(()=>this.vmOption.currentRam, ()=> {
                init();
            });

            $scope.$watch(()=>this.vmOption.currenHdd, ()=> {
                init();
            });


            $scope.$watch(()=>this.vmOption, ()=> {
                calculatePrice();
            }, true);

            this.cloudCalculator.userPrice.$promise.then(()=> {
                calculatePrice();
            });


            $scope.$watch('vm.sliderModel.coreCount', () => {
                this.sliderModel.coreCountValue = Math.round(this.sliderModel.maxCoreCount * this.sliderModel.coreCount / 100000);
                if (this.sliderModel.coreCountValue < this.sliderModel.minCoreCount) {
                    this.sliderModel.coreCountValue = this.sliderModel.minCoreCount;

                }
                this.vmOption.cpu = this.sliderModel.coreCountValue;
            });

            $scope.$watch('vm.sliderModel.ramCount', () => {
                this.sliderModel.ramCountValue = Math.round(this.sliderModel.maxRamCount * this.sliderModel.ramCount / 100000);

                this.sliderModel.ramCountValue = (( this.sliderModel.ramCountValue / 512 | 0)) * 512;

                if (this.sliderModel.ramCountValue < this.sliderModel.minRamCount) {
                    this.sliderModel.ramCountValue = this.sliderModel.minRamCount;
                }
                /*  } else if ($scope.ramCountValue > 1024) {
                 $scope.ramCountValue = Math.round($scope.ramCountValue / 1024);
                 }*/

                this.vmOption.ram = this.sliderModel.ramCountValue;
            });

            $scope.$watch('vm.sliderModel.hddCount', ()=> {
                this.sliderModel.hddCountValue = Math.round(this.sliderModel.maxHddCount * this.sliderModel.hddCount / 100000);
                if (this.sliderModel.hddCountValue < this.sliderModel.minHddCount) {
                    this.sliderModel.hddCountValue = this.sliderModel.minHddCount;
                }
                this.vmOption.hdd = this.sliderModel.hddCountValue;

            });


        }

        public paramCountChange(whatChange:any) {
            switch (whatChange) {
                case 'core':
                    this.sliderModel.coreCount = this.sliderModel.coreCountValue * 100000 / this.sliderModel.maxCoreCount;
                    break;

                case 'ram':
                    //if ($scope.ramCountValue > $scope.minRamCount && $scope.ramCountValue < 1024) {
                    // Значит в мегабайтах
                    this.sliderModel.ramCount = this.sliderModel.ramCountValue * 100000 / this.sliderModel.maxRamCount;
                    /* } else if ($scope.ramCountValue > 1024) {
                     // Переведём в гигабайты
                     $scope.ramCountValue = $scope.ramCountValue / 1024;
                     $scope.ramCount = $scope.ramCountValue * 100000 * 1024 / $scope.maxRamCount;
                     } else if ($scope.ramCountValue < $scope.minRamCount) {
                     // Значит в гигабайтах
                     $scope.ramCount = $scope.ramCountValue * 100000 * 1024 / $scope.maxRamCount;
                     }*/
                    break;

                case 'hdd':
                    this.sliderModel.hddCount = this.sliderModel.hddCountValue * 100000 / this.sliderModel.maxHddCount;
                    break;

                default:
                    break;
            }
        };
    }

    function cloudHostingCalculator():ng.IDirective {
        return {
            restrict: 'AE',
            scope: {
                vmOption: '=vmOption'
            },
            bindToController: true,
            templateUrl: 'app/pages/cloud/cloudParamSliders/cloudParamSliders.html',
            controller: CloudParamSlidersController,
            controllerAs: 'vm'
        };
    }

    angular.module("user.cloud")
        .directive("cloudParamSliders", cloudHostingCalculator);
}