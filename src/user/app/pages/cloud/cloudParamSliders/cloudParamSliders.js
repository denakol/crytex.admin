var user;
(function (user) {
    var CloudParamSlidersController = (function () {
        function CloudParamSlidersController($scope, cloudCalculator, WebServices) {
            var _this = this;
            this.cloudCalculator = cloudCalculator;
            this.WebServices = WebServices;
            this.sliderModel = new user.SliderModel();
            $scope.$watch(function () { return _this.vmOption.userOperatingSystem; }, function () {
                if (_this.vmOption.userOperatingSystem != null) {
                    _this.sliderModel.minCoreCount = _this.vmOption.userOperatingSystem.minCoreCount;
                    _this.sliderModel.minRamCount = _this.vmOption.userOperatingSystem.minRamCount;
                    _this.sliderModel.minHddCount = _this.vmOption.userOperatingSystem.minHardDriveSize;
                }
            });
            var calculatePrice = function () {
                var price = _this.cloudCalculator.CalculatePrice(_this.vmOption);
                _this.corePrice = price.cpuPrice;
                _this.ramPrice = price.ramPrice;
                _this.hddPrice = price.hddPrice;
                _this.vmOption.price = price.price;
            };
            var init = function () {
                _this.sliderModel.coreCountValue = _this.vmOption.cpu || _this.vmOption.currentCpu;
                _this.sliderModel.coreCount = _this.sliderModel.coreCountValue * 100000 / _this.sliderModel.maxCoreCount;
                _this.sliderModel.ramCountValue = _this.vmOption.ram || _this.vmOption.currentRam;
                _this.sliderModel.ramCount = _this.sliderModel.ramCountValue * 100000 / _this.sliderModel.maxRamCount;
                _this.sliderModel.hddCountValue = _this.vmOption.hdd || _this.vmOption.currenHdd;
                _this.sliderModel.hddCount = _this.sliderModel.hddCountValue * 100000 / _this.sliderModel.maxHddCount;
                if (_this.vmOption.hdd) {
                    _this.sliderModel.minHddCount = _this.vmOption.hdd; // Нельзя делать меньше, чем уже есть
                }
            };
            init();
            $scope.$watch(function () { return _this.vmOption.currentCpu; }, function () {
                init();
            });
            $scope.$watch(function () { return _this.vmOption.currentRam; }, function () {
                init();
            });
            $scope.$watch(function () { return _this.vmOption.currenHdd; }, function () {
                init();
            });
            $scope.$watch(function () { return _this.vmOption; }, function () {
                calculatePrice();
            }, true);
            this.cloudCalculator.userPrice.$promise.then(function () {
                calculatePrice();
            });
            $scope.$watch('vm.sliderModel.coreCount', function () {
                _this.sliderModel.coreCountValue = Math.round(_this.sliderModel.maxCoreCount * _this.sliderModel.coreCount / 100000);
                if (_this.sliderModel.coreCountValue < _this.sliderModel.minCoreCount) {
                    _this.sliderModel.coreCountValue = _this.sliderModel.minCoreCount;
                }
                _this.vmOption.cpu = _this.sliderModel.coreCountValue;
            });
            $scope.$watch('vm.sliderModel.ramCount', function () {
                _this.sliderModel.ramCountValue = Math.round(_this.sliderModel.maxRamCount * _this.sliderModel.ramCount / 100000);
                _this.sliderModel.ramCountValue = ((_this.sliderModel.ramCountValue / 512 | 0)) * 512;
                if (_this.sliderModel.ramCountValue < _this.sliderModel.minRamCount) {
                    _this.sliderModel.ramCountValue = _this.sliderModel.minRamCount;
                }
                /*  } else if ($scope.ramCountValue > 1024) {
                 $scope.ramCountValue = Math.round($scope.ramCountValue / 1024);
                 }*/
                _this.vmOption.ram = _this.sliderModel.ramCountValue;
            });
            $scope.$watch('vm.sliderModel.hddCount', function () {
                _this.sliderModel.hddCountValue = Math.round(_this.sliderModel.maxHddCount * _this.sliderModel.hddCount / 100000);
                if (_this.sliderModel.hddCountValue < _this.sliderModel.minHddCount) {
                    _this.sliderModel.hddCountValue = _this.sliderModel.minHddCount;
                }
                _this.vmOption.hdd = _this.sliderModel.hddCountValue;
            });
        }
        CloudParamSlidersController.prototype.paramCountChange = function (whatChange) {
            switch (whatChange) {
                case 'core':
                    this.sliderModel.coreCount = this.sliderModel.coreCountValue * 100000 / this.sliderModel.maxCoreCount;
                    break;
                case 'ram':
                    //if ($scope.ramCountValue > $scope.minRamCount && $scope.ramCountValue < 1024) {
                    // Значит в мегабайтах
                    this.sliderModel.ramCount = this.sliderModel.ramCountValue * 100000 / this.sliderModel.maxRamCount;
                    /* } else if ($scope.ramCountValue > 1024) {
                     // Переведём в гигабайты
                     $scope.ramCountValue = $scope.ramCountValue / 1024;
                     $scope.ramCount = $scope.ramCountValue * 100000 * 1024 / $scope.maxRamCount;
                     } else if ($scope.ramCountValue < $scope.minRamCount) {
                     // Значит в гигабайтах
                     $scope.ramCount = $scope.ramCountValue * 100000 * 1024 / $scope.maxRamCount;
                     }*/
                    break;
                case 'hdd':
                    this.sliderModel.hddCount = this.sliderModel.hddCountValue * 100000 / this.sliderModel.maxHddCount;
                    break;
                default:
                    break;
            }
        };
        ;
        CloudParamSlidersController.$inject = ['$scope', 'cloudCalculator', 'WebServices'];
        return CloudParamSlidersController;
    }());
    user.CloudParamSlidersController = CloudParamSlidersController;
    function cloudHostingCalculator() {
        return {
            restrict: 'AE',
            scope: {
                vmOption: '=vmOption'
            },
            bindToController: true,
            templateUrl: 'app/pages/cloud/cloudParamSliders/cloudParamSliders.html',
            controller: CloudParamSlidersController,
            controllerAs: 'vm'
        };
    }
    angular.module("user.cloud")
        .directive("cloudParamSliders", cloudHostingCalculator);
})(user || (user = {}));
