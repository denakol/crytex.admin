/**
 * Created by denak on 08.02.2016.
 */
var user;
(function (user) {
    var SliderModel = (function () {
        function SliderModel() {
            // Начальный значения для слайдера - в них будут содержаться проценты
            this.coreCount = 1;
            this.ramCount = 1;
            this.hddCount = 1;
            // Минимальные значения для параметров
            this.minCoreCount = 1;
            this.minRamCount = 512;
            this.minHddCount = 10;
            // Начальный значения для полей
            this.coreCountValue = 1;
            this.ramCountValue = 1;
            this.hddCountValue = 1;
            // Максимальный значения для параметров
            this.maxCoreCount = 24;
            this.maxRamCount = 65536;
            this.maxHddCount = 2000;
        }
        return SliderModel;
    }());
    user.SliderModel = SliderModel;
})(user || (user = {}));
