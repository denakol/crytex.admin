/**
 * Created by denak on 08.02.2016.
 */
module user {
    export class SliderModel{
        // Начальный значения для слайдера - в них будут содержаться проценты
        coreCount:number = 1;
        ramCount:number = 1;
        hddCount:number = 1;
        // Минимальные значения для параметров
        minCoreCount:number = 1;
        minRamCount:number = 512;
        minHddCount:number = 10;
        // Начальный значения для полей
        coreCountValue:number = 1;
        ramCountValue:number = 1;
        hddCountValue:number = 1;
        // Максимальный значения для параметров
        maxCoreCount:number = 24;
        maxRamCount:number = 65536;
        maxHddCount :number= 2000;
    }
}