var user;
(function (user) {
    var HelpTooltipController = (function () {
        function HelpTooltipController() {
        }
        return HelpTooltipController;
    }());
    user.HelpTooltipController = HelpTooltipController;
    var helpTooltipComponent = {
        restrict: 'AE',
        bindings: {
            show: '<',
            message: '<'
        },
        templateUrl: 'app/pages/game/buyGame/helpTooltip/helpTooltip.html',
        controller: "HelpTooltipController",
        controllerAs: "vm"
    };
    angular.module("crytexUser.buyGame")
        .component("helpTooltip", helpTooltipComponent)
        .controller("HelpTooltipController", HelpTooltipController);
})(user || (user = {}));
