module user{

    export class HelpTooltipController {

        public message: string;
        public show: boolean;

        constructor() {
        }

    }

    var helpTooltipComponent = {
        restrict: 'AE',
        bindings: {
            show: '<',
            message: '<'
        },
        templateUrl: 'app/pages/game/buyGame/helpTooltip/helpTooltip.html',
        controller: "HelpTooltipController",
        controllerAs: "vm"
    };

    angular.module("crytexUser.buyGame")
        .component("helpTooltip", helpTooltipComponent)
        .controller("HelpTooltipController", HelpTooltipController);
}