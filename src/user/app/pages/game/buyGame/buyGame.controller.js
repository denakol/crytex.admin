var user;
(function (user_1) {
    var BuyGameController = (function () {
        function BuyGameController(webService, messageDialog, $stateParams, baseInfo, localStorageService, loginForm, $state) {
            this.webService = webService;
            this.messageDialog = messageDialog;
            this.$stateParams = $stateParams;
            this.baseInfo = baseInfo;
            this.localStorageService = localStorageService;
            this.loginForm = loginForm;
            this.$state = $state;
            this.gameService = webService.UserGameService;
            this.buyOptions = {};
            this.getGame();
        }
        BuyGameController.prototype.gameBuy = function () {
            var user = this.localStorageService.get('authorizationData');
            this.localStorageService.set('gameServerOption', this.buyOptions);
            if (!user) {
                this.loginForm.show();
            }
            else {
                this.$state.go('personalAccount.buyService.buyGame');
            }
        };
        BuyGameController.prototype.getGame = function () {
            var _this = this;
            this.game = this.gameService.get({ id: this.$stateParams.gameId });
            this.game.$promise.then(function (game) {
                var availableTariffs = [];
                angular.forEach(game.gameServerTariffs, function (tariff) {
                    if (!tariff.disabled) {
                        availableTariffs.push(tariff);
                    }
                });
                game.gameServerTariffs = availableTariffs;
                _this.buyOptions.game = game;
                if (availableTariffs.length) {
                    _this.buyOptions.tariff = availableTariffs[0];
                }
                return game;
            })
                .catch(function (response) {
                //this.toastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        BuyGameController.$inject = ["WebServices", "$messageDialog", "$stateParams",
            "BASE_INFO", "localStorageService", "$loginForm", "$state"];
        return BuyGameController;
    }());
    user_1.BuyGameController = BuyGameController;
    angular.module('crytexUser.buyGame')
        .controller('BuyGameController', BuyGameController);
})(user || (user = {}));
