module user {

    import GameViewModel = core.WebApi.Models.GameViewModel;
    import IResourceClass = angular.resource.IResourceClass;
    import GameServerTariffSimpleView = core.WebApi.Models.GameServerTariffSimpleView;
    import IGameServerBuyOptionsViewModel = core.WebApi.Models.IGameServerBuyOptionsViewModel;

    export class BuyGameController {

        static $inject = ["WebServices", "$messageDialog", "$stateParams",
                            "BASE_INFO", "localStorageService", "$loginForm", "$state"];

        public game: GameViewModel;
        public buyOptions: IGameServerBuyOptionsViewModel;

        private gameService: IResourceClass<GameViewModel>;

        constructor(private webService: any, private messageDialog: any,
                    private $stateParams: any, private baseInfo: any,
                    private localStorageService: any, private loginForm: any,
                    private $state: any) {

            this.gameService = webService.UserGameService;
            this.buyOptions = <IGameServerBuyOptionsViewModel>{};
            this.getGame();
        }

        public gameBuy() {
            var user = this.localStorageService.get('authorizationData');
            this.localStorageService.set('gameServerOption', this.buyOptions);

            if(!user){
                this.loginForm.show();
            } else {
                this.$state.go('personalAccount.buyService.buyGame');
            }
        }

        private getGame() {
            this.game  = this.gameService.get({id: this.$stateParams.gameId});

            this.game.$promise.then((game: GameViewModel) => {
                    var availableTariffs: GameServerTariffSimpleView[] = [];
                    angular.forEach(game.gameServerTariffs, function(tariff) {
                        if (!tariff.disabled) {
                            availableTariffs.push(tariff);
                        }
                    });
                    game.gameServerTariffs = availableTariffs;
                    this.buyOptions.game = game;
                    if (availableTariffs.length) {
                        this.buyOptions.tariff = availableTariffs[0];
                    }
                    return game;
                })
                .catch ( (response:any) => {
                    //this.toastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }
    }

    angular.module('crytexUser.buyGame')
        .controller('BuyGameController', BuyGameController);
}