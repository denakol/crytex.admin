var user;
(function (user) {
    var ResourceType = core.WebApi.Models.Enums.ResourceType;
    var GamePriceController = (function () {
        function GamePriceController(calculateDiscountService, $scope) {
            var _this = this;
            this.calculateDiscountService = calculateDiscountService;
            this.$scope = $scope;
            $scope.$watch(function () { return _this.buyOptions.expirePeriod; }, function () {
                var countMonth = _this.buyOptions.expirePeriod / 30;
                calculateDiscountService.getDiscountSizeWithWait(ResourceType.Gs, countMonth)
                    .then(function (discountSize) {
                    if (discountSize != 0) {
                        _this.discountSize = discountSize / 100;
                    }
                    else {
                        _this.discountSize = discountSize;
                    }
                });
            });
        }
        GamePriceController.$inject = ["calculateDiscountService", "$scope"];
        return GamePriceController;
    }());
    user.GamePriceController = GamePriceController;
    var gamePriceComponent = {
        restrict: 'AE',
        bindings: {
            buyOptions: "<",
            gameBuy: '&',
            buyLoading: "="
        },
        templateUrl: 'app/pages/game/buyGame/gamePrice/gamePrice.html',
        controller: "GamePriceController",
        controllerAs: "vm"
    };
    angular.module("crytexUser.buyGame")
        .component("gamePrice", gamePriceComponent)
        .controller("GamePriceController", GamePriceController);
})(user || (user = {}));
