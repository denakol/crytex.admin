module user{

    import GameViewModel = core.WebApi.Models.GameViewModel;
    import GameServerTariffView = core.WebApi.Models.GameServerTariffView;
    import IGameServerBuyOptionsViewModel = core.WebApi.Models.IGameServerBuyOptionsViewModel;
    import ResourceType = core.WebApi.Models.Enums.ResourceType;

    export class GamePriceController {

        public static $inject = ["calculateDiscountService", "$scope"];

        public buyOptions: IGameServerBuyOptionsViewModel;

        public discountSize:number;

        constructor(private calculateDiscountService:user.CalculateDiscount,
                    private $scope:any){
            $scope.$watch(()=> this.buyOptions.expirePeriod, ()=> {
                var countMonth = this.buyOptions.expirePeriod / 30;
                calculateDiscountService.getDiscountSizeWithWait(ResourceType.Gs, countMonth)
                    .then((discountSize:number)=>{
                        if(discountSize!=0){
                            this.discountSize = discountSize/100;
                        } else {
                            this.discountSize = discountSize;
                        }
                    });
            });
        }
    }

    var gamePriceComponent = {
        restrict: 'AE',
        bindings: {
            buyOptions: "<",
            gameBuy:'&',
            buyLoading: "="
        },
        templateUrl: 'app/pages/game/buyGame/gamePrice/gamePrice.html',
        controller: "GamePriceController",
        controllerAs: "vm"
    };

    angular.module("crytexUser.buyGame")
        .component("gamePrice", gamePriceComponent)
        .controller("GamePriceController", GamePriceController);
}