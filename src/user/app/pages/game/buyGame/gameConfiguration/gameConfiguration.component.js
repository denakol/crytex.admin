var user;
(function (user) {
    var ResourceType = core.WebApi.Models.Enums.ResourceType;
    var GameConfigurationController = (function () {
        function GameConfigurationController($scope, $filter, WebServices, calculateDiscountService) {
            var _this = this;
            this.$scope = $scope;
            this.$filter = $filter;
            this.WebServices = WebServices;
            this.calculateDiscountService = calculateDiscountService;
            // Всплывающие подсказки
            this.showHelpTooltip = [false];
            this.currentTooltip = null;
            this.message = "Версии сервера 6153 и 6027 являются последними. Мы рекомендуем использовать именно эту версию. Однако, не все плагины работают на данных версиях";
            // Селекторы
            this.serverTypes = [{
                    value: 'Оплата за слоты'
                }];
            this.serverType = this.serverTypes[0];
            this.isLocationSelected = false;
            this.slots = [];
            this.currentPeriod = 1;
            this.getDiscounts();
            this.initializeSlots();
            this.$scope.$watch(function () { return _this.buyOptions; }, function () {
                _this.calculateTotalPrice();
            }, true);
            this.$scope.$watch(function () { return _this.buyOptions.game; }, function () {
                if (_this.buyOptions.game != undefined) {
                    _this.getLocation(_this.buyOptions.game.id);
                }
            }, true);
        }
        GameConfigurationController.prototype.getDiscounts = function () {
            var _this = this;
            this.WebServices.UserLongTermDiscountService.query().$promise
                .then(function (data) {
                _this.discounts = data;
                _this.initializePeriods();
                _this.initializeBuyOptions();
            });
        };
        GameConfigurationController.prototype.getLocation = function (gameId) {
            var _this = this;
            this.WebServices.UserLocationService.query({
                gameId: gameId
            }).$promise
                .then(function (data) {
                _this.locations = data;
                if (data.length == 0) {
                    _this.locations.push({ name: "Нет доступных локаций" });
                }
            });
        };
        GameConfigurationController.prototype.showTooltip = function (index) {
            if (this.currentTooltip != null) {
                if (index === this.currentTooltip) {
                    this.showHelpTooltip[index] = !this.showHelpTooltip[index];
                    return;
                }
                this.showHelpTooltip[this.currentTooltip] = false;
            }
            this.showHelpTooltip[index] = true;
            this.currentTooltip = index;
        };
        GameConfigurationController.prototype.btnSelect = function (index) {
            if (index != this.currentPeriod) {
                this.periods[index].isSelected = true;
                this.periods[this.currentPeriod].isSelected = false;
                this.currentPeriod = index;
                this.buyOptions.discount = this.periods[this.currentPeriod].discount;
                this.buyOptions.expirePeriod = this.periods[this.currentPeriod].day;
            }
        };
        GameConfigurationController.prototype.initializeSlots = function () {
            var minCount = 10;
            var maxCount = 50;
            var step = 5;
            for (var i = minCount; i <= maxCount; i += step) {
                var slotPrice = { value: i };
                this.slots.push(slotPrice);
            }
            this.selectedSlot = this.slots[0];
        };
        GameConfigurationController.prototype.initializePeriods = function () {
            this.periods = [
                {
                    day: 30,
                    isSelected: false
                },
                {
                    day: 60,
                    isSelected: false
                },
                {
                    day: 90,
                    isSelected: false
                },
                {
                    day: 180,
                    isSelected: false
                }
            ];
            for (var i = 0; i < this.periods.length; i++) {
                var monthCount = this.periods[i].day / 30;
                var discountItem = this.$filter('filter')(this.discounts, {
                    monthCount: monthCount,
                    resourceType: ResourceType.Gs
                });
                this.periods[i].discount = discountItem.length ? discountItem[0].discountSize : 0;
            }
        };
        GameConfigurationController.prototype.initializeBuyOptions = function () {
            if (this.buyOptions) {
                // Настроим на показ выбранного количества слотов
                var sourceSlot = this.$filter('filter')(this.slots, { value: this.buyOptions.slotCount })[0];
                var index = this.slots.indexOf(sourceSlot);
                this.selectSlotCount(sourceSlot);
                this.selectedSlot = this.slots[index];
                // Настроим на показ выбранного периода
                this.buyOptions.expirePeriod = 180; //по умолчанию 6 месяцев
                var sourcePeriod = this.$filter('filter')(this.periods, { day: this.buyOptions.expirePeriod })[0];
                var index = this.periods.indexOf(sourcePeriod);
                this.btnSelect(index);
            }
            else {
                this.buyOptions = {
                    slotCount: this.slots.length ? this.slots[0].value : 0,
                    expirePeriod: this.periods[this.currentPeriod].day,
                    discount: this.periods[this.currentPeriod].discount,
                    totalPrice: 0
                };
            }
        };
        GameConfigurationController.prototype.selectSlotCount = function (slotCount) {
            this.buyOptions.slotCount = slotCount.value;
        };
        GameConfigurationController.prototype.selectLocation = function (location) {
            if (location.id != undefined) {
                this.isLocationSelected = true;
                this.locationSelected = location;
                this.buyOptions.location = location;
                if (location.gameHosts.length == 0) {
                    location.gameHosts.push({ serverAddress: "Нет доступных локаций для данной игры" });
                }
            }
            this.buyOptions.location = location;
        };
        GameConfigurationController.prototype.selectGameHost = function (gameHost) {
            this.buyOptions.gameHost = gameHost;
        };
        GameConfigurationController.prototype.calculateTotalPrice = function () {
            if (this.buyOptions.tariff) {
                this.buyOptions.totalPrice = this.buyOptions.tariff.slot * this.buyOptions.slotCount *
                    this.buyOptions.expirePeriod * 0.01 * 100;
            }
        };
        GameConfigurationController.$inject = ["$scope", "$filter", "WebServices", "calculateDiscountService"];
        return GameConfigurationController;
    }());
    user.GameConfigurationController = GameConfigurationController;
    var gameConfigurationComponent = {
        restrict: 'AE',
        bindings: {
            buyOptions: "="
        },
        templateUrl: 'app/pages/game/buyGame/gameConfiguration/gameConfiguration.html',
        controller: "GameConfigurationController",
        controllerAs: "vm"
    };
    angular.module("crytexUser.buyGame")
        .component("gameConfiguration", gameConfigurationComponent)
        .controller("GameConfigurationController", GameConfigurationController);
})(user || (user = {}));
