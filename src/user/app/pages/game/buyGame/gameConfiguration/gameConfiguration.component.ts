module user{

    import GameViewModel = core.WebApi.Models.GameViewModel;
    import GameServerTariffView = core.WebApi.Models.GameServerTariffView;
    import IGameServerBuyOptionsViewModel = core.WebApi.Models.IGameServerBuyOptionsViewModel;
    import LocationViewModel = core.WebApi.Models.LocationViewModel;
    import PageModel = core.PageModel;
    import DiscountViewModel = core.WebApi.Models.DiscountViewModel
    import ResourceType = core.WebApi.Models.Enums.ResourceType;

    export interface selectValueArray {
        value: any;
    }

    export interface GamePeriod {
        day: number;
        discount: number;
        isSelected: boolean;
    }

    export class GameConfigurationController {

        static $inject = ["$scope", "$filter", "WebServices", "calculateDiscountService"];

        // Всплывающие подсказки
        public showHelpTooltip: Array<boolean> = [false];
        public currentTooltip: number = null;
        public message: string = "Версии сервера 6153 и 6027 являются последними. Мы рекомендуем использовать именно эту версию. Однако, не все плагины работают на данных версиях";

        // Селекторы
        public serverTypes: Array<selectValueArray> = [{
            value: 'Оплата за слоты'
        }];
        public serverType: selectValueArray = this.serverTypes[0];
        public selectedSlot: selectValueArray;


        public locations: PageModel<LocationViewModel>;

        public discounts: DiscountViewModel;

        public isLocationSelected:boolean = false;

        public locationSelected: LocationViewModel;

        public slots: Array<selectValueArray> = [];
        public periods: Array<GamePeriod>;
        public buyOptions: IGameServerBuyOptionsViewModel;

        private currentPeriod = 1;

        constructor(private $scope: any, private $filter: any,
                    private WebServices:any, private calculateDiscountService:user.CalculateDiscount) {


            this.getDiscounts();
            this.initializeSlots();

            this.$scope.$watch(()=>this.buyOptions, ()=> {
                this.calculateTotalPrice();
            },true);

            this.$scope.$watch(()=>this.buyOptions.game, ()=> {
                if(this.buyOptions.game != undefined){
                    this.getLocation(this.buyOptions.game.id);
                }
            },true);
        }

        public getDiscounts(){
            this.WebServices.UserLongTermDiscountService.query(
            ).$promise
                .then((data:any)=> {
                    this.discounts = data;
                    this.initializePeriods();
                    this.initializeBuyOptions();
                })
        }

        public getLocation(gameId:string){
            this.WebServices.UserLocationService.query({
                gameId: gameId
            }).$promise
                .then((data:any)=> {
                    this.locations = data;
                    if(data.length == 0){
                        this.locations.push({name:"Нет доступных локаций"})
                    }
                })
        }


        public showTooltip(index: number) {
            if (this.currentTooltip != null) {
                if (index === this.currentTooltip) {
                    this.showHelpTooltip[index] = !this.showHelpTooltip[index];
                    return;
                }
                this.showHelpTooltip[this.currentTooltip] = false;
            }
            this.showHelpTooltip[index] = true;
            this.currentTooltip = index;
        }

        public btnSelect(index: number) {
            if (index != this.currentPeriod) {
                this.periods[index].isSelected = true;
                this.periods[this.currentPeriod].isSelected = false;
                this.currentPeriod = index;
                this.buyOptions.discount = this.periods[this.currentPeriod].discount;
                this.buyOptions.expirePeriod = this.periods[this.currentPeriod].day;
            }
        }

        private initializeSlots() {
            var minCount = 10;
            var maxCount = 50;
            var step = 5;

            for (var i = minCount; i <= maxCount; i+=step) {
                var slotPrice: selectValueArray = { value: i };
                this.slots.push(slotPrice);
            }

            this.selectedSlot = this.slots[0];
        }

        private initializePeriods() {

            this.periods = [
                {
                    day: 30,
                    isSelected: false
                },
                {
                    day: 60,
                    isSelected: false
                },
                {
                    day: 90,
                    isSelected: false
                },
                {
                    day: 180,
                    isSelected: false
                }
            ];
            
            for(var i=0; i<this.periods.length;i++){

                var monthCount = this.periods[i].day / 30;

                var discountItem = this.$filter('filter')(this.discounts, {
                    monthCount: monthCount,
                    resourceType: ResourceType.Gs
                });

                this.periods[i].discount = discountItem.length ? discountItem[0].discountSize : 0;
            }
        }

        private initializeBuyOptions() {
            if (this.buyOptions) {
                // Настроим на показ выбранного количества слотов
                var sourceSlot = this.$filter('filter')(this.slots, { value: this.buyOptions.slotCount })[0];
                var index = this.slots.indexOf(sourceSlot);
                this.selectSlotCount(sourceSlot);
                this.selectedSlot = this.slots[index];

                // Настроим на показ выбранного периода
                this.buyOptions.expirePeriod = 180; //по умолчанию 6 месяцев
                var sourcePeriod = this.$filter('filter')(this.periods, { day: this.buyOptions.expirePeriod })[0];
                var index = this.periods.indexOf(sourcePeriod);
                this.btnSelect(index);
            } else {
                this.buyOptions = <IGameServerBuyOptionsViewModel>{
                    slotCount: this.slots.length ? this.slots[0].value : 0,
                    expirePeriod: this.periods[this.currentPeriod].day,
                    discount: this.periods[this.currentPeriod].discount,
                    totalPrice: 0
                };
            }
        }
        
        private selectSlotCount (slotCount: selectValueArray) {
            this.buyOptions.slotCount = slotCount.value;
        }

        public selectLocation(location: LocationViewModel){
            if(location.id != undefined){
                this.isLocationSelected = true;
                this.locationSelected = location;

                this.buyOptions.location = location;

                if(location.gameHosts.length == 0){
                    location.gameHosts.push({serverAddress:"Нет доступных локаций для данной игры"})
                }
            }
            
            this.buyOptions.location = location;
        }

        public selectGameHost(gameHost:any){
            this.buyOptions.gameHost = gameHost;
        }

        private calculateTotalPrice() {
            if (this.buyOptions.tariff) {
                this.buyOptions.totalPrice = this.buyOptions.tariff.slot * this.buyOptions.slotCount *
                    this.buyOptions.expirePeriod * 0.01 * 100;
            }
        }

    }

    var gameConfigurationComponent = {
        restrict: 'AE',
        bindings: {
            buyOptions: "="
        },
        templateUrl: 'app/pages/game/buyGame/gameConfiguration/gameConfiguration.html',
        controller: "GameConfigurationController",
        controllerAs: "vm"
    };

    angular.module("crytexUser.buyGame")
        .component("gameConfiguration", gameConfigurationComponent)
        .controller("GameConfigurationController", GameConfigurationController);
}