var user;
(function (user) {
    var GameFamily = core.WebApi.Models.Enums.GameFamily;
    var GameController = (function () {
        function GameController($state, webService, baseInfo) {
            this.$state = $state;
            this.webService = webService;
            this.baseInfo = baseInfo;
            this.paginationSettings = {
                pageNumber: 1,
                pageSize: 100
            };
            this.gameService = webService.UserGameService;
            this.getGames();
        }
        GameController.prototype.buyGame = function (id) {
            this.$state.go("buyGame", { "gameId": id });
        };
        GameController.prototype.getGames = function () {
            this.gameList = this.gameService.getAllPage({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize,
                familyGame: GameFamily.Unknown
            });
            this.gameList.$promise
                .catch(function (response) {
                //this.ToastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        GameController.$inject = ["$state", "WebServices", "BASE_INFO"];
        return GameController;
    }());
    user.GameController = GameController;
    angular.module('crytexUser.game')
        .controller('GameController', GameController);
})(user || (user = {}));
