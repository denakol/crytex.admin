module user {

    import GameViewModel = core.WebApi.Models.GameViewModel;
    import IWebServices = core.IWebServices;
    import PageModel = core.PageModel;
    import PaginationSetting = core.PaginationSetting;
    import IResourceClass = angular.resource.IResourceClass;
    import GameFamily = core.WebApi.Models.Enums.GameFamily;

    export class GameController {

        static $inject = ["$state", "WebServices", "BASE_INFO"];

        public gameList: PageModel<GameViewModel>;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 100
        }

        private gameService: IResourceClass<PageModel<GameViewModel>>;

        constructor(private $state: any, private webService: IWebServices,
                    public baseInfo: any) {

            this.gameService = webService.UserGameService;
            this.getGames();
        }

        public buyGame(id: number) {
            this.$state.go("buyGame", { "gameId": id });
        }

        private getGames() {
            this.gameList = this.gameService.getAllPage(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    familyGame: GameFamily.Unknown
                });

            this.gameList.$promise
                .catch ( (response:any) => {
                    //this.ToastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }
    }

    angular.module('crytexUser.game')
        .controller('GameController', GameController);

}