(function () {

    angular.module('crytexUser.home')
        .controller('HomeController', homeController);

    homeController.$inject = ['$scope', '$timeout','$messageDialog'];

    function homeController($scope, $timeout,$messageDialog) {
        $scope.newsImage = [
            {isShow: true, url: 'client/content/images/slider/1.jpg'},
            {isShow: false, url: 'client/content/images/slider/2.jpg'},
            {isShow: false, url: 'client/content/images/slider/3.jpg'},
            {isShow: false, url: 'client/content/images/slider/4.jpg'},
            {isShow: false, url: 'client/content/images/slider/5.jpg'},
        ];

        $scope.showLoginForm = false;
        $scope.showRegisterForm = false;
        $scope.showCallMeForm = false;

        $scope.serviceInfoText = [
            {
                header: 'Облачная виртуализация',
                mainText: 'Виртуальные выделенные серверы VPS, VDS, построенные на основе технологий VMware и Hyper-V, виртуальные и облачные хостинги, колокации'
            },
            {
                header: 'Частные и публичные сети',
                mainText: 'Виртуальные выделенные серверы VPS, VDS, построенные на основе технологий VMware и Hyper-V, виртуальные и облачные хостинги, колокации'
            },
            {
                header: 'Игровые, голосовые серверы',
                mainText: 'Виртуальные выделенные серверы VPS, VDS, построенные на основе технологий VMware и Hyper-V, виртуальные и облачные хостинги, колокации'
            },
            {
                header: 'Веб-хостинги и доменные имена',
                mainText: 'Виртуальные выделенные серверы VPS, VDS, построенные на основе технологий VMware и Hyper-V, виртуальные и облачные хостинги, колокации'
            },
            {
                header: 'Сервис хранения данных',
                mainText: 'Виртуальные выделенные серверы VPS, VDS, построенные на основе технологий VMware и Hyper-V, виртуальные и облачные хостинги, колокации'
            }
        ];

        $scope.serviceInfo = $scope.serviceInfoText[0];

        $scope.highliteServiceStyle = {
            'text-decoration': 'none',
            'padding-left': '10px',
            'font-size': '16px',
            'background-color': 'white',
            'padding-bottom': '8px',
            'box-shadow': ' 0px 8px 8px 0px rgba(216, 225, 230, 0.47)'
        };

        $scope.highliteServiceDefaultStyle = {
            'text-decoration': 'underline',
            'padding-left': '19px',
            'font-size': '14px',
            'background-color': 'transparent',
            'padding-bottom': '17px'
        };

        $scope.serviceRightStyle = {'border-top-left-radius': '0px'};

        $scope.serviceStyle = [
            {style: $scope.highliteServiceStyle},
            {style: $scope.highliteServiceDefaultStyle},
            {style: $scope.highliteServiceDefaultStyle},
            {style: $scope.highliteServiceDefaultStyle},
            {style: $scope.highliteServiceDefaultStyle}
        ];

        $scope.showMessage = function(){
            $messageDialog.show({header:"Сообщение пользователю",body:"Пример информационного сообщения пользователю"});
        };


        $scope.currentImage = 0;
        $scope.currentServiceStyle = 0;

        function doSomething() {
           var i=$scope.currentImage+1;
            if(i>4)
            {
                i=0;
            }
            $scope.showImage( i);
            $timeout(doSomething, 10000 + getRandomInt(1000));
        }

       // $timeout(doSomething, 10000 + getRandomInt(1000));
        function getRandomInt(max){
            return Math.floor(Math.random() * max);
        }
        $scope.showImage = function (number) {
            if( $scope.currentImage>number){
                $scope.backImage=true;
            }
            else
            {
                $scope.backImage=false;
            }
            $scope.newsImage[$scope.currentImage].isShow = false;
            $scope.newsImage[number].isShow = true;
            $scope.newsImageStyle = {'background-image': 'url(' + $scope.newsImage[number].url + ')'};
            $scope.newsImageSrc =  $scope.newsImage[number].url ;
            $scope.currentImage = number;
        };

        $scope.highliteService = function (number) {
            if (number != $scope.currentServiceStyle) {
                $scope.serviceStyle[number].style = $scope.highliteServiceStyle;
                $scope.serviceStyle[$scope.currentServiceStyle].style = $scope.highliteServiceDefaultStyle;
                $scope.serviceInfo = $scope.serviceInfoText[number];
                $scope.currentServiceStyle = number;

                if (number == 0) {
                    $scope.serviceRightStyle = {'border-top-left-radius': '0px'};
                } else if (number == 4) {
                    $scope.serviceRightStyle = {'border-bottom-left-radius': '0px'};
                } else {
                    $scope.serviceRightStyle = {'border-top-left-radius': '5px', 'border-bottom-left-radius': '5px'};
                }
            }
        };

        $scope.leaveService = function () {
            $scope.highliteService($scope.currentServiceStyle);
        }


    }

})();
