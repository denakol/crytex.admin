var user;
(function (user) {
    var SupportController = (function () {
        function SupportController(service, upload, baseInfo, mdDialog, fileType) {
            this.loading = false;
            this.webService = service;
            this.fileUploader = upload;
            this.baseInfo = baseInfo;
            this.dialog = mdDialog;
            this.fileType = fileType;
            this.submitted = false;
            this.files = [];
        }
        SupportController.prototype.sendMessage = function (form) {
            this.submitted = true;
            if (form.$valid) {
                this.loading = true;
                if (this.files.length > 0) {
                    this.userQuestion.fileDescriptorParams = this.uploadFilesOnServer(form);
                }
                else {
                    this.sendMessageOnServer();
                }
            }
        };
        SupportController.prototype.removeFile = function (index) {
            this.files.splice(index, 1);
        };
        SupportController.prototype.uploadFiles = function (files) {
            if (files != null) {
                if (files.length > 5) {
                    files.splice(5, files.length - 5);
                }
                this.files = files;
            }
        };
        ;
        // Send file on server
        SupportController.prototype.uploadFilesOnServer = function (form) {
            var _this = this;
            var fileDescriptions = [];
            for (var i = 0; i < this.files.length; i++) {
                // Set params
                var file = this.fileUploader.upload({
                    url: this.baseInfo.URL + this.baseInfo.PORT + this.baseInfo.API_URL + '/User/File/Post',
                    file: this.files[i],
                    data: {
                        fileName: this.files[i].name,
                        fileType: this.fileType.HelpDeskRequestAttachment
                    }
                });
                // Upload
                file.then(function (description) {
                    fileDescriptions.push({ id: description.data.id, path: description.data.path });
                    if (fileDescriptions.length == _this.files.length) {
                        _this.userQuestion.fileDescriptorParams = fileDescriptions;
                        _this.sendMessageOnServer();
                    }
                }, function (response) {
                    // Умолчим, при конечной отправке выползет сообщение
                });
            }
            return fileDescriptions;
        };
        // Send message on server
        SupportController.prototype.sendMessageOnServer = function () {
            var _this = this;
            this.webService.UserHelpDeskRequest.save(this.userQuestion)
                .$promise.then(function () {
                _this.submitted = false;
                _this.loading = false;
                _this.clear();
                _this.showOKMessage(0);
            }, function (response) {
                _this.loading = false;
                if (response == null) {
                    _this.showOKMessage(1);
                    return;
                }
                _this.showOKMessage(2);
            });
        };
        // Clear all params
        SupportController.prototype.clear = function () {
            this.files = [];
            this.userQuestion = null;
        };
        SupportController.prototype.showOKMessage = function (code) {
            var messageTitle = "";
            var messageBody = "";
            switch (code) {
                case 0:
                    messageTitle = "Ваша заявка успешно зарегестрирована";
                    messageBody = "Мы ответим вам в ближайшее время";
                    break;
                case 1:
                    messageTitle = "Сервер недоступен";
                    messageBody = "Приносим вам свои извенениня";
                    break;
                case 2:
                    messageTitle = "Неполадки на сервере";
                    messageBody = "Приносим вам свои извенениня";
                    break;
            }
            this.dialog.show(this.dialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title(messageTitle)
                .textContent(messageBody)
                .ok('ОК'));
        };
        SupportController.$inject = ["WebServices", "Upload", "BASE_INFO", "$mdDialog", "FileType"];
        return SupportController;
    }());
    user.SupportController = SupportController;
    angular.module('crytexUser.support')
        .controller('SupportController', SupportController);
})(user || (user = {}));
