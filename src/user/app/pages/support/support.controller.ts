module user {

    export class SupportController {

        static $inject = ["WebServices","Upload","BASE_INFO","$mdDialog","FileType"];

        constructor(service: any, upload: any, baseInfo: any, mdDialog: any, fileType: any) {
            this.webService = service;
            this.fileUploader = upload;
            this.baseInfo = baseInfo;
            this.dialog = mdDialog;
            this.fileType = fileType;

            this.submitted = false;
            this.files = [];
        }

        userQuestion: {
            userName: string;
            email: string;
            details: string;
            summary: string;
            fileDescriptorParams: any;
        };
        files: any;

        submitted: boolean;

        private webService: any;
        private fileUploader: any;
        private baseInfo: any;
        private dialog: any;
        private fileType: any;
        public loading:boolean = false;

        sendMessage(form:any) {
            this.submitted = true;

            if (form.$valid) {
                this.loading = true;
                if (this.files.length > 0){
                    this.userQuestion.fileDescriptorParams = this.uploadFilesOnServer(form);
                } else {
                    this.sendMessageOnServer();
                }
            }
        }

        removeFile(index: number) {
            this.files.splice(index, 1);
        }

        uploadFiles (files: any) {
            if (files != null){
                if (files.length > 5) {
                    files.splice(5, files.length - 5);
                }
                this.files = files;
            }
        };

        // Send file on server
        private uploadFilesOnServer (form:any) : any {
            var fileDescriptions:Array<any> = [];

            for (var i = 0; i < this.files.length; i++) {

                // Set params
                var file =  this.fileUploader.upload({
                    url: this.baseInfo.URL + this.baseInfo.PORT + this.baseInfo.API_URL + '/User/File/Post',
                    file: this.files[i],
                    data: {
                        fileName: this.files[i].name,
                        fileType: this.fileType.HelpDeskRequestAttachment
                    }
                });

                // Upload
                file.then( (description:any) => {
                        fileDescriptions.push({id: description.data.id, path:description.data.path })
                        if (fileDescriptions.length == this.files.length) {
                            this.userQuestion.fileDescriptorParams = fileDescriptions;
                            this.sendMessageOnServer();
                        }
                    },
                    (response:any) => {
                        // Умолчим, при конечной отправке выползет сообщение
                    }
                );
            }

            return fileDescriptions;
        }


        // Send message on server
        private sendMessageOnServer() {
            this.webService.UserHelpDeskRequest.save(this.userQuestion)
                .$promise.then( () => {
                        this.submitted = false;
                        this.loading = false;
                        this.clear();
                        this.showOKMessage(0);
                    },
                    (response:any) => {
                        this.loading = false;
                        if (response == null) {
                            this.showOKMessage(1);
                            return;
                        }
                        this.showOKMessage(2);
                    }
                );
        }

        // Clear all params
        private clear() {
            this.files = [];
            this.userQuestion = null;
        }

        private showOKMessage(code: number) {
            var messageTitle = "";
            var messageBody = "";
            switch (code) {
                case 0 : messageTitle = "Ваша заявка успешно зарегестрирована";
                    messageBody = "Мы ответим вам в ближайшее время";
                    break;
                case 1 : messageTitle = "Сервер недоступен";
                    messageBody = "Приносим вам свои извенениня";
                    break;
                case 2 : messageTitle = "Неполадки на сервере";
                    messageBody = "Приносим вам свои извенениня";
                    break;
            }
            this.dialog.show(
                this.dialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title(messageTitle)
                    .textContent(messageBody)
                    .ok('ОК')
            );
        }
    }

    angular.module('crytexUser.support')
        .controller('SupportController', SupportController);
}