declare module user {
    class SupportController {
        static $inject: string[];
        constructor(service: any, upload: any, baseInfo: any, mdDialog: any, fileType: any);
        userQuestion: {
            userName: string;
            email: string;
            details: string;
            summary: string;
            fileDescriptorParams: any;
        };
        files: any;
        submitted: boolean;
        private webService;
        private fileUploader;
        private baseInfo;
        private dialog;
        private fileType;
        sendMessage(form: any): void;
        removeFile(index: number): void;
        uploadFiles(files: any): void;
        private uploadFilesOnServer(form);
        private sendMessageOnServer();
        private clear();
        private showOKMessage(code);
    }
}
