(function () {

    angular.module("crytex.user.personalAccount", [
        'personalAccount.myAccount',
        'personalAccount.fullRegistration',
        'personalAccount.myResources',
        'personalAccount.myFinance',
        'personalAccount.buyService',
        'personalAccount.knowledgeBase',
        'personalAccount.support',
        'personalAccount.forum'
    ]);

})();