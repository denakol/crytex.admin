(function () {

    angular.module("crytex.user.personalAccount").config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {

        $stateProvider
            .state('personalAccount', {
                url: '/personalAccount',
                templateUrl: 'app/pages/personalAccount/personalAccount.html',
                controller: 'PersonalAccountController',

                resolve: {
                    $title: function () {
                        return 'Личный кабинет';
                    }
                },
                data: {
                    permissions: {
                        except: ['userHalf'],
                        redirectTo: 'twoStepRegister.fullRegistration'
                    }
                }

            }).state('personalAccount.myAccount', {
            url: '/myAccount',
            templateUrl: 'app/pages/personalAccount/myAccount/myAccount.html',
            controller: 'MyAccountController',
            controllerAs: "vm",
            resolve: {
                $title: function () {
                    return 'Мой аккаунт';
                }
            }

        }).state('personalAccount.editUser', {
            url: '/editUser',
            templateUrl: 'app/pages/personalAccount/editUser/editUser.html',
            controller: 'EditUserController',
            controllerAs: "vm",
            resolve: {
                $title: function () {
                    return 'Редактирование';
                }
            }

        }).state('personalAccount.myResources', {
            url: '/myResources',
            templateUrl: 'app/pages/personalAccount/myResources/myResources.html',
            controller: 'MyResourcesController',
            controllerAs: "vm",
            resolve: {
                $title: function () {
                    return 'Мои ресурсы';
                }
            }
        }).state('personalAccount.myResources.cloud', {
            url: '/cloud',
            templateUrl: 'app/pages/personalAccount/myResources/cloud/cloud.html',
            controller: 'AccountCloud',
            controllerAs: "vm",
            resolve: {
                $title: function () {
                    return 'Виртуальные машины';
                }
            }
        }).state('personalAccount.myResources.game', {
            url: '/game',
            templateUrl: 'app/pages/personalAccount/myResources/game/game.html',
            controller: 'AccountGameController',
            controllerAs: "vm",
            resolve: {
                $title: function () {
                    return 'Виртуальные машины';
                }
            }
        }).state('personalAccount.myResources.web', {
                url: '/web',
                templateUrl: 'app/pages/personalAccount/myResources/game/game.html',
                controller: 'AccountCloud',
                controllerAs: "vm",
                resolve: {
                    $title: function () {
                        return 'Виртуальные машины';
                    }
                }
        }).state('personalAccount.myFinance', {
            url: '/myFinance',
            templateUrl: 'app/pages/personalAccount/myFinance/myFinance.html',
            controller: 'MyFinanceController',
            controllerAs: "vm",
            resolve: {
                $title: function () {
                    return 'Мои финансы';
                }
            }
        }).state('personalAccount.buyService', {
            url: '/buyService',
            templateUrl: 'app/pages/personalAccount/buyService/buyService.html',
            controller: 'AccountBuyServiceController',
            controllerAs: "vm",
            resolve: {
                $title: function () {
                    return 'Заказать услуги';
                }
            }
        }).state('personalAccount.buyService.buyCloud', {
            url: '/buyCloud',
            templateUrl: 'app/pages/personalAccount/buyService/cloud/cloud.html',
            controller: 'AccountBuyCloudController',
            controllerAs: "vm",
            resolve: {
                $title: function () {
                    return 'Заказ виртуальной машины';
                }
            }
        }).state('personalAccount.buyService.buyGame', {
            url: '/buyGame',
            templateUrl: 'app/pages/personalAccount/buyService/game/game.html',
            controller: 'AccountBuyGameController',
            controllerAs: "vm",
            resolve: {
                $title: function () {
                    return 'Заказ игрового сервера';
                }
            }
        }).state('personalAccount.knowledgeBase', {
            url: '/knowledgeBase',
            templateUrl: 'app/pages/personalAccount/knowledgeBase/knowledgeBase.html',
            controller: 'MyKnowledgeBaseController',
            resolve: {
                $title: function () {
                    return 'База знаний';
                }
            }
        }).state('personalAccount.support', {
            url: '/support',
            templateUrl: 'app/pages/personalAccount/support/support.html',
            controller: 'SupportController',
            controllerAs: 'vm',
            resolve: {
                $title: function () {
                    return 'Тех. поддержка';
                }
            }
        }).state('personalAccount.forum', {
                url: '/forum',
                templateUrl: 'app/pages/personalAccount/forum/forum.html',
                controller: 'ForumController',
                resolve: {
                    $title: function () {
                        return 'Форум';
                    }
                }
            })


            .state('twoStepRegister',
                {
                    url: '/personalAccount',
                    templateUrl: 'app/pages/personalAccount/personalAccount.html',
                    controller: 'PersonalAccountController',
                    resolve: {
                        $title: function () {
                            return 'Личный кабинет';
                        }
                    },
                    data: {
                        permissions: {
                            except: ['user'],
                            redirectTo: 'personalAccount.myAccount'
                        }
                    }

                }).state('twoStepRegister.fullRegistration', {
            url: '/fullRegistration',
            templateUrl: 'app/pages/personalAccount/fullRegistration/fullRegistration.html',
            controller: 'FullRegistrationController',
            controllerAs: 'vm',
            resolve: {
                $title: function () {
                    return 'Полная регистрация';
                }
            }
        });
    }

})();
