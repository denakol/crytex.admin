/**
 * Created by denak on 02.03.2016.
 */
module usefr {
    import IAauthService = core.IAauthService;

    import IStateService = angular.ui.IStateService;
  
    import IWebServices = core.IWebServices;
    import ApplicationUserViewModel = core.WebApi.Models.ApplicationUserViewModel;
    import TypeUser = core.WebApi.Models.Enums.TypeUser;
   

    export class EditUserController {
        static $inject:any[] = ['AuthService', '$state','WebServices'];
        public submitted = false;
        public previousSelectedItem:any = null;
        public selectedInput:any[] = [];
        public user:ApplicationUserViewModel;

        public phoneNumber:string;
        public code:string;
        public showCode = false;
        private changeSuccess = false;
        private userType = TypeUser;
        private changePassword=false;
        private changePasswordSuccess=false;

        private oldPassword="";
        private newPassword="";
        private changePasswordError=false;

        public saveLoading:boolean = false;
        public changeNumberRequestLoading:boolean = false;
        public changeNumberLoading:boolean = false;
        public changePasswordLoading:boolean = false;

        constructor(private AuthService:IAauthService, private $state:IStateService,private WebServices:IWebServices) {
            for (var i = 0; i < 11; i++) {
                this.selectedInput.push(false);
            }
            this.user= WebServices.User.get();
            this.user.$promise.then(()=>{
                this.phoneNumber = this.user.phoneNumber;
            })
        }

        selectInput(number:number):void {
            this.selectedInput[number] = true;
            if (this.previousSelectedItem) {
                this.selectedInput[this.previousSelectedItem] = false;
            }
            this.previousSelectedItem = number;
        }

        send(form:any) {
            this.submitted = true;
            if (form.$valid) {
                this.saveLoading = true;
                this.AuthService.updateUser(this.user).then(()=> {
                    this.$state.go('personalAccount.myAccount')
                })
            }
        };

        changeNumberRequest() {
            this.changeNumberRequestLoading = true;
            this.AuthService.changeNumberRequest(this.user.phoneNumber).then(
                ()=> {
                    this.changeNumberRequestLoading = false;
                    this.showCode=true;
                    this.changeSuccess=false;
                }
            )
        }

        changeNumber(){
            this.changeNumberLoading = true;
            this.AuthService.changeNumber({phoneNumber:this.user.phoneNumber,code:this.code}).then(
                ()=> {
                    this.changeNumberLoading = false;
                    this.changeSuccess=true;
                    this.showCode=false;
                }
            )
        }

        startChangePassword(){
            this.changePassword=true;
        }
        endChangePassword(){
            this.changePasswordLoading = true;
            this.AuthService.changePassword(this.oldPassword,this.newPassword).then(
                ()=> {
                    this.changePasswordLoading = false;
                    this.changePasswordSuccess=true;
                    this.changePassword=false;
                    this.changePasswordError=false;
                }
            ).catch(()=>{
                this.changePasswordError=true;
            })
        }



    }


    angular.module('personalAccount.fullRegistration')
        .controller('EditUserController', EditUserController);
}