/**
 * Created by denak on 02.03.2016.
 */
var usefr;
(function (usefr) {
    var TypeUser = core.WebApi.Models.Enums.TypeUser;
    var EditUserController = (function () {
        function EditUserController(AuthService, $state, WebServices) {
            var _this = this;
            this.AuthService = AuthService;
            this.$state = $state;
            this.WebServices = WebServices;
            this.submitted = false;
            this.previousSelectedItem = null;
            this.selectedInput = [];
            this.showCode = false;
            this.changeSuccess = false;
            this.userType = TypeUser;
            this.changePassword = false;
            this.changePasswordSuccess = false;
            this.oldPassword = "";
            this.newPassword = "";
            this.changePasswordError = false;
            this.saveLoading = false;
            this.changeNumberRequestLoading = false;
            this.changeNumberLoading = false;
            this.changePasswordLoading = false;
            for (var i = 0; i < 11; i++) {
                this.selectedInput.push(false);
            }
            this.user = WebServices.User.get();
            this.user.$promise.then(function () {
                _this.phoneNumber = _this.user.phoneNumber;
            });
        }
        EditUserController.prototype.selectInput = function (number) {
            this.selectedInput[number] = true;
            if (this.previousSelectedItem) {
                this.selectedInput[this.previousSelectedItem] = false;
            }
            this.previousSelectedItem = number;
        };
        EditUserController.prototype.send = function (form) {
            var _this = this;
            this.submitted = true;
            if (form.$valid) {
                this.saveLoading = true;
                this.AuthService.updateUser(this.user).then(function () {
                    _this.$state.go('personalAccount.myAccount');
                });
            }
        };
        ;
        EditUserController.prototype.changeNumberRequest = function () {
            var _this = this;
            this.changeNumberRequestLoading = true;
            this.AuthService.changeNumberRequest(this.user.phoneNumber).then(function () {
                _this.changeNumberRequestLoading = false;
                _this.showCode = true;
                _this.changeSuccess = false;
            });
        };
        EditUserController.prototype.changeNumber = function () {
            var _this = this;
            this.changeNumberLoading = true;
            this.AuthService.changeNumber({ phoneNumber: this.user.phoneNumber, code: this.code }).then(function () {
                _this.changeNumberLoading = false;
                _this.changeSuccess = true;
                _this.showCode = false;
            });
        };
        EditUserController.prototype.startChangePassword = function () {
            this.changePassword = true;
        };
        EditUserController.prototype.endChangePassword = function () {
            var _this = this;
            this.changePasswordLoading = true;
            this.AuthService.changePassword(this.oldPassword, this.newPassword).then(function () {
                _this.changePasswordLoading = false;
                _this.changePasswordSuccess = true;
                _this.changePassword = false;
                _this.changePasswordError = false;
            }).catch(function () {
                _this.changePasswordError = true;
            });
        };
        EditUserController.$inject = ['AuthService', '$state', 'WebServices'];
        return EditUserController;
    }());
    usefr.EditUserController = EditUserController;
    angular.module('personalAccount.fullRegistration')
        .controller('EditUserController', EditUserController);
})(usefr || (usefr = {}));
