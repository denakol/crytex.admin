/**
 * Created by denak on 09.02.2016.
 */
var user;
(function (user) {
    var ServerTypesResult = core.ServerTypesResult;
    var SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    var TypeVirtualization = core.WebApi.Models.Enums.TypeVirtualization;
    var AccountBuyCloudController = (function () {
        function AccountBuyCloudController(VmBilling, $messageDialog, $state, localStorageService) {
            this.VmBilling = VmBilling;
            this.$messageDialog = $messageDialog;
            this.$state = $state;
            this.localStorageService = localStorageService;
            this.subscriptionTypes = SubscriptionType;
            this.vmOption = new user.VmOption();
            this.virtualizations = TypeVirtualization;
            this.submitted = false;
            this.buyLoading = false;
            this.vmOption = localStorageService.get('vmOption') || new user.VmOption();
        }
        AccountBuyCloudController.prototype.buy = function (form) {
            var _this = this;
            this.submitted = true;
            if (form.$valid) {
                this.buyLoading = true;
                var date = moment(new Date()).format('DD.MM.YYYY');
                if (this.vmOption.virtualization == 1) {
                    this.vmOption.name = date + " " + "VMware";
                }
                else {
                    this.vmOption.name = date + " " + "HyperV";
                }
                this.VmBilling.buyVm(this.vmOption)
                    .then(function (result) {
                    _this.$messageDialog.show({
                        header: "Покупка машины",
                        body: "Покупка успешно выполнена. Отправлена задача на создание вашей машины. Прогресс создания вы можете наблюдать в разделе \"Мои ресурсы\""
                    }).finally(function () {
                        _this.localStorageService.remove('vmOption');
                        _this.$state.go("personalAccount.myResources.cloud");
                    });
                })
                    .catch(function (result) {
                    if (result.data.errorEnum == ServerTypesResult.NotEnoughMoney) {
                        _this.$messageDialog.show({
                            header: "Покупка машины",
                            body: "У вас недостаточно денег на покупку машины. Пополните счет"
                        });
                    }
                    else {
                        _this.$messageDialog.show({
                            header: "Покупка машины",
                            body: "Произошла ошибка"
                        });
                    }
                });
            }
        };
        AccountBuyCloudController.$inject = ['VmBilling', '$messageDialog', '$state', 'localStorageService'];
        return AccountBuyCloudController;
    }());
    user.AccountBuyCloudController = AccountBuyCloudController;
    angular.module('personalAccount.buyService')
        .controller('AccountBuyCloudController', AccountBuyCloudController);
})(user || (user = {}));
