/**
 * Created by denak on 09.02.2016.
 */
module user {

    import IVmBilling = core.IVmBilling;
    import ServerTypesResult = core.ServerTypesResult;
    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    import TypeVirtualization = core.WebApi.Models.Enums.TypeVirtualization;
    export class AccountBuyCloudController {

        static $inject = ['VmBilling', '$messageDialog','$state','localStorageService'];
        public subscriptionTypes:typeof SubscriptionType = SubscriptionType;
        public vmOption:VmOption = new VmOption();
        public virtualizations:typeof TypeVirtualization = TypeVirtualization;
        public submitted = false;
        public buyLoading:boolean = false;

        constructor(public VmBilling:IVmBilling,
                    public $messageDialog:any,
                    private $state:any,
                    private localStorageService:any
                    ) {
            this.vmOption = localStorageService.get('vmOption')|| new user.VmOption();
        }


        buy(form:any) {
            this.submitted = true;
            if(form.$valid){
                this.buyLoading = true;
                var date =  moment(new Date()).format('DD.MM.YYYY');
                if(this.vmOption.virtualization == 1){
                    this.vmOption.name = date + " " +  "VMware";
                } else {
                    this.vmOption.name = date + " "+"HyperV";
                }
                this.VmBilling.buyVm(this.vmOption)
                    .then((result) => {
                        this.$messageDialog.show({
                            header: "Покупка машины",
                            body: "Покупка успешно выполнена. Отправлена задача на создание вашей машины. Прогресс создания вы можете наблюдать в разделе \"Мои ресурсы\""
                        }).finally(() => {
                            this.localStorageService.remove('vmOption');

                            this.$state.go("personalAccount.myResources.cloud");
                        });
                    })
                    .catch((result) => {
                        if (result.data.errorEnum == ServerTypesResult.NotEnoughMoney) {
                            this.$messageDialog.show({
                                header: "Покупка машины",
                                body: "У вас недостаточно денег на покупку машины. Пополните счет"
                            });
                        }
                        else {
                            this.$messageDialog.show({
                                header: "Покупка машины",
                                body: "Произошла ошибка"
                            });
                        }
                    });


            }
        }

    }

    angular.module('personalAccount.buyService')
        .controller('AccountBuyCloudController', AccountBuyCloudController);
}

