var user;
(function (user) {
    var GameFamily = core.WebApi.Models.Enums.GameFamily;
    var ServerTypesResult = core.ServerTypesResult;
    var CountingPeriodType = core.WebApi.Models.Enums.CountingPeriodType;
    var AccountBuyGameController = (function () {
        function AccountBuyGameController(webService, $messageDialog, $stateParams, baseInfo, localStorageService, gameBilling, $state) {
            this.webService = webService;
            this.$messageDialog = $messageDialog;
            this.$stateParams = $stateParams;
            this.baseInfo = baseInfo;
            this.localStorageService = localStorageService;
            this.gameBilling = gameBilling;
            this.$state = $state;
            this.submitted = false;
            this.paginationSettings = {
                pageNumber: 1,
                pageSize: 100
            };
            this.buyLoading = false;
            this.gameService = webService.UserGameService;
            this.buyOptions = this.localStorageService.get('gameServerOption') || {};
            this.buyOptions.countingPeriodType = CountingPeriodType.Day;
            this.getGames();
        }
        AccountBuyGameController.prototype.gameBuy = function (form) {
            var _this = this;
            this.submitted = true;
            if (form.$valid) {
                this.buyLoading = true;
                var date = moment(new Date()).format('DD.MM.YYYY');
                this.buyOptions.serverName = date + " " + this.buyOptions.game.name;
                this.gameBilling.buyServerGame(this.buyOptions)
                    .then(function (result) {
                    _this.$messageDialog.show({
                        header: "Покупка машины",
                        body: "Покупка успешно выполнена. Отправлена задача на создание вашей машины. Прогресс создания вы можете наблюдать в разделе \"Мои ресурсы\""
                    }).finally(function () {
                        _this.localStorageService.remove('gameServerOption');
                        _this.$state.go("personalAccount.myResources.game");
                    });
                })
                    .catch(function (result) {
                    if (result.data.errorEnum == ServerTypesResult.NotEnoughMoney) {
                        _this.$messageDialog.show({
                            header: "Покупка игрового сервера",
                            body: "У вас недостаточно денег на покупку игрового сервера. Пополните счет"
                        });
                    }
                    else {
                        _this.$messageDialog.show({
                            header: "Покупка игрового сервера",
                            body: "Произошла ошибка"
                        });
                    }
                    _this.buyLoading = false;
                });
            }
        };
        AccountBuyGameController.prototype.selectGame = function (selectedGame) {
            if (selectedGame.gameServerTariffs.length) {
                this.buyOptions.tariff = selectedGame.gameServerTariffs[0];
            }
            else {
                this.buyOptions.tariff = {};
            }
        };
        AccountBuyGameController.prototype.getGames = function () {
            var _this = this;
            this.gameList = this.gameService.getAllPage({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize,
                familyGame: GameFamily.Unknown
            });
            this.gameList.$promise.then(function (data) {
                if (!_this.buyOptions) {
                    if (data.items.length) {
                        _this.buyOptions.game = data.items[0];
                    }
                }
                return data;
            })
                .catch(function (response) {
                //this.ToastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        AccountBuyGameController.$inject = ["WebServices", "$messageDialog", "$stateParams",
            "BASE_INFO", "localStorageService", "GameBillingUser", "$state"];
        return AccountBuyGameController;
    }());
    user.AccountBuyGameController = AccountBuyGameController;
    angular.module('personalAccount.buyService')
        .controller('AccountBuyGameController', AccountBuyGameController);
})(user || (user = {}));
