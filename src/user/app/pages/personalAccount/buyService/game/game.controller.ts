module user {
    import GameViewModel = core.WebApi.Models.GameViewModel;
    import IResourceClass = angular.resource.IResourceClass;
    import GameServerTariffView = core.WebApi.Models.GameServerTariffView;
    import IGameServerBuyOptionsViewModel = core.WebApi.Models.IGameServerBuyOptionsViewModel;
    import PageModel = core.PageModel;
    import PaginationSetting = core.PaginationSetting;
    import GameFamily = core.WebApi.Models.Enums.GameFamily;
    import IGameBilling = core.IGameBilling;
    import ServerTypesResult = core.ServerTypesResult;
    import CountingPeriodType = core.WebApi.Models.Enums.CountingPeriodType;

    export class AccountBuyGameController {

        static $inject = ["WebServices", "$messageDialog", "$stateParams",
            "BASE_INFO", "localStorageService", "GameBillingUser", "$state"];

        public gameList: PageModel<GameViewModel>;
        public buyOptions: IGameServerBuyOptionsViewModel;
        public submitted = false;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 100
        }
        
        public buyLoading:boolean = false;

        private gameService: IResourceClass<PageModel<GameViewModel>>;

        constructor(private webService: any, private $messageDialog: any,
                    private $stateParams: any, private baseInfo: any,
                    private localStorageService: any, public gameBilling:IGameBilling,
                    private $state: any) {

            this.gameService = webService.UserGameService;

            this.buyOptions = this.localStorageService.get('gameServerOption')|| <IGameServerBuyOptionsViewModel>{};
            this.buyOptions.countingPeriodType = CountingPeriodType.Day;
            this.getGames();
        }

        public gameBuy(form: any) {
            this.submitted = true;
            if(form.$valid){
                this.buyLoading = true;
                var date =  moment(new Date()).format('DD.MM.YYYY');

                this.buyOptions.serverName = date + " " + this.buyOptions.game.name;
                this.gameBilling.buyServerGame(this.buyOptions)
                    .then((result) => {
                        this.$messageDialog.show({
                            header: "Покупка машины",
                            body: "Покупка успешно выполнена. Отправлена задача на создание вашей машины. Прогресс создания вы можете наблюдать в разделе \"Мои ресурсы\""
                        }).finally(() => {
                            this.localStorageService.remove('gameServerOption');

                            this.$state.go("personalAccount.myResources.game");
                        });
                    })
                    .catch((result) => {
                        if (result.data.errorEnum == ServerTypesResult.NotEnoughMoney) {
                            this.$messageDialog.show({
                                header: "Покупка игрового сервера",
                                body: "У вас недостаточно денег на покупку игрового сервера. Пополните счет"
                            });
                        }
                        else {
                            this.$messageDialog.show({
                                header: "Покупка игрового сервера",
                                body: "Произошла ошибка"
                            });
                        }
                        this.buyLoading = false;
                    });
            }
        }

        public selectGame(selectedGame: GameViewModel) {
            if (selectedGame.gameServerTariffs.length) {
                this.buyOptions.tariff = selectedGame.gameServerTariffs[0];
            } else {
                this.buyOptions.tariff = <GameServerTariffView>{};
            }
        }

        private getGames() {
            this.gameList = this.gameService.getAllPage(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    familyGame: GameFamily.Unknown
                });

            this.gameList.$promise.then( (data: PageModel<GameViewModel> ) => {
                    if (!this.buyOptions) {
                        if (data.items.length) {
                            this.buyOptions.game = data.items[0];
                        }
                    }
                    return data;
                })
                .catch ( (response:any) => {
                    //this.ToastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

    }

    angular.module('personalAccount.buyService')
        .controller('AccountBuyGameController', AccountBuyGameController);
}