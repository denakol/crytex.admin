var user;
(function (user) {
    var AccountBuyServiceController = (function () {
        function AccountBuyServiceController($scope) {
            this.$scope = $scope;
        }
        AccountBuyServiceController.$inject = ['$scope'];
        return AccountBuyServiceController;
    }());
    user.AccountBuyServiceController = AccountBuyServiceController;
    angular.module('personalAccount.buyService')
        .controller('AccountBuyServiceController', AccountBuyServiceController);
})(user || (user = {}));
