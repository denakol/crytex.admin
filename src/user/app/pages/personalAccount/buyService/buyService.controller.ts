module user {

    export class AccountBuyServiceController {

        static $inject = ['$scope']

        constructor(public $scope: any) {
        }

    }

    angular.module('personalAccount.buyService')
        .controller('AccountBuyServiceController', AccountBuyServiceController);
}

