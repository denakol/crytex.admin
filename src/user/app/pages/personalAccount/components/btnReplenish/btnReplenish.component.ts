module user{
    
    export class BtnReplenishController {

        constructor() {

        }
    }

    var btnReplenishComponent = {
        restrict: 'AE',
        templateUrl: 'app/pages/personalAccount/components/btnReplenish/btnReplenish.html',
        controller: "BtnReplenishController",
        controllerAs: "vm"
    };

    angular.module("crytex.user.personalAccount")
        .component("btnReplenish", btnReplenishComponent)
        .controller("BtnReplenishController", BtnReplenishController);
}