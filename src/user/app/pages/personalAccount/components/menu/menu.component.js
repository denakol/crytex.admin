/**
 * Created by denak on 04.03.2016.
 */
var user;
(function (user) {
    var personalAccount;
    (function (personalAccount) {
        var menu;
        (function (menu) {
            var MenuController = (function () {
                function MenuController($state) {
                    var _this = this;
                    this.$state = $state;
                    angular.forEach(this.items, function (item) {
                        item.active = _this.$state.is(item.sref);
                        if (item.subItems) {
                            item.height = item.subItems.length * 45 + "px";
                            item.expand = $state.includes(item.sref);
                            for (var _i = 0, _a = item.subItems; _i < _a.length; _i++) {
                                var subItem = _a[_i];
                                subItem.active = _this.$state.is(subItem.sref);
                            }
                        }
                    });
                }
                MenuController.prototype.go = function (state, itemMenu) {
                    this.resetState();
                    state.active = true;
                    itemMenu.expand = true;
                    this.$state.go(state.sref);
                };
                MenuController.prototype.goMain = function (itemMenu) {
                    this.resetState();
                    itemMenu.active = true;
                    this.$state.go(itemMenu.sref);
                };
                MenuController.prototype.resetState = function () {
                    for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
                        var item = _a[_i];
                        item.active = false;
                        if (item.subItems) {
                            item.expand = false;
                            for (var _b = 0, _c = item.subItems; _b < _c.length; _b++) {
                                var subItem = _c[_b];
                                subItem.active = false;
                            }
                        }
                    }
                };
                MenuController.$inject = ['$state'];
                return MenuController;
            }());
            menu.MenuController = MenuController;
            var option = {
                restrict: "EA",
                bindings: {
                    items: '<'
                },
                templateUrl: 'app/pages/personalAccount/components/menu/menu.html',
                controller: MenuController,
                controllerAs: 'vm'
            };
            angular.module("crytex.user.personalAccount")
                .component("userMenu", option);
        })(menu = personalAccount.menu || (personalAccount.menu = {}));
    })(personalAccount = user.personalAccount || (user.personalAccount = {}));
})(user || (user = {}));
