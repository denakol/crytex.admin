/**
 * Created by denak on 04.03.2016.
 */
    module user.personalAccount.menu {

        import IState = angular.ui.IState;
        import IStateService = ng.ui.IStateService;
        export interface MenuItems {
            active:boolean;
            name:string;
            sref:string;
            isSelect:boolean;
            height:string;
            expand:boolean;
            subItems:MenuItems[];
        }


        export class MenuController {

            static $inject = ['$state'];
            public items:MenuItems[];
            public select:string;

            constructor(public $state:IStateService) {
                angular.forEach(this.items, (item)=> {

                    item.active = this.$state.is(item.sref);
                    if (item.subItems) {

                        item.height = item.subItems.length * 45 + "px";
                        item.expand = $state.includes(item.sref);
                        for (var subItem of item.subItems) {
                            subItem.active = this.$state.is(subItem.sref);
                        }
                    }
                })
            }


            public go(state:MenuItems, itemMenu:MenuItems) {

                this.resetState();
                state.active = true;
                itemMenu.expand = true;
                this.$state.go(state.sref);
            }

            public goMain(itemMenu:MenuItems) {

                this.resetState();
                itemMenu.active = true;
                this.$state.go(itemMenu.sref);
            }


            private resetState()
            {
                for (var item of this.items) {
                    item.active =false;
                    if (item.subItems) {
                        item.expand = false;
                        for (var subItem of item.subItems) {
                            subItem.active = false
                        }
                    }
                }
            }
        }

        var option:ng.IComponentOptions = {
            restrict: "EA",
            bindings: {
                items: '<'
            },
            templateUrl: 'app/pages/personalAccount/components/menu/menu.html',
            controller: MenuController,
            controllerAs: 'vm'
        };

        angular.module("crytex.user.personalAccount")
            .component("userMenu", option);
    }