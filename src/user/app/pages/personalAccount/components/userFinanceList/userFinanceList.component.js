var user;
(function (user) {
    var UserFinanceListController = (function () {
        function UserFinanceListController(WebServices, messageDialog) {
            var _this = this;
            this.WebServices = WebServices;
            this.messageDialog = messageDialog;
            this.oneDayForecast = {
                usageSubscription: 0
            };
            this.monthForecast = {
                usageSubscription: 0,
                fixedSubscription: 0,
                gameServer: 0,
                webHosting: 0
            };
            this.UserPaymentForecastService = WebServices.UserPaymentForecast;
            this.userPaymentForecast = this.UserPaymentForecastService.get();
            this.userPaymentForecast.$promise.then(function (data) {
                _this.calculatePaymentForecast(data);
                return data;
            })
                .catch(function (response) {
                _this.messageDialog.show({
                    header: "Сервер",
                    body: "Ошибка сервера"
                });
                return;
            });
        }
        UserFinanceListController.prototype.calculatePaymentForecast = function (data) {
            var _this = this;
            this.oneDayForecast.usageSubscription = 0;
            data.usageSubscriptionOneDayForecasts.forEach(function (item) { return _this.oneDayForecast.usageSubscription += item.paymentForecast; });
            this.monthForecast.usageSubscription = 0;
            data.usageSubscriptionsMonthForecasts.forEach(function (item) { return _this.monthForecast.usageSubscription += item.paymentForecast; });
            this.monthForecast.fixedSubscription = 0;
            data.fixedSubscriptionsMonthForecasts.forEach(function (item) { return _this.monthForecast.fixedSubscription += item.paymentForecast; });
            this.monthForecast.gameServer = 0;
            data.gameServerPaymentForecasts.forEach(function (item) { return _this.monthForecast.gameServer += item.paymentForecast; });
            this.monthForecast.webHosting = 0;
            data.webHostingPaymentForecasts.forEach(function (item) { return _this.monthForecast.webHosting += item.paymentForecast; });
        };
        UserFinanceListController.$inject = ["WebServices", "$messageDialog"];
        return UserFinanceListController;
    }());
    user.UserFinanceListController = UserFinanceListController;
    var userFinanceListComponent = {
        restrict: 'AE',
        bindings: {
            isShowCurrentBalance: '<'
        },
        templateUrl: 'app/pages/personalAccount/components/userFinanceList/userFinanceList.html',
        controller: "UserFinanceListController",
        controllerAs: "vm"
    };
    angular.module("crytex.user.personalAccount")
        .component("userFinanceList", userFinanceListComponent)
        .controller("UserFinanceListController", UserFinanceListController);
})(user || (user = {}));
