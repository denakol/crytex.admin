module user{

    import IResourceClass = angular.resource.IResourceClass;
    import IResource= angular.resource.IResource;
    import IResourceArray = angular.resource.IResourceArray;
    import UserPaymentForecast = core.UserPaymentForecastViewModel;

    export class UserFinanceListController {

        static $inject = ["WebServices", "$messageDialog"];

        private UserPaymentForecastService: IResourceClass <UserPaymentForecast>;
        public userPaymentForecast: UserPaymentForecast;

        public oneDayForecast: {
            usageSubscription: number
        } = {
            usageSubscription: 0
        }

        public monthForecast: {
            usageSubscription: number;
            fixedSubscription: number;
            gameServer: number;
            webHosting: number;
        } = {
            usageSubscription: 0,
            fixedSubscription: 0,
            gameServer: 0,
            webHosting: 0
        }

        constructor(private WebServices: any, private messageDialog: any) {
            this.UserPaymentForecastService = WebServices.UserPaymentForecast;

            this.userPaymentForecast = this.UserPaymentForecastService.get();
            this.userPaymentForecast.$promise.then( (data: UserPaymentForecast) => {
                    this.calculatePaymentForecast(data);
                    return data;
                })
                .catch ( (response:any) => {
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }

        private calculatePaymentForecast(data: UserPaymentForecast) {
            this.oneDayForecast.usageSubscription = 0;
            data.usageSubscriptionOneDayForecasts.forEach((item:any) => this.oneDayForecast.usageSubscription += item.paymentForecast);

            this.monthForecast.usageSubscription = 0;
            data.usageSubscriptionsMonthForecasts.forEach((item:any) => this.monthForecast.usageSubscription += item.paymentForecast);

            this.monthForecast.fixedSubscription = 0;
            data.fixedSubscriptionsMonthForecasts.forEach((item:any) => this.monthForecast.fixedSubscription += item.paymentForecast);

            this.monthForecast.gameServer = 0;
            data.gameServerPaymentForecasts.forEach((item:any) => this.monthForecast.gameServer += item.paymentForecast);

            this.monthForecast.webHosting = 0;
            data.webHostingPaymentForecasts.forEach((item:any) => this.monthForecast.webHosting += item.paymentForecast);
        }

    }

    var userFinanceListComponent = {
        restrict: 'AE',
        bindings: {
            isShowCurrentBalance: '<'
        },
        templateUrl: 'app/pages/personalAccount/components/userFinanceList/userFinanceList.html',
        controller: "UserFinanceListController",
        controllerAs: "vm"
    };

    angular.module("crytex.user.personalAccount")
        .component("userFinanceList", userFinanceListComponent)
        .controller("UserFinanceListController", UserFinanceListController);
}