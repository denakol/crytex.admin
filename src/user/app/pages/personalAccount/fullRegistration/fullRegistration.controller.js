var user;
(function (user) {
    var FullRegistrationUser = core.FullRegistrationUser;
    var TypeUser = core.WebApi.Models.Enums.TypeUser;
    var FullRegistrationController = (function () {
        function FullRegistrationController(AuthService, $state) {
            this.AuthService = AuthService;
            this.$state = $state;
            this.submitted = false;
            this.previousSelectedItem = null;
            this.selectedInput = [];
            this.user = new FullRegistrationUser();
            this.showCode = false;
            this.changeSuccess = false;
            for (var i = 0; i < 11; i++) {
                this.selectedInput.push(false);
            }
            this.userType = TypeUser;
            this.user.userType = TypeUser.PhysicalPerson;
        }
        FullRegistrationController.prototype.selectInput = function (number) {
            this.selectedInput[number] = true;
            if (this.previousSelectedItem) {
                this.selectedInput[this.previousSelectedItem] = false;
            }
            this.previousSelectedItem = number;
        };
        FullRegistrationController.prototype.send = function (form) {
            var _this = this;
            this.submitted = true;
            if (form.$valid) {
                this.AuthService.fillRegisterData(this.user).then(function () {
                    _this.$state.go('personalAccount.myAccount');
                });
            }
        };
        ;
        FullRegistrationController.prototype.changeNumberRequest = function () {
            var _this = this;
            this.AuthService.changeNumberRequest(this.phoneNumber).then(function () {
                _this.showCode = true;
                _this.changeSuccess = false;
            });
        };
        FullRegistrationController.prototype.changeNumber = function () {
            var _this = this;
            this.AuthService.changeNumber({ phoneNumber: this.phoneNumber, code: this.code }).then(function () {
                _this.changeSuccess = true;
                _this.showCode = false;
            });
        };
        FullRegistrationController.$inject = ['AuthService', '$state'];
        return FullRegistrationController;
    }());
    user.FullRegistrationController = FullRegistrationController;
    angular.module('personalAccount.fullRegistration')
        .controller('FullRegistrationController', FullRegistrationController);
})(user || (user = {}));
