module user {


    import IAauthService = core.IAauthService;
    import FullRegistrationUser = core.FullRegistrationUser;
    import IStateService = angular.ui.IStateService;
    import TypeUser = core.WebApi.Models.Enums.TypeUser;

    export class FullRegistrationController {
        static $inject:any[] = ['AuthService','$state'];
        public submitted = false;
        public previousSelectedItem:any = null;
        public selectedInput:any[] = [];
        public user:FullRegistrationUser = new FullRegistrationUser();

        public phoneNumber:string;
        public code:string;
        public showCode=false;
        private changeSuccess =false;
        private userType:any;
        constructor(private AuthService:IAauthService, private $state:IStateService) {
            for (var i = 0; i < 11; i++) {
                this.selectedInput.push(false);
            }
            this.userType =TypeUser;
            this.user.userType=TypeUser.PhysicalPerson;
        }

        selectInput(number:number):void {
            this.selectedInput[number] = true;
            if (this.previousSelectedItem) {
                this.selectedInput[this.previousSelectedItem] = false;
            }
            this.previousSelectedItem = number;
        }

        send(form:any) {
            this.submitted = true;
            if (form.$valid) {
                this.AuthService.fillRegisterData(this.user).then(()=> {
                    this.$state.go('personalAccount.myAccount')
                })
            }
        };

        changeNumberRequest() {
            this.AuthService.changeNumberRequest(this.phoneNumber).then(
                ()=> {
                    this.showCode=true;
                    this.changeSuccess=false;
                }
            )

        }

        changeNumber(){
            this.AuthService.changeNumber({phoneNumber:this.phoneNumber,code:this.code}).then(
                ()=> {
                    this.changeSuccess=true;
                    this.showCode=false;
                }
            )
        }

    }


    angular.module('personalAccount.fullRegistration')
        .controller('FullRegistrationController', FullRegistrationController);

}
