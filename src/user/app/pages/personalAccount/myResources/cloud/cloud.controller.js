/**
 * Created by denak on 09.02.2016.
 */
var user;
(function (user) {
    var SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    var TypeVirtualization = core.WebApi.Models.Enums.TypeVirtualization;
    var StatusTask = core.WebApi.Models.Enums.StatusTask;
    var TypeTask = core.WebApi.Models.Enums.TypeTask;
    var StatusVM = core.WebApi.Models.Enums.StatusVM;
    var SerializationHelper = core.SerializationHelper;
    var ChangeStatusOptions = core.WebApi.Models.ChangeStatusOptions;
    var TypeChangeStatus = core.WebApi.Models.Enums.TypeChangeStatus;
    var UpdateVmOptions = core.WebApi.Models.UpdateVmOptions;
    var AccountCloudController = (function () {
        function AccountCloudController($stateParams, WebServices, monitorTask) {
            var _this = this;
            this.WebServices = WebServices;
            this.subscriptionTypes = SubscriptionType;
            this.vmOption = new user.VmOption();
            this.virtualizations = TypeVirtualization;
            this.paginationSettings = {
                pageNumber: 1,
                pageSize: 5
            };
            this.getVirtualMachines();
            this.UserTaskV2Service = WebServices.UserTaskV2Service;
            this.vmOption.virtualization = $stateParams.virtualization;
            monitorTask = new monitorTask({
                callback: function (task) {
                    _this.SubscriptionVms.$promise.then(function () {
                        var vm = _this.SubscriptionVms.items.find(function (item) {
                            return item.id == task.task.resourceId;
                        });
                        if (vm != null) {
                            _this.calculateStateMachineFromTask(vm, task);
                        }
                    });
                }
            });
        }
        AccountCloudController.prototype.GetTask = function (id) {
            var params = {
                resourceId: id,
                pageNumber: 1,
                pageSize: 100,
                statusTasks: [StatusTask.Pending, StatusTask.Processing, StatusTask.Queued]
            };
            var page = this.UserTaskV2Service.pagingQuery(params);
            return page;
        };
        ;
        AccountCloudController.prototype.getVirtualMachines = function () {
            var _this = this;
            this.SubscriptionVms = this.WebServices.UserSubscriptionVirtualMachineService.pagingQuery({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize
            });
            this.SubscriptionVms.$promise.then(function () {
                angular.forEach(_this.SubscriptionVms.items, function (item) {
                    item.userVm.tasks = [];
                    var page = _this.GetTask(item.id);
                    page.$promise.then(function () {
                        item.userVm.tasks = page.items;
                        _this.calculateStateMachine(item.userVm);
                    });
                });
            });
        };
        AccountCloudController.prototype.updateMachineStatus = function (subscription, status) {
            subscription.userVm.status = status;
        };
        AccountCloudController.prototype.updateMachineBackupStoring = function (subscription, dailyBackupStorePeriodDays) {
            subscription.dailyBackupStorePeriodDays = dailyBackupStorePeriodDays;
        };
        AccountCloudController.prototype.updateMachineSettings = function (subscription, conf) {
            subscription.name = conf.name;
            if (subscription.subscriptionType == 1) {
                subscription.autoProlongation = conf.autoProlongation;
            }
        };
        AccountCloudController.prototype.calculateStateMachineFromTask = function (subscription, taskNotify) {
            var _this = this;
            if (taskNotify.success) {
                switch (taskNotify.task.typeTask) {
                    case TypeTask.CreateVm:
                        {
                            subscription.userVm.status = StatusVM.Enable;
                            break;
                        }
                    case TypeTask.ChangeStatus:
                        {
                            var options = SerializationHelper.toInstance(new ChangeStatusOptions(), taskNotify.task.options, true);
                            switch (options.typeChangeStatus) {
                                case TypeChangeStatus.PowerOff:
                                case TypeChangeStatus.Stop:
                                    {
                                        subscription.userVm.status = StatusVM.Disable;
                                        break;
                                    }
                                case TypeChangeStatus.Start:
                                case TypeChangeStatus.Reload:
                                    {
                                        subscription.userVm.status = StatusVM.Enable;
                                        break;
                                    }
                            }
                            break;
                        }
                    case TypeTask.UpdateVm:
                        {
                            var optionsUpdate = SerializationHelper.toInstance(new UpdateVmOptions(), taskNotify.task.options, true);
                            subscription.userVm.coreCount = optionsUpdate.cpu;
                            subscription.userVm.hardDriveSize = optionsUpdate.hddGB;
                            subscription.userVm.ramCount = optionsUpdate.ram;
                            break;
                        }
                }
            }
            else {
                subscription.userVm.status = StatusVM.Error;
            }
            var page = this.GetTask(subscription.id);
            page.$promise.then(function () {
                subscription.userVm.tasks = page.items;
                _this.calculateStateMachine(subscription.userVm);
            });
        };
        AccountCloudController.prototype.calculateStateMachine = function (userVm) {
            var items = userVm.tasks.filter(function (task) {
                return task.statusTask == StatusTask.Pending || task.statusTask == StatusTask.Processing || task.statusTask == StatusTask.Queued;
            });
            if (items.length > 0) {
                switch (items[items.length - 1].typeTask) {
                    case TypeTask.ChangeStatus:
                        {
                            userVm.status = StatusVM.UpdatingStatus;
                            break;
                        }
                    case TypeTask.UpdateVm:
                        {
                            userVm.status = StatusVM.UpdatingConf;
                        }
                }
            }
        };
        AccountCloudController.prototype.pageChangeHandler = function (newPageNumber) {
            this.paginationSettings.pageNumber = newPageNumber;
            this.getVirtualMachines();
        };
        AccountCloudController.$inject = ['$stateParams', 'WebServices', 'monitorTask'];
        return AccountCloudController;
    }());
    user.AccountCloudController = AccountCloudController;
    ;
    angular.module('user.cloud')
        .controller('AccountCloud', AccountCloudController);
})(user || (user = {}));
