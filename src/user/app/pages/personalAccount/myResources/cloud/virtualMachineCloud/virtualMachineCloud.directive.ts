/**
 * Created by denak on 12.02.2016.
 */
module user {
    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;
    export class VirtualMachineCloud {
        public subscriptionTypes : typeof SubscriptionType = SubscriptionType;
        public virtualMachine:SubscriptionVmViewModel;
        public control =false;

        public select:string = 'status';

        constructor() {

        }
     
        public chooseMenu(select:string) {
            this.select = select;
        };

        public choosed(select:string):boolean {
            return select ==this.select;
        };
    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            virtualMachine: '<',
            updateMachineStatus:"&",
            updateMachineBackupStoring: "&",
            updateMachineSettings:"&"
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachineCloud/virtualMachineCloud.html',
        controller: VirtualMachineCloud,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("virtualMachineCloud",option);
}