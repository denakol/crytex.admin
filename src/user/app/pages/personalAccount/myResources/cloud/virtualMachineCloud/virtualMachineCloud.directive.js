/**
 * Created by denak on 12.02.2016.
 */
var user;
(function (user) {
    var SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    var VirtualMachineCloud = (function () {
        function VirtualMachineCloud() {
            this.subscriptionTypes = SubscriptionType;
            this.control = false;
            this.select = 'status';
        }
        VirtualMachineCloud.prototype.chooseMenu = function (select) {
            this.select = select;
        };
        ;
        VirtualMachineCloud.prototype.choosed = function (select) {
            return select == this.select;
        };
        ;
        return VirtualMachineCloud;
    }());
    user.VirtualMachineCloud = VirtualMachineCloud;
    var option = {
        restrict: "EA",
        bindings: {
            virtualMachine: '<',
            updateMachineStatus: "&",
            updateMachineBackupStoring: "&",
            updateMachineSettings: "&"
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachineCloud/virtualMachineCloud.html',
        controller: VirtualMachineCloud,
        controllerAs: 'vm'
    };
    angular.module("user.cloud")
        .component("virtualMachineCloud", option);
})(user || (user = {}));
