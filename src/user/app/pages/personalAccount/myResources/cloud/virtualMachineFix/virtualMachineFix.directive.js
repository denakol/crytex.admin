/**
 * Created by denak on 12.02.2016.
 */
var user;
(function (user) {
    var SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    var VirtualMachineFix = (function () {
        function VirtualMachineFix() {
            this.subscriptionTypes = SubscriptionType;
            this.control = false;
            this.select = 'status';
        }
        VirtualMachineFix.prototype.chooseMenu = function (select) {
            this.select = select;
        };
        ;
        VirtualMachineFix.prototype.choosed = function (select) {
            return select == this.select;
        };
        ;
        return VirtualMachineFix;
    }());
    user.VirtualMachineFix = VirtualMachineFix;
    var option = {
        restrict: "EA",
        bindings: {
            virtualMachine: '<',
            updateMachineStatus: "&",
            updateMachineBackupStoring: "&",
            updateMachineSettings: "&"
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachineFix/virtualMachineFix.html',
        controller: VirtualMachineFix,
        controllerAs: 'vm'
    };
    angular.module("user.cloud")
        .component("virtualMachineFix", option);
})(user || (user = {}));
