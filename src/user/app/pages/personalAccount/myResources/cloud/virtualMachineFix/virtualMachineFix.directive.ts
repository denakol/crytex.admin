/**
 * Created by denak on 12.02.2016.
 */
module user {
    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;


    export class VirtualMachineFix {
        public subscriptionTypes:typeof SubscriptionType = SubscriptionType;
        public control = false;
        public virtualMachine:SubscriptionVmViewModel;
        
        public select:string = 'status';


        constructor() {

        }

        chooseMenu(select:string) {
            this.select = select;
        };

        choosed(select:string):boolean {
            return select ==this.select;
        };
        
        
        
    }

    var option:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            virtualMachine: '<',
            updateMachineStatus:"&",
            updateMachineBackupStoring: "&",
            updateMachineSettings:"&"
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachineFix/virtualMachineFix.html',
        controller: VirtualMachineFix,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("virtualMachineFix", option);
}