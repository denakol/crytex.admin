var user;
(function (user) {
    var virtualMachine;
    (function (virtualMachine) {
        var StatusVM = core.WebApi.Models.Enums.StatusVM;
        var VmSatusController = (function () {
            function VmSatusController(webService, messageDialog, TypeChangeStatus, statusVm, $scope, cloudCalculator) {
                var _this = this;
                this.webService = webService;
                this.messageDialog = messageDialog;
                this.TypeChangeStatus = TypeChangeStatus;
                this.statusVm = statusVm;
                this.cloudCalculator = cloudCalculator;
                this.UpdateMachineStatusOptions = {
                    subscriptionId: this.virtualMachine.id
                };
                this.UserSubscriptionVirtualMachineService = webService.UserSubscriptionVirtualMachineService;
                this.UserStateMachineService = webService.UserStateMachineService;
                this.typeChangeStatus = angular.copy(TypeChangeStatus);
                this.getLastMachineState();
                this.setManageButtons();
                $scope.$watch(function () {
                    return _this.virtualMachine.userVm.status;
                }, function () {
                    _this.setManageButtons();
                });
                this.vmOption = user.VmOption.GetFromVm(this.virtualMachine.userVm, this.virtualMachine.subscriptionType);
                cloudCalculator.CalculatePriceWithWait(this.vmOption).then(function (result) {
                    _this.currentPriceVm = result;
                });
            }
            VmSatusController.prototype.updateMachineStatus = function (stat) { };
            VmSatusController.prototype.setStatus = function (status) {
                var _this = this;
                this.UpdateMachineStatusOptions.status = status.value;
                this.UserSubscriptionVirtualMachineService.updateMachineStatus(this.UpdateMachineStatusOptions).$promise
                    .then(function () {
                    _this.updateMachineStatus({ subscription: _this.virtualMachine, status: StatusVM.UpdatingStatus });
                }).catch(function (response) {
                    _this.messageDialog.show({
                        header: "Сервер",
                        body: "Ошибка сервера"
                    });
                    return;
                });
            };
            VmSatusController.prototype.getLastMachineState = function () {
                var _this = this;
                this.machineState = this.UserStateMachineService.getLastState({
                    vmId: this.virtualMachine.id
                });
                this.machineState.$promise
                    .catch(function (response) {
                    _this.messageDialog.show({
                        header: "Сервер",
                        body: "Ошибка сервера"
                    });
                    return;
                });
            };
            // Устанавливаем активность/неактивность кнопок управления в зависимости от состояния машины
            VmSatusController.prototype.setManageButtons = function () {
                switch (this.virtualMachine.userVm.status) {
                    case this.statusVm.Enable:
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Start].isActive = false;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Stop].isActive = true;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Reload].isActive = true;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.PowerOff].isActive = true;
                        break;
                    case this.statusVm.Disable:
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Start].isActive = true;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Stop].isActive = false;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Reload].isActive = false;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.PowerOff].isActive = false;
                        break;
                    case this.statusVm.Error:
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Start].isActive = true;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Stop].isActive = true;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Reload].isActive = true;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.PowerOff].isActive = true;
                        break;
                    case this.statusVm.Creating:
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Start].isActive = false;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Stop].isActive = false;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Reload].isActive = false;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.PowerOff].isActive = false;
                        break;
                    default:
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Start].isActive = false;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Stop].isActive = false;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.Reload].isActive = false;
                        this.typeChangeStatus.arrayItems[this.typeChangeStatus.PowerOff].isActive = false;
                        break;
                }
            };
            VmSatusController.$inject = ["WebServices", "$messageDialog", "TypeChangeStatus", "StatusVm", '$scope', 'cloudCalculator'];
            return VmSatusController;
        }());
        virtualMachine.VmSatusController = VmSatusController;
        var option = {
            restrict: "EA",
            bindings: {
                virtualMachine: '<',
                updateMachineStatus: "&"
            },
            templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmStatus/vmStatus.html',
            controller: VmSatusController,
            controllerAs: 'vm'
        };
        angular.module("user.cloud")
            .component("vmStatus", option);
    })(virtualMachine = user.virtualMachine || (user.virtualMachine = {}));
})(user || (user = {}));
