module user.virtualMachine {

    import IResourceClass = angular.resource.IResourceClass;
    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;
    import TaskV2ViewModel = core.WebApi.Models.TaskV2ViewModel;
    import PageModel = core.PageModel;
    import PaginationSetting = core.PaginationSetting;
    import TypeDate = core.WebApi.Models.Enums.TypeDate;

    export class VmTaskController {

        static $inject = ["WebServices", "$messageDialog", "TypeTask", "StatusTask", "$scope"];

        public vmTaskList: PageModel<TaskV2ViewModel>;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        };
        public changePage: boolean = false;
        public currentDate: Date = new Date();
        public virtualMachine:SubscriptionVmViewModel;
        public myPaginationId: string = 'vmTask' + this.virtualMachine.id;

        public isUseFilter: boolean = false;
        public searchParams: ISearchParams;

        private TaskV2Service: IResourceClass<PageModel<TaskV2ViewModel>>;

        constructor(private webService: any,  private messageDialog: any,
                    public typeTask: any, private statusTask: any,
                    private $scope: ng.IScope) {
            this.TaskV2Service = webService.UserTaskV2Service;
            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;
            this.getTaskV2List();

            this.$scope.$watchCollection('vm.searchParams', () => {
                if (this.isUseFilter) {
                    this.getTaskV2List();
                }
            });
        }

        public useFilter() {
            this.isUseFilter = !this.isUseFilter;
            this.getTaskV2List();
        }

        public pageChangeHandler (index: number) {
            this.paginationSettings.pageNumber = index;
            this.changePage = true;
            this.getTaskV2List();
        }

        private getTaskV2List() {
            this.vmTaskList = this.TaskV2Service.getAllPage(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    resourceId: this.virtualMachine.id,
                    dateFrom: this.isUseFilter && this.searchParams ? this.searchParams.dateFrom : null,
                    dateTo: this.isUseFilter && this.searchParams ? this.searchParams.dateTo : null,
                    typeDate: this.isUseFilter ? TypeDate.CompletedAt : null
                });

            this.vmTaskList.$promise
                .catch ( (response:any) => {
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }
    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            virtualMachine: '<'
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmTask/vmTask.html',
        controller: VmTaskController,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("vmTask",option);
}
