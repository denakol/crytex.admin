var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var user;
(function (user) {
    var virtualMachine;
    (function (virtualMachine) {
        var BaseVmComponentController = user.cloud.BaseVmComponentController;
        var VmBackupController = (function (_super) {
            __extends(VmBackupController, _super);
            function VmBackupController(cloudCalculator, $scope, VmBilling, WebServices, messageDialog) {
                var _this = this;
                _super.call(this);
                this.cloudCalculator = cloudCalculator;
                this.$scope = $scope;
                this.VmBilling = VmBilling;
                this.WebServices = WebServices;
                this.messageDialog = messageDialog;
                this.SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
                this.vmOption = user.VmOption.GetFromVm(this.virtualMachine.userVm, this.virtualMachine.subscriptionType);
                this.updating = false;
                this.UserSubscriptionVirtualMachineService = WebServices.UserSubscriptionVirtualMachineService;
                this.updateSubscriptionBackupStoragePeriodModel = {
                    subscriptionId: this.virtualMachine.id,
                    newPeriodDays: this.virtualMachine.dailyBackupStorePeriodDays
                };
                cloudCalculator.CalculatePriceWithWait(this.vmOption).then(function (result) {
                    _this.currentPriceVm = result;
                });
                $scope.$watch(function () { return _this.vmOption; }, function () {
                    cloudCalculator.CalculatePriceWithWait(_this.vmOption).then(function (result) {
                        _this.newPriceVm = result;
                    });
                }, true);
            }
            VmBackupController.prototype.updateMachineBackupStoring = function (dailyBackupStorePeriodDays) { };
            VmBackupController.prototype.startUpdate = function () {
                this.updating = true;
            };
            VmBackupController.prototype.modify = function () {
                var _this = this;
                this.UserSubscriptionVirtualMachineService.updateSubscriptionBackupStoragePeriod(this.updateSubscriptionBackupStoragePeriodModel).$promise
                    .then(function (data) {
                    _this.updateMachineBackupStoring({
                        subscription: _this.virtualMachine,
                        dailyBackupStorePeriodDays: _this.updateSubscriptionBackupStoragePeriodModel.newPeriodDays
                    });
                    _this.endUpdate();
                })
                    .catch(function (response) {
                    var message = response.data.errorMessage ? response.data.errorMessage : "Ошибка сервера";
                    _this.messageDialog.show({
                        header: "Сервер",
                        body: message
                    });
                });
            };
            VmBackupController.prototype.endUpdate = function () {
                this.updating = false;
            };
            VmBackupController.prototype.storeCopy = function (index) {
                this.updateSubscriptionBackupStoragePeriodModel.newPeriodDays = index;
            };
            VmBackupController.$inject = ["cloudCalculator", "$scope", "VmBilling", "WebServices", "$messageDialog"];
            return VmBackupController;
        }(BaseVmComponentController));
        virtualMachine.VmBackupController = VmBackupController;
        var option = {
            restrict: "EA",
            bindings: {
                virtualMachine: '<',
                updateMachineBackupStoring: '&'
            },
            templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmBackup/vmBackup.html',
            controller: VmBackupController,
            controllerAs: 'vm'
        };
        angular.module("user.cloud")
            .component("vmBackup", option);
    })(virtualMachine = user.virtualMachine || (user.virtualMachine = {}));
})(user || (user = {}));
