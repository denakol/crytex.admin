/**
 * Created by denak on 25.02.2016.
 */
module user.cloud{
    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;
    export class  BaseVmComponentController
    {
        public virtualMachine:SubscriptionVmViewModel;
    }
    
}
