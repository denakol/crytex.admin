
module user.virtualMachine {

    import IResourceClass = angular.resource.IResourceClass;
    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;
    import PageModel = core.PageModel;
    import PaginationSetting = core.PaginationSetting;
    import UsageSubscriptionPaymentView = core.WebApi.Models.UsageSubscriptionPaymentView;

    export class VmUsagePaymentController {

        static $inject = ["WebServices", "$messageDialog", "$scope"];

        public virtualMachine: SubscriptionVmViewModel;
        public usagePaymentList: PageModel<UsageSubscriptionPaymentView>;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        }
        public changePage: boolean = false;
        public currentDate: Date = new Date();
        public myPaginationId: string = 'vmUsagePayment' + this.virtualMachine.id;

        public isUseFilter: boolean = false;
        public searchParams: ISearchParams;

        private UsageSubscriptionPaymentService: IResourceClass<PageModel<UsageSubscriptionPaymentView>>;

        constructor(private webService: any,  private messageDialog: any,
                    private $scope: ng.IScope) {
            this.UsageSubscriptionPaymentService = webService.UserUsageSubscriptionPayment;

            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;
            this.getUsagePayments();

            this.$scope.$watchCollection('vm.searchParams', () => {
                if (this.isUseFilter) {
                    this.getUsagePayments();
                }
            });
        }

        public useFilter() {
            this.isUseFilter = !this.isUseFilter;
            this.getUsagePayments();
        }

        public pageChangeHandler (index: number) {
            this.paginationSettings.pageNumber = index;
            this.changePage = true;
            this.getUsagePayments();
        }

        private getUsagePayments() {
            this.usagePaymentList = this.UsageSubscriptionPaymentService.getAllPage(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    subscriptionVmId: this.virtualMachine.id,
                    dateFrom: this.isUseFilter && this.searchParams ? this.searchParams.dateFrom : null,
                    dateTo: this.isUseFilter && this.searchParams ? this.searchParams.dateTo : null,
                    periodType: this.isUseFilter ? this.searchParams.countingPeriodType : null
                });

            this.usagePaymentList.$promise
                .catch ( (response:any) => {
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }

    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            virtualMachine: '<'
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmUsagePayment/vmUsagePayment.html',
        controller: VmUsagePaymentController,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("vmUsagePayment",option);
}
