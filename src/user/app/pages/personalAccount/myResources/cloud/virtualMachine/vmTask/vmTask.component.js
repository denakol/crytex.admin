var user;
(function (user) {
    var virtualMachine;
    (function (virtualMachine) {
        var TypeDate = core.WebApi.Models.Enums.TypeDate;
        var VmTaskController = (function () {
            function VmTaskController(webService, messageDialog, typeTask, statusTask, $scope) {
                var _this = this;
                this.webService = webService;
                this.messageDialog = messageDialog;
                this.typeTask = typeTask;
                this.statusTask = statusTask;
                this.$scope = $scope;
                this.paginationSettings = {
                    pageNumber: 1,
                    pageSize: 5
                };
                this.changePage = false;
                this.currentDate = new Date();
                this.myPaginationId = 'vmTask' + this.virtualMachine.id;
                this.isUseFilter = false;
                this.TaskV2Service = webService.UserTaskV2Service;
                this.paginationSettings.pageNumber = 1;
                this.paginationSettings.pageSize = 5;
                this.getTaskV2List();
                this.$scope.$watchCollection('vm.searchParams', function () {
                    if (_this.isUseFilter) {
                        _this.getTaskV2List();
                    }
                });
            }
            VmTaskController.prototype.useFilter = function () {
                this.isUseFilter = !this.isUseFilter;
                this.getTaskV2List();
            };
            VmTaskController.prototype.pageChangeHandler = function (index) {
                this.paginationSettings.pageNumber = index;
                this.changePage = true;
                this.getTaskV2List();
            };
            VmTaskController.prototype.getTaskV2List = function () {
                var _this = this;
                this.vmTaskList = this.TaskV2Service.getAllPage({
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    resourceId: this.virtualMachine.id,
                    dateFrom: this.isUseFilter && this.searchParams ? this.searchParams.dateFrom : null,
                    dateTo: this.isUseFilter && this.searchParams ? this.searchParams.dateTo : null,
                    typeDate: this.isUseFilter ? TypeDate.CompletedAt : null
                });
                this.vmTaskList.$promise
                    .catch(function (response) {
                    _this.messageDialog.show({
                        header: "Сервер",
                        body: "Ошибка сервера"
                    });
                    return;
                });
            };
            VmTaskController.$inject = ["WebServices", "$messageDialog", "TypeTask", "StatusTask", "$scope"];
            return VmTaskController;
        }());
        virtualMachine.VmTaskController = VmTaskController;
        var option = {
            restrict: "EA",
            bindings: {
                virtualMachine: '<'
            },
            templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmTask/vmTask.html',
            controller: VmTaskController,
            controllerAs: 'vm'
        };
        angular.module("user.cloud")
            .component("vmTask", option);
    })(virtualMachine = user.virtualMachine || (user.virtualMachine = {}));
})(user || (user = {}));
