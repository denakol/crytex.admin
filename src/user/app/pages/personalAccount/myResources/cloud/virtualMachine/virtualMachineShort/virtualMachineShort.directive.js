/**
 * Created by denak on 12.02.2016.
 */
var user;
(function (user) {
    var SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    var VirtualMachineShort = (function () {
        function VirtualMachineShort() {
            this.subscriptionTypes = SubscriptionType;
        }
        return VirtualMachineShort;
    }());
    user.VirtualMachineShort = VirtualMachineShort;
    var option = {
        restrict: "EA",
        bindings: {
            virtualMachine: '<',
            control: '&'
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/virtualMachineShort/virtualMachineShort.html',
        controller: VirtualMachineShort,
        controllerAs: 'vm'
    };
    angular.module("user.cloud")
        .component("virtualMachineShort", option);
})(user || (user = {}));
