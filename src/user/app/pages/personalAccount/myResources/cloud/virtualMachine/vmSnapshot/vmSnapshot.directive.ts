module user.virtualMachine {

    export class VmSnaphotController {


    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            virtualMachine: '<'
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmSnapshot/vmSnapshot.html',
        controller: VmSnaphotController,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("vmSnapshot",option);
}