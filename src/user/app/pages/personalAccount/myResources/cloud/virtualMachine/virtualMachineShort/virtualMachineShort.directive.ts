/**
 * Created by denak on 12.02.2016.
 */
module user {
    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;


    export class VirtualMachineShort {
        public subscriptionTypes : typeof SubscriptionType = SubscriptionType;

        public virtualMachine:SubscriptionVmViewModel;



    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            virtualMachine: '<',
            control:'&'
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/virtualMachineShort/virtualMachineShort.html',
        controller: VirtualMachineShort,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("virtualMachineShort",option);
}