
module user.virtualMachine {

    import IResourceClass = angular.resource.IResourceClass;
    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;

    export class VmReinstallController {

        static $inject = ["WebServices", "$messageDialog"];

        public virtualMachine:SubscriptionVmViewModel;

        constructor(private webService: any,  private messageDialog: any) {
        }


    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            virtualMachine: '<'
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmReinstall/vmReinstall.html',
        controller: VmReinstallController,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("vmReinstall",option);
}
