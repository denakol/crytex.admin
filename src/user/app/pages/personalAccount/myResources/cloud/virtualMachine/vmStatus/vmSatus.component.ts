module user.virtualMachine {

    import IResourceClass = angular.resource.IResourceClass;
    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;
    import UpdateMachineStatusOptions = core.WebApi.Models.UpdateMachineStatusOptions;
    import StateMachine = core.WebApi.Models.StateMachine;
    import UserSubscriptionVirtualMachineService = core.IUserSubscriptionVirtualMachineService;
    import UserStateMachineService = core.IUserStateMachineService;
    import StatusVM = core.WebApi.Models.Enums.StatusVM;
    import IScope = angular.IScope;

    export class VmSatusController {

        static $inject = ["WebServices", "$messageDialog", "TypeChangeStatus", "StatusVm", '$scope', 'cloudCalculator'];

        public virtualMachine:SubscriptionVmViewModel;
        public machineState:StateMachine;
        updateMachineStatus(stat:any):any{}

        private UpdateMachineStatusOptions:UpdateMachineStatusOptions = {
            subscriptionId: this.virtualMachine.id
        }
        private UserSubscriptionVirtualMachineService:UserSubscriptionVirtualMachineService;
        private UserStateMachineService:UserStateMachineService;

        public vmOption:VmOption;
        public currentPriceVm:core.PriceVm;
        public typeChangeStatus:any;

        constructor(private webService:any, private messageDialog:any,
                    public TypeChangeStatus:any, public statusVm:any, $scope:IScope,
                    private cloudCalculator:core.CloudCalculator) {
            this.UserSubscriptionVirtualMachineService = webService.UserSubscriptionVirtualMachineService;
            this.UserStateMachineService = webService.UserStateMachineService;

            this.typeChangeStatus = angular.copy(TypeChangeStatus);

            this.getLastMachineState();
            this.setManageButtons();
            $scope.$watch(()=> {
                return this.virtualMachine.userVm.status
            }, ()=> {
                this.setManageButtons();
            });
            this.vmOption = user.VmOption.GetFromVm(this.virtualMachine.userVm, this.virtualMachine.subscriptionType);
            cloudCalculator.CalculatePriceWithWait(this.vmOption).then((result)=> {
                this.currentPriceVm = result;
            });

        }

        public setStatus(status:any) {
            this.UpdateMachineStatusOptions.status = status.value;


            this.UserSubscriptionVirtualMachineService.updateMachineStatus(
                this.UpdateMachineStatusOptions).$promise
                .then(()=> {

                    this.updateMachineStatus({subscription: this.virtualMachine, status: StatusVM.UpdatingStatus});
                }).catch((response:any) => {
                this.messageDialog.show(
                    {
                        header: "Сервер",
                        body: "Ошибка сервера"
                    }
                );
                return;
            });
        }

        private getLastMachineState() {
            this.machineState = this.UserStateMachineService.getLastState(
                {
                    vmId: this.virtualMachine.id
                });

            this.machineState.$promise
                .catch((response:any) => {
                    this.messageDialog.show(
                        {
                            header: "Сервер",
                            body: "Ошибка сервера"
                        }
                    );
                    return;
                });
        }


        // Устанавливаем активность/неактивность кнопок управления в зависимости от состояния машины
        private setManageButtons() {
            switch (this.virtualMachine.userVm.status) {
                case this.statusVm.Enable:
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Start].isActive = false;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Stop].isActive = true;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Reload].isActive = true;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.PowerOff].isActive = true;
                    break;
                case this.statusVm.Disable:
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Start].isActive = true;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Stop].isActive = false;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Reload].isActive = false;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.PowerOff].isActive = false;
                    break;
                case this.statusVm.Error:
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Start].isActive = true;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Stop].isActive = true;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Reload].isActive = true;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.PowerOff].isActive = true;
                    break;
                case this.statusVm.Creating:
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Start].isActive = false;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Stop].isActive = false;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Reload].isActive = false;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.PowerOff].isActive = false;
                    break;

                default:
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Start].isActive = false;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Stop].isActive = false;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.Reload].isActive = false;
                    this.typeChangeStatus.arrayItems[this.typeChangeStatus.PowerOff].isActive = false;
                    break;
            }
        }


    }


    var option:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            virtualMachine: '<',
            updateMachineStatus: "&"
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmStatus/vmStatus.html',
        controller: VmSatusController,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("vmStatus", option);
}
