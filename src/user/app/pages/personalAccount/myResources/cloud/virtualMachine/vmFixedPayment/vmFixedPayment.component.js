var user;
(function (user) {
    var virtualMachine;
    (function (virtualMachine) {
        var VmFixedPaymentController = (function () {
            function VmFixedPaymentController(webService, messageDialog, groupingTypes, $scope) {
                var _this = this;
                this.webService = webService;
                this.messageDialog = messageDialog;
                this.groupingTypes = groupingTypes;
                this.$scope = $scope;
                this.paginationSettings = {
                    pageNumber: 1,
                    pageSize: 5
                };
                this.changePage = false;
                this.currentDate = new Date();
                this.myPaginationId = 'vmFixedPayment' + this.virtualMachine.id;
                this.isUseFilter = false;
                this.FixedSubscriptionPaymentService = webService.UserFixedSubscriptionPayment;
                this.paginationSettings.pageNumber = 1;
                this.paginationSettings.pageSize = 5;
                this.getFixedPayments();
                this.$scope.$watchCollection('vm.searchParams', function () {
                    if (_this.isUseFilter) {
                        _this.getFixedPayments();
                    }
                });
            }
            VmFixedPaymentController.prototype.pageChangeHandler = function (index) {
                this.paginationSettings.pageNumber = index;
                this.changePage = true;
                this.getFixedPayments();
            };
            VmFixedPaymentController.prototype.useFilter = function () {
                this.isUseFilter = !this.isUseFilter;
                this.getFixedPayments();
            };
            VmFixedPaymentController.prototype.getFixedPayments = function () {
                var _this = this;
                this.fixedPaymentList = this.FixedSubscriptionPaymentService.getAllPage({
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    subscriptionVmId: this.virtualMachine.id,
                    dateFrom: this.isUseFilter && this.searchParams ? this.searchParams.dateFrom : null,
                    dateTo: this.isUseFilter && this.searchParams ? this.searchParams.dateTo : null
                });
                this.fixedPaymentList.$promise
                    .catch(function (response) {
                    _this.messageDialog.show({
                        header: "Сервер",
                        body: "Ошибка сервера"
                    });
                    return;
                });
            };
            VmFixedPaymentController.$inject = ["WebServices", "$messageDialog", "UsageSubscriptionPaymentGroupingTypes", "$scope"];
            return VmFixedPaymentController;
        }());
        virtualMachine.VmFixedPaymentController = VmFixedPaymentController;
        var option = {
            restrict: "EA",
            bindings: {
                virtualMachine: '<'
            },
            templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmFixedPayment/vmFixedPayment.html',
            controller: VmFixedPaymentController,
            controllerAs: 'vm'
        };
        angular.module("user.cloud")
            .component("vmFixedPayment", option);
    })(virtualMachine = user.virtualMachine || (user.virtualMachine = {}));
})(user || (user = {}));
