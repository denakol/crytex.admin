var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var user;
(function (user) {
    var cloud;
    (function (cloud) {
        var StatusVM = core.WebApi.Models.Enums.StatusVM;
        var TypeChangeStatus = core.WebApi.Models.Enums.TypeChangeStatus;
        var VmConfigCloudController = (function (_super) {
            __extends(VmConfigCloudController, _super);
            function VmConfigCloudController(cloudCalculator, $scope, VmBilling, $mdDialog, WebServices, messageDialog) {
                var _this = this;
                _super.call(this);
                this.cloudCalculator = cloudCalculator;
                this.VmBilling = VmBilling;
                this.$mdDialog = $mdDialog;
                this.WebServices = WebServices;
                this.messageDialog = messageDialog;
                this.updating = false;
                this.wasChanges = false;
                this.status = StatusVM;
                this.UpdateMachineStatusOptions = {
                    subscriptionId: this.virtualMachine.id
                };
                var _this = this;
                this.vmOption = user.VmOption.GetFromVm(this.virtualMachine.userVm, this.virtualMachine.subscriptionType);
                cloudCalculator.CalculatePriceWithWait(this.vmOption).then(function (result) {
                    _this.currentPriceVm = result;
                });
                $scope.$watch(function () { return _this.vmOption; }, function () {
                    if (_this.vmOption.hdd !== _this.vmOption.currenHdd ||
                        _this.vmOption.cpu !== _this.vmOption.currentCpu ||
                        _this.vmOption.ram !== _this.vmOption.currentRam) {
                        _this.wasChanges = true;
                    }
                    else {
                        _this.wasChanges = false;
                    }
                    cloudCalculator.CalculatePriceWithWait(_this.vmOption).then(function (result) {
                        _this.newPriceVm = result;
                    });
                }, true);
                this.UserSubscriptionVirtualMachineService = WebServices.UserSubscriptionVirtualMachineService;
            }
            VmConfigCloudController.prototype.updateMachineStatus = function (status) { };
            VmConfigCloudController.prototype.startUpdate = function () {
                this.updating = true;
            };
            VmConfigCloudController.prototype.endUpdate = function () {
                this.updating = false;
            };
            VmConfigCloudController.prototype.modify = function () {
                if (this.virtualMachine.userVm.status !== StatusVM.Enable) {
                    this.saveConfiguration();
                }
                else {
                    var confirm = this.$mdDialog.confirm()
                        .title('Предупреждение')
                        .textContent('При смене конфигурации машина будет отключена. Применить изменения?')
                        .ok('Да')
                        .cancel('Отменить');
                    this.$mdDialog.show(confirm).then(function () {
                        this.stopAndSaveMachine();
                    }, function () {
                        // Ну нет так нет
                    });
                }
            };
            VmConfigCloudController.prototype.stopAndSaveMachine = function () {
                var _this = this;
                this.UpdateMachineStatusOptions.status = TypeChangeStatus.PowerOff;
                this.UserSubscriptionVirtualMachineService.updateMachineStatus(this.UpdateMachineStatusOptions).$promise
                    .then(function () {
                    _this.saveConfiguration();
                }).catch(function (response) {
                    _this.messageDialog.show({
                        header: "Сервер",
                        body: "Ошибка сервера"
                    });
                    return;
                });
            };
            VmConfigCloudController.prototype.saveConfiguration = function () {
                var _this = this;
                this.VmBilling.updateMachineConfiguration(this.virtualMachine.id, this.vmOption.cpu, this.vmOption.ram, this.vmOption.hdd).then(function () {
                    _this.updateMachineStatus({ subscription: _this.virtualMachine, status: StatusVM.UpdatingConf });
                    _this.endUpdate();
                });
            };
            VmConfigCloudController.$inject = ['cloudCalculator', '$scope', 'VmBilling',
                '$mdDialog', 'WebServices', '$messageDialog'];
            return VmConfigCloudController;
        }(cloud.BaseVmComponentController));
        cloud.VmConfigCloudController = VmConfigCloudController;
        var option = {
            restrict: "EA",
            bindings: {
                virtualMachine: '<',
                updateMachineStatus: "&"
            },
            templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmConfigCloud/vmConfigCloud.html',
            controller: VmConfigCloudController,
            controllerAs: 'vm'
        };
        angular.module("user.cloud")
            .component("vmConfigCloud", option);
    })(cloud = user.cloud || (user.cloud = {}));
})(user || (user = {}));
