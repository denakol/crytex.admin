/**
 * Created by denak on 12.02.2016.
 */
/**
 * Created by denak on 12.02.2016.
 */
module user {
    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    import StatusVM = core.WebApi.Models.Enums.StatusVM;



    export class VmSateController {
        public StatusVMs : typeof StatusVM = StatusVM;

        public state:StatusVM;
    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            state: '<'
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmState/vmSate.html',
        controller: VmSateController,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("vmState",option);
}