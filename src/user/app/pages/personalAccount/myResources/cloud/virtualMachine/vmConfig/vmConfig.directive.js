var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var user;
(function (user) {
    var cloud;
    (function (cloud) {
        var virtualMachine;
        (function (virtualMachine) {
            var StatusVM = core.WebApi.Models.Enums.StatusVM;
            var BaseVmComponentController = user.cloud.BaseVmComponentController;
            var TypeChangeStatus = core.WebApi.Models.Enums.TypeChangeStatus;
            var VmConfigController = (function (_super) {
                __extends(VmConfigController, _super);
                function VmConfigController(cloudCalculator, $scope, VmBilling, WebServices, messageDialog, $mdDialog) {
                    var _this = this;
                    _super.call(this);
                    this.cloudCalculator = cloudCalculator;
                    this.VmBilling = VmBilling;
                    this.WebServices = WebServices;
                    this.messageDialog = messageDialog;
                    this.$mdDialog = $mdDialog;
                    this.updating = false;
                    this.prolongation = false;
                    this.wasChanges = false;
                    this.status = StatusVM;
                    this.prolongateOption = {
                        subscriptionId: this.virtualMachine.id,
                        monthCount: 0
                    };
                    this.UpdateMachineStatusOptions = {
                        subscriptionId: this.virtualMachine.id
                    };
                    var _this = this;
                    this.vmOption = user.VmOption.GetFromVm(this.virtualMachine.userVm, this.virtualMachine.subscriptionType);
                    cloudCalculator.CalculatePriceWithWait(this.vmOption).then(function (result) {
                        _this.currentPriceVm = result;
                    });
                    $scope.$watch(function () { return _this.vmOption; }, function () {
                        if (_this.vmOption.hdd !== _this.vmOption.currenHdd ||
                            _this.vmOption.cpu !== _this.vmOption.currentCpu ||
                            _this.vmOption.ram !== _this.vmOption.currentRam) {
                            _this.wasChanges = true;
                        }
                        else {
                            _this.wasChanges = false;
                        }
                        cloudCalculator.CalculatePriceWithWait(_this.vmOption).then(function (result) {
                            _this.newPriceVm = result;
                        });
                    }, true);
                    this.UserSubscriptionVirtualMachineService = WebServices.UserSubscriptionVirtualMachineService;
                }
                VmConfigController.prototype.updateMachineStatus = function (status) { };
                VmConfigController.prototype.startUpdate = function () {
                    this.updating = true;
                };
                VmConfigController.prototype.endUpdate = function () {
                    this.updating = false;
                };
                VmConfigController.prototype.modify = function () {
                    if (this.virtualMachine.userVm.status !== StatusVM.Enable) {
                        this.saveConfiguration();
                    }
                    else {
                        var confirm = this.$mdDialog.confirm()
                            .title('Предупреждение')
                            .textContent('При смене конфигурации машина будет отключена. Применить изменения?')
                            .ok('Да')
                            .cancel('Отменить');
                        this.$mdDialog.show(confirm).then(function () {
                            this.stopAndSaveMachine();
                        }, function () {
                            // Ну нет так нет
                        });
                    }
                };
                VmConfigController.prototype.extendSubscription = function () {
                    var _this = this;
                    if (this.prolongateOption.monthCount > 0) {
                        this.UserSubscriptionVirtualMachineService.extendSubscription(this.prolongateOption).$promise
                            .then(function () {
                            _this.prolongateOption.monthCount = 0;
                            _this.prolongation = false;
                        }).catch(function (response) {
                            var message = response.data.errorMessage ? response.data.errorMessage : "Ошибка сервера";
                            _this.prolongation = false;
                            _this.messageDialog.show({
                                header: "Сервер",
                                body: message
                            });
                            return;
                        });
                    }
                };
                VmConfigController.prototype.stopAndSaveMachine = function () {
                    var _this = this;
                    this.UpdateMachineStatusOptions.status = TypeChangeStatus.PowerOff;
                    this.UserSubscriptionVirtualMachineService.updateMachineStatus(this.UpdateMachineStatusOptions).$promise
                        .then(function () {
                        _this.saveConfiguration();
                    }).catch(function (response) {
                        _this.messageDialog.show({
                            header: "Сервер",
                            body: "Ошибка сервера"
                        });
                        return;
                    });
                };
                VmConfigController.prototype.saveConfiguration = function () {
                    var _this = this;
                    this.VmBilling.updateMachineConfiguration(this.virtualMachine.id, this.vmOption.cpu, this.vmOption.ram, this.vmOption.hdd).then(function () {
                        _this.updateMachineStatus({ subscription: _this.virtualMachine, status: StatusVM.UpdatingConf });
                        _this.endUpdate();
                    });
                };
                VmConfigController.$inject = ['cloudCalculator', '$scope', 'VmBilling',
                    'WebServices', '$messageDialog', '$mdDialog'];
                return VmConfigController;
            }(BaseVmComponentController));
            virtualMachine.VmConfigController = VmConfigController;
            var option = {
                restrict: "EA",
                bindings: {
                    virtualMachine: '<',
                    updateMachineStatus: "&"
                },
                templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmConfig/vmConfig.html',
                controller: VmConfigController,
                controllerAs: 'vm'
            };
            angular.module("user.cloud")
                .component("vmConfig", option);
        })(virtualMachine = cloud.virtualMachine || (cloud.virtualMachine = {}));
    })(cloud = user.cloud || (user.cloud = {}));
})(user || (user = {}));
