var user;
(function (user) {
    var virtualMachine;
    (function (virtualMachine) {
        var VmReinstallController = (function () {
            function VmReinstallController(webService, messageDialog) {
                this.webService = webService;
                this.messageDialog = messageDialog;
            }
            VmReinstallController.$inject = ["WebServices", "$messageDialog"];
            return VmReinstallController;
        }());
        virtualMachine.VmReinstallController = VmReinstallController;
        var option = {
            restrict: "EA",
            bindings: {
                virtualMachine: '<'
            },
            templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmReinstall/vmReinstall.html',
            controller: VmReinstallController,
            controllerAs: 'vm'
        };
        angular.module("user.cloud")
            .component("vmReinstall", option);
    })(virtualMachine = user.virtualMachine || (user.virtualMachine = {}));
})(user || (user = {}));
