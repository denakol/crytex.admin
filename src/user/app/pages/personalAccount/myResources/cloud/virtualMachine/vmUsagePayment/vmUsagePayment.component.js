var user;
(function (user) {
    var virtualMachine;
    (function (virtualMachine) {
        var VmUsagePaymentController = (function () {
            function VmUsagePaymentController(webService, messageDialog, $scope) {
                var _this = this;
                this.webService = webService;
                this.messageDialog = messageDialog;
                this.$scope = $scope;
                this.paginationSettings = {
                    pageNumber: 1,
                    pageSize: 5
                };
                this.changePage = false;
                this.currentDate = new Date();
                this.myPaginationId = 'vmUsagePayment' + this.virtualMachine.id;
                this.isUseFilter = false;
                this.UsageSubscriptionPaymentService = webService.UserUsageSubscriptionPayment;
                this.paginationSettings.pageNumber = 1;
                this.paginationSettings.pageSize = 5;
                this.getUsagePayments();
                this.$scope.$watchCollection('vm.searchParams', function () {
                    if (_this.isUseFilter) {
                        _this.getUsagePayments();
                    }
                });
            }
            VmUsagePaymentController.prototype.useFilter = function () {
                this.isUseFilter = !this.isUseFilter;
                this.getUsagePayments();
            };
            VmUsagePaymentController.prototype.pageChangeHandler = function (index) {
                this.paginationSettings.pageNumber = index;
                this.changePage = true;
                this.getUsagePayments();
            };
            VmUsagePaymentController.prototype.getUsagePayments = function () {
                var _this = this;
                this.usagePaymentList = this.UsageSubscriptionPaymentService.getAllPage({
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    subscriptionVmId: this.virtualMachine.id,
                    dateFrom: this.isUseFilter && this.searchParams ? this.searchParams.dateFrom : null,
                    dateTo: this.isUseFilter && this.searchParams ? this.searchParams.dateTo : null,
                    periodType: this.isUseFilter ? this.searchParams.countingPeriodType : null
                });
                this.usagePaymentList.$promise
                    .catch(function (response) {
                    _this.messageDialog.show({
                        header: "Сервер",
                        body: "Ошибка сервера"
                    });
                    return;
                });
            };
            VmUsagePaymentController.$inject = ["WebServices", "$messageDialog", "$scope"];
            return VmUsagePaymentController;
        }());
        virtualMachine.VmUsagePaymentController = VmUsagePaymentController;
        var option = {
            restrict: "EA",
            bindings: {
                virtualMachine: '<'
            },
            templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmUsagePayment/vmUsagePayment.html',
            controller: VmUsagePaymentController,
            controllerAs: 'vm'
        };
        angular.module("user.cloud")
            .component("vmUsagePayment", option);
    })(virtualMachine = user.virtualMachine || (user.virtualMachine = {}));
})(user || (user = {}));
