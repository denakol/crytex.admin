module user.virtualMachine {

    import BaseVmComponentController = user.cloud.BaseVmComponentController;
    import IUserSubscriptionVirtualMachineService = core.IUserSubscriptionVirtualMachineService;
    import UpdateSubscriptionBackupStoragePeriodModel = core.WebApi.Models.UpdateSubscriptionBackupStoragePeriodModel;

    export class VmBackupController extends BaseVmComponentController{

        public static $inject = ["cloudCalculator", "$scope", "VmBilling", "WebServices", "$messageDialog"];

        updateMachineBackupStoring(dailyBackupStorePeriodDays:number):any{}
        public updating:boolean;
        public vmOption:VmOption;
        public currentPriceVm:core.PriceVm;
        public newPriceVm:core.PriceVm;
        public dailyBackupStorePeriodDays: number;
        private UserSubscriptionVirtualMachineService: IUserSubscriptionVirtualMachineService;
        public updateSubscriptionBackupStoragePeriodModel:UpdateSubscriptionBackupStoragePeriodModel;

        public SubscriptionType =core.WebApi.Models.Enums.SubscriptionType;
        constructor(private cloudCalculator:any,
                    public $scope:any,
                    public VmBilling:any,
                    private WebServices:any,
                    public messageDialog:any){
            super();
            this.vmOption = user.VmOption.GetFromVm(this.virtualMachine.userVm,
                this.virtualMachine.subscriptionType);
            this.updating = false;
            this.UserSubscriptionVirtualMachineService = WebServices.UserSubscriptionVirtualMachineService;
            this.updateSubscriptionBackupStoragePeriodModel = {
                subscriptionId: this.virtualMachine.id,
                newPeriodDays: this.virtualMachine.dailyBackupStorePeriodDays
            };

            cloudCalculator.CalculatePriceWithWait(this.vmOption).then((result:any) => {
                this.currentPriceVm = result;
            });

            $scope.$watch(() => this.vmOption, () => {
                cloudCalculator.CalculatePriceWithWait(this.vmOption).then((result:any) => {
                    this.newPriceVm = result;
                });
            }, true);

            
        }

        public startUpdate(){
            this.updating = true;
        }
        public modify(){
            this.UserSubscriptionVirtualMachineService.updateSubscriptionBackupStoragePeriod(
                this.updateSubscriptionBackupStoragePeriodModel
            ).$promise
                .then(
                    (data:any)=>{
                        this.updateMachineBackupStoring({
                            subscription: this.virtualMachine,
                            dailyBackupStorePeriodDays: this.updateSubscriptionBackupStoragePeriodModel.newPeriodDays
                        });
                        this.endUpdate();
                    }
                )
                .catch((response:any)=>{
                    var message = response.data.errorMessage ? response.data.errorMessage : "Ошибка сервера";
                    this.messageDialog.show(
                        {
                            header: "Сервер",
                            body: message
                        }
                    );
                });
        }

        public endUpdate() {
            this.updating = false;
        }
        public storeCopy(index:number){
            this.updateSubscriptionBackupStoragePeriodModel.newPeriodDays = index;
        }
    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            virtualMachine: '<',
            updateMachineBackupStoring: '&'
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmBackup/vmBackup.html',
        controller: VmBackupController,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("vmBackup",option);
}