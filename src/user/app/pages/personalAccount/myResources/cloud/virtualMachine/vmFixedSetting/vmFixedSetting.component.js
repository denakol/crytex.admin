var user;
(function (user) {
    var virtualMachine;
    (function (virtualMachine) {
        var VmFixedSettingController = (function () {
            function VmFixedSettingController(WebServices, messageDialog, $scope) {
                var _this = this;
                this.WebServices = WebServices;
                this.messageDialog = messageDialog;
                this.$scope = $scope;
                this.subscriptionUpdateOption = {
                    id: this.virtualMachine.id,
                    name: this.virtualMachine.name,
                    autoProlongation: this.virtualMachine.autoProlongation
                };
                this.isEnableManageButtons = false;
                this.SubscriptionVirtualMachineService = WebServices.UserSubscriptionVirtualMachineService;
                $scope.$watchCollection(function () { return _this.subscriptionUpdateOption; }, function (newValue) {
                    if (newValue.autoProlongation !== _this.virtualMachine.autoProlongation ||
                        newValue.name !== _this.virtualMachine.name) {
                        _this.isEnableManageButtons = true;
                    }
                    else {
                        _this.isEnableManageButtons = false;
                    }
                });
            }
            VmFixedSettingController.prototype.updateMachineSettings = function (conf) { };
            VmFixedSettingController.prototype.setNewOptions = function () {
                var _this = this;
                this.SubscriptionVirtualMachineService.updateSubscription(this.subscriptionUpdateOption).$promise
                    .then(function () {
                    _this.isEnableManageButtons = false;
                    _this.updateMachineSettings({
                        subscription: _this.virtualMachine,
                        conf: {
                            name: _this.subscriptionUpdateOption.name,
                            autoProlongation: _this.subscriptionUpdateOption.autoProlongation
                        }
                    });
                    return;
                })
                    .catch(function (response) {
                    _this.messageDialog.show({
                        header: "Сервер",
                        body: "Ошибка сервера"
                    });
                    return;
                });
            };
            VmFixedSettingController.prototype.returnValue = function () {
                this.subscriptionUpdateOption.name = this.virtualMachine.name;
            };
            VmFixedSettingController.$inject = ["WebServices", "$messageDialog", "$scope"];
            return VmFixedSettingController;
        }());
        virtualMachine.VmFixedSettingController = VmFixedSettingController;
        var option = {
            restrict: "EA",
            bindings: {
                virtualMachine: '<',
                updateMachineSettings: "&"
            },
            templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmFixedSetting/vmFixedSetting.html',
            controller: VmFixedSettingController,
            controllerAs: 'vm'
        };
        angular.module("user.cloud")
            .component("vmFixedSetting", option);
    })(virtualMachine = user.virtualMachine || (user.virtualMachine = {}));
})(user || (user = {}));
