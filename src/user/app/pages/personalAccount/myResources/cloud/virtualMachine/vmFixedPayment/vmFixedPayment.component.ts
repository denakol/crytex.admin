module user.virtualMachine {

    import IResourceClass = angular.resource.IResourceClass;
    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;
    import PageModel = core.PageModel;
    import PaginationSetting = core.PaginationSetting;
    import FixedSubscriptionPaymentViewModel = core.WebApi.Models.FixedSubscriptionPaymentViewModel;


    export class VmFixedPaymentController {

        static $inject = ["WebServices", "$messageDialog", "UsageSubscriptionPaymentGroupingTypes", "$scope"];

        public virtualMachine: SubscriptionVmViewModel;
        public fixedPaymentList: PageModel<FixedSubscriptionPaymentViewModel>;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        }
        public changePage: boolean = false;
        public currentDate: Date = new Date();
        public myPaginationId: string = 'vmFixedPayment' + this.virtualMachine.id;

        public isUseFilter: boolean = false;
        public searchParams: ISearchParams;

        private FixedSubscriptionPaymentService: IResourceClass<PageModel<FixedSubscriptionPaymentViewModel>>;

        constructor(private webService: any,  private messageDialog: any,
                    private groupingTypes: any, private $scope: ng.IScope) {
            this.FixedSubscriptionPaymentService = webService.UserFixedSubscriptionPayment;

            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;
            this.getFixedPayments();

            this.$scope.$watchCollection('vm.searchParams', () => {
                if (this.isUseFilter) {
                    this.getFixedPayments();
                }
            });
        }

        public pageChangeHandler (index: number) {
            this.paginationSettings.pageNumber = index;
            this.changePage = true;
            this.getFixedPayments();
        }

        public useFilter() {
            this.isUseFilter = !this.isUseFilter;
            this.getFixedPayments();
        }

        private getFixedPayments() {
            this.fixedPaymentList = this.FixedSubscriptionPaymentService.getAllPage(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    subscriptionVmId: this.virtualMachine.id,
                    dateFrom: this.isUseFilter && this.searchParams ? this.searchParams.dateFrom : null,
                    dateTo: this.isUseFilter && this.searchParams ? this.searchParams.dateTo : null
                });

            this.fixedPaymentList.$promise
                .catch ( (response:any) => {
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }

    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            virtualMachine: '<'
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmFixedPayment/vmFixedPayment.html',
        controller: VmFixedPaymentController,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("vmFixedPayment",option);
}
