module user.cloud {

    import VmBilling = core.VmBilling;
    import StatusVM = core.WebApi.Models.Enums.StatusVM;
    import TypeChangeStatus = core.WebApi.Models.Enums.TypeChangeStatus;
    import UpdateMachineStatusOptions = core.WebApi.Models.UpdateMachineStatusOptions;
    import IUserSubscriptionVirtualMachineService = core.IUserSubscriptionVirtualMachineService;

    export class VmConfigCloudController extends BaseVmComponentController {

        public static $inject = ['cloudCalculator','$scope','VmBilling',
            '$mdDialog', 'WebServices', '$messageDialog']

        updateMachineStatus(status:StatusVM):any{}
        public updating:boolean = false;

        public currentPriceVm:core.PriceVm;
        public newPriceVm:core.PriceVm;
        public vmOption:VmOption;
        public wasChanges: boolean = false;

        public status :typeof StatusVM = StatusVM;

        private UpdateMachineStatusOptions:UpdateMachineStatusOptions = {
            subscriptionId: this.virtualMachine.id
        }
        private UserSubscriptionVirtualMachineService: IUserSubscriptionVirtualMachineService;

        public constructor(private cloudCalculator:core.CloudCalculator,$scope:any,
                           public VmBilling:VmBilling, private $mdDialog: any,
                            private WebServices: any, private messageDialog: any) {
            super();

            var _this = this;

            this.vmOption = user.VmOption.GetFromVm(this.virtualMachine.userVm, this.virtualMachine.subscriptionType);
            cloudCalculator.CalculatePriceWithWait(this.vmOption).then((result)=> {
                this.currentPriceVm = result;
            });

            $scope.$watch(()=>this.vmOption, ()=> {
                if (_this.vmOption.hdd !== _this.vmOption.currenHdd ||
                    _this.vmOption.cpu !== _this.vmOption.currentCpu ||
                    _this.vmOption.ram !== _this.vmOption.currentRam) {
                    _this.wasChanges = true;
                } else {
                    _this.wasChanges = false;
                }
                cloudCalculator.CalculatePriceWithWait(this.vmOption).then((result)=> {
                    this.newPriceVm = result;
                });
            },true);

            this.UserSubscriptionVirtualMachineService = WebServices.UserSubscriptionVirtualMachineService;
        }

        public startUpdate() {
            this.updating = true;
        }
        public endUpdate() {
            this.updating = false;
        }
        public modify() {
            if (this.virtualMachine.userVm.status !== StatusVM.Enable) {
                this.saveConfiguration();
            } else {
                var confirm = this.$mdDialog.confirm()
                    .title('Предупреждение')
                    .textContent('При смене конфигурации машина будет отключена. Применить изменения?')
                    .ok('Да')
                    .cancel('Отменить');
                this.$mdDialog.show(confirm).then(function () {
                    this.stopAndSaveMachine();
                }, function () {
                    // Ну нет так нет
                });
            }
        }

        private stopAndSaveMachine() {
            this.UpdateMachineStatusOptions.status = TypeChangeStatus.PowerOff;

            this.UserSubscriptionVirtualMachineService.updateMachineStatus(
                this.UpdateMachineStatusOptions).$promise
                .then(()=> {
                    this.saveConfiguration();
                }).catch((response:any) => {
                this.messageDialog.show(
                    {
                        header: "Сервер",
                        body: "Ошибка сервера"
                    }
                );
                return;
            });
        }

        private saveConfiguration() {
            this.VmBilling.updateMachineConfiguration(this.virtualMachine.id,this.vmOption.cpu,this.vmOption.ram,this.vmOption.hdd).then(
                ()=>{
                    this.updateMachineStatus({subscription: this.virtualMachine, status: StatusVM.UpdatingConf});
                    this.endUpdate();
                }
            );
        }
    }

    var option:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            virtualMachine: '<',

            updateMachineStatus:"&"
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmConfigCloud/vmConfigCloud.html',
        controller: VmConfigCloudController,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("vmConfigCloud", option);
}