var user;
(function (user) {
    var virtualMachine;
    (function (virtualMachine) {
        var VmSnaphotController = (function () {
            function VmSnaphotController() {
            }
            return VmSnaphotController;
        }());
        virtualMachine.VmSnaphotController = VmSnaphotController;
        var option = {
            restrict: "EA",
            bindings: {
                virtualMachine: '<'
            },
            templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmSnapshot/vmSnapshot.html',
            controller: VmSnaphotController,
            controllerAs: 'vm'
        };
        angular.module("user.cloud")
            .component("vmSnapshot", option);
    })(virtualMachine = user.virtualMachine || (user.virtualMachine = {}));
})(user || (user = {}));
