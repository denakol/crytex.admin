var user;
(function (user) {
    var virtualMachine;
    (function (virtualMachine) {
        var VmUsageSettingController = (function () {
            function VmUsageSettingController(WebServices, messageDialog, $scope) {
                var _this = this;
                this.WebServices = WebServices;
                this.messageDialog = messageDialog;
                this.$scope = $scope;
                this.subscriptionUpdateOption = {
                    id: this.virtualMachine.id,
                    name: this.virtualMachine.name
                };
                this.isEnableManageButtons = false;
                this.SubscriptionVirtualMachineService = WebServices.UserSubscriptionVirtualMachineService;
                $scope.$watch(function () { return _this.subscriptionUpdateOption.name; }, function (newValue, oldValue) {
                    if (newValue !== _this.virtualMachine.name) {
                        _this.isEnableManageButtons = true;
                    }
                    else {
                        _this.isEnableManageButtons = false;
                    }
                });
            }
            VmUsageSettingController.prototype.updateMachineSettings = function (conf) { };
            VmUsageSettingController.prototype.setNewOptions = function () {
                var _this = this;
                this.SubscriptionVirtualMachineService.updateSubscription(this.subscriptionUpdateOption).$promise
                    .then(function () {
                    _this.isEnableManageButtons = false;
                    _this.updateMachineSettings({
                        subscription: _this.virtualMachine,
                        conf: {
                            name: _this.subscriptionUpdateOption.name,
                            autoProlongation: _this.subscriptionUpdateOption.autoProlongation
                        }
                    });
                    return;
                })
                    .catch(function (response) {
                    _this.messageDialog.show({
                        header: "Сервер",
                        body: "Ошибка сервера"
                    });
                    return;
                });
            };
            VmUsageSettingController.prototype.returnValue = function () {
                this.subscriptionUpdateOption.name = this.virtualMachine.name;
            };
            VmUsageSettingController.$inject = ["WebServices", "$messageDialog", "$scope"];
            return VmUsageSettingController;
        }());
        virtualMachine.VmUsageSettingController = VmUsageSettingController;
        var option = {
            restrict: "EA",
            bindings: {
                virtualMachine: '<',
                updateMachineSettings: "&"
            },
            templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmUsageSetting/vmUsageSetting.html',
            controller: VmUsageSettingController,
            controllerAs: 'vm'
        };
        angular.module("user.cloud")
            .component("vmUsageSetting", option);
    })(virtualMachine = user.virtualMachine || (user.virtualMachine = {}));
})(user || (user = {}));
