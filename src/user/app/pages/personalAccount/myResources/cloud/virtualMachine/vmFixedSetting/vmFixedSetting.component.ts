module user.virtualMachine {

    import IResourceClass= angular.resource.IResourceClass;
    import SubscriptionUpdateOptions = core.WebApi.Models.SubscriptionUpdateOptions;
    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;
    import UserSubscriptionVirtualMachineService = core.IUserSubscriptionVirtualMachineService;

    export class VmFixedSettingController {
        static $inject = ["WebServices", "$messageDialog", "$scope"];

        updateMachineSettings(conf:any):any{}

        public subscriptionUpdateOption: SubscriptionUpdateOptions = {
            id: this.virtualMachine.id,
            name: this.virtualMachine.name,
            autoProlongation: this.virtualMachine.autoProlongation
        };
        public virtualMachine: SubscriptionVmViewModel;
        public isEnableManageButtons: boolean = false;

        private SubscriptionVirtualMachineService: UserSubscriptionVirtualMachineService;

        constructor(private WebServices: any, private messageDialog: any,
                    private $scope: ng.IScope) {
            this.SubscriptionVirtualMachineService = WebServices.UserSubscriptionVirtualMachineService;

            $scope.$watchCollection(() => { return this.subscriptionUpdateOption; },
                (newValue: SubscriptionUpdateOptions) => {
                    if (newValue.autoProlongation !== this.virtualMachine.autoProlongation ||
                        newValue.name !== this.virtualMachine.name) {
                        this.isEnableManageButtons = true;
                    } else {
                        this.isEnableManageButtons = false;
                    }
                });
        }

        public setNewOptions() {
            this.SubscriptionVirtualMachineService.updateSubscription(this.subscriptionUpdateOption).$promise
                .then( () => {
                    this.isEnableManageButtons = false;
                    this.updateMachineSettings({
                        subscription: this.virtualMachine,
                        conf:{
                            name:this.subscriptionUpdateOption.name,
                            autoProlongation: this.subscriptionUpdateOption.autoProlongation
                        }
                    });

                    return;
                })
                .catch ( (response:any) => {
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }

        public returnValue() {
            this.subscriptionUpdateOption.name = this.virtualMachine.name;
        }

    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            virtualMachine: '<',
            updateMachineSettings:"&"
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmFixedSetting/vmFixedSetting.html',
        controller: VmFixedSettingController,
        controllerAs: 'vm'
    };

    angular.module("user.cloud")
        .component("vmFixedSetting",option);
}