/**
 * Created by denak on 12.02.2016.
 */
/**
 * Created by denak on 12.02.2016.
 */
var user;
(function (user) {
    var StatusVM = core.WebApi.Models.Enums.StatusVM;
    var VmSateController = (function () {
        function VmSateController() {
            this.StatusVMs = StatusVM;
        }
        return VmSateController;
    }());
    user.VmSateController = VmSateController;
    var option = {
        restrict: "EA",
        bindings: {
            state: '<'
        },
        templateUrl: 'app/pages/personalAccount/myResources/cloud/virtualMachine/vmState/vmSate.html',
        controller: VmSateController,
        controllerAs: 'vm'
    };
    angular.module("user.cloud")
        .component("vmState", option);
})(user || (user = {}));
