/**
 * Created by denak on 04.03.2016.
 */
var user;
(function (user) {
    var game;
    (function (game) {
        var StatusTask = core.WebApi.Models.Enums.StatusTask;
        var TypeTask = core.WebApi.Models.Enums.TypeTask;
        var GameServerState = core.WebApi.Models.Enums.GameServerState;
        var ChangeGameServerStatusOptions = core.WebApi.Models.ChangeGameServerStatusOptions;
        var SerializationHelper = core.SerializationHelper;
        var TypeChangeStatus = core.WebApi.Models.Enums.TypeChangeStatus;
        var GameServerController = (function () {
            function GameServerController(WebServices, monitorTask) {
                var _this = this;
                this.WebServices = WebServices;
                this.paginationSettings = {
                    pageNumber: 1,
                    pageSize: 5
                };
                this.getGameServers();
                this.UserTaskV2Service = WebServices.UserTaskV2Service;
                monitorTask = new monitorTask({
                    callback: function (task) {
                        _this.gameServers.$promise.then(function () {
                            var vm = _this.gameServers.items.find(function (item) {
                                return item.id == task.task.resourceId;
                            });
                            if (vm != null) {
                                _this.calculateStateMachineFromTask(vm, task);
                            }
                        });
                    }
                });
            }
            GameServerController.prototype.getGameServers = function () {
                var _this = this;
                this.gameServers = this.WebServices.AdminGameServerService.pagingQuery({
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize
                });
                this.gameServers.$promise.then(function () {
                    angular.forEach(_this.gameServers.items, function (item) {
                        item.tasks = [];
                        var page = _this.GetTask(item.id);
                        page.$promise.then(function () {
                            item.tasks = page.items;
                            _this.calculateStateMachine(item);
                        });
                    });
                });
            };
            GameServerController.prototype.pageChangeHandler = function (newPageNumber) {
                this.paginationSettings.pageNumber = newPageNumber;
                this.getGameServers();
            };
            GameServerController.prototype.GetTask = function (id) {
                var params = {
                    resourceId: id,
                    pageNumber: 1,
                    pageSize: 100,
                    statusTasks: [StatusTask.Pending, StatusTask.Processing, StatusTask.Queued]
                };
                var page = this.UserTaskV2Service.pagingQuery(params);
                return page;
            };
            ;
            GameServerController.prototype.updateMachineStatus = function (serverViewModel, status) {
                serverViewModel.serverState = status;
            };
            GameServerController.prototype.updateGameServerSlot = function (gameServer, slotCount) {
                gameServer.slotCount = slotCount;
            };
            GameServerController.prototype.updateGameServerConf = function (gameServer, conf) {
                gameServer.name = conf.name;
                gameServer.autoProlongation = conf.autoProlongation;
            };
            GameServerController.prototype.calculateStateMachineFromTask = function (serverViewModel, taskNotify) {
                var _this = this;
                if (taskNotify.success) {
                    switch (taskNotify.task.typeTask) {
                        case TypeTask.CreateGameServer:
                            {
                                serverViewModel.serverState = GameServerState.Enable;
                                break;
                            }
                        case TypeTask.GameServerChangeStatus:
                            {
                                var options = SerializationHelper.toInstance(new ChangeGameServerStatusOptions(), taskNotify.task.options, true);
                                switch (options.typeChangeStatus) {
                                    case TypeChangeStatus.PowerOff:
                                    case TypeChangeStatus.Stop:
                                        {
                                            serverViewModel.serverState = GameServerState.Disable;
                                            break;
                                        }
                                    case TypeChangeStatus.Start:
                                    case TypeChangeStatus.Reload:
                                        {
                                            serverViewModel.serverState = GameServerState.Enable;
                                            break;
                                        }
                                }
                                serverViewModel.password = options.gameServerPassword;
                                serverViewModel.firstPortInRange = options.gameServerPort;
                                break;
                            }
                    }
                }
                else {
                    serverViewModel.serverState = GameServerState.Error;
                }
                var page = this.GetTask(serverViewModel.id);
                page.$promise.then(function () {
                    serverViewModel.tasks = page.items;
                    _this.calculateStateMachine(serverViewModel);
                });
            };
            GameServerController.prototype.calculateStateMachine = function (gameServer) {
                var items = gameServer.tasks.filter(function (task) {
                    return task.statusTask == StatusTask.Pending || task.statusTask == StatusTask.Processing || task.statusTask == StatusTask.Queued;
                });
                if (items.length > 0) {
                    switch (items[items.length - 1].typeTask) {
                        case TypeTask.ChangeStatus:
                            {
                                gameServer.serverState = GameServerState.UpdatingStatus;
                                break;
                            }
                        case TypeTask.UpdateVm:
                            {
                                gameServer.serverState = GameServerState.UpdatingConf;
                                break;
                            }
                    }
                }
            };
            GameServerController.prototype.updateGameServerStatus = function (gameServer, status) {
                gameServer.serverState = status;
            };
            GameServerController.$inject = ["WebServices", "monitorTask"];
            return GameServerController;
        }());
        game.GameServerController = GameServerController;
        angular.module('user.game', []);
        angular.module('user.game')
            .controller('AccountGameController', GameServerController);
    })(game = user.game || (user.game = {}));
})(user || (user = {}));
