var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var user;
(function (user) {
    var game;
    (function (game) {
        var BaseGameComponentController = user.game.BaseGameComponentController;
        var GameConsole = (function (_super) {
            __extends(GameConsole, _super);
            function GameConsole($scope, GameConsole, $timeout) {
                _super.call(this);
                this.allCommands = [];
                this.command = '';
                var self = this;
                this.gameConsole = new GameConsole({
                    callback: function (answer) {
                        $timeout(function () {
                            var command = answer + " : " + Math.random().toString(36).substring(7);
                            self.allCommands.push(command);
                            $scope.$broadcast('rebuild:me');
                        }, 1000);
                    }
                });
            }
            GameConsole.prototype.sendCommand = function () {
                this.gameConsole.send(this.command);
                this.command = '';
            };
            GameConsole.prototype.clearConsole = function () {
                this.allCommands = [];
            };
            GameConsole.$inject = ["$scope", "gameConsole", "$timeout"];
            return GameConsole;
        }(BaseGameComponentController));
        game.GameConsole = GameConsole;
        var option = {
            restrict: "EA",
            bindings: {
                gameServer: '<'
            },
            templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameConsole/gameConsole.html',
            controller: GameConsole,
            controllerAs: 'vm'
        };
        angular.module("user.game")
            .component("gameConsole", option);
    })(game = user.game || (user.game = {}));
})(user || (user = {}));
