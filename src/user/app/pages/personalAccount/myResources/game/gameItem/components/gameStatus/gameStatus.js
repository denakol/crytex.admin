var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var user;
(function (user) {
    var game;
    (function (game) {
        var BaseGameComponentController = user.game.BaseGameComponentController;
        var TypeChangeStatus = core.WebApi.Models.Enums.TypeChangeStatus;
        var GameServerState = core.WebApi.Models.Enums.GameServerState;
        var GameStatus = (function (_super) {
            __extends(GameStatus, _super);
            function GameStatus(WebServices) {
                _super.call(this);
                this.WebServices = WebServices;
                this.GameServerStates = GameServerState;
                this.TypeChangeStatuses = TypeChangeStatus;
                this.UserGameServerService = WebServices.UserGameServerService;
            }
            GameStatus.prototype.setStatus = function (newStatus) {
                var _this = this;
                var param = {
                    changeStatusType: newStatus,
                    serverId: this.gameServer.id
                };
                this.UserGameServerService.updateServerStatus(param).$promise.then(function () {
                    _this.updateGameServerStatus({ status: GameServerState.UpdatingStatus });
                });
            };
            GameStatus.$inject = ["WebServices"];
            return GameStatus;
        }(BaseGameComponentController));
        game.GameStatus = GameStatus;
        var option = {
            restrict: "EA",
            bindings: {
                gameServer: '<',
                updateGameServerStatus: '&'
            },
            templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameStatus/gameStatus.html',
            controller: GameStatus,
            controllerAs: 'vm'
        };
        angular.module("user.game")
            .component("gameStatus", option);
    })(game = user.game || (user.game = {}));
})(user || (user = {}));
