module user.game {

    import BaseGameComponentController = user.game.BaseGameComponentController;
    import PaymentGameServerViewModel = core.WebApi.Models.PaymentGameServerViewModel;
    import IResourceClass = angular.resource.IResourceClass;
    import PageModel = core.PageModel;
    import PaginationSetting = core.PaginationSetting;
    import SearchPaymentGameServerParams = core.WebApi.Models.SearchPaymentGameServerParams;


    export class GameFinance extends BaseGameComponentController {

        static $inject = ["WebServices", "$messageDialog", "$scope"];

        public gamePaymentList: PageModel<PaymentGameServerViewModel>;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        }
        public changePage: boolean = false;
        public currentDate: Date = new Date();
        public myPaginationId: string = 'vmGamePayment' + this.gameServer.id;

        public isUseFilter: boolean = false;
        public searchParams: ISearchParams;

        private paymentGameServerService: IResourceClass<PageModel<PaymentGameServerViewModel>>;

        constructor(private webService: any,  private messageDialog: any,
                    private $scope: ng.IScope) {
            super();

            this.paymentGameServerService = webService.UserPaymentGameServerService;

            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;

            this.getGamePayments();

            this.$scope.$watchCollection('vm.searchParams', () => {
                if (this.isUseFilter) {
                    this.getGamePayments();
                }
            });
        }

        public pageChangeHandler (index: number) {
            this.paginationSettings.pageNumber = index;
            this.changePage = true;
            this.getGamePayments();
        }

        public useFilter() {
            this.isUseFilter = !this.isUseFilter;
            this.getGamePayments();
        }

        private getGamePayments() {
            this.gamePaymentList = this.paymentGameServerService.getAllPage(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    serverId: this.gameServer.id,
                    dateFrom: this.isUseFilter && this.searchParams ? this.searchParams.dateFrom : null,
                    dateTo: this.isUseFilter && this.searchParams ? this.searchParams.dateTo : null
                });

            this.gamePaymentList.$promise
                .catch ( (response:any) => {
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }

    }

    var option:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            gameServer: '<'
        },
        templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameFinance/gameFinance.html',
        controller: GameFinance,
        controllerAs: 'vm'
    };

    angular.module("user.game")
        .component("gameFinance", option);
}