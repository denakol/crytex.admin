module user.game {

    import BaseGameComponentController = user.game.BaseGameComponentController;
    
    import UpdateType = core.WebApi.Models.Enums.GameServerUpdateType;

    export class GameConfig extends BaseGameComponentController {

        public static $inject = ["WebServices", "$messageDialog", "$scope"];

        updateGameServerSlot(slotCount:number):any{}

        public updating:boolean = false;
        public slotCount:number = this.gameServer.slotCount;
        public oldPrice:number;
        public newPrice:number;

        constructor(private WebServices:any,
                    private $messageDialog:any,
                    private $scope:any){

            super();

            this.oldPrice = this.newPrice = this.calculateTotalPrice();
            $scope.$watch(()=> this.slotCount, ()=>{
                this.newPrice = this.calculateTotalPrice();
            });
        }

        public modify(){
            this.WebServices.UserGameServerService.extendGameServer({
                serverId: this.gameServer.id,
                serverName: this.gameServer.name,
                slotCount: this.slotCount,
                updateType: UpdateType.UpdateSlotCount
            }).$promise
                .then((data:any)=>{
                    this.updateGameServerSlot({
                        gameServer: this.gameServer,
                        slotCount: this.slotCount
                    });
                    this.endUpdate();
                })
                .catch((response:any)=>{
                    var message = response.data.errorMessage ? response.data.errorMessage : "Ошибка сервера";
                    this.$messageDialog.show(
                        {
                            header: "Сервер",
                            body: message
                        }
                    );
                });
        }

        public startUpdate() {
            this.updating = true;
        }

        public endUpdate() {
            this.updating = false;
        }

        private calculateTotalPrice() {
            return this.gameServer.gameServerTariff.slot * this.gameServer.slotCount *
                    this.gameServer.expireMonthCount * 0.01 * 100;
        }
    }

    var option:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            gameServer: '<',
            updateGameServerSlot:'&'
        },
        templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameConfig/gameConfig.html',
        controller: GameConfig,
        controllerAs: 'vm'
    };

    angular.module("user.game")
        .component("gameConfig", option);
}