module user.game{

    import BaseGameComponentController = user.game.BaseGameComponentController;
    import IResourceClass = angular.resource.IResourceClass;
    import TaskV2ViewModel = core.WebApi.Models.TaskV2ViewModel;
    import PageModel = core.PageModel;
    import PaginationSetting = core.PaginationSetting;
    import TypeDate = core.WebApi.Models.Enums.TypeDate;

    export class GameTask extends BaseGameComponentController {

        static $inject = ["WebServices", "$messageDialog", "TypeTask", "StatusTask", "$scope"];

        public gameTaskList: PageModel<TaskV2ViewModel>;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        };

        public changePage: boolean = false;
        public currentDate: Date = new Date();
        public myPaginationId: string = 'gameTask' + this.gameServer.id;

        public isUseFilter: boolean = false;
        public searchParams: ISearchParams;

        private taskV2Service: IResourceClass<PageModel<TaskV2ViewModel>>;

        constructor(private webService: any,  private messageDialog: any,
                    public typeTask: any, private statusTask: any,
                    private $scope: ng.IScope) {
            super();

            this.taskV2Service = webService.UserTaskV2Service;
            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;
            this.getTaskV2List();

            this.$scope.$watchCollection('vm.searchParams', () => {
                if (this.isUseFilter) {
                    this.getTaskV2List();
                }
            });
        }

        public pageChangeHandler (index: number) {
            this.paginationSettings.pageNumber = index;
            this.changePage = true;
            this.getTaskV2List();
        }

        public useFilter() {
            this.isUseFilter = !this.isUseFilter;
            this.getTaskV2List();
        }

        private getTaskV2List() {
            this.gameTaskList = this.taskV2Service.getAllPage(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    resourceId: this.gameServer.id,
                    dateFrom: this.isUseFilter && this.searchParams ? this.searchParams.dateFrom : null,
                    dateTo: this.isUseFilter && this.searchParams ? this.searchParams.dateTo : null,
                    typeDate: this.isUseFilter ? TypeDate.CompletedAt : null
                });

            this.gameTaskList.$promise
                .catch ( (response:any) => {
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }


    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            gameServer: '<'
        },
        templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameTask/gameTask.html',
        controller: GameTask,
        controllerAs: 'vm'
    };

    angular.module("user.game")
        .component("gameTask",option);
}