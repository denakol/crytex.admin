/**
 * Created by denak on 23.03.2016.
 */
var user;
(function (user) {
    var game;
    (function (game) {
        var BaseGameComponentController = (function () {
            function BaseGameComponentController() {
            }
            return BaseGameComponentController;
        }());
        game.BaseGameComponentController = BaseGameComponentController;
    })(game = user.game || (user.game = {}));
})(user || (user = {}));
