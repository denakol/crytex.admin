var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var user;
(function (user) {
    var game;
    (function (game) {
        var BaseGameComponentController = user.game.BaseGameComponentController;
        var GameFinance = (function (_super) {
            __extends(GameFinance, _super);
            function GameFinance(webService, messageDialog, $scope) {
                var _this = this;
                _super.call(this);
                this.webService = webService;
                this.messageDialog = messageDialog;
                this.$scope = $scope;
                this.paginationSettings = {
                    pageNumber: 1,
                    pageSize: 5
                };
                this.changePage = false;
                this.currentDate = new Date();
                this.myPaginationId = 'vmGamePayment' + this.gameServer.id;
                this.isUseFilter = false;
                this.paymentGameServerService = webService.UserPaymentGameServerService;
                this.paginationSettings.pageNumber = 1;
                this.paginationSettings.pageSize = 5;
                this.getGamePayments();
                this.$scope.$watchCollection('vm.searchParams', function () {
                    if (_this.isUseFilter) {
                        _this.getGamePayments();
                    }
                });
            }
            GameFinance.prototype.pageChangeHandler = function (index) {
                this.paginationSettings.pageNumber = index;
                this.changePage = true;
                this.getGamePayments();
            };
            GameFinance.prototype.useFilter = function () {
                this.isUseFilter = !this.isUseFilter;
                this.getGamePayments();
            };
            GameFinance.prototype.getGamePayments = function () {
                var _this = this;
                this.gamePaymentList = this.paymentGameServerService.getAllPage({
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    serverId: this.gameServer.id,
                    dateFrom: this.isUseFilter && this.searchParams ? this.searchParams.dateFrom : null,
                    dateTo: this.isUseFilter && this.searchParams ? this.searchParams.dateTo : null
                });
                this.gamePaymentList.$promise
                    .catch(function (response) {
                    _this.messageDialog.show({
                        header: "Сервер",
                        body: "Ошибка сервера"
                    });
                    return;
                });
            };
            GameFinance.$inject = ["WebServices", "$messageDialog", "$scope"];
            return GameFinance;
        }(BaseGameComponentController));
        game.GameFinance = GameFinance;
        var option = {
            restrict: "EA",
            bindings: {
                gameServer: '<'
            },
            templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameFinance/gameFinance.html',
            controller: GameFinance,
            controllerAs: 'vm'
        };
        angular.module("user.game")
            .component("gameFinance", option);
    })(game = user.game || (user.game = {}));
})(user || (user = {}));
