var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var user;
(function (user) {
    var game;
    (function (game) {
        var BaseGameComponentController = user.game.BaseGameComponentController;
        var GameShort = (function (_super) {
            __extends(GameShort, _super);
            function GameShort() {
                _super.apply(this, arguments);
            }
            return GameShort;
        }(BaseGameComponentController));
        game.GameShort = GameShort;
        var option = {
            restrict: "EA",
            bindings: {
                gameServer: '<',
                control: '&'
            },
            templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameShort/gameShort.html',
            controller: GameShort,
            controllerAs: 'vm'
        };
        angular.module("user.game")
            .component("gameShort", option);
    })(game = user.game || (user.game = {}));
})(user || (user = {}));
