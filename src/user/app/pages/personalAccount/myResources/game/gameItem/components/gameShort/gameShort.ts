
module user.game{

    import BaseGameComponentController = user.game.BaseGameComponentController;


    export class GameShort extends BaseGameComponentController {
      



    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            gameServer: '<',
            control:'&'
        },
        templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameShort/gameShort.html',
        controller: GameShort,
        controllerAs: 'vm'
    };

    angular.module("user.game")
        .component("gameShort",option);
}