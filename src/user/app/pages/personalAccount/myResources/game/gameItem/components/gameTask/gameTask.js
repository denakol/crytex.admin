var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var user;
(function (user) {
    var game;
    (function (game) {
        var BaseGameComponentController = user.game.BaseGameComponentController;
        var TypeDate = core.WebApi.Models.Enums.TypeDate;
        var GameTask = (function (_super) {
            __extends(GameTask, _super);
            function GameTask(webService, messageDialog, typeTask, statusTask, $scope) {
                var _this = this;
                _super.call(this);
                this.webService = webService;
                this.messageDialog = messageDialog;
                this.typeTask = typeTask;
                this.statusTask = statusTask;
                this.$scope = $scope;
                this.paginationSettings = {
                    pageNumber: 1,
                    pageSize: 5
                };
                this.changePage = false;
                this.currentDate = new Date();
                this.myPaginationId = 'gameTask' + this.gameServer.id;
                this.isUseFilter = false;
                this.taskV2Service = webService.UserTaskV2Service;
                this.paginationSettings.pageNumber = 1;
                this.paginationSettings.pageSize = 5;
                this.getTaskV2List();
                this.$scope.$watchCollection('vm.searchParams', function () {
                    if (_this.isUseFilter) {
                        _this.getTaskV2List();
                    }
                });
            }
            GameTask.prototype.pageChangeHandler = function (index) {
                this.paginationSettings.pageNumber = index;
                this.changePage = true;
                this.getTaskV2List();
            };
            GameTask.prototype.useFilter = function () {
                this.isUseFilter = !this.isUseFilter;
                this.getTaskV2List();
            };
            GameTask.prototype.getTaskV2List = function () {
                var _this = this;
                this.gameTaskList = this.taskV2Service.getAllPage({
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    resourceId: this.gameServer.id,
                    dateFrom: this.isUseFilter && this.searchParams ? this.searchParams.dateFrom : null,
                    dateTo: this.isUseFilter && this.searchParams ? this.searchParams.dateTo : null,
                    typeDate: this.isUseFilter ? TypeDate.CompletedAt : null
                });
                this.gameTaskList.$promise
                    .catch(function (response) {
                    _this.messageDialog.show({
                        header: "Сервер",
                        body: "Ошибка сервера"
                    });
                    return;
                });
            };
            GameTask.$inject = ["WebServices", "$messageDialog", "TypeTask", "StatusTask", "$scope"];
            return GameTask;
        }(BaseGameComponentController));
        game.GameTask = GameTask;
        var option = {
            restrict: "EA",
            bindings: {
                gameServer: '<'
            },
            templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameTask/gameTask.html',
            controller: GameTask,
            controllerAs: 'vm'
        };
        angular.module("user.game")
            .component("gameTask", option);
    })(game = user.game || (user.game = {}));
})(user || (user = {}));
