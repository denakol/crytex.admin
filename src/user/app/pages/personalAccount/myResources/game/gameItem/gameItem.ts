/**
 * Created by denak on 14.03.2016.
 */
module user.game{


export class GameServer {
    public select:string = 'status';
    chooseMenu(select:string) {
        this.select = select;
    };

    choosed(select:string):boolean {
        return select ==this.select;
    };



    constructor() {

    }



}

var option:ng.IComponentOptions = {
    restrict: "EA",
    bindings: {
        gameServer: '<',
        updateGameServerStatus:'&',
        updateGameServerSlot: '&',
        updateGameServerConf: '&'
    },
    templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/gameItem.html',
    controller: GameServer,
    controllerAs: 'vm'
};

angular.module("user.game")
    .component("gameItem", option);
}