module user.game {

    import BaseGameComponentController = user.game.BaseGameComponentController;
    import TypeChangeStatus = core.WebApi.Models.Enums.TypeChangeStatus;
    import GameServerState = core.WebApi.Models.Enums.GameServerState;
    import IWebServices = core.IWebServices;
  
    import GameServerChangeStatusViewModel = core.WebApi.Models.GameServerChangeStatusViewModel;
    import IUserGameServerService = core.IUserGameServerService;


    export class GameStatus extends BaseGameComponentController {


        public static $inject = ["WebServices"];
        public GameServerStates = GameServerState;

        public TypeChangeStatuses = TypeChangeStatus;

        private UserGameServerService:IUserGameServerService;


        constructor(private WebServices:IWebServices) {
            super();
            this.UserGameServerService = WebServices.UserGameServerService;
        }

        public setStatus(newStatus:TypeChangeStatus) {

            var param:GameServerChangeStatusViewModel = {
                changeStatusType: newStatus,
                serverId: this.gameServer.id

            };

            this.UserGameServerService.updateServerStatus(param).$promise.then(
                ()=> {

                    this.updateGameServerStatus({status: GameServerState.UpdatingStatus});
                }
            )
        }


    }

    var option:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            gameServer: '<',
            updateGameServerStatus: '&'
        },
        templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameStatus/gameStatus.html',
        controller: GameStatus,
        controllerAs: 'vm'
    };

    angular.module("user.game")
        .component("gameStatus", option);
}