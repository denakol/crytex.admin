module user.game {
    import BaseGameComponentController = user.game.BaseGameComponentController;
    import IResourceClass= angular.resource.IResourceClass;
    import SubscriptionUpdateOptions = core.WebApi.Models.SubscriptionUpdateOptions;
    import UserSubscriptionGameServerService = core.IAdminGameServerService;
    import UpdateType = core.WebApi.Models.Enums.GameServerUpdateType;

    //IAdminGameServerService
    export class GameSetting extends BaseGameComponentController {
        static $inject = ["WebServices", "$messageDialog", "$scope"];

        updateGameServerConf(conf:any):any{}
        
        public subscriptionGameServerService: UserSubscriptionGameServerService;

        public isEnableManageButtons: boolean = false;

        public subscriptionUpdateGameServerOption: SubscriptionUpdateOptions = {
            id: this.gameServer.id,
            name: this.gameServer.name,
            autoProlongation: this.gameServer.autoProlongation
        };

        constructor(private WebServices:any,
                    private $messageDialog:any,
                    private $scope:ng.IScope){
            super();
            this.subscriptionGameServerService = WebServices.UserGameServerService;

            $scope.$watchCollection(() => { return this.subscriptionUpdateGameServerOption; },
                (newValue: SubscriptionUpdateOptions) => {
                    if (newValue.autoProlongation !== this.gameServer.autoProlongation ||
                        newValue.name !== this.gameServer.name) {
                        this.isEnableManageButtons = true;
                    } else {
                        this.isEnableManageButtons = false;
                    }
                });
        }

        public setNewOptions() {
            this.subscriptionGameServerService.extendGameServer({
                serverId: this.subscriptionUpdateGameServerOption.id,
                serverName: this.subscriptionUpdateGameServerOption.name,
                autoProlongation: this.subscriptionUpdateGameServerOption.autoProlongation,
                updateType: UpdateType.UpdateSettings
            }).$promise
                .then( (data:any) => {
                    this.isEnableManageButtons = false;
                    this.updateGameServerConf({
                        gameServer: this.gameServer,
                        conf:{
                            name: this.subscriptionUpdateGameServerOption.name,
                            autoProlongation: this.subscriptionUpdateGameServerOption.autoProlongation
                        }
                    });
                    return;
                })
                .catch ( (response:any) => {
                    this.$messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }

        public returnValue() {
            this.subscriptionUpdateGameServerOption.name = this.gameServer.name;
            this.subscriptionUpdateGameServerOption.autoProlongation = this.gameServer.autoProlongation;
        }
    }

    var option:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            gameServer: '<',
            updateGameServerConf: '&'
        },
        templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameSetting/gameSetting.html',
        controller: GameSetting,
        controllerAs: 'vm'
    };

    angular.module("user.game")
        .component("gameSetting", option);
}