var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var user;
(function (user) {
    var game;
    (function (game) {
        var BaseGameComponentController = user.game.BaseGameComponentController;
        var UpdateType = core.WebApi.Models.Enums.GameServerUpdateType;
        //IAdminGameServerService
        var GameSetting = (function (_super) {
            __extends(GameSetting, _super);
            function GameSetting(WebServices, $messageDialog, $scope) {
                var _this = this;
                _super.call(this);
                this.WebServices = WebServices;
                this.$messageDialog = $messageDialog;
                this.$scope = $scope;
                this.isEnableManageButtons = false;
                this.subscriptionUpdateGameServerOption = {
                    id: this.gameServer.id,
                    name: this.gameServer.name,
                    autoProlongation: this.gameServer.autoProlongation
                };
                this.subscriptionGameServerService = WebServices.UserGameServerService;
                $scope.$watchCollection(function () { return _this.subscriptionUpdateGameServerOption; }, function (newValue) {
                    if (newValue.autoProlongation !== _this.gameServer.autoProlongation ||
                        newValue.name !== _this.gameServer.name) {
                        _this.isEnableManageButtons = true;
                    }
                    else {
                        _this.isEnableManageButtons = false;
                    }
                });
            }
            GameSetting.prototype.updateGameServerConf = function (conf) { };
            GameSetting.prototype.setNewOptions = function () {
                var _this = this;
                this.subscriptionGameServerService.extendGameServer({
                    serverId: this.subscriptionUpdateGameServerOption.id,
                    serverName: this.subscriptionUpdateGameServerOption.name,
                    autoProlongation: this.subscriptionUpdateGameServerOption.autoProlongation,
                    updateType: UpdateType.UpdateSettings
                }).$promise
                    .then(function (data) {
                    _this.isEnableManageButtons = false;
                    _this.updateGameServerConf({
                        gameServer: _this.gameServer,
                        conf: {
                            name: _this.subscriptionUpdateGameServerOption.name,
                            autoProlongation: _this.subscriptionUpdateGameServerOption.autoProlongation
                        }
                    });
                    return;
                })
                    .catch(function (response) {
                    _this.$messageDialog.show({
                        header: "Сервер",
                        body: "Ошибка сервера"
                    });
                    return;
                });
            };
            GameSetting.prototype.returnValue = function () {
                this.subscriptionUpdateGameServerOption.name = this.gameServer.name;
                this.subscriptionUpdateGameServerOption.autoProlongation = this.gameServer.autoProlongation;
            };
            GameSetting.$inject = ["WebServices", "$messageDialog", "$scope"];
            return GameSetting;
        }(BaseGameComponentController));
        game.GameSetting = GameSetting;
        var option = {
            restrict: "EA",
            bindings: {
                gameServer: '<',
                updateGameServerConf: '&'
            },
            templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameSetting/gameSetting.html',
            controller: GameSetting,
            controllerAs: 'vm'
        };
        angular.module("user.game")
            .component("gameSetting", option);
    })(game = user.game || (user.game = {}));
})(user || (user = {}));
