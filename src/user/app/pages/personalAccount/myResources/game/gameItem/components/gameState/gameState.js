var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var user;
(function (user) {
    var game;
    (function (game) {
        var BaseGameComponentController = user.game.BaseGameComponentController;
        var GameServerState = core.WebApi.Models.Enums.GameServerState;
        var GameState = (function (_super) {
            __extends(GameState, _super);
            function GameState() {
                _super.apply(this, arguments);
                this.GameServerStates = GameServerState;
            }
            return GameState;
        }(BaseGameComponentController));
        game.GameState = GameState;
        var option = {
            restrict: "EA",
            bindings: {
                state: '<'
            },
            templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameState/gameState.html',
            controller: GameState,
            controllerAs: 'vm'
        };
        angular.module("user.game")
            .component("gameState", option);
    })(game = user.game || (user.game = {}));
})(user || (user = {}));
