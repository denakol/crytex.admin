
module user.game{

    import BaseGameComponentController = user.game.BaseGameComponentController;
    import GameServerState = core.WebApi.Models.Enums.GameServerState;


    export class GameState extends BaseGameComponentController {

        public GameServerStates = GameServerState;


        
    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            state: '<'
        },
        templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameState/gameState.html',
        controller: GameState,
        controllerAs: 'vm'
    };

    angular.module("user.game")
        .component("gameState",option);
}