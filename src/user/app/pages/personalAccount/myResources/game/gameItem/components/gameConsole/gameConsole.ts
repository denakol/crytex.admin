module user.game {

    import BaseGameComponentController = user.game.BaseGameComponentController;


    export class GameConsole extends BaseGameComponentController {
        public static $inject = ["$scope", "gameConsole", "$timeout"];
        public allCommands:any = [];
        public command:string = '';
        public gameConsole:any;

        constructor($scope:any, GameConsole:any, $timeout:any){
            super();
            var self:any = this;
            this.gameConsole = new GameConsole({
                callback: (answer:any) => {
                    $timeout(()=>{
                        var command = answer + " : " + Math.random().toString(36).substring(7);
                        self.allCommands.push(command);
                        $scope.$broadcast('rebuild:me');
                    }, 1000)

                }
            });
        }

        public sendCommand(){

            this.gameConsole.send(this.command);
            this.command = '';
        }

        public clearConsole(){
            this.allCommands = [];
        }


    }

    var option:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            gameServer: '<'

        },
        templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameConsole/gameConsole.html',
        controller: GameConsole,
        controllerAs: 'vm'
    };

    angular.module("user.game")
        .component("gameConsole", option);
}