var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var user;
(function (user) {
    var game;
    (function (game) {
        var BaseGameComponentController = user.game.BaseGameComponentController;
        var UpdateType = core.WebApi.Models.Enums.GameServerUpdateType;
        var GameConfig = (function (_super) {
            __extends(GameConfig, _super);
            function GameConfig(WebServices, $messageDialog, $scope) {
                var _this = this;
                _super.call(this);
                this.WebServices = WebServices;
                this.$messageDialog = $messageDialog;
                this.$scope = $scope;
                this.updating = false;
                this.slotCount = this.gameServer.slotCount;
                this.oldPrice = this.newPrice = this.calculateTotalPrice();
                $scope.$watch(function () { return _this.slotCount; }, function () {
                    _this.newPrice = _this.calculateTotalPrice();
                });
            }
            GameConfig.prototype.updateGameServerSlot = function (slotCount) { };
            GameConfig.prototype.modify = function () {
                var _this = this;
                this.WebServices.UserGameServerService.extendGameServer({
                    serverId: this.gameServer.id,
                    serverName: this.gameServer.name,
                    slotCount: this.slotCount,
                    updateType: UpdateType.UpdateSlotCount
                }).$promise
                    .then(function (data) {
                    _this.updateGameServerSlot({
                        gameServer: _this.gameServer,
                        slotCount: _this.slotCount
                    });
                    _this.endUpdate();
                })
                    .catch(function (response) {
                    var message = response.data.errorMessage ? response.data.errorMessage : "Ошибка сервера";
                    _this.$messageDialog.show({
                        header: "Сервер",
                        body: message
                    });
                });
            };
            GameConfig.prototype.startUpdate = function () {
                this.updating = true;
            };
            GameConfig.prototype.endUpdate = function () {
                this.updating = false;
            };
            GameConfig.prototype.calculateTotalPrice = function () {
                return this.gameServer.gameServerTariff.slot * this.gameServer.slotCount *
                    this.gameServer.expireMonthCount * 0.01 * 100;
            };
            GameConfig.$inject = ["WebServices", "$messageDialog", "$scope"];
            return GameConfig;
        }(BaseGameComponentController));
        game.GameConfig = GameConfig;
        var option = {
            restrict: "EA",
            bindings: {
                gameServer: '<',
                updateGameServerSlot: '&'
            },
            templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/components/gameConfig/gameConfig.html',
            controller: GameConfig,
            controllerAs: 'vm'
        };
        angular.module("user.game")
            .component("gameConfig", option);
    })(game = user.game || (user.game = {}));
})(user || (user = {}));
