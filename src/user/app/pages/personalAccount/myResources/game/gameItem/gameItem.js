/**
 * Created by denak on 14.03.2016.
 */
var user;
(function (user) {
    var game;
    (function (game) {
        var GameServer = (function () {
            function GameServer() {
                this.select = 'status';
            }
            GameServer.prototype.chooseMenu = function (select) {
                this.select = select;
            };
            ;
            GameServer.prototype.choosed = function (select) {
                return select == this.select;
            };
            ;
            return GameServer;
        }());
        game.GameServer = GameServer;
        var option = {
            restrict: "EA",
            bindings: {
                gameServer: '<',
                updateGameServerStatus: '&',
                updateGameServerSlot: '&',
                updateGameServerConf: '&'
            },
            templateUrl: 'app/pages/personalAccount/myResources/game/gameItem/gameItem.html',
            controller: GameServer,
            controllerAs: 'vm'
        };
        angular.module("user.game")
            .component("gameItem", option);
    })(game = user.game || (user.game = {}));
})(user || (user = {}));
