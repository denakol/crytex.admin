/**
 * Created by denak on 04.03.2016.
 */
    module user.game {

        import GameServerViewModel = core.WebApi.Models.GameServerViewModel;
        import PageModel = core.PageModel;
        import IWebServices = core.IWebServices;
        import TaskV2ViewModel = core.WebApi.Models.TaskV2ViewModel;
        import TaskV2SearchParamsViewModel = core.WebApi.Models.TaskV2SearchParamsViewModel;
        import StatusTask = core.WebApi.Models.Enums.StatusTask;
        import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;
        import IResourceClass = angular.resource.IResourceClass;
        import TaskEndNotify = core.WebApi.Models.TaskEndNotify;
        import TypeTask = core.WebApi.Models.Enums.TypeTask;
        import GameServerState = core.WebApi.Models.Enums.GameServerState;
        import StatusVM = core.WebApi.Models.Enums.StatusVM;
        import ChangeGameServerStatusOptions = core.WebApi.Models.ChangeGameServerStatusOptions;
        import SerializationHelper = core.SerializationHelper;
        import TypeChangeStatus = core.WebApi.Models.Enums.TypeChangeStatus;
        import PaginationSetting = core.PaginationSetting;

        export class GameServerController {

            public static $inject = [ "WebServices","monitorTask"];

            public gameServers:PageModel<GameServerViewModel>;
            private UserTaskV2Service:IResourceClass<TaskV2ViewModel>;

            public paginationSettings: PaginationSetting = {
                pageNumber: 1,
                pageSize: 5
            };


            constructor( private WebServices:IWebServices, monitorTask:any) {

                this.getGameServers();

                this.UserTaskV2Service = WebServices.UserTaskV2Service;

                monitorTask = new monitorTask({
                    callback: (task:TaskEndNotify)=> {
                        this.gameServers.$promise.then(()=> {
                            var vm = this.gameServers.items.find((item:GameServerViewModel)=> {
                                return item.id == task.task.resourceId;
                            });


                            if (vm != null) {
                                this.calculateStateMachineFromTask(vm, task);
                            }
                        })
                    }
                });


            }

            public getGameServers(){
                this.gameServers = this.WebServices.AdminGameServerService.pagingQuery({
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize
                });

                this.gameServers.$promise.then(()=> {
                    angular.forEach(this.gameServers.items, (item:GameServerViewModel)=> {
                        item.tasks = [];
                        var page = this.GetTask(item.id);
                        page.$promise.then(()=> {
                            item.tasks = page.items;
                            this.calculateStateMachine(item);
                        });

                    })
                })
            }

            public pageChangeHandler(newPageNumber:number){
                this.paginationSettings.pageNumber = newPageNumber;
                this.getGameServers();
            }


            protected GetTask(id:string):PageModel<TaskV2ViewModel> {
                var params:TaskV2SearchParamsViewModel = {
                    resourceId: id,
                    pageNumber: 1,
                    pageSize: 100,
                    statusTasks: [StatusTask.Pending, StatusTask.Processing, StatusTask.Queued]
                };

                var page = this.UserTaskV2Service.pagingQuery(params);
                return page;

            };


            updateMachineStatus(serverViewModel:GameServerViewModel, status:GameServerState):any {
                serverViewModel.serverState = status;
            }
            
            updateGameServerSlot(gameServer:GameServerViewModel, slotCount:number):any {
                gameServer.slotCount = slotCount;
            }

            updateGameServerConf(gameServer:GameServerViewModel, conf:any):any{
                gameServer.name = conf.name;
                gameServer.autoProlongation = conf.autoProlongation;
            }

            calculateStateMachineFromTask(serverViewModel:GameServerViewModel, taskNotify:TaskEndNotify) {

                if (taskNotify.success) {
                    switch (taskNotify.task.typeTask) {
                        case TypeTask.CreateGameServer:
                        {
                            serverViewModel.serverState = GameServerState.Enable;

                            break;
                        }
                        case TypeTask.GameServerChangeStatus:
                        {

                            var options = SerializationHelper.toInstance<ChangeGameServerStatusOptions>(new ChangeGameServerStatusOptions(), taskNotify.task.options, true);
                            switch (options.typeChangeStatus) {
                                case TypeChangeStatus.PowerOff:
                                case TypeChangeStatus.Stop:
                                {
                                    serverViewModel.serverState = GameServerState.Disable;
                                    break;
                                }
                                case TypeChangeStatus.Start:
                                case TypeChangeStatus.Reload:
                                {
                                    serverViewModel.serverState = GameServerState.Enable;
                                    break;
                                }
                            }
                            serverViewModel.password = options.gameServerPassword;

                            serverViewModel.firstPortInRange = options.gameServerPort;
                            break;
                        }

                    }
                }
                else {
                    serverViewModel.serverState = GameServerState.Error;
                }
                var page = this.GetTask(serverViewModel.id);
                page.$promise.then(()=> {
                    serverViewModel.tasks = page.items;
                    this.calculateStateMachine(serverViewModel);
                })


            }


            calculateStateMachine(gameServer:GameServerViewModel) {

                var items = gameServer.tasks.filter((task:TaskV2ViewModel)=> {
                    return task.statusTask == StatusTask.Pending || task.statusTask == StatusTask.Processing || task.statusTask == StatusTask.Queued;
                });
                if (items.length > 0) {
                    switch (items[items.length - 1].typeTask) {
                        case TypeTask.ChangeStatus:
                        {
                            gameServer.serverState = GameServerState.UpdatingStatus;
                            break;
                        }
                        case TypeTask.UpdateVm:
                        {
                            gameServer.serverState = GameServerState.UpdatingConf;
                            break;
                        }
                    }

                }

            }


            public updateGameServerStatus(gameServer:GameServerViewModel,status:GameServerState)
            {
                gameServer.serverState = status;
            }

        }


        angular.module('user.game', []);
        angular.module('user.game')
            .controller('AccountGameController', GameServerController);
    }