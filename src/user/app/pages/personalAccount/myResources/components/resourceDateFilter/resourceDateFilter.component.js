var ResourceDateFilterController = user.ResourceDateFilterController;
var user;
(function (user) {
    var ResourceDateFilterController = (function () {
        function ResourceDateFilterController(countingPeriodType) {
            var _this = this;
            this.countingPeriodType = countingPeriodType;
            this.maxDate = new Date();
            this.periods = [];
            // Пока мы можем только сортировать по дням и месяцам, поэтому убираем лишнее
            angular.forEach(this.countingPeriodType.arrayItems, (function (period) {
                if (period.name == "Day" || period.name == "Month") {
                    _this.periods.push(period);
                }
            }));
            this.periods.push({ translate: "Без группировки", value: null });
            // Если не задано в родительском контроллере, то создаём пустой
            if (!this.searchParams) {
                this.searchParams = {
                    dateFrom: new Date(),
                    dateTo: new Date(),
                    countingPeriodType: this.periods[0].value
                };
                this.setCurrentTime();
            }
        }
        ResourceDateFilterController.prototype.setCurrentTime = function () {
            this.searchParams.dateFrom.setHours(0, 0, 0, 0);
            this.searchParams.dateTo.setHours(23, 59, 59, 999);
        };
        ResourceDateFilterController.$inject = ["CountingPeriodType"];
        return ResourceDateFilterController;
    }());
    user.ResourceDateFilterController = ResourceDateFilterController;
    var option = {
        restrict: "EA",
        bindings: {
            isShowGrouping: '<',
            searchParams: '='
        },
        templateUrl: 'app/pages/personalAccount/myResources/components/resourceDateFilter/resourceDateFilter.html',
        controller: ResourceDateFilterController,
        controllerAs: 'vm'
    };
    angular.module("personalAccount.myResources")
        .component("resourceDateFilter", option)
        .controller("ResourceDateFilterController", ResourceDateFilterController);
})(user || (user = {}));
