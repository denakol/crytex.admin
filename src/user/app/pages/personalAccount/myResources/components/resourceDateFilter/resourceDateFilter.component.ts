import ResourceDateFilterController = user.ResourceDateFilterController;

module user {

    import IConstant = core.IConstant;

    export interface ISearchParams {
        dateFrom: Date;
        dateTo: Date;
        countingPeriodType: number;
    }

    export class ResourceDateFilterController {

        static $inject = ["CountingPeriodType"];

        public searchParams: ISearchParams;
        public isShowGrouping: boolean;
        public maxDate: Date = new Date();

        public periods: Array<IConstant> = [];

        constructor(private countingPeriodType: any) {
            // Пока мы можем только сортировать по дням и месяцам, поэтому убираем лишнее
            angular.forEach(this.countingPeriodType.arrayItems, ( (period: IConstant) => {
                if (period.name == "Day" || period.name == "Month") {
                    this.periods.push(period);
                }
            }));
            this.periods.push(<IConstant>{translate: "Без группировки", value: null})

            // Если не задано в родительском контроллере, то создаём пустой
            if (!this.searchParams) {
                this.searchParams = <ISearchParams>{
                    dateFrom: new Date(),
                    dateTo: new Date(),
                    countingPeriodType: this.periods[0].value
                }
                this.setCurrentTime();
            }
        }

        public setCurrentTime () {
            this.searchParams.dateFrom.setHours(0,0,0,0);
            this.searchParams.dateTo.setHours(23,59,59,999);
        }


    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            isShowGrouping: '<',
            searchParams: '='
        },
        templateUrl: 'app/pages/personalAccount/myResources/components/resourceDateFilter/resourceDateFilter.html',
        controller: ResourceDateFilterController,
        controllerAs: 'vm'
    };

    angular.module("personalAccount.myResources")
        .component("resourceDateFilter", option)
        .controller("ResourceDateFilterController", ResourceDateFilterController);
}
