var user;
(function (user) {
    var MyFinanceController = (function () {
        function MyFinanceController(WebServices) {
            this.WebServices = WebServices;
        }
        MyFinanceController.$inject = ["WebServices"];
        return MyFinanceController;
    }());
    user.MyFinanceController = MyFinanceController;
    angular.module('personalAccount.myFinance')
        .controller('MyFinanceController', MyFinanceController);
})(user || (user = {}));
