module user {

    export class MyFinanceController {

        static $inject = ["WebServices"];

        constructor(private WebServices:any) {

        }

    }

    angular.module('personalAccount.myFinance')
        .controller('MyFinanceController', MyFinanceController);
}