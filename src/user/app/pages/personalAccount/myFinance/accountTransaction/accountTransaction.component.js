var user;
(function (user) {
    var AccountTransactionController = (function () {
        function AccountTransactionController(WebServices, messageDialog, transactionType) {
            this.WebServices = WebServices;
            this.messageDialog = messageDialog;
            this.transactionType = transactionType;
            this.periods = [
                { text: "Сегодня", synonym: "today", isActive: false, index: 0,
                    emptyText: 'Не найдено сегодняшних транзакций', isHaveIcon: false },
                { text: "Вчера", synonym: "yesterday", isActive: true, index: 1,
                    emptyText: 'Не найдено вчерашних транзакций', isHaveIcon: false },
                { text: "Неделя", synonym: "week", isActive: false, index: 2,
                    emptyText: 'Не найдено транзакций за неделю', isHaveIcon: false },
                { text: "Месяц", synonym: "month", isActive: false, index: 3,
                    emptyText: 'Не найдено транзакций за месяц', isHaveIcon: false },
                { text: "Год", synonym: "year", isActive: false, index: 4,
                    emptyText: 'Не найдено транзакций за год', isHaveIcon: false },
                { text: "", synonym: "custom", isActive: false, index: 5,
                    emptyText: 'Не найдено транзакций за указанный период', isHaveIcon: true }
            ];
            this.currentPeriod = this.periods[1];
            this.customPeriod = {
                dateFrom: new Date(),
                dateTo: new Date()
            };
            this.maxDate = new Date();
            this.paginationSettings = {
                pageNumber: 1,
                pageSize: 5
            };
            this.isShow = false;
            this.changePage = false;
            this.searchParams = {
                dateFrom: new Date(),
                dateTo: new Date()
            };
            this.BillingTransactionInfoService = WebServices.UserPayment;
            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;
            this.setSearchParams("yesterday");
            this.getTransactions();
        }
        AccountTransactionController.prototype.choosePeriod = function (index) {
            this.periods[this.currentPeriod.index].isActive = false;
            this.periods[index].isActive = true;
            this.currentPeriod = this.periods[index];
            this.changePage = false;
            this.setSearchParams(this.periods[index].synonym);
            if (this.periods[index].synonym !== "custom") {
                this.getTransactions();
            }
            else {
            }
        };
        AccountTransactionController.prototype.pageChangeHandler = function (index) {
            this.paginationSettings.pageNumber = index;
            this.changePage = true;
            this.getTransactions();
        };
        AccountTransactionController.prototype.changeCustomDate = function () {
            this.searchParams = this.customPeriod;
            this.getTransactions();
        };
        AccountTransactionController.prototype.getTransactions = function () {
            var _this = this;
            this.billingTransactionInfoList = this.BillingTransactionInfoService.billingTransactionInfo({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize,
                from: this.searchParams.dateFrom,
                to: this.searchParams.dateTo
            });
            this.billingTransactionInfoList.$promise
                .catch(function (response) {
                _this.messageDialog.show({
                    header: "Сервер",
                    body: "Ошибка сервера"
                });
                return;
            });
        };
        AccountTransactionController.prototype.setSearchParams = function (synonym) {
            var dateFrom = new Date();
            dateFrom.setHours(0, 0, 0);
            var dateTo = new Date();
            switch (synonym) {
                case "today":
                    // Сегодня
                    break;
                case "yesterda":
                    // Вчера
                    dateFrom.setDate(dateFrom.getDate() - 1);
                    dateTo = new Date(dateFrom.toString());
                    dateTo.setHours(23, 59, 59);
                    break;
                case "week":
                    // Неделя
                    dateFrom.setDate(dateTo.getDate() - 7);
                    break;
                case "month":
                    // Месяц
                    dateFrom.setDate(dateTo.getDate() - 31);
                    break;
                case "year":
                    // Год
                    dateFrom.setDate(dateTo.getDate() - 365);
                    break;
                default: break;
            }
            this.searchParams = {
                dateFrom: dateFrom,
                dateTo: dateTo
            };
        };
        AccountTransactionController.$inject = ["WebServices", "$messageDialog", "BillingTransactionType"];
        return AccountTransactionController;
    }());
    user.AccountTransactionController = AccountTransactionController;
    var accountTransactionComponent = {
        restrict: 'AE',
        templateUrl: 'app/pages/personalAccount/myFinance/accountTransaction/accountTransaction.html',
        controller: "AccountTransactionController",
        controllerAs: "vm"
    };
    angular.module("personalAccount.myFinance")
        .component("accountTransaction", accountTransactionComponent)
        .controller("AccountTransactionController", AccountTransactionController);
})(user || (user = {}));
