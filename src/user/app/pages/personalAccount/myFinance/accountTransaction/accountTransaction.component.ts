module user{

    import IResourceClass = angular.resource.IResourceClass;
    import IResource= angular.resource.IResource;
    import IResourceArray = angular.resource.IResourceArray;
    import PageModel = core.PageModel;
    import BillingViewModel = core.WebApi.Models.BillingViewModel;
    import BillingSearchParamsViewModel = core.WebApi.Models.BillingSearchParamsViewModel;
    import PaginationSetting = core.PaginationSetting;
    import BillingTransactionInfoViewModel = core.WebApi.Models.BillingTransactionInfoViewModel;
    import IUserPayment = core.IUserPayment;

    export interface periodItem {
        text: string;
        synonym: string,
        isActive: boolean;
        index: number;
        emptyText: string;
        isHaveIcon: boolean;
    }

    export interface datePeriod {
        dateFrom : Date,
        dateTo: Date
    }

    export class AccountTransactionController {

        static $inject = ["WebServices", "$messageDialog", "BillingTransactionType"];

        public billingTransactionInfoList:PageModel<BillingTransactionInfoViewModel>;

        public periods: Array<periodItem> = [
            {text: "Сегодня", synonym:"today", isActive: false, index: 0,
                emptyText: 'Не найдено сегодняшних транзакций', isHaveIcon: false},
            {text: "Вчера", synonym:"yesterday", isActive: true, index: 1,
                emptyText: 'Не найдено вчерашних транзакций', isHaveIcon: false},
            {text: "Неделя", synonym:"week", isActive: false, index: 2,
                emptyText: 'Не найдено транзакций за неделю', isHaveIcon: false},
            {text: "Месяц", synonym:"month", isActive: false, index: 3,
                emptyText: 'Не найдено транзакций за месяц', isHaveIcon: false},
            {text: "Год", synonym:"year", isActive: false, index: 4,
                emptyText: 'Не найдено транзакций за год', isHaveIcon: false},
            {text:"", synonym:"custom", isActive: false, index: 5,
                emptyText: 'Не найдено транзакций за указанный период', isHaveIcon: true}
        ]
        public currentPeriod: periodItem = this.periods[1];
        public customPeriod: datePeriod = {
            dateFrom : new Date(),
            dateTo: new Date()
        }
        public maxDate: Date = new Date();

        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        }

        public isShow: boolean = false;
        public changePage: boolean = false;

        private BillingTransactionInfoService: IUserPayment;
        private searchParams: datePeriod = {
            dateFrom : new Date(),
            dateTo: new Date()
        };

        constructor(private WebServices: any, private messageDialog: any, private transactionType: any) {
            this.BillingTransactionInfoService = WebServices.UserPayment;
            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;
            this.setSearchParams("yesterday");
            this.getTransactions();
        }

        public choosePeriod (index: number) {
            this.periods[this.currentPeriod.index].isActive = false;
            this.periods[index].isActive = true;
            this.currentPeriod = this.periods[index];

            this.changePage = false;
            this.setSearchParams(this.periods[index].synonym);
            if (this.periods[index].synonym !== "custom"){
                this.getTransactions();
            } else {
                /*this.billingTransactionInfoList = {
                    items: [],
                    totalRows: 0
                }*/
            }
        }

        public pageChangeHandler (index: number) {
            this.paginationSettings.pageNumber = index;
            this.changePage = true;
            this.getTransactions();
        }

        public changeCustomDate(){
            this.searchParams = this.customPeriod;
            this.getTransactions();
        }

        private getTransactions() {
            this.billingTransactionInfoList = this.BillingTransactionInfoService.billingTransactionInfo(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    from: this.searchParams.dateFrom,
                    to: this.searchParams.dateTo
                });

            this.billingTransactionInfoList.$promise
            .catch ( (response:any) => {
                this.messageDialog.show(
                    {
                        header:"Сервер",
                        body:"Ошибка сервера"
                    }
                );
                return;
            });
        }

        private setSearchParams (synonym: string) {
            var dateFrom = new Date();
            dateFrom.setHours(0,0,0);
            var dateTo = new Date();

            switch (synonym) {
                case "today":
                    // Сегодня
                    break;
                case "yesterda":
                    // Вчера
                    dateFrom.setDate(dateFrom.getDate() - 1);
                    dateTo = new Date(dateFrom.toString());
                    dateTo.setHours(23,59,59);
                    break;
                case "week":
                    // Неделя
                    dateFrom.setDate(dateTo.getDate() - 7);
                    break;
                case "month":
                    // Месяц
                    dateFrom.setDate(dateTo.getDate() - 31);
                    break;
                case "year":
                    // Год
                    dateFrom.setDate(dateTo.getDate() - 365);
                    break;
                default: break;
            }

            this.searchParams = {
                dateFrom : dateFrom,
                dateTo: dateTo
            }

        }

    }

    var accountTransactionComponent = {
        restrict: 'AE',
        templateUrl: 'app/pages/personalAccount/myFinance/accountTransaction/accountTransaction.html',
        controller: "AccountTransactionController",
        controllerAs: "vm"
    };

    angular.module("personalAccount.myFinance")
        .component("accountTransaction", accountTransactionComponent)
        .controller("AccountTransactionController", AccountTransactionController);
}