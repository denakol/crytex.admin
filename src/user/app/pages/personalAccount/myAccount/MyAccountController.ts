/**
 * Created by denak on 03.02.2016.
 */
module user {


    import IResource = angular.resource.IResource;
    import UserInfo = core.UserInfo;
  
    import IPromise = angular.IPromise;
    import UserPaymentForecast = core.UserPaymentForecastViewModel;
    import IResourceClass = angular.resource.IResourceClass;
    import TypeUser = core.WebApi.Models.Enums.TypeUser;

    export class MyAccountController {

        static $inject = ["WebServices", "$messageDialog"];

        public user: UserInfo;
        public userPaymentForecast: UserPaymentForecast;

        private UserService:IResourceClass<UserInfo>;
        private userType: any;
        private UserPaymentForecastService: IResourceClass <UserPaymentForecast>;

        constructor(private WebServices:any, private messageDialog: any) {
            this.userType = TypeUser;
            this.UserService = WebServices.User;

            this.user= this.UserService.get();

            this.user.$promise.then(()=>{
                var k=465;
                k++;
                var f = this.user;
            })

            this.UserPaymentForecastService = WebServices.UserPaymentForecast;
            this.getCurrentBalance();
        }

        private getCurrentBalance () {
            this.userPaymentForecast = this.UserPaymentForecastService.get();

            this.userPaymentForecast.$promise.then( (data: UserPaymentForecast) => {
                    return data;
                })
                .catch ( (response:any) => {
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }

    }

    angular.module('personalAccount.myAccount')
        .controller('MyAccountController', MyAccountController);
}