/**
 * Created by denak on 03.02.2016.
 */
var user;
(function (user) {
    var TypeUser = core.WebApi.Models.Enums.TypeUser;
    var MyAccountController = (function () {
        function MyAccountController(WebServices, messageDialog) {
            var _this = this;
            this.WebServices = WebServices;
            this.messageDialog = messageDialog;
            this.userType = TypeUser;
            this.UserService = WebServices.User;
            this.user = this.UserService.get();
            this.user.$promise.then(function () {
                var k = 465;
                k++;
                var f = _this.user;
            });
            this.UserPaymentForecastService = WebServices.UserPaymentForecast;
            this.getCurrentBalance();
        }
        MyAccountController.prototype.getCurrentBalance = function () {
            var _this = this;
            this.userPaymentForecast = this.UserPaymentForecastService.get();
            this.userPaymentForecast.$promise.then(function (data) {
                return data;
            })
                .catch(function (response) {
                _this.messageDialog.show({
                    header: "Сервер",
                    body: "Ошибка сервера"
                });
                return;
            });
        };
        MyAccountController.$inject = ["WebServices", "$messageDialog"];
        return MyAccountController;
    }());
    user.MyAccountController = MyAccountController;
    angular.module('personalAccount.myAccount')
        .controller('MyAccountController', MyAccountController);
})(user || (user = {}));
