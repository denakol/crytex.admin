var user;
(function (user) {
    var SingleRequestController = (function () {
        function SingleRequestController(WebServices, messageDialog, AuthService, $scope, $timeout) {
            this.WebServices = WebServices;
            this.messageDialog = messageDialog;
            this.AuthService = AuthService;
            this.$scope = $scope;
            this.$timeout = $timeout;
            this.newComment = {
                id: null,
                userId: '',
                userName: '',
                userEmail: '',
                comment: '',
                creationDate: new Date(),
                isUserComment: false,
                isRead: false
            };
            this.loading = false;
            this.HelpDeskRequestService = WebServices.UserHelpDeskRequest;
            this.HelpDeskRequestCommentService = WebServices.UserHelpDeskRequestComment;
            this.getCommentList();
        }
        SingleRequestController.prototype.addNewComment = function () {
            var _this = this;
            if (this.newComment.comment != '') {
                this.loading = true;
                this.HelpDeskRequestCommentService.createComment({ Id: this.helpDeskRequest.id }, this.newComment).$promise
                    .then(function (newCommentId) {
                    _this.loading = false;
                    var addedComment = angular.copy(_this.newComment);
                    addedComment.id = newCommentId.id;
                    addedComment.isUserComment = true;
                    _this.commentList.push(addedComment);
                    _this.newComment.comment = '';
                    _this.$scope.$broadcast('rebuild:me');
                })
                    .catch(function (response) {
                    _this.loading = false;
                    _this.messageDialog.show({
                        header: "Сервер",
                        body: "Ошибка сервера"
                    });
                    return;
                });
            }
        };
        SingleRequestController.prototype.getCommentList = function () {
            var _this = this;
            this.commentList = this.HelpDeskRequestCommentService.query({
                id: this.helpDeskRequest.id
            });
            this.commentList.$promise
                .then(function (data) {
                _this.$scope.$broadcast('rebuild:me');
                _this.$timeout(function () {
                    _this.markCommentAsRead();
                }, 5000);
                return _this.sortComments(data);
            })
                .catch(function (response) {
                _this.messageDialog.show({
                    header: "Сервер",
                    body: "Ошибка сервера"
                });
                return;
            });
        };
        SingleRequestController.prototype.sortComments = function (comments) {
            var userName = this.AuthService.authentication.userName;
            angular.forEach(comments, function (comment) {
                comment.isUserComment = comment.userName === userName ? true : false;
                comment.isRead = true;
            });
            if (!this.helpDeskRequest.read) {
                for (var i = comments.length - 1; i >= 0; i--) {
                    if (!comments[i].isUserComment) {
                        comments[i].isRead = false;
                    }
                    else
                        break;
                }
            }
            return comments;
        };
        SingleRequestController.prototype.markCommentAsRead = function () {
            var _this = this;
            angular.forEach(this.commentList, function (comment) {
                comment.isRead = true;
            });
            this.helpDeskRequest.read = true;
            this.HelpDeskRequestService.updateRequest({ Id: this.helpDeskRequest.id }, this.helpDeskRequest).$promise
                .catch(function (response) {
                _this.messageDialog.show({
                    header: "Сервер",
                    body: "Ошибка сервера"
                });
                return;
            });
        };
        SingleRequestController.$inject = ["WebServices", "$messageDialog", "AuthService", "$scope", "$timeout"];
        return SingleRequestController;
    }());
    user.SingleRequestController = SingleRequestController;
    var singleRequestComponent = {
        restrict: 'AE',
        bindings: {
            helpDeskRequest: '<'
        },
        templateUrl: 'app/pages/personalAccount/myAccount/singleRequest/singleRequest.html',
        controller: SingleRequestController,
        controllerAs: "vm"
    };
    angular.module("personalAccount.myAccount")
        .component("singleRequest", singleRequestComponent);
})(user || (user = {}));
