module user{

    import IResource = angular.resource.IResource;
    import IResourceClass= angular.resource.IResourceClass;
    import IAauthService = core.IAauthService;
    import HelpDeskRequestViewModel = core.WebApi.Models.HelpDeskRequestViewModel;
    import HelpDeskRequestCommentViewModel = core.WebApi.Models.HelpDeskRequestCommentViewModel;
    import UserHelpDeskRequestService = core.IUserHelpDeskRequestService;
    import IUserHelpDeskRequestCommentService = core.IUserHelpDeskRequestCommentService;

    export class SingleRequestController {

        static $inject = ["WebServices", "$messageDialog", "AuthService", "$scope", "$timeout"];

        public helpDeskRequest: HelpDeskRequestViewModel;
        public commentList: Array<HelpDeskRequestCommentViewModel>;
        public newComment: HelpDeskRequestCommentViewModel = {
            id: null,
            userId: '',
            userName: '',
            userEmail: '',
            comment: '',
            creationDate: new Date(),
            isUserComment: false,
            isRead: false
        }

        private HelpDeskRequestService: UserHelpDeskRequestService;
        private HelpDeskRequestCommentService: IUserHelpDeskRequestCommentService;

        public loading:boolean = false;

        constructor(private WebServices:any, private messageDialog: any,
                    private AuthService:IAauthService, public $scope: ng.IScope,
                    private $timeout: any) {

            this.HelpDeskRequestService = WebServices.UserHelpDeskRequest;
            this.HelpDeskRequestCommentService = WebServices.UserHelpDeskRequestComment;
            this.getCommentList();
        }

        public addNewComment() {
            if (this.newComment.comment != '') {
                this.loading = true;
                this.HelpDeskRequestCommentService.createComment({Id: this.helpDeskRequest.id}, this.newComment).$promise
                    .then((newCommentId: any) => {
                        this.loading = false;
                        var addedComment:HelpDeskRequestCommentViewModel = angular.copy(this.newComment);
                        addedComment.id = newCommentId.id;
                        addedComment.isUserComment = true;

                        this.commentList.push(addedComment);

                        this.newComment.comment = '';
                        this.$scope.$broadcast('rebuild:me');
                    })
                    .catch((response:any) => {
                        this.loading = false;
                        this.messageDialog.show(
                            {
                                header: "Сервер",
                                body: "Ошибка сервера"
                            }
                        );
                        return;
                    });
            }

        }

        private getCommentList() {
            this.commentList = this.HelpDeskRequestCommentService.query(
                {
                    id: this.helpDeskRequest.id
                });

            this.commentList.$promise
                .then( (data: Array<HelpDeskRequestCommentViewModel>) => {
                    this.$scope.$broadcast('rebuild:me');
                    this.$timeout(() => {
                        this.markCommentAsRead();
                    }, 5000);
                    return this.sortComments(data);
                })
                .catch ( (response:any) => {
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }

        private sortComments(comments: Array<HelpDeskRequestCommentViewModel>){
            var userName = this.AuthService.authentication.userName;

            angular.forEach(comments, function(comment) {
                comment.isUserComment = comment.userName === userName ? true : false;
                comment.isRead = true;
            });

            if (!this.helpDeskRequest.read) {
                for (var i = comments.length - 1; i >= 0; i--) {
                    if (!comments[i].isUserComment) {
                        comments[i].isRead = false;
                    } else break;
                }
            }

            return comments;
        }

        private markCommentAsRead() {
            angular.forEach(this.commentList, function(comment) {
                comment.isRead = true;
            });

            this.helpDeskRequest.read = true;

            this.HelpDeskRequestService.updateRequest( {Id: this.helpDeskRequest.id}, this.helpDeskRequest).$promise
                .catch((response:any) => {
                    this.messageDialog.show(
                        {
                            header: "Сервер",
                            body: "Ошибка сервера"
                        }
                    );
                    return;
                });
        }

    }

    var singleRequestComponent = {
        restrict: 'AE',
        bindings: {
            helpDeskRequest: '<'
        },
        templateUrl: 'app/pages/personalAccount/myAccount/singleRequest/singleRequest.html',
        controller: SingleRequestController,
        controllerAs: "vm"
    };

    angular.module("personalAccount.myAccount")
        .component("singleRequest", singleRequestComponent)

}