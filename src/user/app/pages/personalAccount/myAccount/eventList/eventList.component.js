var user;
(function (user) {
    var EventListController = (function () {
        function EventListController(WebServices, messageDialog, statusTask, typeTask) {
            this.WebServices = WebServices;
            this.messageDialog = messageDialog;
            this.statusTask = statusTask;
            this.typeTask = typeTask;
            this.paginationSettings = {
                pageNumber: 1,
                pageSize: 5
            };
            this.changePage = false;
            this.currentDate = new Date();
            this.searchParams = {
                statusTasks: []
            };
            this.TaskV2Service = WebServices.UserTaskV2Service;
            this.searchParams.statusTasks.push(this.statusTask.End);
            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;
            this.getTaskV2List();
        }
        EventListController.prototype.pageChangeHandler = function (index) {
            this.paginationSettings.pageNumber = index;
            this.changePage = true;
            this.getTaskV2List();
        };
        EventListController.prototype.getTaskV2List = function () {
            var _this = this;
            this.TaskV2Service.getAllPage({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize,
                statusTasks: this.searchParams.statusTasks
            }).$promise
                .then(function (data) {
                _this.taskList = data;
                angular.forEach(_this.taskList.items, function (task) {
                    _this.getVirtualMachine(task);
                });
            })
                .catch(function (err) {
                _this.messageDialog.show({
                    header: "Сервер",
                    body: "Ошибка сервера"
                });
                return;
            });
        };
        EventListController.prototype.getVirtualMachine = function (task) {
            var _this = this;
            this.WebServices.UserSubscriptionVirtualMachineService.get({
                id: task.resourceId
            }).$promise
                .then(function (data) {
                task.vmName = data.name;
            })
                .catch(function (err) {
                _this.messageDialog.show({
                    header: "Сервер",
                    body: "Ошибка сервера"
                });
            });
        };
        EventListController.$inject = ["WebServices", "$messageDialog", "StatusTask", "TypeTask"];
        return EventListController;
    }());
    user.EventListController = EventListController;
    var eventListComponent = {
        restrict: 'AE',
        templateUrl: 'app/pages/personalAccount/myAccount/eventList/eventList.html',
        controller: EventListController,
        controllerAs: "vm"
    };
    angular.module("personalAccount.myAccount")
        .component("eventList", eventListComponent);
})(user || (user = {}));
