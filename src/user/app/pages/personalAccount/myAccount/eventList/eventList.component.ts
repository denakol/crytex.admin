module user{

    import IResourceClass = angular.resource.IResourceClass;
    import TaskV2ViewModel = core.WebApi.Models.TaskV2ViewModel;
    import PageModel = core.PageModel;
    import PaginationSetting = core.PaginationSetting;
    import TaskV2SearchParamsViewModel = core.WebApi.Models.TaskV2SearchParamsViewModel;

    export class EventListController {

        static $inject = ["WebServices", "$messageDialog", "StatusTask", "TypeTask"];

        public taskList: PageModel<TaskV2ViewModel>;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        }
        public changePage: boolean = false;
        public currentDate: Date = new Date();

        private TaskV2Service: IResourceClass<PageModel<TaskV2ViewModel>>;
        private searchParams: TaskV2SearchParamsViewModel = {
            statusTasks: []
        }

        constructor(private WebServices:any, private messageDialog: any,
                    private statusTask: any, public typeTask: any) {

            this.TaskV2Service = WebServices.UserTaskV2Service;

            this.searchParams.statusTasks.push(this.statusTask.End);
            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;
            this.getTaskV2List();
        }

        public pageChangeHandler (index: number) {
            this.paginationSettings.pageNumber = index;
            this.changePage = true;
            this.getTaskV2List();
        }

        private getTaskV2List() {
            this.TaskV2Service.getAllPage(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    statusTasks: this.searchParams.statusTasks
                }).$promise
                .then((data:any)=>{
                    this.taskList = data;
                    angular.forEach(this.taskList.items, (task)=>{
                        this.getVirtualMachine(task);
                    });
                })
                .catch((err:any)=>{
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }

        private getVirtualMachine(task:any){
            this.WebServices.UserSubscriptionVirtualMachineService.get({
                id: task.resourceId
            }).$promise
                .then((data:any)=>{
                    task.vmName = data.name;
                })
                .catch((err:any)=>{
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                });
        }

    }

    var eventListComponent = {
        restrict: 'AE',
        templateUrl: 'app/pages/personalAccount/myAccount/eventList/eventList.html',
        controller: EventListController,
        controllerAs: "vm"
    };

    angular.module("personalAccount.myAccount")
        .component("eventList", eventListComponent)
}