/**
 * Created by denak on 03.02.2016.
 */
module user{


    import IResourceClass = angular.resource.IResourceClass;
    import IResource= angular.resource.IResource;
    import IResourceArray = angular.resource.IResourceArray;
    import PageModel = core.PageModel;
    import PaginationSetting = core.PaginationSetting;

    export class HelpDeskRequestListController {

        static $inject = ["WebServices", "$messageDialog"];

        private HelpDeskRequestService:IResourceClass<PageModel<HelpDeskRequest>>;

        public helpDeskRequestList:PageModel<HelpDeskRequest>;
        public urgencyLevel:any;
        public requestStatus:any;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        }

        constructor(private WebServices:any, private messageDialog: any) {
            this.urgencyLevel = user.UrgencyLevel;
            this.requestStatus = user.RequestStatus;
            this.HelpDeskRequestService = WebServices.UserHelpDeskRequest;
            this.getHelpDeskRequest();
        }

        public pageChangeHandler (index: number) {
            this.paginationSettings.pageNumber = index;
            this.getHelpDeskRequest();
        }

        private getHelpDeskRequest() {
            this.helpDeskRequestList = this.HelpDeskRequestService.getAllPage(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                });

            this.helpDeskRequestList.$promise
                .catch ( (response:any) => {
                    this.messageDialog.show(
                        {
                            header:"Сервер",
                            body:"Ошибка сервера"
                        }
                    );
                    return;
                });
        }
    }


    var options: ng.IDirective = {
            restrict: 'AE',
            templateUrl: 'app/pages/personalAccount/support/helpDeskRequestList/HelpDeskRequestList.html',
            controller: HelpDeskRequestListController,
            controllerAs: 'vm'
    };

    angular.module("personalAccount.support")
        .component("helpDeskRequestList", options);
}