/**
 * Created by denak on 03.02.2016.
 */
declare module user {
    import PageModel = core.PageModel;
    class HelpDeskRequestListController {
        static $inject: string[];
        private HelpDeskRequestService;
        helpDeskRequestList: PageModel<HelpDeskRequest>;
        urgencyLevel: any;
        requestStatus: any;
        constructor(WebServices: any);
    }
}
