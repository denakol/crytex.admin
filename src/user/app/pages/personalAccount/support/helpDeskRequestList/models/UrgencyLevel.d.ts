/**
 * Created by denak on 03.02.2016.
 */
declare module user {
    enum UrgencyLevel {
        Low = 0,
        Normal = 1,
        High = 2,
        Critical = 3,
    }
}
