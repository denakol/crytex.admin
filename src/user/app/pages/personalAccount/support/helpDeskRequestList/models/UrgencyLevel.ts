/**
 * Created by denak on 03.02.2016.
 */
module user {

    export enum UrgencyLevel{
        Low = 0,
        Normal = 1,
        High = 2,
        Critical = 3
    }

}