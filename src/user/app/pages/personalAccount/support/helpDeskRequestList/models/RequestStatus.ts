/**
 * Created by denak on 03.02.2016.
 */
module user {

    export enum RequestStatus{
        New = 0,
        InProcessing = 1,
        Completed = 2
    }

}