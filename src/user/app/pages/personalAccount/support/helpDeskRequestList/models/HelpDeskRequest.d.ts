/**
 * Created by denak on 03.02.2016.
 */
declare module user {
    import IResource = angular.resource.IResource;
    interface HelpDeskRequest extends IResource<HelpDeskRequest> {
        Id: number;
        Summary: string;
        Details: string;
        Status: RequestStatus;
        CreationDate: Date;
        Read: boolean;
        UserName: string;
        Email: string;
        Urgency: UrgencyLevel;
    }
}
