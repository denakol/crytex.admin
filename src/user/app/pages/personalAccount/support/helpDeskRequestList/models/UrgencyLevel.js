/**
 * Created by denak on 03.02.2016.
 */
var user;
(function (user) {
    (function (UrgencyLevel) {
        UrgencyLevel[UrgencyLevel["Low"] = 0] = "Low";
        UrgencyLevel[UrgencyLevel["Normal"] = 1] = "Normal";
        UrgencyLevel[UrgencyLevel["High"] = 2] = "High";
        UrgencyLevel[UrgencyLevel["Critical"] = 3] = "Critical";
    })(user.UrgencyLevel || (user.UrgencyLevel = {}));
    var UrgencyLevel = user.UrgencyLevel;
})(user || (user = {}));
