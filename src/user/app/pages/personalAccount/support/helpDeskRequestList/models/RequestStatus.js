/**
 * Created by denak on 03.02.2016.
 */
var user;
(function (user) {
    (function (RequestStatus) {
        RequestStatus[RequestStatus["New"] = 0] = "New";
        RequestStatus[RequestStatus["InProcessing"] = 1] = "InProcessing";
        RequestStatus[RequestStatus["Completed"] = 2] = "Completed";
    })(user.RequestStatus || (user.RequestStatus = {}));
    var RequestStatus = user.RequestStatus;
})(user || (user = {}));
