/**
 * Created by denak on 03.02.2016.
 */
var user;
(function (user) {
    var HelpDeskRequestListController = (function () {
        function HelpDeskRequestListController(WebServices, messageDialog) {
            this.WebServices = WebServices;
            this.messageDialog = messageDialog;
            this.paginationSettings = {
                pageNumber: 1,
                pageSize: 5
            };
            this.urgencyLevel = user.UrgencyLevel;
            this.requestStatus = user.RequestStatus;
            this.HelpDeskRequestService = WebServices.UserHelpDeskRequest;
            this.getHelpDeskRequest();
        }
        HelpDeskRequestListController.prototype.pageChangeHandler = function (index) {
            this.paginationSettings.pageNumber = index;
            this.getHelpDeskRequest();
        };
        HelpDeskRequestListController.prototype.getHelpDeskRequest = function () {
            var _this = this;
            this.helpDeskRequestList = this.HelpDeskRequestService.getAllPage({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize,
            });
            this.helpDeskRequestList.$promise
                .catch(function (response) {
                _this.messageDialog.show({
                    header: "Сервер",
                    body: "Ошибка сервера"
                });
                return;
            });
        };
        HelpDeskRequestListController.$inject = ["WebServices", "$messageDialog"];
        return HelpDeskRequestListController;
    }());
    user.HelpDeskRequestListController = HelpDeskRequestListController;
    var options = {
        restrict: 'AE',
        templateUrl: 'app/pages/personalAccount/support/helpDeskRequestList/HelpDeskRequestList.html',
        controller: HelpDeskRequestListController,
        controllerAs: 'vm'
    };
    angular.module("personalAccount.support")
        .component("helpDeskRequestList", options);
})(user || (user = {}));
