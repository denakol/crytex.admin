(function () {

    angular.module('crytex.user.personalAccount')
        .controller('PersonalAccountController', PersonalAccountController);

    PersonalAccountController.$inject = ['$scope', '$timeout', '$state'];

    function PersonalAccountController($scope, $timeout, $state) {
        $scope.choosePersonalAccountMenu = choosePersonalAccountMenu;

        // Параметры для меню
        $scope.choosedPersonalAccountMenu = [true];
        $scope.defaultChoosedPersonalAccountMenu = 0;
    
        function choosePersonalAccountMenu(number) {
            if (number !== $scope.defaultChoosedPersonalAccountMenu) {
                $scope.choosedPersonalAccountMenu[number] = true;
                $scope.choosedPersonalAccountMenu[$scope.defaultChoosedPersonalAccountMenu] = false;
                $scope.defaultChoosedPersonalAccountMenu = number;
            }
        }


        $scope.items = [
            {
                name: "Мой аккаунт",
                sref: "personalAccount.myAccount",
            },
            {
                name: "Мои ресурсы",
                sref: "personalAccount.myResources",
                subItems: [
                    {
                        name: "Виртуальные машины",
                        sref: "personalAccount.myResources.cloud"
                    },
                    {
                        name: "Игровые сервера",
                        sref: "personalAccount.myResources.game"
                    },
                    {
                        name: "Веб-приложения",
                        sref: "personalAccount.myResources.web"
                    }
                ]
            },
            {
                name: "Заказать услугу",
                sref: "personalAccount.buyService",
                subItems: [
                    {
                        name: "Виртуальные машины",
                        sref: "personalAccount.buyService.buyCloud"
                    },
                    {
                        name: "Игровые сервера",
                        sref: "personalAccount.buyService.buyGame"
                    }
                ]
            },
            {
                name: "Мои финансы",
                sref: "personalAccount.myFinance",
            },
            {
                name: "База знаний",
                sref: "personalAccount.knowledgeBase",
            },
            {
                name: "Тех. поддержка",
                sref: "personalAccount.support",
            },
            {
                name: "Форум",
                sref: "personalAccount.forum",
            }
        ];

    }

})();
