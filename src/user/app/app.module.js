﻿(function () {

	angular.module("crytexUser", [
		"crytex.core",
		"crytexUser.home",
		"crytex.user.about",
		"crytexUser.domen",
		"crytexUser.webHosting",
		"crytexUser.game",
		"crytexUser.buyGame",
		"crytexUser.support",
		"user.cloud",
		"crytex.basket",
		"crytex.user.personalAccount",
		"crytex.user.knowledgeBase",
		"crytexUser.blocks",
		"crytexUser.widgets",
		"crytex.replenishBalance",
		"templates",
		"pages.auth"
	]);

})();