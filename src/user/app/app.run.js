(function () {

    angular.module("crytexUser").run(running);

    running.$inject = ['Permission','AuthService', '$rootScope', '$urlRouter'];

    function running(Permission, AuthService, $rootScope, $urlRouter) {


        Permission.defineRole('anonymous', function (stateParams) {

            if (!AuthService.authentication.isAuth) {
                return true;
            }
            return false;
        });
        Permission.defineRole('userHalf', function (stateParams) {

            if (AuthService.isInRole('FirstStepRegister')) {
                return true;
            }
            return false;
        });
        Permission.defineRole('user', function (stateParams) {

            if (AuthService.isInRole('User')) {
                return true;
            }
            return false;
        });

        

    }

})();
