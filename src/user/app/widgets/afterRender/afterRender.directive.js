(function () {

    angular.module("crytex.afterRenderDirective", [])
        .directive("afterRender", afterRender);

    afterRender.$inject = ['$timeout'];

    function afterRender($timeout) {

        return {
			restrict: 'A',
			terminal: true,
			transclude: false,
			link: function (scope, element, attrs) {
				$timeout(scope.$eval(attrs.afterRender), 0);  //Calling a scoped method
			}
        };

    }

})();