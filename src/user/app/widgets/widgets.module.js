﻿(function () {
    angular.module("crytexUser.widgets", [
        "crytex.popUpWidgets",
        "widgets.topLogin",
        "crytex.sliderWidget",
        "crytex.afterRenderDirective",
        "crytex.callMeService"
    ]);
})();