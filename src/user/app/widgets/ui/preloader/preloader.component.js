var user;
(function (user) {
    var widgets;
    (function (widgets) {
        var preloader;
        (function (preloader) {
            var PreloaderController = (function () {
                function PreloaderController() {
                }
                return PreloaderController;
            }());
            preloader.PreloaderController = PreloaderController;
            var option = {
                restrict: "EA",
                bindings: {
                    promise: '<',
                    margin: '@'
                },
                templateUrl: 'app/widgets/ui/preloader/preloader.html',
                controller: PreloaderController,
                controllerAs: 'vm'
            };
            angular.module("crytexUser.widgets")
                .component("preloader", option);
        })(preloader = widgets.preloader || (widgets.preloader = {}));
    })(widgets = user.widgets || (user.widgets = {}));
})(user || (user = {}));
