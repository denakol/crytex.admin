module user.widgets.preloader {



    export class PreloaderController {



        constructor() {

        }

    }

    var option:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            promise: '<',
            margin:'@'
        },
        templateUrl: 'app/widgets/ui/preloader/preloader.html',
        controller: PreloaderController,
        controllerAs: 'vm'
    };

    angular.module("crytexUser.widgets")
        .component("preloader", option);
}