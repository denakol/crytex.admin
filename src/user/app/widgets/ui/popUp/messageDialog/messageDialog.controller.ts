module user {

    import IDialogService = angular.material.IDialogService;

    export class MessageDialogController {
        static $inject = ['$mdDialog', "message"];

        constructor(private $mdDialog: IDialogService, public message:any) {
        }

        public close() {
            this.$mdDialog.hide();
        }
    }

    angular.module('popUpWidgets.messageDialog')
        .controller('MessageDialogController', MessageDialogController);

}

