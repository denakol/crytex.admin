var user;
(function (user) {
    var MessageDialogController = (function () {
        function MessageDialogController($mdDialog, message) {
            this.$mdDialog = $mdDialog;
            this.message = message;
        }
        MessageDialogController.prototype.close = function () {
            this.$mdDialog.hide();
        };
        MessageDialogController.$inject = ['$mdDialog', "message"];
        return MessageDialogController;
    }());
    user.MessageDialogController = MessageDialogController;
    angular.module('popUpWidgets.messageDialog')
        .controller('MessageDialogController', MessageDialogController);
})(user || (user = {}));
