var user;
(function (user) {
    var MessageDialogService = (function () {
        function MessageDialogService($mdDialog) {
            this.$mdDialog = $mdDialog;
            this.lastTime = new Date();
            this.isShowed = false;
            this.timeDifferent = 5; // Устанавливаем min-разницу межде показываемыми ошибками в 5 секунд
        }
        MessageDialogService.prototype.show = function (message) {
            var currentTime = new Date();
            var timeDiff = Math.abs(currentTime.getTime() - this.lastTime.getTime());
            var diffSeconds = Math.ceil(timeDiff / 1000);
            if (diffSeconds > this.timeDifferent) {
                this.lastTime = currentTime;
                return this.showForm(message);
            }
            else {
                if (!this.isShowed) {
                    return this.showForm(message);
                }
                this.lastTime = currentTime;
            }
            return;
        };
        MessageDialogService.prototype.showMessage = function (message) {
            return this.showForm(message);
        };
        MessageDialogService.prototype.showForm = function (message) {
            this.isShowed = true;
            return this.$mdDialog.show({
                controller: 'MessageDialogController',
                clickOutsideToClose: true,
                controllerAs: 'vm',
                templateUrl: "app/widgets/ui/popUp/messageDialog/messageDialog.html",
                locals: {
                    message: message
                }
            });
        };
        MessageDialogService.$inject = ['$mdDialog'];
        return MessageDialogService;
    }());
    angular.module("popUpWidgets.messageDialog")
        .service('$messageDialog', MessageDialogService);
})(user || (user = {}));
