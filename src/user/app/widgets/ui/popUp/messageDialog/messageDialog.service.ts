module user {

    import IConfirmDialog = angular.material.IConfirmDialog;
    import IDialogService = angular.material.IDialogService;


    class MessageDialogService implements IUserDialog {

        static $inject = ['$mdDialog'];
        public lastTime:Date = new Date();
        public isShowed:boolean = false;
        public timeDifferent:number = 5; // Устанавливаем min-разницу межде показываемыми ошибками в 5 секунд

        constructor(private $mdDialog:IDialogService) {

        }

        public show(message:any):angular.IPromise<any> {
            var currentTime = new Date();
            var timeDiff = Math.abs(currentTime.getTime() - this.lastTime.getTime());
            var diffSeconds = Math.ceil(timeDiff / 1000);

            if (diffSeconds > this.timeDifferent) {
                this.lastTime = currentTime;
                return this.showForm(message);
            } else {
                if (!this.isShowed) {
                    return this.showForm(message);
                }
                this.lastTime = currentTime;
            }
            return;
        }

        public showMessage(message:any):angular.IPromise<any> {

            return this.showForm(message);
            
        }

        private showForm(message:any):any {
            this.isShowed = true;

            return this.$mdDialog.show({
                controller: 'MessageDialogController',
                clickOutsideToClose: true,
                controllerAs: 'vm',
                templateUrl: "app/widgets/ui/popUp/messageDialog/messageDialog.html",
                locals: {
                    message: message
                }
            })
        }
    }

    angular.module("popUpWidgets.messageDialog")
        .service('$messageDialog', MessageDialogService);
}