declare module user {
    class CallMeFormController {
        private callMeService;
        private dialog;
        static $inject: string[];
        userPhone: string;
        constructor(callMeService: any, dialog: any);
        orderCall(): void;
        showMessage(data: ICallMeResult): void;
        clearUserPhone(): void;
    }
}
