declare module user {
    class callMeFormDirective implements ng.IDirective {
        restrict: string;
        scope: {
            show: string;
        };
        templateUrl: string;
        controller: string;
        controllerAs: string;
    }
}
