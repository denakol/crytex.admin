var user;
(function (user) {
    var callMeFormDirective = (function () {
        function callMeFormDirective() {
            this.restrict = 'E';
            this.scope = {
                show: '='
            };
            this.templateUrl = 'app/widgets/ui/popUp/callMeForm/callMeForm.html';
            this.controller = 'CallMeFormController';
            this.controllerAs = 'vm';
        }
        return callMeFormDirective;
    }());
    user.callMeFormDirective = callMeFormDirective;
    angular.module("popUpWidgets.callMeForm", [])
        .directive('callMeForm', [function () { return new callMeFormDirective(); }])
        .controller('CallMeFormController', user.CallMeFormController);
})(user || (user = {}));
