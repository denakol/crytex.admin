module user {

    export class callMeFormDirective implements ng.IDirective {
        restrict = 'E';
        scope = {
            show: '='
        };
        templateUrl: string = 'app/widgets/ui/popUp/callMeForm/callMeForm.html';
        controller: string = 'CallMeFormController';
        controllerAs: string = 'vm';
    }

    angular.module("popUpWidgets.callMeForm", [])
        .directive('callMeForm', [() => new callMeFormDirective()])
        .controller('CallMeFormController', user.CallMeFormController);
}