var user;
(function (user) {
    var CallMeFormController = (function () {
        function CallMeFormController(callMeService, dialog) {
            this.callMeService = callMeService;
            this.dialog = dialog;
            this.userPhone = null;
        }
        CallMeFormController.prototype.orderCall = function () {
            var _this = this;
            this.callMeService.orderCall(this.userPhone).then(function (result) {
                _this.showMessage(result);
                _this.clearUserPhone();
            }, function (response) {
                var error = {
                    errorCode: 3,
                    errorText: '',
                    messageHeader: 'У нас ошибка',
                    messageText: 'Попробуйте позже'
                };
                _this.showMessage(error);
                _this.clearUserPhone();
                return error;
            });
        };
        CallMeFormController.prototype.showMessage = function (data) {
            this.dialog.show(this.dialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title(data.messageHeader)
                .textContent(data.messageText)
                .ok('ОК'));
        };
        CallMeFormController.prototype.clearUserPhone = function () {
            this.userPhone = null;
        };
        CallMeFormController.$inject = ["callMeService", "$mdDialog"];
        return CallMeFormController;
    }());
    user.CallMeFormController = CallMeFormController;
})(user || (user = {}));
