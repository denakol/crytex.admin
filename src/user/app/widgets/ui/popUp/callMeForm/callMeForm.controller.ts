module user {

    export class CallMeFormController {
        static $inject = ["callMeService", "$mdDialog"];

        userPhone: string = null;

        constructor (private callMeService: any, private  dialog: any){
        }

        orderCall() {
            this.callMeService.orderCall(this.userPhone).then( (result: ICallMeResult) => {
                    this.showMessage(result);
                    this.clearUserPhone();
                },
                (response:any) => {
                    var error : ICallMeResult = {
                        errorCode: 3,
                        errorText: '',
                        messageHeader: 'У нас ошибка',
                        messageText: 'Попробуйте позже'
                    }
                    this.showMessage(error);
                    this.clearUserPhone();
                    return error;
                }
            );
        }

        showMessage(data: ICallMeResult) {
            this.dialog.show(
                this.dialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title(data.messageHeader)
                    .textContent(data.messageText)
                    .ok('ОК')
            );
        }

        clearUserPhone() {
            this.userPhone = null;
        }


    }
}
