(function () {

    angular.module("crytex.popUpWidgets", [
        "popUpWidgets.callMeForm",
        "popUpWidgets.loginForm",
        "popUpWidgets.registerForm",
        "popUpWidgets.messageDialog",
        "popUpWidgets.resetPasswordRequestDialog",
        "popUpWidgets.resetPasswordDialog",
        "popUpWidgets.testPeriodForm"
    ]);

})();