/**
 * Created by ruslan on 26.01.2016.
 */
module user {

    import IAauthService = core.IAauthService;
    import IDialogService = angular.material.IDialogService;
    import ResetPassword = core.ResetPassword;


    export class ResetPasswordRequestDialogController {
        static $inject = ['$mdDialog', 'AuthService', '$messageDialog'];

        public email:string;

        public resetLoading:boolean = false;
        
        public submited:boolean = false;

        constructor(private $mdDialog:IDialogService, private AuthService:IAauthService, private $messageDialog:any) {


        }

        close() {
            this.$mdDialog.cancel();
        }


        reset(formReset:any) {
            this.submited = true;
            if(formReset.$valid){
                this.resetLoading = true;
                this.AuthService.resetPasswordRequest(this.email)
                    .then(()=> {
                        this.$mdDialog.hide(true);
                    })
                    .catch((err:any)=>{
                        this.$messageDialog.show({
                            header: "Восстановление пароля",
                            body: "Ваш пароль не удалось изменить. Выполните повторную попытку, либо обратитесь в поддержку."
                        }).then(()=> {
                            this.resetLoading = false;
                        });
                    });
            }
        };
    }


    angular.module('popUpWidgets.resetPasswordRequestDialog')
        .controller('ResetPasswordRequestDialogController', ResetPasswordRequestDialogController);

}

