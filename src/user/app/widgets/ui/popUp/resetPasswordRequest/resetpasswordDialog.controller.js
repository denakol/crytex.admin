/**
 * Created by ruslan on 26.01.2016.
 */
var user;
(function (user) {
    var ResetPasswordRequestDialogController = (function () {
        function ResetPasswordRequestDialogController($mdDialog, AuthService, $messageDialog) {
            this.$mdDialog = $mdDialog;
            this.AuthService = AuthService;
            this.$messageDialog = $messageDialog;
            this.resetLoading = false;
            this.submited = false;
        }
        ResetPasswordRequestDialogController.prototype.close = function () {
            this.$mdDialog.cancel();
        };
        ResetPasswordRequestDialogController.prototype.reset = function (formReset) {
            var _this = this;
            this.submited = true;
            if (formReset.$valid) {
                this.resetLoading = true;
                this.AuthService.resetPasswordRequest(this.email)
                    .then(function () {
                    _this.$mdDialog.hide(true);
                })
                    .catch(function (err) {
                    _this.$messageDialog.show({
                        header: "Восстановление пароля",
                        body: "Ваш пароль не удалось изменить. Выполните повторную попытку, либо обратитесь в поддержку."
                    }).then(function () {
                        _this.resetLoading = false;
                    });
                });
            }
        };
        ;
        ResetPasswordRequestDialogController.$inject = ['$mdDialog', 'AuthService', '$messageDialog'];
        return ResetPasswordRequestDialogController;
    }());
    user.ResetPasswordRequestDialogController = ResetPasswordRequestDialogController;
    angular.module('popUpWidgets.resetPasswordRequestDialog')
        .controller('ResetPasswordRequestDialogController', ResetPasswordRequestDialogController);
})(user || (user = {}));
