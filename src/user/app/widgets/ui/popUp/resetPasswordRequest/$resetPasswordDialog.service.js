var user;
(function (user) {
    var ResetPasswordRequestDialogService = (function () {
        function ResetPasswordRequestDialogService($mdDialog, $messageDialog) {
            this.$mdDialog = $mdDialog;
            this.$messageDialog = $messageDialog;
        }
        ResetPasswordRequestDialogService.prototype.show = function () {
            var _this = this;
            return this.$mdDialog.show({
                controller: 'ResetPasswordRequestDialogController',
                clickOutsideToClose: true,
                controllerAs: 'vm',
                templateUrl: "app/widgets/ui/popUp/resetPasswordRequest/resetPasswordRequestDialog.html"
            }).then(function (result) {
                if (result) {
                    _this.$messageDialog.show({
                        header: "Восстановление пароля",
                        body: "На вашу почту отправлено сообщение. Для восстановления пароля перейдите по ссылке указанной в письме."
                    });
                }
            });
        };
        ResetPasswordRequestDialogService.$inject = ['$mdDialog', '$messageDialog'];
        return ResetPasswordRequestDialogService;
    }());
    angular.module("popUpWidgets.resetPasswordRequestDialog")
        .service('resetPasswordRequestDialogService', ResetPasswordRequestDialogService);
})(user || (user = {}));
