/**
 * Created by ruslan on 26.01.2016.
 */
declare module user {
    import IAauthService = core.IAauthService;
    import IDialogService = angular.material.IDialogService;
    class ResetPasswordRequestDialogController {
        private $mdDialog;
        private AuthService;
        private $messageDialog;
        static $inject: string[];
        email: string;
        constructor($mdDialog: IDialogService, AuthService: IAauthService, $messageDialog: any);
        close(): void;
        reset(): void;
    }
}
