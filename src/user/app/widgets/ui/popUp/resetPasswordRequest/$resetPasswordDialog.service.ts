
module user {

    import IConfirmDialog = angular.material.IConfirmDialog;
    import IDialogService = angular.material.IDialogService;


    class ResetPasswordRequestDialogService  {

        static $inject = ['$mdDialog','$messageDialog'];

        show():angular.IPromise<any> {


            return this.$mdDialog.show({
                controller: 'ResetPasswordRequestDialogController',
                clickOutsideToClose: true,
                controllerAs: 'vm',
                templateUrl: "app/widgets/ui/popUp/resetPasswordRequest/resetPasswordRequestDialog.html"
            }).then((result)=>{
                if(result){
                    this.$messageDialog.show({
                        header: "Восстановление пароля",
                        body: "На вашу почту отправлено сообщение. Для восстановления пароля перейдите по ссылке указанной в письме."
                    });
                }
            });
        }

        constructor(private $mdDialog:IDialogService,private $messageDialog:any) {

        }
    }

    angular.module("popUpWidgets.resetPasswordRequestDialog")
        .service('resetPasswordRequestDialogService', ResetPasswordRequestDialogService);


}