(function () {

    angular.module('popUpWidgets.loginForm')
        .service('$loginForm', service);

    service.$inject = ['$mdDialog','$state', 'localStorageService'];

    function service($mdDialog,$state, localStorageService) {

        return {

            show: function () {

                return     $mdDialog.show({
                    controller: 'LoginFormController',
                    clickOutsideToClose: true,
                    controllerAs: 'vm',
                    templateUrl: 'app/widgets/ui/popUp/loginForm/loginForm.html'
                }).then(function (result) {
                    if(result) {
                        var vmOption = localStorageService.get('vmOption');
                        if(vmOption){
                            $state.go('personalAccount.buyService');
                        } else {
                            $state.go('personalAccount.myAccount');
                        }
                    }
                });

            }

        };
    }

})();
