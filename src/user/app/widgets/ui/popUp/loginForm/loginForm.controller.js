(function () {

    angular.module('popUpWidgets.loginForm')
        .controller('LoginFormController', controller);

    controller.$inject = ['$mdDialog', 'AuthService', 'resetPasswordRequestDialogService', '$registerFromType', '$testPeriodForm'];

    function controller($mdDialog, AuthService, resetPasswordRequestDialogService, $registerFromType, $testPeriodForm) {
        var vm = this;
        vm.user = {};
        vm.close = function () {
            $mdDialog.cancel();
        };
        vm.submitted = false;
        vm.loginLoading = false;
        vm.login = function (form) {
            vm.submitted = true;
            if (form.$valid) {
                vm.loginLoading = true;
                AuthService.signIn(vm.user).then(function () {
                    $mdDialog.hide(true);
                    // Если есть запрос на тестовый период
                    if (AuthService.authentication.isRequestTestPeriod) {
                        $testPeriodForm.show();
                    }
                }, function (response) {
                    vm.loginLoading = false;
                    if (response !== null) {
                    }
                    else {
                        form.$error.serverError = true;
                        return;
                    }
                    form.$error[response.error] = true;
                });
            }

        };

        vm.register = function () {
            $mdDialog.hide().then(function () {
                $registerFromType.show();
            });

        };


        vm.resetPassword = function () {
            $mdDialog.hide().then(function () {
                resetPasswordRequestDialogService.show();
            });

        };
    }

})();
