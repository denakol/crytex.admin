(function () {

    angular.module("popUpWidgets.loginForm")
        .directive("loginForm", loginForm);

    loginForm.$inject = ['WebServices', '$mdDialog', '$compile'];

    function loginForm(WebServices, $mdDialog, $compile) {
        return {
            scope: {
                show: '='
            },
            link: function (scope, element, attrs) {

                scope.login = login;

                function login(index) {
                    $mdDialog.show({
                            controller: 'LoginFormController',
                            clickOutsideToClose: true,
                            controllerAs: 'vm',
                            templateUrl: 'app/widgets/ui/popUp/loginForm/loginForm.html'
                        })
                        .then(function (answer) {

                        }, function () {

                        });
                }
                element.removeAttr("login-form");
                element.attr("ng-click", "login()");
                $compile(element)(scope);
            },

            restrict: "A"

        };


    }

})();
