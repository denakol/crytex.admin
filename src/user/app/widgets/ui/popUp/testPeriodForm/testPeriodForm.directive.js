var user;
(function (user) {
    var TestPeriod = (function () {
        function TestPeriod(mdDialog) {
            var _this = this;
            this.mdDialog = mdDialog;
            this.restrict = 'A';
            this.link = function (scope, elem, attributes) {
                elem.bind('click', function () {
                    _this.mdDialog.show({
                        controller: 'TestPeriodController',
                        clickOutsideToClose: true,
                        controllerAs: 'vm',
                        templateUrl: 'app/widgets/ui/popUp/testPeriodForm/testPeriodForm.html'
                    })
                        .then(function (answer) {
                    }, function () {
                    });
                });
            };
        }
        TestPeriod.factory = function () {
            var directive = function (mdDialog) { return new TestPeriod(mdDialog); };
            directive.$inject = ['$mdDialog'];
            return directive;
        };
        return TestPeriod;
    }());
    user.TestPeriod = TestPeriod;
    angular.module("popUpWidgets.testPeriodForm")
        .directive('testPeriod', TestPeriod.factory());
})(user || (user = {}));
