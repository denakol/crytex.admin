var user;
(function (user) {
    var TestPeriodController = (function () {
        function TestPeriodController($mdDialog, AuthService, $loginForm, $registerForm, testPeriodService) {
            this.$mdDialog = $mdDialog;
            this.AuthService = AuthService;
            this.$loginForm = $loginForm;
            this.$registerForm = $registerForm;
            this.testPeriodService = testPeriodService;
            this.userMessageList = {
                info: "Для тестового периода вам будет предоставлено 1000 рублей. Эти деньги вы можете потратить на виртуальные машины.",
                activated: "Тестовый период уже был активирован.",
                success: "Тестовый период успешно активирован.",
                error: "Попробуйте позже."
            };
            this.testPeriodState = {
                message: this.userMessageList.info,
                btnGroupYesNo: true,
                btnGroupOk: false,
                btnGroupLoginRegister: false
            };
            if (this.AuthService.authentication.isAuth) {
                // Пользователь авторизован
                this.testPeriodState.message = this.userMessageList.info;
                this.testPeriodState.btnGroupYesNo = true;
                this.testPeriodState.btnGroupOk = false;
                this.testPeriodState.btnGroupLoginRegister = false;
            }
            else {
                this.testPeriodState.message = this.userMessageList.info;
                this.testPeriodState.btnGroupYesNo = false;
                this.testPeriodState.btnGroupOk = false;
                this.testPeriodState.btnGroupLoginRegister = true;
            }
        }
        TestPeriodController.prototype.close = function () {
            this.$mdDialog.cancel();
        };
        TestPeriodController.prototype.showLoginForm = function () {
            this.testPeriodService.setRequestOnTestPeriod();
            this.$loginForm.show();
        };
        TestPeriodController.prototype.showRegisterForm = function () {
            this.$registerForm.show();
        };
        TestPeriodController.prototype.getTestPeriod = function () {
            var _this = this;
            this.testPeriodService.activateTestPeriod()
                .then(function (response) {
                if (response === true) {
                    _this.AuthService.authentication.isRequestTestPeriod = false;
                    _this.testPeriodState.message = _this.userMessageList.success;
                    _this.testPeriodState.btnGroupYesNo = false;
                    _this.testPeriodState.btnGroupOk = true;
                    _this.testPeriodState.btnGroupLoginRegister = false;
                }
                else if (response.data) {
                    // Что-то пошло не так
                    _this.testPeriodState.message = response.data.errorMessage;
                    _this.testPeriodState.btnGroupYesNo = false;
                    _this.testPeriodState.btnGroupOk = true;
                    _this.testPeriodState.btnGroupLoginRegister = false;
                }
                else {
                    _this.testPeriodState.message = _this.userMessageList.error;
                    _this.testPeriodState.btnGroupYesNo = false;
                    _this.testPeriodState.btnGroupOk = true;
                    _this.testPeriodState.btnGroupLoginRegister = false;
                }
            })
                .catch(function (response) {
                return this.userMessageList.error;
            });
        };
        TestPeriodController.$inject = ['$mdDialog', 'AuthService', '$loginForm', '$registerFromType', 'testPeriodService'];
        return TestPeriodController;
    }());
    user.TestPeriodController = TestPeriodController;
    angular.module('popUpWidgets.testPeriodForm')
        .controller('TestPeriodController', TestPeriodController);
})(user || (user = {}));
