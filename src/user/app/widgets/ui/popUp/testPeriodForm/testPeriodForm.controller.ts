module user {

    import IAauthService = core.IAauthService;
    import LoginModel = core.LoginModel;
    import IDialogService = angular.material.IDialogService;
    import IStateService = angular.ui.IStateService;
    import TestPeriodService = user.TestPeriodService;

    export interface testPeriodStateModel {
        message: string;
        btnGroupYesNo: boolean;
        btnGroupOk: boolean;
        btnGroupLoginRegister: boolean;
    }

    export interface testPeriodMessage {
        info: string;
        activated: string;
        success: string;
        error: string;
    }

    export class TestPeriodController {
        static $inject = ['$mdDialog', 'AuthService', '$loginForm', '$registerFromType', 'testPeriodService'];

        private userMessageList: testPeriodMessage = {
            info: "Для тестового периода вам будет предоставлено 1000 рублей. Эти деньги вы можете потратить на виртуальные машины.",
            activated: "Тестовый период уже был активирован.",
            success: "Тестовый период успешно активирован.",
            error: "Попробуйте позже."
        };

        public testPeriodState: testPeriodStateModel = {
            message: this.userMessageList.info,
            btnGroupYesNo: true,
            btnGroupOk: false,
            btnGroupLoginRegister: false
        };

        constructor(private $mdDialog:IDialogService, private AuthService:IAauthService,
                    private $loginForm:any, private $registerForm:any,
                    private testPeriodService: any) {

            if (this.AuthService.authentication.isAuth) {
                // Пользователь авторизован
                this.testPeriodState.message = this.userMessageList.info;
                this.testPeriodState.btnGroupYesNo = true;
                this.testPeriodState.btnGroupOk = false;
                this.testPeriodState.btnGroupLoginRegister = false;
            } else {
                this.testPeriodState.message = this.userMessageList.info;
                this.testPeriodState.btnGroupYesNo = false;
                this.testPeriodState.btnGroupOk = false;
                this.testPeriodState.btnGroupLoginRegister = true;
            }
        }

        public close() {
            this.$mdDialog.cancel();
        }

        public showLoginForm() {
            this.testPeriodService.setRequestOnTestPeriod();
            this.$loginForm.show();
        }

        public showRegisterForm() {
            this.$registerForm.show();
        }

        public getTestPeriod() {
            this.testPeriodService.activateTestPeriod()
                .then((response:any) => {
                    if (response === true) {
                        this.AuthService.authentication.isRequestTestPeriod = false;
                        this.testPeriodState.message = this.userMessageList.success;
                        this.testPeriodState.btnGroupYesNo = false;
                        this.testPeriodState.btnGroupOk = true;
                        this.testPeriodState.btnGroupLoginRegister = false;
                    } else if (response.data){
                        // Что-то пошло не так
                        this.testPeriodState.message = response.data.errorMessage;
                        this.testPeriodState.btnGroupYesNo = false;
                        this.testPeriodState.btnGroupOk = true;
                        this.testPeriodState.btnGroupLoginRegister = false;
                    } else {
                        this.testPeriodState.message = this.userMessageList.error;
                        this.testPeriodState.btnGroupYesNo = false;
                        this.testPeriodState.btnGroupOk = true;
                        this.testPeriodState.btnGroupLoginRegister = false;
                    }
                })
                .catch(function (response:any) {
                    return this.userMessageList.error;
                });
        }
    }


    angular.module('popUpWidgets.testPeriodForm')
        .controller('TestPeriodController', TestPeriodController);

}

