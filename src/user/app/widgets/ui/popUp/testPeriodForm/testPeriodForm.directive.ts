module user {

    export class TestPeriod implements ng.IDirective {
        restrict = 'A';

        constructor(private mdDialog: any) {
        }

        link = (scope: ng.IScope, elem: ng.IAugmentedJQuery, attributes:ng.IAttributes) => {

            elem.bind('click', ()=> {

                this.mdDialog.show({
                        controller: 'TestPeriodController',
                        clickOutsideToClose: true,
                        controllerAs: 'vm',
                        templateUrl: 'app/widgets/ui/popUp/testPeriodForm/testPeriodForm.html'
                    })
                    .then(function (answer: any) {

                    }, function () {

                    });

            });
        }


        static factory():ng.IDirectiveFactory {
            var directive = (mdDialog:any ) => new TestPeriod(mdDialog);
            directive.$inject = ['$mdDialog'];
            return directive;
        }
    }

    angular.module("popUpWidgets.testPeriodForm")
        .directive('testPeriod', TestPeriod.factory());
}