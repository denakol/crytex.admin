var user;
(function (user) {
    var $testPeriodForm = (function () {
        function $testPeriodForm($mdDialog, $messageDialog) {
            this.$mdDialog = $mdDialog;
            this.$messageDialog = $messageDialog;
        }
        $testPeriodForm.prototype.show = function () {
            return this.$mdDialog.show({
                controller: 'TestPeriodController',
                clickOutsideToClose: true,
                controllerAs: 'vm',
                templateUrl: "app/widgets/ui/popUp/testPeriodForm/testPeriodForm.html"
            });
        };
        $testPeriodForm.$inject = ['$mdDialog', '$messageDialog'];
        return $testPeriodForm;
    }());
    angular.module("popUpWidgets.testPeriodForm")
        .service('$testPeriodForm', $testPeriodForm);
})(user || (user = {}));
