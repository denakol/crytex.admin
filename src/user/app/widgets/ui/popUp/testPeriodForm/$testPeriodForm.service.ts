module user {

    import IConfirmDialog = angular.material.IConfirmDialog;
    import IDialogService = angular.material.IDialogService;


    class $testPeriodForm implements IUserDialog {

        static $inject = ['$mdDialog','$messageDialog'];

        constructor(private $mdDialog:IDialogService, private $messageDialog:any) {

        }

        show():angular.IPromise<any> {

            return this.$mdDialog.show({
                controller: 'TestPeriodController',
                clickOutsideToClose: true,
                controllerAs: 'vm',
                templateUrl: "app/widgets/ui/popUp/testPeriodForm/testPeriodForm.html"
            })
        }
    }

    angular.module("popUpWidgets.testPeriodForm")
        .service('$testPeriodForm', $testPeriodForm);


}