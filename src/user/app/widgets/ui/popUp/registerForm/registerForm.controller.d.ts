declare module user {
    import IAauthService = core.IAauthService;
    import LoginModel = core.LoginModel;
    import IDialogService = angular.material.IDialogService;
    import IStateService = angular.ui.IStateService;
    class RegisterController {
        private $mdDialog;
        private AuthService;
        private $state;
        private $messageDialog;
        private $loginForm;
        static $inject: string[];
        user: LoginModel;
        submitted: boolean;
        constructor($mdDialog: IDialogService, AuthService: IAauthService, $state: IStateService, $messageDialog: any, $loginForm: any);
        close(): void;
        register(form: any): void;
        login(): void;
    }
}
