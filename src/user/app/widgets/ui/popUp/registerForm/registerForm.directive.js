(function () {

    angular.module("popUpWidgets.registerForm")
        .directive("registerForm", registerForm);

    registerForm.$inject = ['WebServices', '$mdDialog', '$compile'];


    function registerForm(WebServices, $mdDialog, $compile) {

        return {
            scope: {
                show: '='
            },
            link: function (scope, element, attrs) {

                scope.register = register;

                function register(index) {
                    $mdDialog.show({
                            controller: 'RegisterController',
                            clickOutsideToClose: true,
                            controllerAs: 'vm',
                            templateUrl: "app/widgets/ui/popUp/registerForm/registerForm.html"
                        })
                        .then(function (answer) {

                        }, function () {

                        });
                };

                element.removeAttr("register-form");
                element.attr("ng-click", "register()");
                $compile(element)(scope);
            },

            restrict: "A"

        };

    }

})();
