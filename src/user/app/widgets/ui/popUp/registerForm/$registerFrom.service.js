var user;
(function (user) {
    var $registerFrom = (function () {
        function $registerFrom($mdDialog, $messageDialog) {
            this.$mdDialog = $mdDialog;
            this.$messageDialog = $messageDialog;
        }
        $registerFrom.prototype.show = function () {
            var _this = this;
            return this.$mdDialog.show({
                controller: 'RegisterController',
                clickOutsideToClose: true,
                controllerAs: 'vm',
                templateUrl: "app/widgets/ui/popUp/registerForm/registerForm.html"
            }).then(function (result) {
                if (result) {
                    _this.$messageDialog.show({
                        header: "Подтверждение email",
                        body: "На вашу почту отправлено сообщение. Для подтверждения email перейдите по ссылке указанной в письме."
                    });
                }
            });
        };
        $registerFrom.$inject = ['$mdDialog', '$messageDialog'];
        return $registerFrom;
    }());
    angular.module("popUpWidgets.registerForm")
        .service('$registerFromType', $registerFrom);
})(user || (user = {}));
