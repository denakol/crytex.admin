module user {

    import IAauthService = core.IAauthService;
    import LoginModel = core.LoginModel;
    import IDialogService = angular.material.IDialogService;
    import IStateService = angular.ui.IStateService;


    export class RegisterController {
        static $inject = ['$mdDialog', 'AuthService', '$state', '$messageDialog', '$loginForm'];

        public user:LoginModel;
        public submitted = false;
        public regLoading:boolean = false;

        constructor(private $mdDialog:IDialogService, private AuthService:IAauthService, private $state:IStateService, private $messageDialog:any, private $loginForm:any) {

        }

        close() {
            this.$mdDialog.cancel();
        }

        register(form:any) {
            this.submitted = true;
            if (form.$valid) {
                this.regLoading = true;
                this.AuthService.registerFirstStep(this.user).then(()=> {
                        this.$mdDialog.hide(true);
                    },
                    (response)=> {
                        this.regLoading = false;
                        if (response == null) {
                            form.$error["serverError"] = true;
                            return;
                        }
                        form.$error[response.error] = true;
                    });
            }
        };

        login() {
            this.$mdDialog.hide().then(()=> {


                    this.$loginForm.show();
                }
            );
        }
    }


    angular.module('popUpWidgets.registerForm')
        .controller('RegisterController', RegisterController);

}

