
module user {

    import IConfirmDialog = angular.material.IConfirmDialog;
    import IDialogService = angular.material.IDialogService;


    class $registerFrom implements IUserDialog {

        static $inject = ['$mdDialog','$messageDialog'];
        show():angular.IPromise<any> {


            return this.$mdDialog.show({
                controller: 'RegisterController',
                clickOutsideToClose: true,
                controllerAs: 'vm',
                templateUrl: "app/widgets/ui/popUp/registerForm/registerForm.html"
            }).then( (result)=> {

                if(result) {
                    this.$messageDialog.show({
                        header: "Подтверждение email",
                        body: "На вашу почту отправлено сообщение. Для подтверждения email перейдите по ссылке указанной в письме."
                    });
                }

            });
        }

        constructor(private $mdDialog:IDialogService, private $messageDialog:any) {

        }
    }

    angular.module("popUpWidgets.registerForm")
        .service('$registerFromType', $registerFrom);


}