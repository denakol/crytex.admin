/**
 * Created by denak on 26.01.2016.
 */
declare module user {
    import IPromise = angular.IPromise;
    interface IUserDialog {
        show(): IPromise<any>;
    }
}
