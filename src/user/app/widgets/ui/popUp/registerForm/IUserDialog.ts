/**
 * Created by denak on 26.01.2016.
 */
module user {
    import IPromise = angular.IPromise;
    export interface IUserDialog{
        show(message: any):IPromise<any>
    }
}