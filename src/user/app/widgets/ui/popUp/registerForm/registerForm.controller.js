var user;
(function (user) {
    var RegisterController = (function () {
        function RegisterController($mdDialog, AuthService, $state, $messageDialog, $loginForm) {
            this.$mdDialog = $mdDialog;
            this.AuthService = AuthService;
            this.$state = $state;
            this.$messageDialog = $messageDialog;
            this.$loginForm = $loginForm;
            this.submitted = false;
            this.regLoading = false;
        }
        RegisterController.prototype.close = function () {
            this.$mdDialog.cancel();
        };
        RegisterController.prototype.register = function (form) {
            var _this = this;
            this.submitted = true;
            if (form.$valid) {
                this.regLoading = true;
                this.AuthService.registerFirstStep(this.user).then(function () {
                    _this.$mdDialog.hide(true);
                }, function (response) {
                    _this.regLoading = false;
                    if (response == null) {
                        form.$error["serverError"] = true;
                        return;
                    }
                    form.$error[response.error] = true;
                });
            }
        };
        ;
        RegisterController.prototype.login = function () {
            var _this = this;
            this.$mdDialog.hide().then(function () {
                _this.$loginForm.show();
            });
        };
        RegisterController.$inject = ['$mdDialog', 'AuthService', '$state', '$messageDialog', '$loginForm'];
        return RegisterController;
    }());
    user.RegisterController = RegisterController;
    angular.module('popUpWidgets.registerForm')
        .controller('RegisterController', RegisterController);
})(user || (user = {}));
