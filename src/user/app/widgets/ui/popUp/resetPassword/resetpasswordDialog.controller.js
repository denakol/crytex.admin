var user;
(function (user) {
    var ResetPassword = core.ResetPassword;
    var ResetPasswordDialogController = (function () {
        function ResetPasswordDialogController($mdDialog, AuthService, $messageDialog, $stateParams) {
            this.$mdDialog = $mdDialog;
            this.AuthService = AuthService;
            this.$messageDialog = $messageDialog;
            this.user = new ResetPassword($stateParams.userId, $stateParams.code, "");
        }
        ResetPasswordDialogController.prototype.close = function () {
            this.$mdDialog.cancel();
        };
        ResetPasswordDialogController.prototype.reset = function () {
            var _this = this;
            this.AuthService.resetPassword(this.user).then(function () {
                _this.$mdDialog.hide(true);
            });
        };
        ;
        ResetPasswordDialogController.$inject = ['$mdDialog', 'AuthService', '$messageDialog', "$stateParams"];
        return ResetPasswordDialogController;
    }());
    user.ResetPasswordDialogController = ResetPasswordDialogController;
    angular.module('popUpWidgets.resetPasswordDialog')
        .controller('ResetPasswordDialogController', ResetPasswordDialogController);
})(user || (user = {}));
