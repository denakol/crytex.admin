/**
 * Created by ruslan on 28.01.2016.
 */
var user;
(function (user) {
    var ResetPasswordDialogService = (function () {
        function ResetPasswordDialogService($mdDialog, $messageDialog, $state) {
            this.$mdDialog = $mdDialog;
            this.$messageDialog = $messageDialog;
            this.$state = $state;
        }
        ResetPasswordDialogService.prototype.show = function () {
            var _this = this;
            return this.$mdDialog.show({
                controller: 'ResetPasswordDialogController',
                clickOutsideToClose: true,
                controllerAs: 'vm',
                templateUrl: "app/widgets/ui/popUp/resetPassword/resetPasswordDialog.html"
            }).then(function (result) {
                if (result) {
                    _this.$state.go('home');
                    _this.$messageDialog.show({
                        header: "Восстановление пароля",
                        body: "Ваш пароль успешно сменен на новый."
                    });
                }
            }, function () {
                _this.$state.go('home');
            });
        };
        ResetPasswordDialogService.$inject = ['$mdDialog', '$messageDialog', '$state'];
        return ResetPasswordDialogService;
    }());
    user.ResetPasswordDialogService = ResetPasswordDialogService;
    angular.module("popUpWidgets.resetPasswordDialog")
        .service('resetPasswordDialogService', ResetPasswordDialogService);
})(user || (user = {}));
