module user {

    import IAauthService = core.IAauthService;
    import LoginModel = core.LoginModel;
    import IDialogService = angular.material.IDialogService;
    import ResetPassword = core.ResetPassword;


    export class ResetPasswordDialogController {
        static $inject = ['$mdDialog', 'AuthService', '$messageDialog', "$stateParams"];

        public user:ResetPassword;

        constructor(private $mdDialog:IDialogService, private AuthService:IAauthService, private $messageDialog:any, $stateParams:any) {


            this.user = new ResetPassword($stateParams.userId, $stateParams.code, "");
        }

        close() {
            this.$mdDialog.cancel();
        }


        reset() {
            this.AuthService.resetPassword(this.user).then(()=> {
                this.$mdDialog.hide(true);
            });
        };
    }


    angular.module('popUpWidgets.resetPasswordDialog')
        .controller('ResetPasswordDialogController', ResetPasswordDialogController);

}
