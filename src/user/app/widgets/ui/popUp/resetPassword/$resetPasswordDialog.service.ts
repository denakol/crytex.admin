/**
 * Created by ruslan on 28.01.2016.
 */
module user {

    import IConfirmDialog = angular.material.IConfirmDialog;
    import IDialogService = angular.material.IDialogService;
    import IStateService = angular.ui.IStateService;


    export class ResetPasswordDialogService {

        static $inject = ['$mdDialog', '$messageDialog','$state'];

        show():angular.IPromise<any> {


            return this.$mdDialog.show({
                controller: 'ResetPasswordDialogController',
                clickOutsideToClose: true,
                controllerAs: 'vm',
                templateUrl: "app/widgets/ui/popUp/resetPassword/resetPasswordDialog.html"
            }).then((result)=> {
                if (result) {
                    this.$state.go('home');
                    this.$messageDialog.show({
                        header: "Восстановление пароля",
                        body: "Ваш пароль успешно сменен на новый."
                    });


                }
            },()=>{
                this.$state.go('home');
            });
        }

        constructor(private $mdDialog:IDialogService, private $messageDialog:any,public $state:IStateService) {

        }
    }

    angular.module("popUpWidgets.resetPasswordDialog")
        .service('resetPasswordDialogService', ResetPasswordDialogService);


}
