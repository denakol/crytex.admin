var user;
(function (user) {
    // Пример использования <button call-me="true" user-phone="userPhone">Заказать звонок</button>
    var CallMe = (function () {
        function CallMe(webService, mdDialog) {
            this.webService = webService;
            this.mdDialog = mdDialog;
            this.restrict = 'A';
            this.scope = {
                userPhone: '=userPhone'
            };
            this.link = function (scope, elem, attributes) {
                elem.bind('click', function () {
                    webService.UserPhoneCallRequest.save({ phoneNumber: scope.userPhone })
                        .$promise.then(function () {
                        showMessage(0);
                    }, function (response) {
                        if (response == null) {
                            showMessage(1);
                            return;
                        }
                        showMessage(2);
                    });
                });
                function showMessage(code) {
                    var messageTitle = "";
                    var messageBody = "";
                    switch (code) {
                        case 0:
                            messageTitle = "Ваша заявка принята";
                            messageBody = "Мы перезвоним вам в ближайшее время";
                            break;
                        case 1:
                            messageTitle = "Сервер недоступен";
                            messageBody = "Приносим вам свои извенениня";
                            break;
                        case 2:
                            messageTitle = "Неполадки на сервере";
                            messageBody = "Приносим вам свои извенениня";
                            break;
                    }
                    mdDialog.show(mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title(messageTitle)
                        .textContent(messageBody)
                        .ok('ОК'));
                    clearPhone();
                }
                function clearPhone() {
                    scope.userPhone = null;
                }
            };
        }
        CallMe.factory = function () {
            var directive = function (webService, mdDialog) { return new CallMe(webService, mdDialog); };
            directive.$inject = ['WebServices', '$mdDialog'];
            return directive;
        };
        return CallMe;
    })();
    user.CallMe = CallMe;
    angular.module("crytex.callMeWidget", [])
        .directive('callMe', CallMe.factory());
})(user || (user = {}));
//# sourceMappingURL=callMe.directive.js.map