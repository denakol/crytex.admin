module user {
    export interface ICallMeResult {
        errorCode: Number;
        errorText: String;
        messageHeader: String;
        messageText: String;
    }
}