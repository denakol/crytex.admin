declare module user {
    interface ICallMeResult {
        errorCode: Number;
        errorText: String;
        messageHeader: String;
        messageText: String;
    }
}
