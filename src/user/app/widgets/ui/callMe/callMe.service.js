var user;
(function (user) {
    var CallMeService = (function () {
        function CallMeService(webService) {
            this.webService = webService;
            this.result = {
                errorCode: 0,
                errorText: '',
                messageHeader: '',
                messageText: ''
            };
        }
        CallMeService.prototype.orderCall = function (userPhoneNumber) {
            var _this = this;
            return this.webService.UserPhoneCallRequest.save({ phoneNumber: userPhoneNumber })
                .$promise.then(function (data) {
                _this.result = {
                    errorCode: 0,
                    errorText: '',
                    messageHeader: 'Ваша заявка принята',
                    messageText: 'Мы перезвоним вам в ближайшее время'
                };
                return _this.result;
            }, function (response) {
                if (response == null) {
                    _this.result = {
                        errorCode: 1,
                        errorText: '',
                        messageHeader: 'Сервер недоступен',
                        messageText: 'Попробуйте позже'
                    };
                    return _this.result;
                }
                _this.result = {
                    errorCode: 1,
                    errorText: '',
                    messageHeader: 'Ошибка сервера',
                    messageText: 'Попробуйте позже'
                };
                return _this.result;
            });
        };
        CallMeService.$inject = ["WebServices"];
        return CallMeService;
    }());
    user.CallMeService = CallMeService;
    angular.module("crytex.callMeService", [])
        .service('callMeService', CallMeService);
})(user || (user = {}));
