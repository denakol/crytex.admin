declare module user {
    class CallMe implements ng.IDirective {
        private webService;
        private mdDialog;
        restrict: string;
        scope: {
            userPhone: string;
        };
        constructor(webService: any, mdDialog: any);
        link: (scope: ng.IScope, elem: ng.IAugmentedJQuery, attributes: ng.IAttributes) => void;
        static factory(): ng.IDirectiveFactory;
    }
}
