module user {

    export class CallMeService {
        static $inject = ["WebServices"];

        private result: ICallMeResult = {
            errorCode: 0,
            errorText: '',
            messageHeader: '',
            messageText: ''
        };

        constructor(private webService: any) {
        }

        public orderCall(userPhoneNumber: string) : any {
            return this.webService.UserPhoneCallRequest.save({phoneNumber: userPhoneNumber})
                .$promise.then( (data: any) => {
                    this.result = {
                        errorCode: 0,
                        errorText: '',
                        messageHeader: 'Ваша заявка принята',
                        messageText: 'Мы перезвоним вам в ближайшее время'
                    }
                    return this.result;
                },
                (response: any) => {
                    if (response == null) {
                        this.result = {
                            errorCode: 1,
                            errorText: '',
                            messageHeader: 'Сервер недоступен',
                            messageText: 'Попробуйте позже'
                        }
                        return this.result;
                    }
                    this.result = {
                        errorCode: 1,
                        errorText: '',
                        messageHeader: 'Ошибка сервера',
                        messageText: 'Попробуйте позже'
                    }
                    return this.result;
                }
            );
        }
    }

    angular.module("crytex.callMeService", [])
        .service('callMeService', CallMeService);
}