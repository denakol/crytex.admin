declare module user {
    class CallMeService {
        private webService;
        static $inject: string[];
        private result;
        constructor(webService: any);
        orderCall(userPhoneNumber: string): any;
    }
}
