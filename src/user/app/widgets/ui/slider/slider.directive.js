(function () {

    angular.module("crytex.sliderWidget", [])
        .directive("slider", slider);

    slider.$inject = [];

    function slider() {

        return {
            require: 'ngModel',
            scope: {
                show: '=',
                firstText: '@firstText',
                secondText: '@secondText',
                thirdText: '@thirdText',
                valueResult: '=ngModel'
            },
            restrict: "E",
            controller: sliderController,
            controllerAs: "vm",
            templateUrl: 'app/widgets/ui/slider/slider.html'
        };
    };

    sliderController.$inject = ['$scope'];

    function sliderController($scope) {
        var vm = this;

        $scope.slider = {
            options: {
                floor: 0,
                ceil: 100000
            }
        };
        $scope.mySlider = null;

        $scope.getNumber = getNumber;
        $scope.missionCompled = missionCompled;

        function getNumber (number) {
            return new Array(number);
        };

        function missionCompled () {
            var pointers = document.getElementsByClassName("rz-pointer");
            angular.forEach(pointers, function(pointer) {
                if (pointer.style.display != 'none') {
                    pointer.innerHTML = '<hr class="short-line"/><hr class="short-line"/><hr class="short-line"/>';
                }
            });
        };

    }

})();