(function () {
    angular.module('crytexUser.widgets')
        .directive("ripple", directive);

    directive.$inject = ["$document","$compile"];

    function directive($document,$compile) {

        return {

            link: function (scope, element, attrs, modelCtrl) {

                element.removeAttr("ripple");


                $compile(element)(scope);
            },
            restrict: "A"
        };
    };
})();