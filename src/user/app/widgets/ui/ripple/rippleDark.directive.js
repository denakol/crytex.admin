(function () {
    angular.module('crytexUser.widgets')
        .directive("rippleDark", directive);

    directive.$inject = ["$document","$compile"];

    function directive($document,$compile) {

        return {

            link: function (scope, element, attrs, modelCtrl) {

                element.removeAttr("ripple-dark");

                $compile(element)(scope);
            },
            restrict: "A"
        };
    };
})();/**
 * Created by denak on 26.01.2016.
 */
