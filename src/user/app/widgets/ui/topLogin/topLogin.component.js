var usefr;
(function (usefr) {
    var TopLoginController = (function () {
        function TopLoginController($messageDialog, $registerForm, $loginForm, AuthService, $state, $scope) {
            var _this = this;
            this.$messageDialog = $messageDialog;
            this.$registerForm = $registerForm;
            this.$loginForm = $loginForm;
            this.AuthService = AuthService;
            this.$state = $state;
            this.$scope = $scope;
            this.isAuth = false;
            $scope.$watch(function () { return AuthService.authentication.isAuth; }, function () {
                _this.checkState();
            });
        }
        TopLoginController.prototype.checkState = function () {
            if (this.AuthService.authentication.isAuth) {
                this.isAuth = true;
                this.userName = this.AuthService.authentication.userName;
            }
            else {
                this.isAuth = false;
            }
        };
        TopLoginController.prototype.register = function () {
            this.$registerForm.show();
        };
        TopLoginController.prototype.login = function () {
            this.$loginForm.show();
        };
        TopLoginController.prototype.logout = function () {
            this.AuthService.signOut();
            this.isAuth = false;
            this.$state.go('home');
        };
        TopLoginController.prototype.showPersonalAccount = function () {
            this.$state.go('personalAccount.myAccount');
        };
        TopLoginController.$inject = ['$messageDialog', '$registerFromType', '$loginForm', 'AuthService', '$state', '$scope'];
        return TopLoginController;
    }());
    usefr.TopLoginController = TopLoginController;
    var topLoginComponent = {
        restrict: 'AE',
        templateUrl: 'app/widgets/ui/topLogin/topLogin.html',
        controller: "TopLoginController",
        controllerAs: "vm"
    };
    angular.module("widgets.topLogin")
        .component("topLogin", topLoginComponent)
        .controller("TopLoginController", TopLoginController);
})(usefr || (usefr = {}));
