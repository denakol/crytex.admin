module usefr {

    import IAauthService = core.IAauthService;
    import IScope = angular.IScope;

    export class TopLoginController {

        static $inject = ['$messageDialog', '$registerFromType', '$loginForm', 'AuthService', '$state', '$scope'];

        public isAuth:boolean = false;
        public userName:string;

        constructor(private $messageDialog:any, private $registerForm:any,
                    private $loginForm:any, private AuthService:IAauthService,
                    private $state:any, private $scope:IScope) {

            $scope.$watch(()=>AuthService.authentication.isAuth, ()=> {
                this.checkState();
            })
        }

        public checkState() {
            if (this.AuthService.authentication.isAuth) {
                this.isAuth = true;
                this.userName = this.AuthService.authentication.userName;
            } else {
                this.isAuth = false;
            }
        }

        public register() {
            this.$registerForm.show();
        }

        public login() {
            this.$loginForm.show();
        }

        public logout() {
            this.AuthService.signOut();
            this.isAuth = false;
            this.$state.go('home');
        }

        public showPersonalAccount() {
            this.$state.go('personalAccount.myAccount');
        }

    }

    var topLoginComponent = {
        restrict: 'AE',
        templateUrl: 'app/widgets/ui/topLogin/topLogin.html',
        controller: "TopLoginController",
        controllerAs: "vm"
    };

    angular.module("widgets.topLogin")
        .component("topLogin", topLoginComponent)
        .controller("TopLoginController", TopLoginController);
}