﻿(function () {
    angular.module("crytexUser.blocks", [
        "blocksUser.router",
        "blocks.auth",
        "blocks.constantsService",
        "blocks.providers",
        "blocks.services",
        "blocksUser.services"
    ]);

})();