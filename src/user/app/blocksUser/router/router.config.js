﻿(function () {

    angular.module("blocksUser.router").config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/home');

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'app/pages/home/home.html',
                controller: 'HomeController',
                resolve: {
                    $title: function () {
                        return 'Домашняя страница';
                    }
                }
            }).state('basket', {
                url: '/basket',
                templateUrl: 'app/pages/basket/basket.html',
                controller: 'BasketController',
                resolve: {
                    $title: function () {
                        return 'Корзина';
                    }
                }
            }).state('support', {
            url: '/support',
            templateUrl: 'app/pages/support/support.html',
            controller: 'SupportController',
            controllerAs: 'vm',
            resolve: {
                $title: function () {
                    return 'Техподдержка';
                }
            }
            }).state('webHosting', {
                url: '/webHosting',
                templateUrl: 'app/pages/webHosting/webHosting.html',
                controller: 'webHostingController',
                controllerAs: 'vm',
                resolve: {
                    $title: function () {
                        return 'Веб хостинг';
                    }
                }
            }).state('game', {
                url: '/game',
                templateUrl: 'app/pages/game/game.html',
                controller: 'GameController',
                controllerAs: 'vm',
                resolve: {
                    $title: function () {
                        return 'Игровые сервера';
                    }
                }
            }).state('buyGame', {
                url: '/buyGame/:gameId',
                templateUrl: 'app/pages/game/buyGame/buyGame.html',
                controller: 'BuyGameController',
                controllerAs: 'vm',
                resolve: {
                    $title: function () {
                        return 'Покупка игрового сервера';
                    }
                }
            }).state('domen', {
                url: '/domen',
                templateUrl: 'app/pages/domen/domen.html',
                controller: 'DomenController',
                resolve: {
                    $title: function () {
                        return 'Регистрация домена';
                    }
                }
            }).state('replenishBalance', {
            url: '/replenishBalance',
            templateUrl: 'app/pages/replenishBalance/replenishBalance.html',
            controller: 'ReplenishBalanceController',
            controllerAs: 'vm',
            resolve: {
                $title: function () {
                    return 'Пополнение баланса';
                }
            }
        });
    }

})();