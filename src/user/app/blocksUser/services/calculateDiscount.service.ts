module user {

    import ResourceType = core.WebApi.Models.Enums.ResourceType;
    import DiscountViewModel = core.WebApi.Models.DiscountViewModel;
    import IQService = angular.IQService;
    import IPromise = angular.IPromise;
    
    export class CalculateDiscount {
        
        public static $inject = ["WebServices", "ResourceType", "$filter", "$q"];
        
        public discounts: DiscountViewModel;
        
        constructor(private WebServices:any, private ResourceType:any,
                    private $filter:any, private $q:IQService){
            this.discounts = WebServices.UserLongTermDiscountService.query();
        }

        public getDiscountSize(resouceType:number, monthCount:number){

            var discountItem = this.$filter('filter')(this.discounts, {
                monthCount: monthCount,
                resourceType: resouceType
            });

            return discountItem.length ? discountItem[0].discountSize : 0;
        }

        public getDiscountSizeWithWait(resouceType:number, monthCount:number):IPromise<number>{
            var deferred = this.$q.defer();

            this.discounts.$promise.then(()=> {
                    deferred.resolve( this.getDiscountSize(resouceType, monthCount));
                }
            );

            return deferred.promise;
        }
    }

    angular.module('blocksUser.calculateDiscountService', [])
        .service('calculateDiscountService', CalculateDiscount);
}