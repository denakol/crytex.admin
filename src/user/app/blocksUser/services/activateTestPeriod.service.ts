module user {

    import IResourceClass = angular.resource.IResourceClass;
    import IAauthService = core.IAauthService;
    import TestPeriodViewModel = core.WebApi.Models.TestPeriodViewModel;
    import IUserSubscriptionVirtualMachineService = core.IUserSubscriptionVirtualMachineService;

    export class TestPeriodService{

        static $inject = ["WebServices", "AuthService", "$q"];

        public TestPeriodViewModel: TestPeriodViewModel;
        private UserSubscriptionVirtualMachineService: IUserSubscriptionVirtualMachineService;

        constructor(private webService:any, private AuthService:IAauthService, private $q: any){
            this.UserSubscriptionVirtualMachineService = webService.UserSubscriptionVirtualMachineService;
        }

        public setRequestOnTestPeriod() {
            this.AuthService.authentication.isRequestTestPeriod = true;
        }

        public activateTestPeriod() {
            var deferred = this.$q.defer();

            this.TestPeriodViewModel = {
                userId: this.AuthService.authentication.userId,
                countDay: 30,
                cashAmount: 1000
            }

            this.UserSubscriptionVirtualMachineService.addTestPeriod(this.TestPeriodViewModel).$promise
            .then(() => {
                this.AuthService.authentication.isRequestTestPeriod = false;
                deferred.resolve(true);
            })
            .catch(function (response: any) {
                deferred.resolve(response);
            });

            return deferred.promise;
        }
    }

    angular.module('blocksUser.activateTestPeriodService', [])
        .service('testPeriodService', TestPeriodService);
}