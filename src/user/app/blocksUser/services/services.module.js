﻿(function () {

    angular.module("blocksUser.services", [
        "blocksUser.activateTestPeriodService",
        "blocksUser.calculateDiscountService"
    ]);

})();