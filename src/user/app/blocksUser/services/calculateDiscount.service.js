var user;
(function (user) {
    var CalculateDiscount = (function () {
        function CalculateDiscount(WebServices, ResourceType, $filter, $q) {
            this.WebServices = WebServices;
            this.ResourceType = ResourceType;
            this.$filter = $filter;
            this.$q = $q;
            this.discounts = WebServices.UserLongTermDiscountService.query();
        }
        CalculateDiscount.prototype.getDiscountSize = function (resouceType, monthCount) {
            var discountItem = this.$filter('filter')(this.discounts, {
                monthCount: monthCount,
                resourceType: resouceType
            });
            return discountItem.length ? discountItem[0].discountSize : 0;
        };
        CalculateDiscount.prototype.getDiscountSizeWithWait = function (resouceType, monthCount) {
            var _this = this;
            var deferred = this.$q.defer();
            this.discounts.$promise.then(function () {
                deferred.resolve(_this.getDiscountSize(resouceType, monthCount));
            });
            return deferred.promise;
        };
        CalculateDiscount.$inject = ["WebServices", "ResourceType", "$filter", "$q"];
        return CalculateDiscount;
    }());
    user.CalculateDiscount = CalculateDiscount;
    angular.module('blocksUser.calculateDiscountService', [])
        .service('calculateDiscountService', CalculateDiscount);
})(user || (user = {}));
