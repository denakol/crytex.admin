var user;
(function (user) {
    var TestPeriodService = (function () {
        function TestPeriodService(webService, AuthService, $q) {
            this.webService = webService;
            this.AuthService = AuthService;
            this.$q = $q;
            this.UserSubscriptionVirtualMachineService = webService.UserSubscriptionVirtualMachineService;
        }
        TestPeriodService.prototype.setRequestOnTestPeriod = function () {
            this.AuthService.authentication.isRequestTestPeriod = true;
        };
        TestPeriodService.prototype.activateTestPeriod = function () {
            var _this = this;
            var deferred = this.$q.defer();
            this.TestPeriodViewModel = {
                userId: this.AuthService.authentication.userId,
                countDay: 30,
                cashAmount: 1000
            };
            this.UserSubscriptionVirtualMachineService.addTestPeriod(this.TestPeriodViewModel).$promise
                .then(function () {
                _this.AuthService.authentication.isRequestTestPeriod = false;
                deferred.resolve(true);
            })
                .catch(function (response) {
                deferred.resolve(response);
            });
            return deferred.promise;
        };
        TestPeriodService.$inject = ["WebServices", "AuthService", "$q"];
        return TestPeriodService;
    }());
    user.TestPeriodService = TestPeriodService;
    angular.module('blocksUser.activateTestPeriodService', [])
        .service('testPeriodService', TestPeriodService);
})(user || (user = {}));
