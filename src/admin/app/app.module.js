﻿(function () {

	angular.module("crytex", [
        "crytex.core",
        "crytexAdmin.blocks",
        "crytex.layout",
        // pages
        "crytex.home",
        "crytex.signIn",
        "crytex.machine",
        "crytex.emailMessages",
        "crytex.money",
        "crytex.user",
        "crytex.logs",
        "crytex.preferences",
        "crytex.task",
        "crytex.statistic",
        "crytex.tariffOfVirtualMachine",
        "crytex.subscriptionVirtualMachine",
        "crytex.subscriptionGameServer",
        "crytex.singleSubscriptionGameServer",
        "crytex.subscriptionVirtualMachine",
        "crytex.singleSubscriptionVirtualMachine",
        "crytex.subscriptionGameServerComponent",
        "crytex.subscriptionGameServerComponents",
        "crytex.subscriptionVirtualMachineComponents",
        'templates',
            "crytexAdmin.widgets"
	]);

})();