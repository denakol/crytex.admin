﻿(function () {
    angular.module("crytexAdmin.widgets", [
         "crytex.numbersOnly",
         "crytex.confirmPassword",
         "crytex.monitoringChart",
         "crytex.discount",
         "crytex.filterLocalDate",
         "crytex.selectUser",
         "crytex.decimalDigit",
         "crytex.subscriptionWidgets",
         "crytex.viewFieldsWidgets"
         //may include 3d-party libraries
    ]);
})();