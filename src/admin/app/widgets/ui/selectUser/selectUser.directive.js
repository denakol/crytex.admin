(function () {
    
    angular
        .module('crytex.selectUser')
        .directive('selectUser', selectUser);

    selectUser.$inject = ['WebServices','ToastNotificationService'];

    function selectUser(WebServices, ToastNotificationService) {
        return {
            require: 'ngModel',
            scope: { user: '=ngModel'},
            link: function(scope, element, attrs) {
                scope.usersList = [];
                scope.searchText = searchText;

                scope.search = {
                    user: scope.user
                };

                scope.$watch('search.user', function() {
                    scope.user = scope.search.user;
                });

                //////////////////////////////////////////////////////////////
                function searchText(){
                    if(!scope.searchUserValue=="") {


                        scope.usersList = WebServices.AdminUserSearch.query({
                            searchValue: scope.searchUserValue
                        }).$promise
                            .then(function (data) {
                                return data;
                            })
                            .catch(function (error) {
                                ToastNotificationService('Не удалось выгрузить данные');
                            });
                    }
                    else
                    {
                        scope.usersList = [];
                    }

                };
            },
            restrict: "E",
            template: "<div class='md-padding'>" +
                            "<form ng-submit='$event.preventDefault()'>" + 
                                  "<md-autocomplete md-selected-item='search.user'" +
                                  "md-search-text='searchUserValue'" + 
                                  "md-search-text-change='searchText(searchUserValue)'" +                               
                                  "md-items='item in usersList'" +
                                  "md-item-text='item.userName'" +
                                  "placeholder='Ввведите имя или email'>" +
                                        "<md-item-template>" +
                                          "<span class='item-metadata'>" +
                                                "<span class='item-metastat'><strong>{{item.userName}} </strong>" +
                                                "<span class='item-metastat'><strong>{{item.email}}</strong>" +
                                           "</span>" +
                                        "</md-item-template>" +
                                        "<md-not-found>" +
                                          "Не найдено совпадений по '{{searchUserValue}}'." +
                                        "</md-not-found>" +
                                  "</md-autocomplete>" +                                       
                            "</form>" +
                      "</div>"
        };
    }

})();