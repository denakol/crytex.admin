﻿(function () {

    angular.module("subscriptionWidgets.updateMachineConfiguration")
        .directive("updateMachineConfigurationButton", updateMachineConfigurationButton);

    updateMachineConfigurationButton.$inject = ['$mdDialog', 'ToastNotificationService', 'WebServices', 'VmBillingAdmin'];

    function updateMachineConfigurationButton($mdDialog, ToastNotificationService, WebServices, VmBillingAdmin) {

        return {
            scope: {
                'userVm' : '=userVm',
                'success': '&success',
                'failed': '&failed'
            },
            link: function(scope, element, attrs) {

                scope.showConfigurationEdit = showConfigurationEdit;

                function showConfigurationEdit(index) {

                    $mdDialog.show({
                        controller: DialogVirtualMachineConfigurationCtrl,
                        templateUrl: "updateMachineConfiguration.tmpl.html",
                        parent: angular.element(document.body),
                        targetEvent: index,
                        clickOutsideToClose: true,
                        locals: {
                            userVm: scope.userVm
                        }


                    })
                    .then(function (answer) {
                        updateVmConfiguration(answer);
                    }, function () {
                    });
                }

                function updateVmConfiguration(machine){
                    switch(machine.whatChanges){
                        case 'cpuRamHdd':
                            updateCpuRamHdd(machine);
                            break;
                        case 'backapingStoring':
                            updateBackapingStoring(machine);
                            break;
                        case 'both':
                            updateVmBothConfiguration(machine);
                            break;
                    }
                }
                
                function updateCpuRamHdd(machine) {
                    VmBillingAdmin.updateMachineConfiguration(
                        machine.subscriptionId,
                        machine.cpu,
                        machine.ram,
                        machine.hdd
                        )
                        .then(function(data){
                            scope.success();
                            ToastNotificationService('Добавлено');
                        })
                        .catch(function(error){

                            ToastNotificationService('Не удалось выгрузить данные');
                        });
                }

                function updateBackapingStoring(machine) {
                    VmBillingAdmin.updateSubscriptionBackupStoragePeriod(
                        machine.subscriptionId,
                        machine.backapingStoring
                    )
                    .then(function(data){
                        scope.success();
                        ToastNotificationService('Добавлено');
                    })
                    .catch(function(error){
                        ToastNotificationService('Не удалось выгрузить данные');
                    });
                }
                
                function updateVmBothConfiguration(machine) {
                    VmBillingAdmin.updateMachineConfiguration(
                        machine.subscriptionId,
                        machine.cpu,
                        machine.ram,
                        machine.hdd
                        )
                        .then(function(data){
                            VmBillingAdmin.updateSubscriptionBackupStoragePeriod(
                                machine.subscriptionId,
                                machine.backapingStoring
                                )
                                .then(function(data){
                                    scope.success();
                                    ToastNotificationService('Добавлено');
                                })
                                .catch(function(error){
                                    ToastNotificationService('Не удалось выгрузить данные');
                                });
                        })
                        .catch(function(error){
                            ToastNotificationService('Не удалось выгрузить данные');
                        });
                }

            },
            restrict: "E",
            templateUrl: 'app/widgets/ui/subscription/updateMachineConfigurationButton/updateMachineConfigurationButton.html'
        };

    }

    function DialogVirtualMachineConfigurationCtrl($scope, $mdDialog, userVm, ToastNotificationService) {
        
        $scope.machine = {};
        $scope.operatingSystem = userVm.operatingSystem;

        $scope.machine.cpu = userVm.coreCount;
        $scope.machine.ram = userVm.ramCount;
        $scope.machine.hdd = userVm.hardDriveSize;
        $scope.machine.subscriptionId = userVm.id;
        $scope.machine.backapingStoring = userVm.backapingStoring;


        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.answer = function () {
            var whatChanges = checkInfo();
            if (whatChanges) {
                $scope.machine.whatChanges = whatChanges;
                $mdDialog.hide($scope.machine);
            }
        };

        function checkInfo() {
            if ($scope.machine.cpu >= $scope.operatingSystem.minCoreCount && $scope.machine.ram >= $scope.operatingSystem.minRamCount && $scope.machine.hdd >= $scope.operatingSystem.minHardDriveSize && typeof($scope.machine.backapingStoring) != 'undefined') {
                if($scope.machine.cpu != userVm.coreCount || $scope.machine.ram != userVm.ramCount || $scope.machine.hdd != userVm.hardDriveSize){
                    if($scope.machine.backapingStoring != userVm.backapingStoring){
                        return 'both';
                    }
                    return 'cpuRamHdd';
                } else if($scope.machine.backapingStoring != userVm.backapingStoring){
                    return 'backapingStoring';
                }
                else {
                    ToastNotificationService('Должен быть изменен хотябы один параметр машины');
                    return false;
                }
            } else {
                ToastNotificationService('Необходимо корректно заполнить все поля');
                return false;
            }
        };
    }
})();
