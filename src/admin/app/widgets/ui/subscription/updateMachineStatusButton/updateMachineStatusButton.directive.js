﻿(function () {

    angular.module("subscriptionWidgets.updateMachineStatus")
        .directive("updateMachineStatusButton", updateMachineStatusButton);

    updateMachineStatusButton.$inject = ['ToastNotificationService', 'WebServices', 'ManageMachine'];

    function updateMachineStatusButton(ToastNotificationService, WebServices, ManageMachine) {

        return {
            scope: {
                'vmId' : '=vmId',
                'success': '&success',
                'failed': '&failed',
                'pending': '=pending'
            },
            link: function(scope, element, attrs) {

                scope.manageMachine = ManageMachine.arrayItems;

                var originatorEv;
                scope.openMenu = function ($mdOpenMenu, ev) {
                    originatorEv = ev;
                    $mdOpenMenu(ev);
                };

                scope.changeState = function (value) {

                    WebServices.AdminSubscriptionVirtualMachineService.updateMachineStatus({
                            status: value,
                            subscriptionId: scope.vmId

                        },
                        function success(data) {

                            ToastNotificationService('Статус изменен');
                            scope.success();
                        },
                        function err(error) {

                            scope.failed();
                            ToastNotificationService('Не удалось изменить статус');
                        });
                    scope.pending = true;
                };
            },
            restrict: "E",
            templateUrl: 'app/widgets/ui/subscription/updateMachineStatusButton/updateMachineStatusButton.html'
        };

    }

})();
