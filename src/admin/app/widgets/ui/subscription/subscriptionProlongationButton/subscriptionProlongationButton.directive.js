﻿(function () {

    angular.module("subscriptionWidgets.subscriptionProlongation")
        .directive("subscriptionProlongationButton", subscriptionProlongationButton);

    subscriptionProlongationButton.$inject = ['$mdDialog', 'ToastNotificationService', 'WebServices'];

    function subscriptionProlongationButton($mdDialog, ToastNotificationService, WebServices) {

        return {
            scope: {
                'vmId' : '=vmId',
                'success': '&success',
                'failed': '&failed'
            },
            link: function(scope, element, attrs) {
                scope.showProlongateEdit = showProlongateEdit;

                function showProlongateEdit(index) {
                    $mdDialog.show({
                        controller: DialogProlongateCtrl,
                        templateUrl: "prolongateSubscription.tmpl.html",
                        parent: angular.element(document.body),
                        targetEvent: index,
                        clickOutsideToClose: true,
                        locals: {
                            subscriptionId: scope.vmId
                        }


                    })
                        .then(function (answer) {
                            prolongateSubscriptionVm(answer);
                        }, function () {
                        });
                }

                function prolongateSubscriptionVm(prolongateVm){
                    WebServices.AdminSubscriptionVirtualMachineService.extendSubscription({
                            subscriptionId: prolongateVm.subscriptionVmId,
                            monthCount: prolongateVm.MonthCount},
                        function (data) {

                            scope.success();
                            ToastNotificationService('Добавлено');
                        },
                        function (error) {

                            ToastNotificationService('Не удалось выгрузить данные');
                        }
                    );
                }

            },
            restrict: "E",
            templateUrl: 'app/widgets/ui/subscription/subscriptionProlongationButton/subscriptionProlongationButton.html'
        };

    }


    function DialogProlongateCtrl($scope, $mdDialog, subscriptionId) {

        $scope.prolongateVm = {subscriptionVmId: subscriptionId};

        ////Functions
        $scope.cancel = cancel;
        $scope.answer = answer;


        function cancel() {
            $mdDialog.cancel();
        };

        function answer() {
            $mdDialog.hide($scope.prolongateVm);
        };
    }
})();
