﻿(function () {

    angular.module("subscriptionWidgets.subscriptionBuy")
        .directive("subscriptionBuyButton", subscriptionBuyButton);

    subscriptionBuyButton.$inject = ['$mdDialog', 'ToastNotificationService', 'WebServices'];

    function subscriptionBuyButton($mdDialog, ToastNotificationService, WebServices) {

        return {
            scope: {
                'userId' : '=userId',
                'success': '&success',
                'failed': '&failed'
            },
            link: function(scope, element, attrs) {
                scope.showSubscriptionEdit = showSubscriptionEdit;

                function showSubscriptionEdit(index) {
                    $mdDialog.show({
                        controller: DialogSubscriptionBuyCtrl,
                        templateUrl: "addSubscriptionVmForm.tmpl.html",
                        parent: angular.element(document.body),
                        targetEvent: index,
                        clickOutsideToClose: true,
                        locals: {
                            userId: (scope.userId) ? scope.userId : null
                        }
                    })
                        .then(function (answer) {
                            BuySubscriptionVm(answer);
                        }, function () {

                        });
                }

                function BuySubscriptionVm(subscriptionVm){
                    if (!subscriptionVm.autoProlongation){
                        subscriptionVm.autoProlongation = false;
                    }
                    WebServices.AdminSubscriptionVirtualMachineService.save(subscriptionVm,
                        function (data) {
                            ToastNotificationService('Добавлено');
                            scope.success();

                        },
                        function (error) {
                            ToastNotificationService('Не удалось выгрузить данные');
                            scope.failed();
                        }
                    );
                }

            },
            restrict: "E",
            templateUrl: 'app/widgets/ui/subscription/subscriptionBuyButton/subscriptionBuyButton.html'
        };

    }


    function DialogSubscriptionBuyCtrl($scope, userId, $mdDialog, TypeVirtualization, SubscriptionType, WebServices, ToastNotificationService) {
        $scope.subscriptionVm = {};
        $scope.typeVirtualization = TypeVirtualization;
        $scope.operatingSystem = [];
        $scope.subscriptionType = SubscriptionType;
        $scope.user = null;
        $scope.typeDisk = 'HDD';
        getOS();

        if(userId){
            $scope.subscriptionVm.userId = userId;
        }
        ////Functions
        $scope.cancel = cancel;
        $scope.answer = answer;


        function getOS() {
            WebServices.AdminOperatingSystem.query({},
                function success(os) {
                    $scope.operatingSystem = os;
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        };

        function cancel() {
            $mdDialog.cancel();
        };

        function answer() {
            if(!$scope.subscriptionVm.userId){
                if($scope.user){
                    $scope.subscriptionVm.userId = $scope.user ? $scope.user.id : null;
                }
                else return false;
            }
            $mdDialog.hide($scope.subscriptionVm);
        };
    }
})();
