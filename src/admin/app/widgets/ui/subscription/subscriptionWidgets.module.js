﻿(function () {

    angular.module("crytex.subscriptionWidgets", [
        "subscriptionWidgets.subscriptionBuy",
        "subscriptionWidgets.subscriptionProlongation",
        "subscriptionWidgets.updateMachineStatus",
        "subscriptionWidgets.updateMachineConfiguration"
    ]);

})();