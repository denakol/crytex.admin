﻿(function () {

    angular.module("crytex.discount")
        .directive("mdDiscount", discount);

    discount.$inject = ['WebServices' ,'ToastNotificationService', 'ResourceType', 'TypeDiscount'];

    function discount(WebServices, ToastNotificationService, ResourceType, TypeDiscount) {

        return {
            scope: {
                'discounts' : '=discounts',
                'header' : '=header',
                'type' : '=type',
                'resourceType' : '=resourceType',
                'symbol' : '=symbol'
            },
            link: function(scope, element, attrs) {
                scope.try = false;

                scope.addDiscount = function(discount, form){
                    scope.try = true;
                    if(form.$valid){
                        discount.disable = true;
                        discount.discountType = scope.type;
                        discount.resourceType = scope.resourceType;

                        WebServices.AdminDiscountService.save(discount,
                            function (data) {
                                discount.count = null;
                                discount.discountSize = null;

                                scope.discounts.splice(0, 0, data);
                                scope.try = false;
                                ToastNotificationService('Добавлено');
                            },
                            function (error) {
                                if (!isNaN(error)) {
                                    ToastNotificationService('Не удалось выгрузить данные');
                                }
                            }
                        );
                    }
                };
                scope.blockDiscountType = function(){
                    blockType(true);
                };

                scope.unBlockDiscountType = function(){
                    blockType(false);
                };
                scope.startEdit = function(discount){
                    discount.DiscountSizeEdit = discount.discountSize;
                    discount.CountEdit = discount.count;
                    discount.edit = true;
                    discount.try = true;
                };
                scope.backEdit = function(discount){
                    discount.edit = false;
                    discount.try = false;
                };
                scope.endEdit = function(discount, form_repeat){
                    if(form_repeat.$valid){
                        discount.discountSize = discount.DiscountSizeEdit;
                        discount.count = discount.CountEdit;
                        discountUpdate(discount);
                        discount.try = false;
                        discount.edit = false;
                    }
                };
                scope.removeDiscount = function(discount){
                    WebServices.AdminDiscountService.delete({id: discount.id},
                        function (ok) {
                            // Удаляем из списка
                            var index = scope.discounts.map(function (e) {
                                return e.id;
                            }).indexOf(discount.id);
                            scope.discounts.splice(index, 1);
                            ToastNotificationService('Удалено');
                        },
                        function (error) {
                            ToastNotificationService('Не удалось выгрузить данные');
                        }
                    );
                };
                scope.unBlockDiscount = function(discount){
                    discount.disable = false;
                    discountUpdate(discount);
                };
                scope.blockDiscount = function(discount){
                    discount.disable = true;
                    discountUpdate(discount);
                };


                function discountUpdate(discount){
                    WebServices.AdminDiscountService.update(discount.id, discount,
                        function (data) {
                            var index = scope.discounts.map(function (e) {
                                return e.id;
                            }).indexOf(discount.id);
                            scope.discounts[index] = discount;
                            ToastNotificationService('Добавлено');
                        },
                        function (error) {
                            if (!isNaN(error)) {
                                ToastNotificationService('Не удалось выгрузить данные');
                            }
                        });
                };

                function blockType(disable){
                    WebServices.AdminDiscountService.blockType({typeDiscount : scope.type, disable: disable},
                        function (data) {
                            for (var i in scope.discounts) {
                                if(scope.discounts[i].discountType == scope.type) scope.discounts[i].disable = disable;
                            }
                            ToastNotificationService('Добавлено');
                        },
                        function (error) {
                            if (!isNaN(error)) {
                                ToastNotificationService('Не удалось выгрузить данные');
                            }
                        });
                };
            },
            restrict: "E",
            templateUrl: 'app/widgets/ui/discount/discount.directive.html'
        };

    }
})();
