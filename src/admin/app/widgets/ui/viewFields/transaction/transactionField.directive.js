﻿(function () {

    angular.module("viewFieldsWidgets.transactionFieldDirective")
        .directive("transactionField", transactionField);

    transactionField.$inject = ['TransactionType'];

    function transactionField(TransactionType) {

        return {
            scope: {
                'transaction' : '=transaction',
            },
            link: function(scope, element, attrs) {
                scope.show = false;
                scope.transactionType = TransactionType;

                scope.changePresentation = function(){
                    scope.show = !scope.show;
                }
            },
            restrict: "E",
            templateUrl: 'app/widgets/ui/viewFields/transaction/transactionField.html'
        };

    }
})();
