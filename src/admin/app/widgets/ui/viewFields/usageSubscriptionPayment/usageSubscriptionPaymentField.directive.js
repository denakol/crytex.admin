﻿(function () {

    angular.module("viewFieldsWidgets.usageSubscriptionPaymentDirective")
        .directive("usageSubscriptionPaymentField", usageSubscriptionPaymentField);

    usageSubscriptionPaymentField.$inject = [];

    function usageSubscriptionPaymentField() {

        return {
            scope: {
                'usageVm' : '=cloudSubscriptionPayment',
                'periodType': '=periodType'
            },
            link: function(scope, element, attrs) {
                scope.show = false;

                scope.changePresentation = function(){
                    scope.show = !scope.show;
                }
            },
            restrict: "E",
            templateUrl: 'app/widgets/ui/viewFields/usageSubscriptionPayment/usageSubscriptionPaymentField.html'
        };

    }
})();
