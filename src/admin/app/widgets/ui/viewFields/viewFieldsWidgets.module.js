﻿(function () {

    angular.module("crytex.viewFieldsWidgets", [
        "viewFieldsWidgets.paymentFieldDirective",
        "viewFieldsWidgets.transactionFieldDirective",
        "viewFieldsWidgets.fixedSubscriptionPaymentDirective",
        "viewFieldsWidgets.usageSubscriptionPaymentDirective",
    ]);

})();