﻿(function () {

    angular.module("viewFieldsWidgets.fixedSubscriptionPaymentDirective")
        .directive("fixedSubscriptionPaymentField", fixedSubscriptionPaymentField);

    fixedSubscriptionPaymentField.$inject = ['TypeVirtualization', 'OSSource'];

    function fixedSubscriptionPaymentField(TypeVirtualization, OSSource) {

        return {
            scope: {
                'subscriptionPayment' : '=vpsSubscriptionPayment',
            },
            link: function(scope, element, attrs) {
                scope.show = false;
                scope.virtualizationType = TypeVirtualization;
                scope.operatingSystems = OSSource;

                scope.changePresentation = function(){
                    scope.show = !scope.show;
                }
            },
            restrict: "E",
            templateUrl: 'app/widgets/ui/viewFields/fixedSubscriptionPayment/fixedSubscriptionPaymentField.html'
        };

    }
})();
