﻿(function () {

    angular.module("viewFieldsWidgets.paymentFieldDirective")
        .directive("paymentField", paymentField);

    paymentField.$inject = ['PaymentType'];

    function paymentField(PaymentType) {

        return {
            scope: {
                'payment' : '=payment',
            },
            link: function(scope, element, attrs) {
                scope.show = false;
                scope.paymentType = PaymentType;

                scope.changePresentation = function(){
                    scope.show = !scope.show;
                }
            },
            restrict: "E",
            templateUrl: 'app/widgets/ui/viewFields/payment/paymentField.html'
        };

    }
})();
