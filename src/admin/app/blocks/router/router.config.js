﻿(function () {

    angular.module("crytexAdmin.router").config(config);

    config.$inject = [ '$stateProvider', '$urlRouterProvider' ];

    function config($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/home');

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'app/pages/home/home.html',
                controller: 'HomeController',
                resolve: {
                  $title: function() { return 'Домашняя страница'; }
                }
            })
            .state('emailMessages', {
                url: '/emailMessages',
                templateUrl: 'app/pages/emailMessages/emailMessages.html',
                controller: 'EmailMessagesController',
                resolve: {
                  $title: function() { return 'Просмотр сообщений'; }
                }
            })
            .state('moneyStatistic', {
                url: '/money/statistic',
                templateUrl: 'app/pages/money/statistic/statistic.money.html',
                controller: 'MoneyStatisticController',
                resolve: {
                  $title: function() { return 'Статистика денежных средств'; }
                }
            })
            .state('moneyUsers', {
                url: '/money/users',
                templateUrl: 'app/pages/money/users/users.money.html',
                controller: 'MoneyUsersController',
                resolve: {
                  $title: function() { return 'Денежные средства пользователей'; }
                }
            })
            .state('users', {
                url: '/users',
                templateUrl: 'app/pages/user/list/list.user.html',
                controller: 'UserListController',
                resolve: {
                  $title: function() { return 'Просмотр пользователей'; }
                }
            })
            .state('usersSingle', {
                url: '/users/single/:id',
                templateUrl: 'app/pages/user/single/single.user.html',
                controller: 'UserSingleController',
                resolve: {
                  $title: function() { return 'Пользователи'; }
                }
            })
            .state('userHelpDeskRequests', {
                url: '/users/helpdesk',
                templateUrl: 'app/pages/user/helpDeskRequests/helpDeskRequests.user.html',
                controller: 'UserHelpDeskRequestsController',
                resolve: {
                  $title: function() { return 'Обращения пользователей'; }
                }
            })
            .state('userHelpDeskRequest', {
                url: '/users/helpdesk/:id',
                templateUrl: 'app/pages/user/helpDeskRequest/helpDeskRequest.user.html',
                controller: 'UserHelpDeskRequestController'
            })
            .state('usersStatistic', {
                url: '/users/statistic',
                templateUrl: 'app/pages/user/statistic/statistic.user.html',
                controller: 'UserStatisticController',
                resolve: {
                  $title: function() { return 'Статистика пользователей'; }
                }
            })
            .state('logs', {
                url: '/logs',
                templateUrl: 'app/pages/logs/logs.html',
                controller: 'LogsController',
                resolve: {
                  $title: function() { return 'Логи'; }
                }
            })
            .state('subscriptionVirtualMachines', {
                url: '/subscriptionVirtualMachine',
                templateUrl: 'app/pages/subscriptions/vm/virtualMachine/virtualMachine.html',
                controller: 'subscriptionVirtualMachineController',
                resolve: {
                  $title: function() { return 'Подписки виртуальных машин'; }
                }
            })
            .state('subscriptionVirtualMachine', {
                url: '/subscriptionVirtualMachine/:id',
                templateUrl: 'app/pages/subscriptions/vm/singleVirtualMachine/singleVirtualMachine.html',
                controller: 'singleSubscriptionVirtualMachineController as vm',
                resolve: {
                  $title: function() { return 'Подписка виртуальной машины'; }
                }
            })
            .state('subscriptionGameServers', {
                url: '/subscriptionGameServer',
                templateUrl: 'app/pages/subscriptions/game/gameServer/gameServer.html',
                controller: 'subscriptionGameServerController as vm',
                resolve: {
                  $title: function() { return 'Подписки игровых серверов'; }
                }
            })
            .state('subscriptionGameServer', {
                url: '/subscriptionGameServer/:id',
                templateUrl: 'app/pages/subscriptions/game/singleGameServer/singleGameServer.html',
                controller: 'singleSubscriptionGameServerController as vm',
                resolve: {
                  $title: function() { return 'Подписка игрового сервера'; }
                }
            })
            .state('preferencesMachineTemplates', {
                url: "/preferences/machineTemplates",
                views: {
                    '': {
                        templateUrl: 'app/pages/preferences/machineTemplates/machineTemplates.html'
                    },
                    'operatingSystemView@preferencesMachineTemplates': {
                        controller: 'OperatingSystemController',
                        templateUrl: 'app/pages/preferences/machineTemplates/operatingSystem/operatingSystem.machineTemplates.html'
                    },
                    'serverTemplateView@preferencesMachineTemplates': {
                        controller: 'ServerTemplateController',
                        templateUrl: 'app/pages/preferences/machineTemplates/serverTemplate/serverTemplate.machineTemplates.html'
                    }
                },
                resolve: {
                  $title: function() { return 'Шаблоны машин'; }
                }
            })
            /*.state('preferencesConfigurationHyperV', {
                url: '/preferences/configurationHyperV',
                templateUrl: 'app/pages/preferences/configurationHyperV/configurationHyperV.html',
                controller: 'ConfigurationHyperVController',
                resolve: {
                  $title: function() { return 'Конфигурация HYPER V'; }
                }
            })*/
            .state('preferencesConfigurationHyperV', {
                url: '/preferences/hyperVHost',
                templateUrl: 'app/pages/preferences/HyperVHost/HyperVHost.html',
                controller: 'HyperVHostController as vm',
                resolve: {
                    $title: function() { return 'Вывод HyperVHost модели'; }
                }
            })
            .state('preferencesTariff', {
                url: '/preferences/tariff',
                //template: 'Настройка - тарифы'
                templateUrl: "app/pages/preferences/test.html",
                resolve: {
                  $title: function() { return 'Настройка тарифов'; }
                }
            })
            .state('preferencesGameMachines', {
                url: '/preferences/gameMachines',
                views: {
                    '': {
                        templateUrl: 'app/pages/preferences/gameMachines/gameMachines.html'
                    },
                    'gameView@preferencesGameMachines': {
                        controller: 'GameController',
                        controllerAs: 'vm',
                        templateUrl: 'app/pages/preferences/gameMachines/game/game.template.html'
                    },
                    'hostView@preferencesGameMachines': {
                        controller: 'HostController',
                        controllerAs: 'vm',
                        templateUrl: 'app/pages/preferences/gameMachines/host/host.template.html'
                    }
                },
                resolve: {
                    $title: function() { return 'Настройка игровых машин'; }
                }
            })
            .state('singleGame', {
                url: '/preferences/gameMachines/singleGame/:id',
                templateUrl: 'app/pages/preferences/gameMachines/singleGame/game.single.html',
                controller: 'SingleGameController',
                controllerAs: 'vm',
                resolve: {
                    $title: function() { return 'Настройка игры'; }
                }
            })
            .state('preferencesConfigurationVmWare', {
                url: '/preferences/configurationVmWare',
                templateUrl: 'app/pages/preferences/configurationVmWareCenter/configurationVmWareCenter.html',
                controller: 'ConfigurationVmWareController',
                resolve: {
                  $title: function() { return 'Конфигурация WMWARE'; }
                }
            })
            .state('preferencesEmailTemplates', {
                url: '/preferences/emailTemplates',
                templateUrl: 'app/pages/preferences/emailTemplates/emailTemplate.html',
                controller: 'emailTemplateListController',
                resolve: {
                  $title: function() { return 'Шаблоны email'; }
                }
            })
            .state('tariffOfVirtualMachine', {
                url: '/tariffOfVirtualMachine',  
                templateUrl: 'app/pages/tariffOfVirtualMachine/tariffOfVirtualMachine.html',              
                controller: 'tariffOfVirtualMachineController',
                resolve: {
                  $title: function() { return 'Тарифы виртуальных машин'; }
                }
            })
            .state('preferencesFileManager', {
                url: '/preferences/fileManager',
                template: 'Настройка - управление файлами',
                resolve: {
                  $title: function() { return 'Управление файлами'; }
                }
            })
            .state('news', {
                url: '/preferences/news',
                templateUrl: 'app/pages/preferences/news/news.html',
                controller: 'AdminNewsController as news',
                resolve: {
                    $title: function() { return 'Новости'; }
                }
            })
            .state('preferencesLocations', {
                url: '/preferences/locations',
                templateUrl: 'app/pages/preferences/locations/locations.template.html',
                controller: 'AdminLocationsController as vm',
                resolve: {
                    $title: function() { return 'Локации'; }
                }
            })
            .state('task', {
                url: '/task',
                templateUrl: 'app/pages/task/task.html',
                controller: 'TaskController',
                resolve: {
                  $title: function() { return 'Задачи'; }
                }
            })
            .state('statisticGeneral', {
                url: '/statistic/',
                templateUrl: 'app/pages/statistic/general/general.html',
                controller: 'GeneralController',
                resolve: {
                  $title: function() { return 'Общая статистика'; }
                }
            })
            .state('statisticVirtualMachine', {
                url: '/statistic/virtualMachine',
                templateUrl: 'app/pages/statistic/statisticVm/statisticVm.html',
                controller: 'StatisticVmController',
                resolve: {
                  $title: function() { return 'Статистика виртуальных машин'; }
                }
            })
            .state('statisticUser', {
                url: '/statistic/user',
                template: 'Статистика Пользователей',
                resolve: {
                  $title: function() { return 'Статистика пользователей'; }
                }
            })
            .state('statisticBilling', {
                url: '/statistic/billing',
                template: 'Статистика Billing',
                resolve: {
                  $title: function() { return 'Статистика Billing'; }
                }
            })
            .state('statisticReport', {
                url: '/statistic/report',
                template: 'Статистика отчётов',
                resolve: {
                  $title: function() { return 'Статистика отчётов' }
                }
            })
            .state('moneyPayment', {
                url: '/money/payment',
                templateUrl: 'app/pages/money/payment/payment.html',
                controller: 'PaymentController',
                resolve: {
                  $title: function() { return 'Пополнение денежных средств'; }
                }
            })
            .state('userCalls', {
                url: '/users/calls',
                templateUrl: 'app/pages/user/phoneCalls/phoneCall.user.html',
                controller: 'PhoneCallController',
                resolve: {
                  $title: function() { return 'Перезвони мне'; }
                }
            })
            .state('paymentBillingTransaction', {
                url: '/payment/billingTransaction',
                templateUrl: 'app/pages/money/billingTransaction/billingTransaction.html',
                controller: 'BillingTransactionController',
                resolve: {
                  $title: function() { return 'Транзакции пользователей'; }
                }
            })
            .state('moneySystem', {
                url: '/money/moneySystem',
                templateUrl: 'app/pages/money/moneySystem/moneySystem.html',
                controller: 'MoneySystemController',
                controllerAs: 'vm',
                resolve: {
                    $title: function() { return 'Денежные системы'; }
                }
            })
            .state('virtualMachines', {
                url: '/virtualMachines',
                templateUrl: 'app/pages/virtualMachines/virtualMachines/virtualMachines.html',
                controller: 'VirtualMachinesController',
                resolve: {
                    $title: function() { return 'Просмотр виртуальных машин'; }
                }
            })
            .state('virtualMachine', {
                url: '/virtualMachines/:id',
                templateUrl: 'app/pages/virtualMachines/virtualMachine/virtualMachine.html',
                controller: 'VirtualMachineController'
            })
            .state('moneyDiscounts', {
                url: '/money/discounts',
                templateUrl: 'app/pages/money/discounts/discounts.money.html',
                controller: 'DiscountsController',
                controllerAs: 'vm'
            })
            .state('virtualMachinePayment', {
                url: '/money/virtualMachinePayment',
                templateUrl: 'app/pages/money/virtualMachinePayment/virtualMachinePayment.html',
            })
            .state('virtualMachinePayment.cloud', {
                url: '/cloud',
                controller: 'CloudPaymentController',
                templateUrl: 'app/pages/money/virtualMachinePayment/cloud/cloudPayment.html'
            })
            .state('virtualMachinePayment.vps', {
                url: '/vps',
                controller: 'VpsPaymentController',
                templateUrl: 'app/pages/money/virtualMachinePayment/vps/vpsPayment.html'
            })
        ;

    };

})();