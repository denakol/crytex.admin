(function () {

    angular.module("crytex").run(running);

    running.$inject = ['amMoment'];

    function running(amMoment) {
        amMoment.changeLocale('ru');
    };

})();
