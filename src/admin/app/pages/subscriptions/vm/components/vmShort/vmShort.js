var admin;
(function (admin) {
    var SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    var vmShortController = (function () {
        function vmShortController(BASE_INFO, statusVm, subscriptionType, cloudCalculator) {
            var _this = this;
            this.BASE_INFO = BASE_INFO;
            this.statusVm = statusVm;
            this.subscriptionType = subscriptionType;
            this.cloudCalculator = cloudCalculator;
            this.subscriptionTypes = SubscriptionType;
            this.vmOption = core.VmOption.GetFromVm(this.virtualMachine.userVm, this.virtualMachine.subscriptionType);
            cloudCalculator.CalculatePriceWithWait(this.vmOption).then(function (result) {
                _this.currentPriceVm = result;
            });
        }
        vmShortController.$inject = [
            "BASE_INFO",
            "StatusVm",
            "SubscriptionType",
            "cloudCalculator"
        ];
        return vmShortController;
    }());
    admin.vmShortController = vmShortController;
    var options = {
        restrict: "EA",
        bindings: {
            virtualMachine: '<',
            single: '<',
            showEditVm: '&',
            showConfirmVm: '&',
            prolongateSuccess: '&'
        },
        templateUrl: 'app/pages/subscriptions/vm/components/vmShort/vmShort.html',
        controller: vmShortController,
        controllerAs: 'vm'
    };
    angular.module("crytex.subscriptionVirtualMachineComponents")
        .component('vmShort', options);
})(admin || (admin = {}));
