module admin {

    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;

    export class vmShortController {

        public static $inject = [
            "BASE_INFO",
            "StatusVm",
            "SubscriptionType",
            "cloudCalculator"
        ];
        public subscriptionTypes : typeof SubscriptionType = SubscriptionType;

        public virtualMachine:SubscriptionVmViewModel;

        public currentPriceVm:core.PriceVm;

        public vmOption: core.VmOption;
        
        constructor(public BASE_INFO:any,
                    public statusVm:any,
                    public subscriptionType:any,
                    private cloudCalculator: core.CloudCalculator){


            this.vmOption = core.VmOption.GetFromVm(this.virtualMachine.userVm,
                this.virtualMachine.subscriptionType);

            cloudCalculator.CalculatePriceWithWait(this.vmOption).then((result:any)=> {
                this.currentPriceVm = result;

            });
        }
    }

    var options:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            virtualMachine: '<',
            single: '<',
            showEditVm: '&',
            showConfirmVm: '&',
            prolongateSuccess: '&'
        },
        templateUrl: 'app/pages/subscriptions/vm/components/vmShort/vmShort.html',
        controller: vmShortController,
        controllerAs: 'vm'
    };

    angular.module("crytex.subscriptionVirtualMachineComponents")
        .component('vmShort', options);
}