var admin;
(function (admin) {
    var StatusVM = core.WebApi.Models.Enums.StatusVM;
    var AdminVmStateController = (function () {
        function AdminVmStateController() {
            this.StatusVMs = StatusVM;
        }
        return AdminVmStateController;
    }());
    admin.AdminVmStateController = AdminVmStateController;
    var option = {
        restrict: "EA",
        bindings: {
            state: '<'
        },
        templateUrl: 'app/pages/subscriptions/vm/components/vmState/vmState.html',
        controller: AdminVmStateController,
        controllerAs: 'vm'
    };
    angular.module("crytex.subscriptionVirtualMachineComponents")
        .component("adminVmState", option);
})(admin || (admin = {}));
