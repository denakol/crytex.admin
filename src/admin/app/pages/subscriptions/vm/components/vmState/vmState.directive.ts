module admin {
    import SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    import StatusVM = core.WebApi.Models.Enums.StatusVM;

    export class AdminVmStateController {
        public StatusVMs : typeof StatusVM = StatusVM;

        public state:StatusVM;
        
    }

    var option:ng.IComponentOptions = {
        restrict:"EA",
        bindings: {
            state: '<'
        },
        templateUrl: 'app/pages/subscriptions/vm/components/vmState/vmState.html',
        controller: AdminVmStateController,
        controllerAs: 'vm'
    };

    angular.module("crytex.subscriptionVirtualMachineComponents")
        .component("adminVmState",option);
}