(function () {

	angular.module('crytex.subscriptionVirtualMachine')
        .controller('subscriptionVirtualMachineController', subscriptionVirtualMachineController);

	subscriptionVirtualMachineController.$inject = ['$scope', 'WebServices', 'ToastNotificationService', 'SubscriptionType', '$mdDialog', 'BASE_INFO', 'StatusVm'];

	function subscriptionVirtualMachineController($scope, WebServices, ToastNotificationService, SubscriptionType, $mdDialog, BASE_INFO, StatusVm) {
		$scope.virtualMachines = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 5
        };

		$scope.currentVirtualMachine = null;
		$scope.BASE_INFO = BASE_INFO;
		$scope.statusVm = StatusVm;

        $scope.searchParams = {
        	createdFrom: null,
        	createdTo: null,
        	endFrom: null,
        	endTo: null
        };

        $scope.subscriptionType = SubscriptionType;
        $scope.clearSearchParams = clearSearchParams;
        $scope.applyFilter = applyFilter;
		$scope.getVirtualMachines = getVirtualMachines;
		$scope.prolongateSuccess = prolongateSuccess;

        initialize();
		function initialize() {
			getVirtualMachines();
		};

		function prolongateSuccess(){
			getVirtualMachines();
		}

		function getVirtualMachines() {
			WebServices.AdminSubscriptionVirtualMachineService.getAllPage({
				pageNumber: $scope.virtualMachines.currentPage,
                pageSize: $scope.virtualMachines.pageSize,
                createdFrom: $scope.searchParams.createdFrom,
	        	createdTo: $scope.searchParams.createdTo,
	        	endFrom: $scope.searchParams.endFrom,
	        	endTo: $scope.searchParams.endTo
			},
			function (data) {
				if (data.items.length === 0){
					ToastNotificationService('На сервере нет данных');
					$scope.virtualMachines.items = [];
				} else {
					$scope.virtualMachines.items = data.items;
					angular.forEach($scope.virtualMachines.items, function(value) {
						value.autoProlongation = value.autoProlongation ? "Включено" : "Отключено";
						value.isShowButton = true;
						value.pending = false;
						value.userVm.backapingStoring = 1;
					});
	                $scope.virtualMachines.totalRows = data.totalRows;
	                $scope.virtualMachines.totalPages = data.totalPages;
				}
			},
			function (error) {
				ToastNotificationService('Не удалось выгрузить данные');
			});
		};


		$scope.pageChangeHandler = function(type, numberPage){
			if(type == 'subscription'){
				$scope.virtualMachines.currentPage = numberPage;
				getVirtualMachines();
			}
		}

		function clearSearchParams() {
			$scope.searchParams = {
	        	createdFrom: null,
	        	createdTo: null,
	        	endFrom: null,
	        	endTo: null
	        };
	        getVirtualMachines();
		};

		function applyFilter() {
			getVirtualMachines();
		};



	}

})();

