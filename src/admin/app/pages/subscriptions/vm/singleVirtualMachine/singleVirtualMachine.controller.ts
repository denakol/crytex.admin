module admin {

    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;
    import PageModel = core.PageModel;
    import IResourceClass = angular.resource.IResourceClass;

    interface editSubscriptionVm {
        name: string;
        id: string;
        autoProlongation: boolean;
    }

    export class singleSubscriptionVirtualMachineController {
        static inject = [
            "$mdDialog",
            "$filter",
            "$stateParams",
            "WebServices",
            "ToastNotificationService",
            "SubscriptionType",
            "BASE_INFO",
            "$state"
        ];

        public VmService: IResourceClass<PageModel<SubscriptionVmViewModel>>;

        public virtualMachine: SubscriptionVmViewModel;
        
        private virtualMachineId:string;

        public subscriptionType:any;

        public type:string;

        constructor(private $mdDialog:any,
                    private $filter:any,
                    private $stateParams:any,
                    private WebServices:any,
                    private ToastNotificationService:any,
                    private SubscriptionType:any,
                    private BASE_INFO:any,
                    private $state:any){

            this.virtualMachineId = $stateParams.id;
            this.VmService = WebServices.AdminSubscriptionVirtualMachineService;
            this.subscriptionType = SubscriptionType;
            this.getVirtualmachine();
        }


        public getVirtualmachine(){
            this.VmService.get({
                id: this.virtualMachineId
            },
                (data:any) => {
                    this.virtualMachine = data;
                    this.virtualMachine.show = false;
                    this.virtualMachine.pending = false;
                    this.virtualMachine.userVm.pendingStatus= false;
                    this.virtualMachine.userVm.backapingStoring = this.virtualMachine.dailyBackupStorePeriodDays;

                    this.type = this.$filter('filter')(this.subscriptionType.arrayItems,
                        {
                            value: this.virtualMachine.subscriptionType
                        })[0].name;
                }
                ,
                (err:any) => {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }
        
        
        public showEdit(){
            this.$mdDialog.show({
                controller: "DialogSubscriptionEditCtrl as dialogSub",
                templateUrl: 'app/pages/subscriptions/vm/singleVirtualMachine/editSubscribe.modal.html',
                parent: angular.element(document.body),
                targetEvent: null,
                clickOutsideToClose: true,
                locals: {
                    subscriptionVm: angular.copy(this.virtualMachine)
                }
            })
            .then((answer:any) => {
                    this.editSubscription(answer);
                }, ()=>{})
        }

        public prolongateSuccess(){
            this.getVirtualmachine();
        }

        public editSubscription(subscription:any){
            this.VmService.updateData(subscription,
                (data:any) => {
                    this.virtualMachine.name = subscription.name;
                    this.virtualMachine.autoProlongation = subscription.autoProlongation;
                    this.ToastNotificationService('Добавлено');
                },
                (error:any) => {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                }
            )
        }

        public showConfirm(){

            var confirm = this.$mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            this.$mdDialog.show(confirm)
                .then(()=>{
                    this.deleteVirtualMachine();
                }, () => {})
        }

        public deleteVirtualMachine(){
            this.VmService.delete({
                id: this.virtualMachineId
            },
                (data:any) => {
                    this.ToastNotificationService('Удалено');
                    this.$state.go('subscriptionVirtualMachines', {}, {reload: true});
                },
                (err:any) => {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                }
            )
        }
    }

    class DialogSubscriptionEditCtrl {

        static inject = [
            "$mdDialog",
            "subscriptionVm",
            "SubscriptionType"
        ];

        public editSubscriptionVm: editSubscriptionVm;
        public fixed:boolean;

        constructor(private $mdDialog:any,
                    public subscriptionVm:any,
                    private SubscriptionType:any){
            this.editSubscriptionVm = {
                name: subscriptionVm.name,
                id: subscriptionVm.id,
                autoProlongation: subscriptionVm.autoProlongation
            };

            if(subscriptionVm.subscriptionType == SubscriptionType.Fixed){
                this.fixed = true;
            }
        }

        public cancel(){
            this.$mdDialog.cancel();
        }

        public answer(){
            this.$mdDialog.hide(this.editSubscriptionVm);
        }
    }

    angular.module('preferences.news')
        .controller('singleSubscriptionVirtualMachineController', singleSubscriptionVirtualMachineController)
        .controller('DialogSubscriptionEditCtrl', DialogSubscriptionEditCtrl);
}