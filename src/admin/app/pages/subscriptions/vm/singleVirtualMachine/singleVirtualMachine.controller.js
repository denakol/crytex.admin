var admin;
(function (admin) {
    var singleSubscriptionVirtualMachineController = (function () {
        function singleSubscriptionVirtualMachineController($mdDialog, $filter, $stateParams, WebServices, ToastNotificationService, SubscriptionType, BASE_INFO, $state) {
            this.$mdDialog = $mdDialog;
            this.$filter = $filter;
            this.$stateParams = $stateParams;
            this.WebServices = WebServices;
            this.ToastNotificationService = ToastNotificationService;
            this.SubscriptionType = SubscriptionType;
            this.BASE_INFO = BASE_INFO;
            this.$state = $state;
            this.virtualMachineId = $stateParams.id;
            this.VmService = WebServices.AdminSubscriptionVirtualMachineService;
            this.subscriptionType = SubscriptionType;
            this.getVirtualmachine();
        }
        singleSubscriptionVirtualMachineController.prototype.getVirtualmachine = function () {
            var _this = this;
            this.VmService.get({
                id: this.virtualMachineId
            }, function (data) {
                _this.virtualMachine = data;
                _this.virtualMachine.show = false;
                _this.virtualMachine.pending = false;
                _this.virtualMachine.userVm.pendingStatus = false;
                _this.virtualMachine.userVm.backapingStoring = _this.virtualMachine.dailyBackupStorePeriodDays;
                _this.type = _this.$filter('filter')(_this.subscriptionType.arrayItems, {
                    value: _this.virtualMachine.subscriptionType
                })[0].name;
            }, function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        singleSubscriptionVirtualMachineController.prototype.showEdit = function () {
            var _this = this;
            this.$mdDialog.show({
                controller: "DialogSubscriptionEditCtrl as dialogSub",
                templateUrl: 'app/pages/subscriptions/vm/singleVirtualMachine/editSubscribe.modal.html',
                parent: angular.element(document.body),
                targetEvent: null,
                clickOutsideToClose: true,
                locals: {
                    subscriptionVm: angular.copy(this.virtualMachine)
                }
            })
                .then(function (answer) {
                _this.editSubscription(answer);
            }, function () { });
        };
        singleSubscriptionVirtualMachineController.prototype.prolongateSuccess = function () {
            this.getVirtualmachine();
        };
        singleSubscriptionVirtualMachineController.prototype.editSubscription = function (subscription) {
            var _this = this;
            this.VmService.updateData(subscription, function (data) {
                _this.virtualMachine.name = subscription.name;
                _this.virtualMachine.autoProlongation = subscription.autoProlongation;
                _this.ToastNotificationService('Добавлено');
            }, function (error) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        singleSubscriptionVirtualMachineController.prototype.showConfirm = function () {
            var _this = this;
            var confirm = this.$mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            this.$mdDialog.show(confirm)
                .then(function () {
                _this.deleteVirtualMachine();
            }, function () { });
        };
        singleSubscriptionVirtualMachineController.prototype.deleteVirtualMachine = function () {
            var _this = this;
            this.VmService.delete({
                id: this.virtualMachineId
            }, function (data) {
                _this.ToastNotificationService('Удалено');
                _this.$state.go('subscriptionVirtualMachines', {}, { reload: true });
            }, function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        singleSubscriptionVirtualMachineController.inject = [
            "$mdDialog",
            "$filter",
            "$stateParams",
            "WebServices",
            "ToastNotificationService",
            "SubscriptionType",
            "BASE_INFO",
            "$state"
        ];
        return singleSubscriptionVirtualMachineController;
    }());
    admin.singleSubscriptionVirtualMachineController = singleSubscriptionVirtualMachineController;
    var DialogSubscriptionEditCtrl = (function () {
        function DialogSubscriptionEditCtrl($mdDialog, subscriptionVm, SubscriptionType) {
            this.$mdDialog = $mdDialog;
            this.subscriptionVm = subscriptionVm;
            this.SubscriptionType = SubscriptionType;
            this.editSubscriptionVm = {
                name: subscriptionVm.name,
                id: subscriptionVm.id,
                autoProlongation: subscriptionVm.autoProlongation
            };
            if (subscriptionVm.subscriptionType == SubscriptionType.Fixed) {
                this.fixed = true;
            }
        }
        DialogSubscriptionEditCtrl.prototype.cancel = function () {
            this.$mdDialog.cancel();
        };
        DialogSubscriptionEditCtrl.prototype.answer = function () {
            this.$mdDialog.hide(this.editSubscriptionVm);
        };
        DialogSubscriptionEditCtrl.inject = [
            "$mdDialog",
            "subscriptionVm",
            "SubscriptionType"
        ];
        return DialogSubscriptionEditCtrl;
    }());
    angular.module('preferences.news')
        .controller('singleSubscriptionVirtualMachineController', singleSubscriptionVirtualMachineController)
        .controller('DialogSubscriptionEditCtrl', DialogSubscriptionEditCtrl);
})(admin || (admin = {}));
