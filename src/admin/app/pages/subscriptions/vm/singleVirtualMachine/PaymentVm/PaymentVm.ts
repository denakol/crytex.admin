module admin {

    import PaginationSetting = core.PaginationSetting;
    import FixedSubscriptionPaymentViewModel = core.WebApi.Models.FixedSubscriptionPaymentViewModel;
    import UsageSubscriptionPaymentView = core.WebApi.Models.UsageSubscriptionPaymentView;


    export class AdminPaymentVirtualMachineController {

        public static $inject = [
            "WebServices",
            "ToastNotificationService",
            "CountingPeriodType",
            "UsageSubscriptionPaymentGroupingTypes",
            "$filter"
        ];

        public subscriptionPayments: FixedSubscriptionPaymentViewModel;

        public usageSubscriptionPayments: UsageSubscriptionPaymentView;


        public paginationSettingFixPayment:PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        };

        public paginationSettingUsagePayment:PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        };

        public searchParams:any;
        
        public periodType:any;

        constructor(private WebServices:any,
                    private ToastNotificationService:any,
                    private CountingPeriodType:any,
                    private UsageSubscriptionPaymentGroupingTypes:any,
                    private $filter:any){
            this.periodType = CountingPeriodType.arrayItems;
            this.searchParams = {
                periodType:null
            };
            this.getUsablePeriods();
            if(this.type=='Fixed'){
                this.getSubscriptionPayments();
            } else{
                this.getUsageSubscriptionPayments();
            }
        }

        public getSubscriptionPayments(){
            this.WebServices.AdminFixedSubscriptionPayment.getAllPage({
                    pageNumber: this.paginationSettingFixPayment.pageNumber,
                    pageSize: this.paginationSettingFixPayment.pageSize,
                    subscriptionVmId: this.vmId
                },
                (result:any) => {
                    this.subscriptionPayments = result;

                    angular.forEach(this.subscriptionPayments.items, (subscriptionPayment:any) =>{
                        subscriptionPayment.show = false;
                    });

                    if(result.items.length === 0){
                        this.ToastNotificationService('Ещё нет данных');
                        //this.subscriptionPayments.items = [];
                    }
                },
                (err:any) => {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }

        public getUsageSubscriptionPayments(){
            var groupingType:any = this.$filter('filter')(this.UsageSubscriptionPaymentGroupingTypes.arrayItems, {name: 'GroupByPeriod'})[0].value;

            if(!this.searchParams.periodType){
                groupingType = this.$filter('filter')(this.UsageSubscriptionPaymentGroupingTypes.arrayItems, {name: 'GroupNone'})[0].value;
            }

            this.WebServices.AdminUsageSubscriptionPaymentService.getAllPage({
                    pageNumber: this.paginationSettingUsagePayment.pageNumber,
                    pageSize: this.paginationSettingUsagePayment.pageSize,
                    subscriptionVmId: this.vmId,
                    periodType: this.searchParams.periodType,
                    groupingType: groupingType
                },
                (result:any) => {

                    this.usageSubscriptionPayments = result;
                    
                    if(this.searchParams.periodType){
                        this.usageSubscriptionPayments.periodType = this.$filter('filter')(this.periodType, {value: this.searchParams.periodType})[0].translate;
                    } else {
                        this.usageSubscriptionPayments.periodType = null;
                    }
                    
                    var totalAmount:number = 0;

                    angular.forEach(this.usageSubscriptionPayments.items, (usageSubscriptionPayment:any) => {
                        usageSubscriptionPayment.show = false;
                        usageSubscriptionPayment.periodType = this.$filter('filter')(this.periodType, {value: this.searchParams.periodType})[0].translate;

                        if(this.searchParams.periodType){
                            angular.forEach(usageSubscriptionPayment.usageSubscriptionPayment, (payment:any) => {
                                totalAmount += payment.amount;
                            });
                        }
                        usageSubscriptionPayment.totalAmount = totalAmount;

                    });

                    if(result.items.length === 0){
                        this.ToastNotificationService('Ещё нет данных');
                        this.usageSubscriptionPayments.items = [];
                    }
                },
                (err:any) => {
                    this.usageSubscriptionPayments.items = [];
                    this.ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }

        public visibleSubscriptionPayment(subscriptionPayment:any){
            subscriptionPayment.show = !subscriptionPayment.show;
        }

        public visibleUsageSubscriptionPayment(usageSubscriptionPayment:any){

            if(!this.usageSubscriptionPayments.periodType){
                usageSubscriptionPayment.show = !usageSubscriptionPayment.show;
            }
        }

        public clearSearchParams() {
            this.searchParams = {
                periodType: null
            };
            this.getUsageSubscriptionPayments();
        }

        public applyFilter(){
            this.getUsageSubscriptionPayments();
        }

        public pageChangeHandler(type:any, num:number){
            if(type == 'subscriptionPayment'){
                this.paginationSettingFixPayment.pageNumber = num;
                this.getSubscriptionPayments();
            } else if(type == 'usageSubscriptionPayment'){
                this.subscriptionPayments.show = !this.subscriptionPayments.show;
                this.paginationSettingUsagePayment.pageNumber = num;
                this.getUsageSubscriptionPayments();
            }
        }

        public getUsablePeriods(){
            angular.forEach(this.periodType, (periodType:any) => {
                if(periodType.name == 'Day' || periodType.name == 'Month'){
                    periodType.isShow = true;
                } else {
                    periodType.isShow = false;
                }
            });

            this.periodType.push({value:null, translate:'Не группировать', isShow: true});

            var init = this.$filter('filter')(this.periodType, {name: 'Day'})[0].value;

            this.searchParams.periodType = init;
        }
    }

    var options:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            vmId: '<',
            type: '<'
        },
        templateUrl: 'app/pages/subscriptions/vm/singleVirtualMachine/PaymentVm/PaymentVm.html',
        controller: AdminPaymentVirtualMachineController,
        controllerAs: 'vm'
    }

    angular.module("crytex.singleSubscriptionVirtualMachine")
        .component('paymentVirtualMachine', options);

}