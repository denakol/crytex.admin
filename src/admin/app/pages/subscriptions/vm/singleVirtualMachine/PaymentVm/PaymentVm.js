var admin;
(function (admin) {
    var AdminPaymentVirtualMachineController = (function () {
        function AdminPaymentVirtualMachineController(WebServices, ToastNotificationService, CountingPeriodType, UsageSubscriptionPaymentGroupingTypes, $filter) {
            this.WebServices = WebServices;
            this.ToastNotificationService = ToastNotificationService;
            this.CountingPeriodType = CountingPeriodType;
            this.UsageSubscriptionPaymentGroupingTypes = UsageSubscriptionPaymentGroupingTypes;
            this.$filter = $filter;
            this.paginationSettingFixPayment = {
                pageNumber: 1,
                pageSize: 5
            };
            this.paginationSettingUsagePayment = {
                pageNumber: 1,
                pageSize: 5
            };
            this.periodType = CountingPeriodType.arrayItems;
            this.searchParams = {
                periodType: null
            };
            this.getUsablePeriods();
            if (this.type == 'Fixed') {
                this.getSubscriptionPayments();
            }
            else {
                this.getUsageSubscriptionPayments();
            }
        }
        AdminPaymentVirtualMachineController.prototype.getSubscriptionPayments = function () {
            var _this = this;
            this.WebServices.AdminFixedSubscriptionPayment.getAllPage({
                pageNumber: this.paginationSettingFixPayment.pageNumber,
                pageSize: this.paginationSettingFixPayment.pageSize,
                subscriptionVmId: this.vmId
            }, function (result) {
                _this.subscriptionPayments = result;
                angular.forEach(_this.subscriptionPayments.items, function (subscriptionPayment) {
                    subscriptionPayment.show = false;
                });
                if (result.items.length === 0) {
                    _this.ToastNotificationService('Ещё нет данных');
                }
            }, function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminPaymentVirtualMachineController.prototype.getUsageSubscriptionPayments = function () {
            var _this = this;
            var groupingType = this.$filter('filter')(this.UsageSubscriptionPaymentGroupingTypes.arrayItems, { name: 'GroupByPeriod' })[0].value;
            if (!this.searchParams.periodType) {
                groupingType = this.$filter('filter')(this.UsageSubscriptionPaymentGroupingTypes.arrayItems, { name: 'GroupNone' })[0].value;
            }
            this.WebServices.AdminUsageSubscriptionPaymentService.getAllPage({
                pageNumber: this.paginationSettingUsagePayment.pageNumber,
                pageSize: this.paginationSettingUsagePayment.pageSize,
                subscriptionVmId: this.vmId,
                periodType: this.searchParams.periodType,
                groupingType: groupingType
            }, function (result) {
                _this.usageSubscriptionPayments = result;
                if (_this.searchParams.periodType) {
                    _this.usageSubscriptionPayments.periodType = _this.$filter('filter')(_this.periodType, { value: _this.searchParams.periodType })[0].translate;
                }
                else {
                    _this.usageSubscriptionPayments.periodType = null;
                }
                var totalAmount = 0;
                angular.forEach(_this.usageSubscriptionPayments.items, function (usageSubscriptionPayment) {
                    usageSubscriptionPayment.show = false;
                    usageSubscriptionPayment.periodType = _this.$filter('filter')(_this.periodType, { value: _this.searchParams.periodType })[0].translate;
                    if (_this.searchParams.periodType) {
                        angular.forEach(usageSubscriptionPayment.usageSubscriptionPayment, function (payment) {
                            totalAmount += payment.amount;
                        });
                    }
                    usageSubscriptionPayment.totalAmount = totalAmount;
                });
                if (result.items.length === 0) {
                    _this.ToastNotificationService('Ещё нет данных');
                    _this.usageSubscriptionPayments.items = [];
                }
            }, function (err) {
                _this.usageSubscriptionPayments.items = [];
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminPaymentVirtualMachineController.prototype.visibleSubscriptionPayment = function (subscriptionPayment) {
            subscriptionPayment.show = !subscriptionPayment.show;
        };
        AdminPaymentVirtualMachineController.prototype.visibleUsageSubscriptionPayment = function (usageSubscriptionPayment) {
            if (!this.usageSubscriptionPayments.periodType) {
                usageSubscriptionPayment.show = !usageSubscriptionPayment.show;
            }
        };
        AdminPaymentVirtualMachineController.prototype.clearSearchParams = function () {
            this.searchParams = {
                periodType: null
            };
            this.getUsageSubscriptionPayments();
        };
        AdminPaymentVirtualMachineController.prototype.applyFilter = function () {
            this.getUsageSubscriptionPayments();
        };
        AdminPaymentVirtualMachineController.prototype.pageChangeHandler = function (type, num) {
            if (type == 'subscriptionPayment') {
                this.paginationSettingFixPayment.pageNumber = num;
                this.getSubscriptionPayments();
            }
            else if (type == 'usageSubscriptionPayment') {
                this.subscriptionPayments.show = !this.subscriptionPayments.show;
                this.paginationSettingUsagePayment.pageNumber = num;
                this.getUsageSubscriptionPayments();
            }
        };
        AdminPaymentVirtualMachineController.prototype.getUsablePeriods = function () {
            angular.forEach(this.periodType, function (periodType) {
                if (periodType.name == 'Day' || periodType.name == 'Month') {
                    periodType.isShow = true;
                }
                else {
                    periodType.isShow = false;
                }
            });
            this.periodType.push({ value: null, translate: 'Не группировать', isShow: true });
            var init = this.$filter('filter')(this.periodType, { name: 'Day' })[0].value;
            this.searchParams.periodType = init;
        };
        AdminPaymentVirtualMachineController.$inject = [
            "WebServices",
            "ToastNotificationService",
            "CountingPeriodType",
            "UsageSubscriptionPaymentGroupingTypes",
            "$filter"
        ];
        return AdminPaymentVirtualMachineController;
    }());
    admin.AdminPaymentVirtualMachineController = AdminPaymentVirtualMachineController;
    var options = {
        restrict: "EA",
        bindings: {
            vmId: '<',
            type: '<'
        },
        templateUrl: 'app/pages/subscriptions/vm/singleVirtualMachine/PaymentVm/PaymentVm.html',
        controller: AdminPaymentVirtualMachineController,
        controllerAs: 'vm'
    };
    angular.module("crytex.singleSubscriptionVirtualMachine")
        .component('paymentVirtualMachine', options);
})(admin || (admin = {}));
