var admin;
(function (admin) {
    var AdminTasksVirtualMachineController = (function () {
        function AdminTasksVirtualMachineController(WebServices, StatusTask, ToastNotificationService, TypeTask) {
            this.WebServices = WebServices;
            this.StatusTask = StatusTask;
            this.ToastNotificationService = ToastNotificationService;
            this.TypeTask = TypeTask;
            this.paginationSettingCompletedTasks = {
                pageNumber: 1,
                pageSize: 10
            };
            this.paginationSettingBackupVms = {
                pageNumber: 1,
                pageSize: 10
            };
            this.paginationSettingSnapshotVms = {
                pageNumber: 1,
                pageSize: 10
            };
            this.typeTask = TypeTask;
            this.getEndTasks();
            this.getSnapShotVms();
            this.getProcessingTasks();
        }
        AdminTasksVirtualMachineController.prototype.getEndTasks = function () {
            var _this = this;
            this.WebServices.AdminTaskV2.getAllPage({
                pageNumber: this.paginationSettingCompletedTasks.pageNumber,
                pageSize: this.paginationSettingCompletedTasks.pageSize,
                resourceId: this.vmId,
                statusTasks: [this.StatusTask.EndWithErrors, this.StatusTask.End]
            }, function (tasks) {
                _this.completedTasks = tasks;
                angular.forEach(_this.completedTasks.items, function (task) {
                    task.arrayOptions = JSON.parse(task.options);
                });
            }, function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminTasksVirtualMachineController.prototype.getVmBackup = function () {
            var _this = this;
            this.WebServices.AdminVmBackupService.getAllPage({
                pageNumber: this.paginationSettingBackupVms.pageNumber,
                pageSize: this.paginationSettingBackupVms.pageSize,
                vmId: this.vmId
            }, function (backup) {
                _this.backupVms = backup;
            }, function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminTasksVirtualMachineController.prototype.getProcessingTasks = function () {
            var _this = this;
            this.WebServices.AdminTaskV2.getAllPage({
                pageNumber: 1,
                pageSize: 20,
                resourceId: this.vmId,
                statusTasks: [this.StatusTask.Processing, this.StatusTask.Start, this.StatusTask.Pending]
            }, function (tasks) {
                _this.processingTasks = tasks;
                angular.forEach(_this.processingTasks.items, function (task) {
                    task.arrayOptions = JSON.parse(task.options);
                });
            }, function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminTasksVirtualMachineController.prototype.getSnapShotVms = function () {
            var _this = this;
            this.WebServices.AdminSnapShotVmService.getAllPage({
                pageNumber: this.paginationSettingSnapshotVms.pageNumber,
                pageSize: this.paginationSettingSnapshotVms.pageSize,
                vmId: this.vmId
            }, function (snapshot) {
                _this.snapshotVms = snapshot;
            }, function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminTasksVirtualMachineController.prototype.newPageChangeHandler = function (type, num) {
            if (type == 'task') {
                this.getProcessingTasks();
                this.getEndTasks();
            }
            else if (type == 'snapshot') {
                this.getSnapShotVms();
            }
            else if (type == 'backup') {
                this.getVmBackup();
            }
        };
        AdminTasksVirtualMachineController.prototype.updateTaskValue = function (task) {
            var value = "";
            if (task.typeTask == this.TypeTask.UpdateVm) {
                value = "CPU:" + task.arrayOptions.Cpu + ", RAM:" + task.arrayOptions.Ram + ", HDD:" + task.arrayOptions.Hdd;
            }
            else {
                value = task.arrayOptions;
            }
            return value;
        };
        AdminTasksVirtualMachineController.prototype.visibleTask = function (task) {
            task.show = !task.show;
        };
        AdminTasksVirtualMachineController.$inject = [
            "WebServices",
            "StatusTask",
            "ToastNotificationService",
            "TypeTask"
        ];
        return AdminTasksVirtualMachineController;
    }());
    admin.AdminTasksVirtualMachineController = AdminTasksVirtualMachineController;
    var options = {
        restrict: "EA",
        bindings: {
            vmId: '<'
        },
        templateUrl: 'app/pages/subscriptions/vm/singleVirtualMachine/TasksVm/TasksVm.html',
        controller: AdminTasksVirtualMachineController,
        controllerAs: 'vm'
    };
    angular.module("crytex.singleSubscriptionVirtualMachine")
        .component('tasksVirtualMachine', options);
})(admin || (admin = {}));
