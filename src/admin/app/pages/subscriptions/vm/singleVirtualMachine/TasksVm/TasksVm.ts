module admin{

    import TaskV2ViewModel = core.WebApi.Models.TaskV2ViewModel;
    import VmBackupViewModel = core.WebApi.Models.VmBackupViewModel;
    import SnapshotVmViewModel = core.WebApi.Models.SnapshotVmViewModel;
    import PaginationSetting = core.PaginationSetting;

    export class AdminTasksVirtualMachineController {

        public static $inject = [
            "WebServices",
            "StatusTask",
            "ToastNotificationService",
            "TypeTask"
        ];

        public processingTasks: TaskV2ViewModel;
        public completedTasks: TaskV2ViewModel;

        public backupVms: VmBackupViewModel;

        public snapshotVms: SnapshotVmViewModel;

        public paginationSettingCompletedTasks:PaginationSetting = {
            pageNumber: 1,
            pageSize: 10
        };

        public paginationSettingBackupVms:PaginationSetting = {
            pageNumber: 1,
            pageSize: 10
        };

        public paginationSettingSnapshotVms:PaginationSetting = {
            pageNumber: 1,
            pageSize: 10
        };
        
        public typeTask:any;

        constructor(private WebServices:any,
                    private StatusTask:any,
                    private ToastNotificationService:any,
                    private TypeTask:any){
            this.typeTask = TypeTask;
            this.getEndTasks();
            this.getSnapShotVms();
            this.getProcessingTasks();
        }

        public getEndTasks(){
            this.WebServices.AdminTaskV2.getAllPage({
                    pageNumber: this.paginationSettingCompletedTasks.pageNumber,
                    pageSize: this.paginationSettingCompletedTasks.pageSize,
                    resourceId: this.vmId,
                    statusTasks: [this.StatusTask.EndWithErrors, this.StatusTask.End]
                },
                (tasks:any) => {
                    this.completedTasks= tasks;

                    angular.forEach(this.completedTasks.items, (task:any) => {
                        task.arrayOptions = JSON.parse(task.options);
                    });
                },
                (err:any) => {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }

        public getVmBackup(){
            this.WebServices.AdminVmBackupService.getAllPage({
                    pageNumber: this.paginationSettingBackupVms.pageNumber,
                    pageSize: this.paginationSettingBackupVms.pageSize,
                    vmId: this.vmId
                },
                (backup:any) => {
                    this.backupVms = backup;
                },
                (err:any) => {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }

        public getProcessingTasks(){
            this.WebServices.AdminTaskV2.getAllPage({
                    pageNumber: 1,
                    pageSize: 20,
                    resourceId: this.vmId,
                    statusTasks: [this.StatusTask.Processing, this.StatusTask.Start, this.StatusTask.Pending]
                },
                (tasks:any) => {
                    this.processingTasks = tasks;

                    angular.forEach(this.processingTasks.items, (task:any) => {
                        task.arrayOptions = JSON.parse(task.options);
                    })
                },
                (err:any) => {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }

        public getSnapShotVms(){

            this.WebServices.AdminSnapShotVmService.getAllPage({
                    pageNumber: this.paginationSettingSnapshotVms.pageNumber,
                    pageSize: this.paginationSettingSnapshotVms.pageSize,
                    vmId: this.vmId
                },
                (snapshot:any) => {
                    this.snapshotVms = snapshot;
                },
                (err:any) => {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                }
            )
        }

        public newPageChangeHandler(type:any, num:any){
            if(type == 'task'){
                this.getProcessingTasks();
                this.getEndTasks();
            }else if(type == 'snapshot'){
                this.getSnapShotVms();
            }else if(type == 'backup'){
                this.getVmBackup();
            }
        }

        public updateTaskValue(task:any){
            var value = "";

            if(task.typeTask == this.TypeTask.UpdateVm){
                value = "CPU:" + task.arrayOptions.Cpu + ", RAM:" + task.arrayOptions.Ram + ", HDD:" + task.arrayOptions.Hdd;
            }else{
                value = task.arrayOptions;
            }
            return value;
        }

        public visibleTask(task:any){
            task.show = !task.show
        }
    }


    var options:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            vmId: '<'
        },
        templateUrl: 'app/pages/subscriptions/vm/singleVirtualMachine/TasksVm/TasksVm.html',
        controller: AdminTasksVirtualMachineController,
        controllerAs: 'vm'
    }

    angular.module("crytex.singleSubscriptionVirtualMachine")
        .component('tasksVirtualMachine', options);
}