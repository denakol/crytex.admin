module admin{

    export class updateStatusController{

        public static $inject = ["WebServices", "ManageGameServer", "ToastNotificationService"];

        public manageGameServer:any;
        public originatorEv:any;

        constructor(private WebServices:any,
                    private ManageGameServer:any,
                    private ToastNotificationService:any){
            this.manageGameServer = ManageGameServer.arrayItems;
        }
        
        public openMenu($mdOpenMenu:any, ev:any){
            this.originatorEv = ev;
            $mdOpenMenu(ev);
        }
        
        public changeState(value:any){
            this.WebServices.AdminGameServerService.updateServerStatus({
                serverId: this.gsId,
                changeStatusType: value
            }).$promise
                .then((data:any)=>{
                    this.ToastNotificationService('Статус изменен');
                    this.pending = true;
                })
                .catch((err:any)=>{
                    this.ToastNotificationService('Не удалось изменить статус');
                });

        }

    }



    var options:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            gsId: '<',
            pending: '='
        },
        templateUrl: 'app/pages/subscriptions/game/components/updateStatus/updateStatus.html',
        controller: updateStatusController,
        controllerAs: 'vm'
    };

    angular.module("crytex.subscriptionGameServerComponents")
        .component('updateStatusGameServer', options)
}