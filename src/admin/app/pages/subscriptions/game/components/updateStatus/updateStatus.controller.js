var admin;
(function (admin) {
    var updateStatusController = (function () {
        function updateStatusController(WebServices, ManageGameServer, ToastNotificationService) {
            this.WebServices = WebServices;
            this.ManageGameServer = ManageGameServer;
            this.ToastNotificationService = ToastNotificationService;
            this.manageGameServer = ManageGameServer.arrayItems;
        }
        updateStatusController.prototype.openMenu = function ($mdOpenMenu, ev) {
            this.originatorEv = ev;
            $mdOpenMenu(ev);
        };
        updateStatusController.prototype.changeState = function (value) {
            var _this = this;
            this.WebServices.AdminGameServerService.updateServerStatus({
                serverId: this.gsId,
                changeStatusType: value
            }).$promise
                .then(function (data) {
                _this.ToastNotificationService('Статус изменен');
                _this.pending = true;
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось изменить статус');
            });
        };
        updateStatusController.$inject = ["WebServices", "ManageGameServer", "ToastNotificationService"];
        return updateStatusController;
    }());
    admin.updateStatusController = updateStatusController;
    var options = {
        restrict: "EA",
        bindings: {
            gsId: '<',
            pending: '='
        },
        templateUrl: 'app/pages/subscriptions/game/components/updateStatus/updateStatus.html',
        controller: updateStatusController,
        controllerAs: 'vm'
    };
    angular.module("crytex.subscriptionGameServerComponents")
        .component('updateStatusGameServer', options);
})(admin || (admin = {}));
