var admin;
(function (admin) {
    var UpdateType = core.WebApi.Models.Enums.GameServerUpdateType;
    var updateGameServerController = (function () {
        function updateGameServerController(WebServices, $mdDialog, ToastNotificationService) {
            this.WebServices = WebServices;
            this.$mdDialog = $mdDialog;
            this.ToastNotificationService = ToastNotificationService;
        }
        updateGameServerController.prototype.updateSlotCount = function (slotCount) { };
        updateGameServerController.prototype.showUpdateSlotCountEdit = function () {
            var _this = this;
            this.$mdDialog.show({
                controller: "DialogUpdateSlotCountCtrl as dialog",
                templateUrl: "updateSlotCountGameServer.tmpl.html",
                parent: angular.element(document.body),
                targetEvent: null,
                clickOutsideToClose: true,
                locals: {
                    gameServer: angular.copy(this.gameServer)
                }
            })
                .then(function (answer) {
                _this.updateSlotCountGameServer(answer);
            }, function () {
            });
        };
        updateGameServerController.prototype.updateSlotCountGameServer = function (gameServer) {
            var _this = this;
            this.WebServices.AdminGameServerService.extendGameServer({
                serverId: gameServer.id,
                serverName: gameServer.name,
                slotCount: gameServer.slotCount,
                updateType: UpdateType.UpdateSlotCount
            }).$promise
                .then(function (data) {
                _this.updateSlotCount({
                    gameServer: _this.gameServer,
                    slotCount: gameServer.slotCount
                });
                _this.ToastNotificationService('Изменено');
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        updateGameServerController.$inject = ["WebServices", "$mdDialog", "ToastNotificationService"];
        return updateGameServerController;
    }());
    admin.updateGameServerController = updateGameServerController;
    var DialogUpdateSlotCountCtrl = (function () {
        function DialogUpdateSlotCountCtrl($mdDialog, gameServer) {
            this.$mdDialog = $mdDialog;
            this.gameServer = gameServer;
            this.editSubscriptionGs = gameServer;
        }
        DialogUpdateSlotCountCtrl.prototype.cancel = function () {
            this.$mdDialog.cancel();
        };
        DialogUpdateSlotCountCtrl.prototype.answer = function () {
            this.$mdDialog.hide(this.editSubscriptionGs);
        };
        DialogUpdateSlotCountCtrl.$inject = ["$mdDialog", "gameServer"];
        return DialogUpdateSlotCountCtrl;
    }());
    var options = {
        restrict: "EA",
        bindings: {
            gameServer: '<',
            updateSlotCount: '&'
        },
        templateUrl: 'app/pages/subscriptions/game/components/updateGameServer/updateGameServer.html',
        controller: updateGameServerController,
        controllerAs: 'vm'
    };
    angular.module("crytex.subscriptionGameServerComponents")
        .component('updateGameServer', options)
        .controller('DialogUpdateSlotCountCtrl', DialogUpdateSlotCountCtrl);
})(admin || (admin = {}));
