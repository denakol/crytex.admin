module admin {

    import UpdateType = core.WebApi.Models.Enums.GameServerUpdateType;

    export class updateGameServerController {
        
        public static $inject = ["WebServices", "$mdDialog", "ToastNotificationService"];
        
        constructor(private WebServices:any,
                    private $mdDialog:any,
                    private ToastNotificationService:any){
        }

        updateSlotCount(slotCount:number):any{}

        public showUpdateSlotCountEdit(){
            this.$mdDialog.show({
                    controller: "DialogUpdateSlotCountCtrl as dialog",
                    templateUrl: "updateSlotCountGameServer.tmpl.html",
                    parent: angular.element(document.body),
                    targetEvent: null,
                    clickOutsideToClose: true,
                    locals: {
                        gameServer: angular.copy(this.gameServer)
                    }
                })
                .then((answer:any)=> {
                    this.updateSlotCountGameServer(answer);
                }, () => {
                });
        }

        public updateSlotCountGameServer(gameServer:any){
            this.WebServices.AdminGameServerService.extendGameServer({
                serverId: gameServer.id,
                serverName: gameServer.name,
                slotCount: gameServer.slotCount,
                updateType: UpdateType.UpdateSlotCount
            }).$promise
                .then((data:any)=>{
                    this.updateSlotCount({
                        gameServer: this.gameServer,
                        slotCount: gameServer.slotCount
                    });
                    this.ToastNotificationService('Изменено');
                })
                .catch((err:any)=>{
                    this.ToastNotificationService('Не удалось выгрузить данные');
                });
        }
    }

    class DialogUpdateSlotCountCtrl {
        
        public static $inject = ["$mdDialog", "gameServer"];

        public editSubscriptionGs:any;

        constructor(private $mdDialog:any,
                    public gameServer:any){
            this.editSubscriptionGs = gameServer;
        }
        public cancel(){
            this.$mdDialog.cancel();
        }

        public answer(){
            this.$mdDialog.hide(this.editSubscriptionGs);
        }
    }

    var options:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            gameServer: '<',
            updateSlotCount: '&'
        },
        templateUrl: 'app/pages/subscriptions/game/components/updateGameServer/updateGameServer.html',
        controller: updateGameServerController,
        controllerAs: 'vm'
    };

    angular.module("crytex.subscriptionGameServerComponents")
        .component('updateGameServer', options)
        .controller('DialogUpdateSlotCountCtrl', DialogUpdateSlotCountCtrl);
}