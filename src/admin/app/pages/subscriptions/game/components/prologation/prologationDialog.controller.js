var admin;
(function (admin) {
    var UpdateType = core.WebApi.Models.Enums.GameServerUpdateType;
    var prologationDialogController = (function () {
        function prologationDialogController(WebServices, $mdDialog, ToastNotificationService) {
            this.WebServices = WebServices;
            this.$mdDialog = $mdDialog;
            this.ToastNotificationService = ToastNotificationService;
        }
        prologationDialogController.prototype.showProlongateEdit = function () {
            var _this = this;
            this.$mdDialog.show({
                controller: "DialogProlongateCtrl as vm",
                templateUrl: "prolongateSubscriptionGs.tmpl.html",
                parent: angular.element(document.body),
                targetEvent: null,
                clickOutsideToClose: true,
                locals: {
                    gameServerId: this.gsId,
                    gameServerName: this.gsName
                }
            })
                .then(function (answer) {
                _this.prolongateSubscriptionGameServer(answer);
            }, function () {
            });
        };
        prologationDialogController.prototype.prolongateSubscriptionGameServer = function (prologationGs) {
            var _this = this;
            this.WebServices.AdminGameServerService.extendGameServer({
                serverId: prologationGs.id,
                serverName: prologationGs.name,
                prolongatePeriodType: prologationGs.prolongatePeriodType,
                prolongatePeriod: prologationGs.prolongatePeriod,
                updateType: UpdateType.Prolongation
            }).$promise
                .then(function (data) {
                _this.ToastNotificationService('Добавлено');
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        prologationDialogController.$inject = ["WebServices", "$mdDialog", "ToastNotificationService"];
        return prologationDialogController;
    }());
    admin.prologationDialogController = prologationDialogController;
    var DialogProlongateCtrl = (function () {
        function DialogProlongateCtrl($mdDialog, gameServerId, gameServerName, CountingPeriodType) {
            this.$mdDialog = $mdDialog;
            this.gameServerId = gameServerId;
            this.gameServerName = gameServerName;
            this.CountingPeriodType = CountingPeriodType;
            this.prolongateGs = {
                name: gameServerName,
                id: gameServerId,
                prolongatePeriodType: "",
                prolongatePeriod: ""
            };
            this.periodType = CountingPeriodType.arrayItems;
        }
        DialogProlongateCtrl.prototype.cancel = function () {
            this.$mdDialog.cancel();
        };
        DialogProlongateCtrl.prototype.answer = function () {
            this.$mdDialog.hide(this.prolongateGs);
        };
        DialogProlongateCtrl.$inject = ["$mdDialog", "gameServerId", "gameServerName", "CountingPeriodType"];
        return DialogProlongateCtrl;
    }());
    var options = {
        restrict: "EA",
        bindings: {
            gsId: '<',
            gsName: '<'
        },
        templateUrl: 'app/pages/subscriptions/game/components/prologation/prologationDialogDialog.html',
        controller: prologationDialogController,
        controllerAs: 'vm'
    };
    angular.module("crytex.subscriptionGameServerComponents")
        .component('prologationDialogGameServer', options)
        .controller('DialogProlongateCtrl', DialogProlongateCtrl);
})(admin || (admin = {}));
