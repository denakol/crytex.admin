module admin{

    import UpdateType = core.WebApi.Models.Enums.GameServerUpdateType;

    export class prologationDialogController{
        
        public static $inject = ["WebServices", "$mdDialog", "ToastNotificationService"];

        constructor(private WebServices:any,
                    private $mdDialog:any,
                    private ToastNotificationService:any
                    ){

        }
        
        public showProlongateEdit(){
            this.$mdDialog.show({
                    controller: "DialogProlongateCtrl as vm",
                    templateUrl: "prolongateSubscriptionGs.tmpl.html",
                    parent: angular.element(document.body),
                    targetEvent: null,
                    clickOutsideToClose: true,
                    locals: {
                        gameServerId: this.gsId,
                        gameServerName: this.gsName
                    }
                })
                .then((answer:any)=> {
                    this.prolongateSubscriptionGameServer(answer);
                }, () => {
                });
        }

        public prolongateSubscriptionGameServer(prologationGs:any){
            this.WebServices.AdminGameServerService.extendGameServer({
                serverId: prologationGs.id,
                serverName: prologationGs.name,
                prolongatePeriodType: prologationGs.prolongatePeriodType,
                prolongatePeriod: prologationGs.prolongatePeriod,
                updateType: UpdateType.Prolongation
            }).$promise
                .then((data:any)=>{
                    this.ToastNotificationService('Добавлено');
                })
                .catch((err:any)=>{
                    this.ToastNotificationService('Не удалось выгрузить данные');
                });
        }
    }

    interface editSubscriptionGs {
        name: string;
        id: string;
        prolongatePeriodType: "";
        prolongatePeriod: "";
    }

    class DialogProlongateCtrl{

        public static $inject = ["$mdDialog", "gameServerId", "gameServerName", "CountingPeriodType"];

        public prolongateGs: editSubscriptionGs;

        public periodType:any;

        constructor(private $mdDialog:any,
                    private gameServerId:string,
                    private gameServerName:string,
                    public CountingPeriodType:any){
            this.prolongateGs = {
                name: gameServerName,
                id: gameServerId,
                prolongatePeriodType: "",
                prolongatePeriod: ""
            };
            this.periodType = CountingPeriodType.arrayItems;
        }

        public cancel(){
            this.$mdDialog.cancel();
        }

        public answer(){
            this.$mdDialog.hide(this.prolongateGs);
        }
    }

    var options:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            gsId: '<',
            gsName: '<'
        },
        templateUrl: 'app/pages/subscriptions/game/components/prologation/prologationDialogDialog.html',
        controller: prologationDialogController,
        controllerAs: 'vm'
    };

    angular.module("crytex.subscriptionGameServerComponents")
        .component('prologationDialogGameServer', options)
        .controller('DialogProlongateCtrl', DialogProlongateCtrl);
}