var admin;
(function (admin) {
    var AdminGameServerComponentController = (function () {
        function AdminGameServerComponentController(BASE_INFO, gameFamily) {
            this.BASE_INFO = BASE_INFO;
            this.gameFamily = gameFamily;
        }
        AdminGameServerComponentController.prototype.updateSlotCount = function (gameServer, slotCount) {
            gameServer.slotCount = slotCount;
        };
        AdminGameServerComponentController.$inject = ["BASE_INFO", "GameFamily"];
        return AdminGameServerComponentController;
    }());
    admin.AdminGameServerComponentController = AdminGameServerComponentController;
    var options = {
        restrict: "EA",
        bindings: {
            gameServer: '<',
            single: '<',
            showEditGs: '&'
        },
        templateUrl: 'app/pages/subscriptions/game/gameServerComponent/gameServer/gameServerComponent.html',
        controller: AdminGameServerComponentController,
        controllerAs: 'vm'
    };
    angular.module('crytex.subscriptionGameServerComponent')
        .component('gameServer', options);
})(admin || (admin = {}));
