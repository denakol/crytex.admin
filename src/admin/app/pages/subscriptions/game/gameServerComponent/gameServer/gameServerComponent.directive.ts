module admin{
    export class AdminGameServerComponentController{

        public static $inject = ["BASE_INFO", "GameFamily"];
        
        constructor(private BASE_INFO:any,
                    public gameFamily:any){
        }
        
        public updateSlotCount(gameServer:any, slotCount:number):any{
            gameServer.slotCount = slotCount;
        }
    }

    var options:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            gameServer: '<',
            single: '<',
            showEditGs: '&'
        },
        templateUrl: 'app/pages/subscriptions/game/gameServerComponent/gameServer/gameServerComponent.html',
        controller: AdminGameServerComponentController,
        controllerAs: 'vm'
    };

    angular.module('crytex.subscriptionGameServerComponent')
        .component('gameServer', options)
}