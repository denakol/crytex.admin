module admin {

    import IResourceClass = angular.resource.IResourceClass;
    import PageModel = core.PageModel;
    import PaginationSetting = core.PaginationSetting;
    import GameServerViewModel = core.WebApi.Models.GameServerViewModel;
    import GameFamily = core.WebApi.Models.Enums.GameFamily;

    export class subscriptionGameServerController {
        public static $inject = [
            '$scope',
            '$filter',
            'WebServices',
            'ToastNotificationService',
            'TypeServerPayment',
            'GameFamily',
            'BASE_INFO'
        ];
        //AdminGameServerService
        public gameServers:PageModel<GameServerViewModel>;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        };

        public test:GameFamily;
        
        public typeOfServerPayment:any;
        
        private GameService: IResourceClass<PageModel<GameServerViewModel>>;
        
        constructor(private $scope:any,
                    private $filter:any,
                    private WebServices:any,
                    private ToastNotificationService:any,
                    private TypeServerPayment:any,
                    public gameFamily:any,
                    public BASE_INFO:any){
            this.GameService = WebServices.AdminGameServerService;
            this.typeOfServerPayment = TypeServerPayment.arrayItems;
            this.gameFamily = gameFamily;
            $scope.gameFamily = gameFamily;
            $scope.BASE_INFO = BASE_INFO;
            this.getGameServers();
        }

        public getGameServers(){
            this.GameService.getAllPage({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize
            }).$promise
                .then((data:any) => {
                    this.gameServers = data;
                    angular.forEach(this.gameServers.items, (value)=>{
                        value.paymentType = this.$filter('filter')(this.typeOfServerPayment, { value: value.paymentType })[0];
                        value.isShowButton = true;
                    });
                })
                .catch((err:any) => {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                });
        }
        
    }

    angular.module('crytex.subscriptionGameServer')
        .controller('subscriptionGameServerController', subscriptionGameServerController);
}