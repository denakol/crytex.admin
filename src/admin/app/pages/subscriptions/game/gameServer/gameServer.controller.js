var admin;
(function (admin) {
    var subscriptionGameServerController = (function () {
        function subscriptionGameServerController($scope, $filter, WebServices, ToastNotificationService, TypeServerPayment, gameFamily, BASE_INFO) {
            this.$scope = $scope;
            this.$filter = $filter;
            this.WebServices = WebServices;
            this.ToastNotificationService = ToastNotificationService;
            this.TypeServerPayment = TypeServerPayment;
            this.gameFamily = gameFamily;
            this.BASE_INFO = BASE_INFO;
            this.paginationSettings = {
                pageNumber: 1,
                pageSize: 5
            };
            this.GameService = WebServices.AdminGameServerService;
            this.typeOfServerPayment = TypeServerPayment.arrayItems;
            this.gameFamily = gameFamily;
            $scope.gameFamily = gameFamily;
            $scope.BASE_INFO = BASE_INFO;
            this.getGameServers();
        }
        subscriptionGameServerController.prototype.getGameServers = function () {
            var _this = this;
            this.GameService.getAllPage({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize
            }).$promise
                .then(function (data) {
                _this.gameServers = data;
                angular.forEach(_this.gameServers.items, function (value) {
                    value.paymentType = _this.$filter('filter')(_this.typeOfServerPayment, { value: value.paymentType })[0];
                    value.isShowButton = true;
                });
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        subscriptionGameServerController.$inject = [
            '$scope',
            '$filter',
            'WebServices',
            'ToastNotificationService',
            'TypeServerPayment',
            'GameFamily',
            'BASE_INFO'
        ];
        return subscriptionGameServerController;
    }());
    admin.subscriptionGameServerController = subscriptionGameServerController;
    angular.module('crytex.subscriptionGameServer')
        .controller('subscriptionGameServerController', subscriptionGameServerController);
})(admin || (admin = {}));
