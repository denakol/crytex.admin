module admin{

    import PaginationSetting = core.PaginationSetting;
    import BaseVmComponentController = admin.BaseGsComponentController;
    import TaskV2ViewModel = core.WebApi.Models.TaskV2ViewModel;
    import PageModel = core.PageModel;

    export class AdminTaskV2Controller extends BaseGsComponentController{

        public static $inject = [
            "WebServices", 
            "StatusTask", 
            "ToastNotificationService", 
            "TypeTask"
        ];
        public completedTasks: PageModel<TaskV2ViewModel>;
        public processingTasks: PageModel<TaskV2ViewModel>;

        public paginationSettingCompletedTasks:PaginationSetting = {
            pageNumber: 1,
            pageSize: 10
        };

        public paginationSettingProcessingTasks:PaginationSetting = {
            pageNumber: 1,
            pageSize: 20
        };

        constructor(private WebServices:any,
                    private StatusTask:any,
                    public ToastNotificationService:any,
                    private TypeTask:any){
            super();
            this.init();
        }

        public init(){
            this.getProcessingTasks();
            this.getEndTasks();
        }

        public getProcessingTasks(){
            this.WebServices.AdminTaskV2.getAllPage({
                    pageNumber: this.paginationSettingProcessingTasks.pageNumber,
                    pageSize: this.paginationSettingProcessingTasks.pageSize,
                    resourceId: this.gameServerId,
                    statusTasks: [this.StatusTask.Processing, this.StatusTask.Start, this.StatusTask.Pending]
                },
                (tasks:any) => {
                    this.processingTasks = tasks.items;

                    angular.forEach(this.processingTasks, (task:any) => {
                        task.arrayOptions = JSON.parse(task.options);
                    })
                },
                (err:any) => {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }

        public getEndTasks(){
            this.WebServices.AdminTaskV2.getAllPage({
                    pageNumber: this.paginationSettingCompletedTasks.pageNumber,
                    pageSize: this.paginationSettingCompletedTasks.pageSize,
                    resourceId: this.gameServerId,
                    statusTasks: [this.StatusTask.EndWithErrors, this.StatusTask.End]
                },
                (tasks:any) => {
                    this.completedTasks = tasks.items;
                    this.completedTasks.totalPages = tasks.totalPages;
                    this.completedTasks.totalRows = tasks.totalRows;
                    angular.forEach(this.completedTasks, (task:any) => {
                        task.arrayOptions = JSON.parse(task.options);
                    });
                },
                (err:any) => {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }

        public visibleTask(task:any){
            task.show = !task.show
        }

        public pageChangeHandler(num:number){
            this.paginationSettingCompletedTasks.pageNumber = num;
            this.getProcessingTasks();
            this.getEndTasks();
        }

        public updateTaskValue(task:any){
            var value = "";

            if(task.typeTask == this.TypeTask.UpdateVm){
                value = "CPU:" + task.arrayOptions.Cpu + ", RAM:" + task.arrayOptions.Ram + ", HDD:" + task.arrayOptions.Hdd;
            }else{
                value = task.arrayOptions;
            }
            return value;
        }
    }

    var options:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            gameServerId: '<'
        },
        templateUrl: 'app/pages/subscriptions/game/singleGameServer/PaymentTask2/PaymentTask2.html',
        controller: AdminTaskV2Controller,
        controllerAs: 'vm'
    };

    angular.module("crytex.singleSubscriptionGameServer")
        .component('paymentTask', options);
}
