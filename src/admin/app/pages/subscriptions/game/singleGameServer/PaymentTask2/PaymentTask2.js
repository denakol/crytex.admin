var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var admin;
(function (admin) {
    var AdminTaskV2Controller = (function (_super) {
        __extends(AdminTaskV2Controller, _super);
        function AdminTaskV2Controller(WebServices, StatusTask, ToastNotificationService, TypeTask) {
            _super.call(this);
            this.WebServices = WebServices;
            this.StatusTask = StatusTask;
            this.ToastNotificationService = ToastNotificationService;
            this.TypeTask = TypeTask;
            this.paginationSettingCompletedTasks = {
                pageNumber: 1,
                pageSize: 10
            };
            this.paginationSettingProcessingTasks = {
                pageNumber: 1,
                pageSize: 20
            };
            this.init();
        }
        AdminTaskV2Controller.prototype.init = function () {
            this.getProcessingTasks();
            this.getEndTasks();
        };
        AdminTaskV2Controller.prototype.getProcessingTasks = function () {
            var _this = this;
            this.WebServices.AdminTaskV2.getAllPage({
                pageNumber: this.paginationSettingProcessingTasks.pageNumber,
                pageSize: this.paginationSettingProcessingTasks.pageSize,
                resourceId: this.gameServerId,
                statusTasks: [this.StatusTask.Processing, this.StatusTask.Start, this.StatusTask.Pending]
            }, function (tasks) {
                _this.processingTasks = tasks.items;
                angular.forEach(_this.processingTasks, function (task) {
                    task.arrayOptions = JSON.parse(task.options);
                });
            }, function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminTaskV2Controller.prototype.getEndTasks = function () {
            var _this = this;
            this.WebServices.AdminTaskV2.getAllPage({
                pageNumber: this.paginationSettingCompletedTasks.pageNumber,
                pageSize: this.paginationSettingCompletedTasks.pageSize,
                resourceId: this.gameServerId,
                statusTasks: [this.StatusTask.EndWithErrors, this.StatusTask.End]
            }, function (tasks) {
                _this.completedTasks = tasks.items;
                _this.completedTasks.totalPages = tasks.totalPages;
                _this.completedTasks.totalRows = tasks.totalRows;
                angular.forEach(_this.completedTasks, function (task) {
                    task.arrayOptions = JSON.parse(task.options);
                });
            }, function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminTaskV2Controller.prototype.visibleTask = function (task) {
            task.show = !task.show;
        };
        AdminTaskV2Controller.prototype.pageChangeHandler = function (num) {
            this.paginationSettingCompletedTasks.pageNumber = num;
            this.getProcessingTasks();
            this.getEndTasks();
        };
        AdminTaskV2Controller.prototype.updateTaskValue = function (task) {
            var value = "";
            if (task.typeTask == this.TypeTask.UpdateVm) {
                value = "CPU:" + task.arrayOptions.Cpu + ", RAM:" + task.arrayOptions.Ram + ", HDD:" + task.arrayOptions.Hdd;
            }
            else {
                value = task.arrayOptions;
            }
            return value;
        };
        AdminTaskV2Controller.$inject = [
            "WebServices",
            "StatusTask",
            "ToastNotificationService",
            "TypeTask"
        ];
        return AdminTaskV2Controller;
    }(admin.BaseGsComponentController));
    admin.AdminTaskV2Controller = AdminTaskV2Controller;
    var options = {
        restrict: "EA",
        bindings: {
            gameServerId: '<'
        },
        templateUrl: 'app/pages/subscriptions/game/singleGameServer/PaymentTask2/PaymentTask2.html',
        controller: AdminTaskV2Controller,
        controllerAs: 'vm'
    };
    angular.module("crytex.singleSubscriptionGameServer")
        .component('paymentTask', options);
})(admin || (admin = {}));
