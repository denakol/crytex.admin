var admin;
(function (admin) {
    var UpdateType = core.WebApi.Models.Enums.GameServerUpdateType;
    var singleSubscriptionGameServerController = (function () {
        function singleSubscriptionGameServerController($scope, $filter, $stateParams, WebServices, ToastNotificationService, TypeServerPayment, BASE_INFO, gameFamily, $mdDialog) {
            this.$scope = $scope;
            this.$filter = $filter;
            this.$stateParams = $stateParams;
            this.WebServices = WebServices;
            this.ToastNotificationService = ToastNotificationService;
            this.TypeServerPayment = TypeServerPayment;
            this.BASE_INFO = BASE_INFO;
            this.gameFamily = gameFamily;
            this.$mdDialog = $mdDialog;
            this.gameServerId = $stateParams.id;
            this.GameService = WebServices.AdminGameServerService;
            this.getGameServer();
        }
        singleSubscriptionGameServerController.prototype.getGameServer = function () {
            var _this = this;
            this.GameService.get({
                id: this.gameServerId
            }).$promise
                .then(function (data) {
                _this.gameServer = data;
                _this.gameServer.pending = false;
                _this.gameServer.show = false;
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        singleSubscriptionGameServerController.prototype.showEdit = function () {
            var _this = this;
            this.$mdDialog.show({
                controller: "DialogGameServerEditCtrl as dialog",
                templateUrl: 'app/pages/subscriptions/game/singleGameServer/singleGameServerEdit.modal.html',
                parent: angular.element(document.body),
                targetEvent: null,
                clickOutsideToClose: true,
                locals: {
                    subscriptionGs: angular.copy(this.gameServer)
                }
            })
                .then(function (answer) {
                _this.editGameServer(answer);
            }, function () { });
        };
        singleSubscriptionGameServerController.prototype.editGameServer = function (gameServer) {
            var _this = this;
            this.WebServices.AdminGameServerService.extendGameServer({
                serverId: gameServer.id,
                serverName: gameServer.name,
                autoProlongation: gameServer.autoProlongation,
                updateType: UpdateType.UpdateSettings
            }).$promise
                .then(function (data) {
                _this.gameServer.name = gameServer.name;
                _this.gameServer.autoProlongation = gameServer.autoProlongation;
                _this.ToastNotificationService('Изменено');
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        singleSubscriptionGameServerController.$inject = [
            '$scope', '$filter', '$stateParams',
            'WebServices', 'ToastNotificationService',
            'TypeServerPayment',
            'BASE_INFO', 'GameFamily', '$mdDialog'
        ];
        return singleSubscriptionGameServerController;
    }());
    admin.singleSubscriptionGameServerController = singleSubscriptionGameServerController;
    var DialogGameServerEditCtrl = (function () {
        function DialogGameServerEditCtrl($mdDialog, subscriptionGs) {
            this.$mdDialog = $mdDialog;
            this.subscriptionGs = subscriptionGs;
            this.editSubscriptionGs = subscriptionGs;
        }
        DialogGameServerEditCtrl.prototype.cancel = function () {
            this.$mdDialog.cancel();
        };
        DialogGameServerEditCtrl.prototype.answer = function () {
            this.$mdDialog.hide(this.editSubscriptionGs);
        };
        DialogGameServerEditCtrl.inject = [
            "$mdDialog",
            "subscriptionGs"
        ];
        return DialogGameServerEditCtrl;
    }());
    angular.module('crytex.singleSubscriptionGameServer')
        .controller('singleSubscriptionGameServerController', singleSubscriptionGameServerController)
        .controller('DialogGameServerEditCtrl', DialogGameServerEditCtrl);
})(admin || (admin = {}));
