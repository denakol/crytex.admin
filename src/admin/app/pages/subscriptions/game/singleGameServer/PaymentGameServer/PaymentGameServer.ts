module admin{

    //import IResourceClass = angular.resource.IResourceClass;
    import PaginationSetting = core.PaginationSetting;
    import PaymentGameServerViewModel = core.WebApi.Models.PaymentGameServerViewModel;
    import PageModel = core.PageModel;
    import PaymentViewModelType = core.WebApi.Models.Enums.PaymentViewModelType;

    export class AdminPaymentGameServerController extends BaseGsComponentController{

        public static $inject = ["WebServices", "ToastNotificationService"];

        public paginationSetting:PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        };

        //public PaymentViewModelType: PaymentViewModelType;

        public paymentGameServer: PageModel<PaymentGameServerViewModel>;

        public paymentViewModelType: any;
        constructor(private WebServices:any,
                    private ToastNotificationService:any){
            super();
            this.paymentViewModelType = PaymentViewModelType;
            this.getPaymentGameServer();
        }

        public getPaymentGameServer(){
            this.WebServices.AdminPaymentGameServerService.getAllPage({
                pageNumber: this.paginationSetting.pageNumber,
                pageSize: this.paginationSetting.pageSize,
                serverId: this.gameServerId
            }).$promise
                .then((data:any)=>{
                    this.paymentGameServer = data;
                    angular.forEach(this.paymentGameServer.items, (paymentGameServer)=>{
                        paymentGameServer.show = false;
                    });
                })
                .catch((err:any)=>{
                    this.ToastNotificationService('Не удалось выгрузить данные');
                })
        }
        
        public visiblePaymentGameServer(paymentGameServer:any){
            paymentGameServer.show = !paymentGameServer.show;
        }

        public pageChangeHandler(num:number){
            this.paginationSetting.pageNumber = num;
            this.getPaymentGameServer();
        }
    }

    var options:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            gameServerId: '<'
        },
        templateUrl: 'app/pages/subscriptions/game/singleGameServer/PaymentGameServer/PaymentGameServer.html',
        controller: AdminPaymentGameServerController,
        controllerAs: 'vm'
    }

    angular.module("crytex.singleSubscriptionGameServer")
        .component('paymentGameServer', options);
}