var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var admin;
(function (admin) {
    var PaymentViewModelType = core.WebApi.Models.Enums.PaymentViewModelType;
    var AdminPaymentGameServerController = (function (_super) {
        __extends(AdminPaymentGameServerController, _super);
        function AdminPaymentGameServerController(WebServices, ToastNotificationService) {
            _super.call(this);
            this.WebServices = WebServices;
            this.ToastNotificationService = ToastNotificationService;
            this.paginationSetting = {
                pageNumber: 1,
                pageSize: 5
            };
            this.paymentViewModelType = PaymentViewModelType;
            this.getPaymentGameServer();
        }
        AdminPaymentGameServerController.prototype.getPaymentGameServer = function () {
            var _this = this;
            this.WebServices.AdminPaymentGameServerService.getAllPage({
                pageNumber: this.paginationSetting.pageNumber,
                pageSize: this.paginationSetting.pageSize,
                serverId: this.gameServerId
            }).$promise
                .then(function (data) {
                _this.paymentGameServer = data;
                angular.forEach(_this.paymentGameServer.items, function (paymentGameServer) {
                    paymentGameServer.show = false;
                });
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminPaymentGameServerController.prototype.visiblePaymentGameServer = function (paymentGameServer) {
            paymentGameServer.show = !paymentGameServer.show;
        };
        AdminPaymentGameServerController.prototype.pageChangeHandler = function (num) {
            this.paginationSetting.pageNumber = num;
            this.getPaymentGameServer();
        };
        AdminPaymentGameServerController.$inject = ["WebServices", "ToastNotificationService"];
        return AdminPaymentGameServerController;
    }(admin.BaseGsComponentController));
    admin.AdminPaymentGameServerController = AdminPaymentGameServerController;
    var options = {
        restrict: "EA",
        bindings: {
            gameServerId: '<'
        },
        templateUrl: 'app/pages/subscriptions/game/singleGameServer/PaymentGameServer/PaymentGameServer.html',
        controller: AdminPaymentGameServerController,
        controllerAs: 'vm'
    };
    angular.module("crytex.singleSubscriptionGameServer")
        .component('paymentGameServer', options);
})(admin || (admin = {}));
