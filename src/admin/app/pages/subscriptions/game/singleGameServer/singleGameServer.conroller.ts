module admin {

    import IResourceClass = angular.resource.IResourceClass;
    import AdminGameService = core.IAdminGameService;
    import PageModel = core.PageModel;
    import GameViewModel = core.WebApi.Models.GameViewModel;
    import GameServerViewModel = core.WebApi.Models.GameServerViewModel;
    import GameFamily = core.WebApi.Models.Enums.GameFamily;
    import UpdateType = core.WebApi.Models.Enums.GameServerUpdateType;
    
    export class singleSubscriptionGameServerController {
        public static $inject = [
            '$scope', '$filter', '$stateParams',
            'WebServices', 'ToastNotificationService',
            'TypeServerPayment',
            'BASE_INFO', 'GameFamily', '$mdDialog'
        ];

        public gameServerId:string;
        private GameService: IResourceClass<PageModel<GameServerViewModel>>;
        public gameServer: GameServerViewModel;

        constructor(private $scope:any,
                    private $filter:any,
                    private $stateParams:any,
                    private WebServices:any,
                    private ToastNotificationService:any,
                    private TypeServerPayment:any,
                    public BASE_INFO:any,
                    public gameFamily:any,
                    private $mdDialog:any){
            this.gameServerId = $stateParams.id;
            this.GameService = WebServices.AdminGameServerService;
            this.getGameServer();
        }

        public getGameServer(){
            this.GameService.get({
                id: this.gameServerId
            }).$promise
                .then((data:any)=>{
                    this.gameServer = data;
                    this.gameServer.pending = false;
                    this.gameServer.show = false;
                })
                .catch((err:any)=>{
                    this.ToastNotificationService('Не удалось выгрузить данные')
                });
        }

        public showEdit(){
            this.$mdDialog.show({
                    controller: "DialogGameServerEditCtrl as dialog",
                    templateUrl: 'app/pages/subscriptions/game/singleGameServer/singleGameServerEdit.modal.html',
                    parent: angular.element(document.body),
                    targetEvent: null,
                    clickOutsideToClose: true,
                    locals: {
                        subscriptionGs: angular.copy(this.gameServer)
                    }
                })
                .then((answer:any) => {
                    this.editGameServer(answer);
                }, ()=>{})
        }
        
        public editGameServer(gameServer:any){
            this.WebServices.AdminGameServerService.extendGameServer({
                serverId: gameServer.id,
                serverName: gameServer.name,
                autoProlongation: gameServer.autoProlongation,
                updateType: UpdateType.UpdateSettings
            }).$promise
                .then((data:any)=>{
                    this.gameServer.name = gameServer.name;
                    this.gameServer.autoProlongation = gameServer.autoProlongation;
                    this.ToastNotificationService('Изменено');
                })
                .catch((err:any)=>{
                    this.ToastNotificationService('Не удалось выгрузить данные');
                });
        }

    }
    
    class DialogGameServerEditCtrl{
        static inject = [
            "$mdDialog",
            "subscriptionGs"
        ];

        public editSubscriptionGs:any;
        
        constructor(private $mdDialog:any, public subscriptionGs:any){
            this.editSubscriptionGs = subscriptionGs;
        }

        public cancel(){
            this.$mdDialog.cancel();
        }

        public answer(){
            this.$mdDialog.hide(this.editSubscriptionGs);
        }
    }


    angular.module('crytex.singleSubscriptionGameServer')
        .controller('singleSubscriptionGameServerController', singleSubscriptionGameServerController)
        .controller('DialogGameServerEditCtrl', DialogGameServerEditCtrl);
}