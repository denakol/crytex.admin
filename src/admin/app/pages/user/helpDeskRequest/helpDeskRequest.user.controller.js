﻿(function () {

    angular.module('user.helpDeskRequest')
        .controller('UserHelpDeskRequestController', userHelpDeskRequestController);

    userHelpDeskRequestController.$inject = ['$scope', '$http', '$stateParams', 'WebServices', '$mdToast', '$mdDialog', 'UrgencyLevel','TypeRequest'];

    function userHelpDeskRequestController($scope, $http, $stateParams, WebServices, $mdToast, $mdDialog, UrgencyLevel,TypeRequest) {
        $scope.helpRequest = null;
        $scope.pageNumber = 1;
        $scope.pageSize = 50;
        $scope.helpRequestComments = [];


        $scope.typeRequest = TypeRequest.arrayItems;
        $scope.urgencyLevel = UrgencyLevel;
        getRequest();
        getRequestComments();


        ///////////////////////////////////////////////////////////////
        $scope.updateRequest = updateHelpRequest;

        function updateHelpRequest() {
            WebServices.AdminHelpDeskService.update({ id: $scope.helpRequest.id }, $scope.helpRequest,
                function (ok) {

                },
                function (error) {
                    if (!isNaN(error)) {
                        showMessage('Не удалось выгрузить данные');
                    }
                }
            );
        }

        function getRequest() {
            WebServices.AdminHelpDeskService.get({
                id: $stateParams.id,
            },
            function (data) {
                $scope.helpRequest = data;
                updateReadStatus();
            },
            function (error) {
                if (!isNaN(error)) {
                    showMessage('Не удалось выгрузить данные');
                }
            }
            );
        };

        function getRequestComments() {
            WebServices.AdminHelpDeskCommentService.getAllPage({
                    Id: $stateParams.id,
                    pageNumber: $scope.pageNumber,
                    pageSize: $scope.pageSize,
                },
                function (data) {
                    $scope.helpRequestComments = data.items;
                    $scope.totalRows = data.totalRows;
                    $scope.totalPages = data.totalPages;
                },
                function (error) {
                    if (!isNaN(error)) {
                        showMessage('Не удалось выгрузить данные');
                    }
                }
            );
        }

        function updateReadStatus() {
            if (!$scope.helpRequest.read) {
                $scope.helpRequest.read = true;
                updateHelpRequest();
            }
        }

        $scope.createComment = function (comment) {
            WebServices.AdminHelpDeskCommentService.post({ Id: $scope.helpRequest.id }, comment,
                function (data) {
                    comment = data;
                    $scope.helpRequestComments.splice(0, 0, comment);
                    showMessage('Добавлено');
                },
                function (error) {
                    if (!isNaN(error)) {
                        showMessage('Не удалось выгрузить данные');
                    }
                }
            );
        }

        $scope.isAdmin = function (helpRequestUserId, helpRequestCommentsUserId) {
            var isAdmin = false;
            if (helpRequestUserId != helpRequestCommentsUserId) { isAdmin = true; }
            return isAdmin;
        }

        $scope.showEdit = function () {
            $mdDialog.show({
                controller: DialogCommentCtrl,
                templateUrl: "editCommentForm.tmpl.html",
                parent: angular.element(document.body),
                // targetEvent: index,
                clickOutsideToClose: true,
            })
                .then(function (answer) {
                    $scope.createComment(answer);
                },
                function () {

                });
        }

        function DialogCommentCtrl($scope, $mdDialog) {
            $scope.newComment = null;

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.answer = function () {
                $mdDialog.hide($scope.newComment);
            };
        }

        function showMessage(message) {
            $mdToast.show(
                $mdToast.simple()
                    .content(message)
                    .position($scope.getToastPosition())
                    .hideDelay(3000));
        }

        var last = { bottom: false, top: true, left: false, right: true };
        $scope.toastPosition = angular.extend({}, last);

        $scope.getToastPosition = function () {
            return Object.keys($scope.toastPosition)
                .filter(function (pos) { return $scope.toastPosition[pos]; })
                .join(' ');
        };
    }

})();
