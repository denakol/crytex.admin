﻿(function () {

    angular.module("crytex.user", [
        "user.list",
        "user.single",
        "user.helpDeskRequest",
        "user.helpDeskRequests",
        "user.statistic",
        "user.phoneCall"
    ]);

})();