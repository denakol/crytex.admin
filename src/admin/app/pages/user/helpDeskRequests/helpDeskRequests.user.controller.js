﻿(function () {

    angular.module('user.helpDeskRequests')
        .controller('UserHelpDeskRequestsController', userHelpDeskRequestsController);

    userHelpDeskRequestsController.$inject = ['$scope', 'WebServices','ToastNotificationService', 'UrgencyLevel','ReadType','TypeRequest'];

    function userHelpDeskRequestsController($scope, WebServices, ToastNotificationService, UrgencyLevel,ReadType,TypeRequest) {

        $scope.helpRequests = {
            items: [],
            pageNumber: 1,
            pageSize: 10,
            totalRows: 0,
            totalPages: 0,
            filter: 2
        };

        $scope.typeRequest = TypeRequest.arrayItems;
        $scope.urgencyLevel = UrgencyLevel;
        $scope.typeFilter =ReadType.arrayItems;
        $scope.typeFilter.push({ type: 2, translate: "Все" })



        loadPage();
        //////////////////////////////////////////////////////////

        $scope.loadPage = loadPage;

        function loadPage() {
            WebServices.AdminHelpDeskService.getAllPage({
                pageNumber: $scope.helpRequests.pageNumber,
                pageSize: $scope.helpRequests.pageSize,
                filter: $scope.helpRequests.filter
            },
                function (data) {
                    $scope.helpRequests.items = data.items;
                    $scope.helpRequests.totalRows = data.totalRows;
                    $scope.helpRequests.totalPages = data.totalPages;
                },
                function (error) {
                    if (!isNaN(error)) {
                        ToastNotificationService('Не удалось выгрузить данные');
                    }
                }
            );
        }

        $scope.updateRequest = function (helpRequest) {
            WebServices.AdminHelpDeskService.update({ id: helpRequest.id }, helpRequest,
                function (ok) {
                    ToastNotificationService('Обновленно');
                },
                function (error) {
                    if (!isNaN(error)) {
                        ToastNotificationService('Не удалось выгрузить данные');
                    }
                }
            );
        }

        $scope.pageChangeHandler = function(page) {
            loadPage();
        }

    }

})();