﻿(function () {

    angular.module('user.list')
        .controller('UserListController', userListController);

    userListController.$inject = ['$scope', 'WebServices', '$mdDialog', 'monitor', 'UserType', 'ToastNotificationService'];

    function userListController($scope, WebServices, $mdDialog, monitor, UserType, ToastNotificationService) {               
        $scope.users = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 5
        };
        $scope.searchParam = {
            lastname: null,
            name: null,
            patronymic: null,
            email: null,
            userName: null, //login
            typeOfUser: null,
            registerDateFrom: null,
            registerDateTo: null
        };

        $scope.userType = UserType;
        $scope.changePasswordView = false;
        $scope.isShowAddBtn = true; 

        //Functions
        $scope.pageChangeHandler = pageChangeHandler;    
        $scope.showConfirm = showConfirm;
        $scope.showEdit = showEdit;
        $scope.clearSearchParams = clearSearchParams;
        $scope.applyFilter = applyFilter;

        init();

        function init(){
            loadUsers();
        };  

        function loadUsers() {
            WebServices.AdminUserService.getAllPage({
                    pageNumber: $scope.users.currentPage,
                    pageSize: $scope.users.pageSize,                 
                    lastname: $scope.searchParam.lastname,
                    name: $scope.searchParam.name,
                    patronymic: $scope.searchParam.patronymic,
                    email: $scope.searchParam.email,
                    userName: $scope.searchParam.userName,
                    typeOfUser: $scope.searchParam.typeOfUser,
                    registerDateFrom: $scope.searchParam.registerDateFrom,
                    registerDateTo: $scope.searchParam.registerDateTo                    
                },
                function (data) {
                    $scope.users.items = data.items;
                    $scope.users.totalRows = data.totalRows;
                    $scope.users.totalPages = data.totalPages;
                },
                function (error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        };       

        function deleteUser(index) {        
            WebServices.AdminUserService.delete({id: $scope.users.items[index].id},
                function (ok) {
                    loadUsers();
                    ToastNotificationService('Удалено');
                },
                function (error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        };

        function createOrUpdateUser(user) {
            if (user.id) {
                // Обновляем
                WebServices.AdminUserService.update(user.id, user,
                    function (data) {
                        loadUsers();
                        ToastNotificationService('Добавлено');
                    },
                    function (error) {
                        ToastNotificationService('Не удалось выгрузить данные');
                    }
                );
            } else {
                // Добавляем
                WebServices.AdminUserService.save(user,
                    function (data) {
                        loadUsers();
                        ToastNotificationService('Добавлено');
                    },
                    function (error) {
                        ToastNotificationService('Не удалось выгрузить данные');
                    }
                );
            }
        };

        function showConfirm(index) {
            var confirm = $mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            $mdDialog.show(confirm).then(function () {
                deleteUser(index);
            }, function () {
                // Ну нет так нет
            });
        };

        function showEdit(index) {
            $mdDialog.show({
                    controller: DialogUserEditCtrl,
                    templateUrl: "editUserForm.tmpl.html",
                    parent: angular.element(document.body),
                    targetEvent: index,
                    clickOutsideToClose: true,
                    locals: {
                        userEdit: (typeof index !== "undefined") ? angular.copy($scope.users.items[index]) : null
                    }
                })
                .then(function (answer) {
                    createOrUpdateUser(answer);
                }, function () {

                });
        };

        function pageChangeHandler(num) {
            $scope.users.currentPage = num;
            loadUsers();
        }; 

        function clearSearchParams(){
            $scope.searchParam = {
                lastname: null,
                name: null,
                patronymic: null,
                email: null,
                userName: null, //login
                typeOfUser: null,
                registerDateFrom: null,
                registerDateTo: null
            };
            loadUsers();
        };

        function applyFilter(){
            loadUsers();
        };
        
    }

})();

function DialogUserEditCtrl($scope, $mdDialog, userEdit, UserType) {
    $scope.userEdit = userEdit;
    $scope.userType = UserType;

    ////Functions
    $scope.cancel = cancel;
    $scope.answer = answer;
    $scope.passwordTrigger = passwordTrigger;

    if ($scope.userEdit) {
        if ($scope.userEdit.password) $scope.userEdit.password = null;
    }

    function cancel() {
        $mdDialog.cancel();
    };

    function answer() {
        $mdDialog.hide($scope.userEdit);
    };

    function passwordTrigger() {
        $scope.userEdit.password = null;
        $scope.confirmPassword = null;
        $scope.changePasswordView = !$scope.changePasswordView;
        $scope.userEdit.changePassword = $scope.changePasswordView;
    }
}


