﻿(function () {

    angular.module('user.single')
        .controller('UserSingleController', userSingleController);

    var PaymentViewModelType = core.WebApi.Models.Enums.PaymentViewModelType;

    userSingleController.$inject = ['$scope', '$filter', 'WebServices', '$stateParams', 'ToastNotificationService', '$mdDialog', 'UserType', 'PaymentType', 'TransactionType', 'TypeVirtualization', 'TypeServerPayment', 'SubscriptionType', 'CountingPeriodType', 'UsageSubscriptionPaymentGroupingTypes'];

    function userSingleController($scope, $filter, WebServices, $stateParams, ToastNotificationService, $mdDialog, UserType, PaymentType, TransactionType, TypeVirtualization, TypeServerPayment, SubscriptionType, CountingPeriodType, UsageSubscriptionPaymentGroupingTypes) {

        $scope.user = null;
        $scope.payments = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 10,
        };

        $scope.transactions = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 10,
        };

        $scope.virtualMachines = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 4,
        };

        $scope.subscribeVirtualMachines = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 4,
        };

        $scope.gameServers = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 10,
        };

        $scope.fixedSubscriptionPayments= {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 10,
        };

        $scope.usageSubscriptionPayments = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 10,
            periodType: null,
            groupingType: $filter('filter')(UsageSubscriptionPaymentGroupingTypes.arrayItems, { name: 'GroupByPeriod' })[0].value
        };

        $scope.paymentsGameServer = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 10,
        };

        $scope.searchUsageSubscriptionPaymentParams = {
            periodType: null
        };

        $scope.paymentViewModelType = PaymentViewModelType;


        $scope.userType = UserType;
        $scope.paymentType = PaymentType;
        $scope.transactionType = TransactionType;
        $scope.virtualizationType = TypeVirtualization;
        $scope.serverPaymentType = TypeServerPayment;
        $scope.subscriptionType = SubscriptionType;
        $scope.periodType = CountingPeriodType.arrayItems;
        $scope.getVirtualMachineSubscriptions = getVirtualMachineSubscriptions;
        $scope.clearSearchUsageParams = clearSearchUsageParams;
        $scope.applyFilter = applyFilter;
        $scope.visiblePaymentGameServer = visiblePaymentGameServer;

        $scope.changePasswordView = false;

        getUser();
        getPayments();
        getTransactions();
        getVirtualMachines();
        getGameServerSubscriptions();
        getVirtualMachineSubscriptions();
        getFixedSubscriptionPayments();
        getUsablePeriods();
        getUsageSubscriptionPayments();
        getPaymentGameServer();


        ////////////////////////////////////////////////////////////

        $scope.pageChangeHandler = function(type, num) {
            if(type == 'payment'){
                getPayments();
            }else if(type == 'transaction'){
                getTransactions();
            }else if(type == 'machine'){
                getVirtualMachines();
            }else if(type == 'gameServer'){
                getGameServerSubscriptions();
            }else if(type == 'subscribeVirtualMachine'){
                getVirtualMachineSubscriptions();
            }else if(type == 'fixedSubscriptionPayments'){
                getFixedSubscriptionPayments();
            }else if(type == 'usageSubscriptionPayments'){
                getUsageSubscriptionPayments();
            }else if(type == 'paymentsGameServer'){
                getPaymentGameServer();
            }
        }

        function getUser() {
            WebServices.AdminUserService.get({
                id: $stateParams.id
                },
                function (data) {
                    $scope.user = data;
                    $scope.user.showUpper = false;
                    $scope.user.showBack = false;
                    $scope.user.amount = null;
                    $scope.isActive = !$scope.user.isBlocked;
                },
                function (error) {
                    if (!isNaN(error)) {
                        ToastNotificationService('Не удалось выгрузить данные');
                    }
                }
            );
        }

        $scope.deleteUser = function () {

            WebServices.AdminUserService.delete({id: $scope.user.id},
                function (ok) {
                    // Удаляем из списка

                    ToastNotificationService('Удалено');
                },
                function (error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }


        $scope.showConfirm = function () {
            var confirm = $mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            $mdDialog.show(confirm).then(function () {
                $scope.deleteUser();
            }, function () {
                // Ну нет так нет
            });
        };

        $scope.showEdit = function () {
            $mdDialog.show({
                    controller: DialogSingleUserEditCtrl,
                    templateUrl: "editUserFormOne.tmpl.html",
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    locals: {
                        userEdit: angular.copy($scope.user)
                    }
                })
                .then(function (answer) {
                    $scope.user = answer;
                    ToastNotificationService('Редактирование успешно выполнено');
                }, function () {

                });
        };

        function getPayments() {
            WebServices.AdminPaymentService.getAllPage({
                    pageNumber: $scope.payments.currentPage,
                    pageSize: $scope.payments.pageSize,
                    userId: $stateParams.id
                },
                function success(payments) {
                    $scope.payments.items = payments.items;
                    $scope.payments.totalPages = payments.totalPages;
                    $scope.payments.totalRows = payments.totalRows;
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        };

        function getTransactions() {
            WebServices.AdminBillingTransaction.getAllPage({
                    pageNumber: $scope.transactions.currentPage,
                    pageSize: $scope.transactions.pageSize,
                    userId: $stateParams.id
                },
                function success(transactions) {
                    $scope.transactions.items = transactions.items;
                    $scope.transactions.totalPages = transactions.totalPages;
                    $scope.transactions.totalRows = transactions.totalRows;
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        };

        function getVirtualMachines() {
            WebServices.AdminVirtualMachine.getAllPage({
                pageNumber: $scope.virtualMachines.currentPage,
                pageSize: $scope.virtualMachines.pageSize,
                userId: $stateParams.id
            },
            function success(machines) {
                $scope.virtualMachines.items = machines.items;
                $scope.virtualMachines.totalPages = machines.totalPages;
                $scope.virtualMachines.totalRows = machines.totalRows;
                angular.forEach($scope.virtualMachines.items, function (machine) {
                    machine.show = false;
                });
            },
            function err(error) {
                ToastNotificationService('Не удалось выгрузить данные');
            });
        };

        function getVirtualMachineSubscriptions() {
            WebServices.AdminSubscriptionVirtualMachineService.getAllPage({
                pageNumber: $scope.subscribeVirtualMachines.currentPage,
                pageSize: $scope.subscribeVirtualMachines.pageSize,
                userId: $stateParams.id
            },
            function (subscriptionsVirtualMachine) {
                if (subscriptionsVirtualMachine.items.length === 0){
                    $scope.subscribeVirtualMachines.items = [];
                } else {
                    $scope.subscribeVirtualMachines.items = subscriptionsVirtualMachine.items;
                    angular.forEach($scope.subscribeVirtualMachines.items, function(value) {
                        value.autoProlongation = value.autoProlongation ? "Включено" : "Отключено";
                        value.isShowButton = false;
                        value.show = false;
                    });
                    $scope.subscribeVirtualMachines.totalRows = subscriptionsVirtualMachine.totalRows;
                    $scope.subscribeVirtualMachines.totalPages = subscriptionsVirtualMachine.totalPages;
                }
            },
            function (error) {
                ToastNotificationService('Не удалось выгрузить данные');
            });
        };

        function getGameServerSubscriptions() {
            WebServices.AdminGameServerService.getAllPage({
                pageNumber: $scope.gameServers.currentPage,
                pageSize: $scope.gameServers.pageSize,
                userId: $stateParams.id
            },
            function success(gameServerSubscriptions) {
                $scope.gameServers.items = gameServerSubscriptions.items;
                angular.forEach($scope.gameServers.items, function(gameServer) {
                    gameServer.paymentType = $filter('filter')($scope.serverPaymentType.arrayItems, { value: gameServer.paymentType })[0];
                    gameServer.isShowButton = false;
                    gameServer.show = false;
                });
                $scope.gameServers.totalRows = gameServerSubscriptions.totalRows;
                $scope.gameServers.totalPages = gameServerSubscriptions.totalPages;
            },
            function (error) {
                ToastNotificationService('Не удалось выгрузить данные');
            });
        }

        function getFixedSubscriptionPayments(){
            WebServices.AdminFixedSubscriptionPayment.getAllPage({
                    pageNumber: $scope.fixedSubscriptionPayments.currentPage,
                    pageSize: $scope.fixedSubscriptionPayments.pageSize,
                    userId: $stateParams.id
                },
                function success(result) {
                    $scope.fixedSubscriptionPayments.items = result.items;
                    $scope.fixedSubscriptionPayments.totalRows = result.totalRows;
                    $scope.fixedSubscriptionPayments.totalPages = result.totalPages;

                    if (result.items.length === 0) {
                        ToastNotificationService('Ещё нет данных списания по фиксированным подпискам');
                        $scope.fixedSubscriptionPayments.items = [];
                    }
                },
                function err(error) {
                    $scope.fixedSubscriptionPayments.items = [];
                    ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        };

        function getUsageSubscriptionPayments(){

            var groupingType = $filter('filter')(UsageSubscriptionPaymentGroupingTypes.arrayItems, { name: 'GroupByPeriod' })[0].value;

            if (!$scope.searchUsageSubscriptionPaymentParams.periodType){
                groupingType = $filter('filter')(UsageSubscriptionPaymentGroupingTypes.arrayItems, { name: 'GroupNone' })[0].value;
            }

            WebServices.AdminUsageSubscriptionPaymentService.getAllPage({
                    pageNumber: $scope.usageSubscriptionPayments.currentPage,
                    pageSize: $scope.usageSubscriptionPayments.pageSize,
                    userId: $stateParams.id,
                    periodType: $scope.searchUsageSubscriptionPaymentParams.periodType,
                    groupingType: groupingType
                },
                function success(result) {
                    $scope.usageSubscriptionPayments.items = result.items;
                    $scope.usageSubscriptionPayments.totalRows = result.totalRows;
                    $scope.usageSubscriptionPayments.totalPages = result.totalPages;
                    if ($scope.searchUsageSubscriptionPaymentParams.periodType){
                        $scope.usageSubscriptionPayments.periodType = $filter('filter')($scope.periodType, { value: $scope.searchUsageSubscriptionPaymentParams.periodType })[0].translate;
                    } else {
                        $scope.usageSubscriptionPayments.periodType = null;
                    }

                    var totalAmount = 0;

                    angular.forEach($scope.usageSubscriptionPayments.items, function (usageSubscriptionPayment) {
                        usageSubscriptionPayment.show = false;
                        usageSubscriptionPayment.periodType = $filter('filter')($scope.periodType, { value: $scope.searchUsageSubscriptionPaymentParams.periodType })[0].translate;

                        if ($scope.searchUsageSubscriptionPaymentParams.periodType){
                            angular.forEach(usageSubscriptionPayment.usageSubscriptionPayment, function (payment) {
                                totalAmount += payment.amount;
                            });
                        }
                        usageSubscriptionPayment.totalAmount = totalAmount;
                    });

                    if (result.items.length === 0) {
                        ToastNotificationService('Ещё нет данных');
                        $scope.usageSubscriptionPayments.items = [];
                    }
                },
                function err(error) {
                    $scope.usageSubscriptionPayments.items = [];
                    ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        };

        function getPaymentGameServer(){

            WebServices.AdminPaymentGameServerService.getAllPage({
                pageNumber: $scope.paymentsGameServer.currentPage,
                pageSize: $scope.paymentsGameServer.pageSize,
                userId: $stateParams.id
            }).$promise
                .then(function(data){
                    $scope.paymentsGameServer.items = data.items;
                    $scope.paymentsGameServer.totalPages = data.totalPages;
                    $scope.paymentsGameServer.totalRows = data.totalRows;
                    angular.forEach($scope.paymentsGameServer.items, function(paymentGameServer){
                        paymentGameServer.show = false;
                    });
                })
                .catch(function(err){
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        }

        function visiblePaymentGameServer(paymentGameServer) {
            paymentGameServer.show = !paymentGameServer.show;
        }

        function applyFilter() {
            getUsageSubscriptionPayments();
        };

        function clearSearchUsageParams() {
            $scope.searchUsageSubscriptionPaymentParams = {
                periodType: null
            };
            getUsageSubscriptionPayments();
        };

        function getUsablePeriods() {
            angular.forEach($scope.periodType, function (periodType) {
                if (periodType.name == 'Day' || periodType.name == 'Month'){
                    periodType.isShow = true;
                } else {
                    periodType.isShow = false;
                }
            });
            $scope.periodType.push({value:null, translate:'Не группировать',isShow: true});

            var init = $filter('filter')($scope.periodType, { name: 'Day' })[0].value;
            $scope.searchUsageSubscriptionPaymentParams.periodType = init;
        };

        $scope.locking = function (user){
            updateState(user.isBlocked);
        }

        function updateState(state){
            WebServices.AdminUserService.updateState({
                userId: $stateParams.id,
                block: !state
            },
            function success() {
                ToastNotificationService('Состояние изменено');
            },
            function err(error) {
                ToastNotificationService('Не удалось выгрузить данные');
            });
        }

        $scope.visibleMachine = function(machine){
            machine.show = !machine.show;
        }

        $scope.visibleGameServerSubscription = function(GameServerSubscription){
            GameServerSubscription.show = !GameServerSubscription.show;
        }

        $scope.visibleVirtualMachineSubscription = function(virtualMachineSubscription){
            virtualMachineSubscription.show = !virtualMachineSubscription.show;
        }

        $scope.up = function(user){
            updateBalance(user.amount);
        }
        $scope.back = function(user){
            updateBalance(-(user.amount));
        }

        function updateBalance(amount){
            WebServices.AdminUserService.updateBalance({
                    "userId": $stateParams.id,
                    "amount": amount
                },
                function () {
                    $scope.user.showUpper = false;
                    $scope.user.showBack = false;
                    $scope.user.amount = null;
                    $scope.user.balance += amount;
                },
                function (error) {
                    if (!isNaN(error)) {
                        ToastNotificationService('Не удалось выгрузить данные');
                    }
                }

            );
        }

    }

})();

function DialogSingleUserEditCtrl($scope, $mdDialog, userEdit, UserType, WebServices, ToastNotificationService) {
    $scope.userEdit = userEdit;
    $scope.userType = UserType;

    if ($scope.userEdit.password) $scope.userEdit.password = null;

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.answer = function () {
        $scope.updateUser($scope.userEdit);
    };

    $scope.passwordTrigger = function () {
        $scope.userEdit.password = null;
        $scope.confirmPassword = null;
        $scope.changePasswordView = !$scope.changePasswordView;
        $scope.userEdit.changePassword = $scope.changePasswordView;
    };

    $scope.updateUser = function (user) {
        // Обновляем
        WebServices.AdminUserService.update(user.id, user,
            function (data) {
                $scope.user = user;
                $mdDialog.hide(user);
            },
            function (error) {
                if (!isNaN(error)) {
                    ToastNotificationService('Не удалось выгрузить данные');
                }
            }
        );
    };
}


