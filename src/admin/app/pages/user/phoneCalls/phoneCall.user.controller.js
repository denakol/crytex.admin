﻿(function () {

    angular.module('user.phoneCall')
        .controller('PhoneCallController', phoneCallController);

    phoneCallController.$inject = ['$scope', 'WebServices', 'ToastNotificationService', 'ReadType'];

    function phoneCallController($scope, WebServices, ToastNotificationService, ReadType) {
        $scope.phoneCalls = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 5,
        };
        $scope.readType = ReadType;
        getPhoneCalls();


        function getPhoneCalls() {
            WebServices.AdminPhoneCallService.getAllPage({
                    pageNumber: $scope.phoneCalls.currentPage,
                    pageSize: $scope.phoneCalls.pageSize,
                },
                function (data) {
                    $scope.phoneCalls.items = data.items;
                    $scope.phoneCalls.totalRows = data.totalRows;
                    $scope.phoneCalls.totalPages = data.totalPages;
                },
                function (error) {
                    if (!isNaN(error)) {
                        ToastNotificationService('Не удалось выгрузить данные');
                    }
                });
        }

        $scope.readCallRequest =function (call){
            call.isRead = true;
            WebServices.AdminPhoneCallService.update(call.id, call,
                function (data) {
                    var index = $scope.phoneCalls.items.map(function (e) {
                        return e.id;
                    }).indexOf(call.id);
                    $scope.phoneCalls.items[index] = call;
                    ToastNotificationService('Обновлено');
                },
                function (error) {
                    if (!isNaN(error)) {
                        ToastNotificationService('Не удалось выгрузить данные');
                    }
                }
            );
        }
    }

})();
