(function () {

	angular.module('crytex.tariffOfVirtualMachine')
        .controller('tariffOfVirtualMachineController', tariffOfVirtualMachineController);

	tariffOfVirtualMachineController.$inject = ['$scope', '$filter', 'WebServices', 'ToastNotificationService', 'TypeVirtualization', 'TypeOfOperatinSystem'];

	function tariffOfVirtualMachineController($scope, $filter, WebServices, ToastNotificationService, TypeVirtualization, TypeOfOperatinSystem) {
		$scope.tariffs = [];
		$scope.currences = [
			{name: 'rubl', sign: '₽'},
			{name: 'funt', sign: '£'},
			{name: 'euro', sign: '€'},
			{name: 'dollar', sign: '$'}
		];
		$scope.currentCurrence = $scope.currences[0];

		$scope.editPrice = editPrice;
		$scope.savePrice = savePrice;
		$scope.closeEditPrice = closeEditPrice;

		$scope.typeOfVirtualization = TypeVirtualization.arrayItems;
		$scope.typeOfOperatingSystem = TypeOfOperatinSystem.arrayItems;

		initialize();

		function initialize(){
			initializeTariffs();
			getPrices();
		};

		function initializeTariffs(){
			$scope.tariffs = [];
			for (var i = 0; i <$scope.typeOfOperatingSystem.length ; i++) {
				for (var j = 0; j < $scope.typeOfVirtualization.length; j++) {
					var tariff = {};
					tariff.processor1 = '-';
					tariff.RAM512 = '-';
					tariff.HDD1 = '-';
					tariff.SSD1 = '-';
					tariff.load10Percent = '-';
					tariff.operatingSystem = $scope.typeOfOperatingSystem[i].value;
					tariff.virtualization = $scope.typeOfVirtualization[j].value;
					tariff.name = $scope.typeOfOperatingSystem[i].name + ' ' + $scope.typeOfVirtualization[j].name;
					tariff.isEdit = false;
					tariff.editablePrice = angular.copy(tariff);
					$scope.tariffs.push(tariff);
				}
			};
		};

		function getPrices(){
			WebServices.AdminPriceService.getAll({
				virtualization: null
			},
			function (data) {
				for (var i = 0; i < data.length; i++) {
					// Найдём тариф(среди созданных раннее шаблонов) с таким же типом виртуализации и заменим данные в нём
					var tariff = $filter('filter')($scope.tariffs, { virtualization: data[i].virtualization, operatingSystem: data[i].operatingSystem })[0];
					if (tariff){
						var tariffTemplate = convertValues(data[i]);
						tariffTemplate.name = tariff.name;
						
						// Заменим сам элемент в массиве всех тарифов
						var index = $scope.tariffs.indexOf(tariff);
						$scope.tariffs[index] = tariffTemplate;
					} else {
						var tariffTemplate = convertValues(data[i]);
						var operatingSystemName = $filter('filter')($scope.typeOfOperatingSystem, { value: data[i].operatingSystem})[0].name;
						var virtualizationName = $filter('filter')($scope.typeOfVirtualization, { value: data[i].virtualization})[0].name;
						tariffTemplate.name = operatingSystemName + ' ' + virtualizationName;
						// Добавляем новый элемент в список
						$scope.tariffs.push(tariffTemplate);
					}
				}
			},
			function (error) {
				ToastNotificationService('Нет данных о тарифах');
			});
		};

		function convertValues(tariffFromServer){
			var tariffTemplate = {};
			// Установим параметры(с сервера приходят не совсем адекватные названия переменных)
			tariffTemplate.id = tariffFromServer.id;
			tariffTemplate.processor1 = tariffFromServer.processor1;
			tariffTemplate.RAM512 = tariffFromServer.raM512;
			tariffTemplate.HDD1 = tariffFromServer.hdD1;
			tariffTemplate.SSD1 = tariffFromServer.ssD1;
			tariffTemplate.createDate = tariffFromServer.createDate;
			tariffTemplate.updateDate = tariffFromServer.updateDate;
			tariffTemplate.load10Percent = tariffFromServer.load10Percent;
			tariffTemplate.virtualization = tariffFromServer.virtualization;
			tariffTemplate.operatingSystem = tariffFromServer.operatingSystem;
			tariffTemplate.isEdit = false;
			tariffTemplate.editablePrice = angular.copy(tariffTemplate);

			return tariffTemplate;
		}

		function editPrice(tariffVirtualization, tariffOS){
			var editableTariff = $filter('filter')($scope.tariffs, { virtualization: tariffVirtualization, operatingSystem : tariffOS })[0];
			editableTariff.isEdit = true;
		};

		function closeEditPrice(tariffVirtualization, tariffOS){
			var editableTariff = $filter('filter')($scope.tariffs, { virtualization: tariffVirtualization, operatingSystem : tariffOS })[0];
			editableTariff.isEdit = false;
		}

		function savePrice(tariffVirtualization, tariffOS){
			var tariff = $filter('filter')($scope.tariffs, { virtualization: tariffVirtualization, operatingSystem : tariffOS })[0];
			tariff = tariff.editablePrice;

			if (!isNaN(tariff.processor1) && tariff.processor1 > 0 &&
					!isNaN(tariff.RAM512) && tariff.RAM512 > 0 &&
					!isNaN(tariff.HDD1) && tariff.HDD1 > 0 &&
					!isNaN(tariff.SSD1) && tariff.SSD1 > 0 &&
					!isNaN(tariff.load10Percent) && tariff.load10Percent >= 0
				){
				if (!tariff.isUpdate){
					// Сохраняем тариф
					WebServices.AdminPriceService.save({
							virtualization: tariff.virtualization,
							processor1: tariff.processor1,
							RAM512: tariff.RAM512,
							HDD1: tariff.HDD1,
							SSD1: tariff.SSD1,
							load10Percent: tariff.load10Percent,
							createDate: new Date()
						},
						function (data) {
							ToastNotificationService('Сохранено');	
							tariff.isEdit = false;
							getPrices();				
						},
						function (error) {
							ToastNotificationService('Не удалось сохранить данные');
							$scope.isHyperVPriceEdit = false;					
					});
				} else {
					// Обновляем тариф
					WebServices.AdminPriceService.update({
							id: tariff.id,
							virtualization: tariff.virtualization,
							processor1: tariff.processor1,
							RAM512: tariff.RAM512,
							HDD1: tariff.HDD1,
							SSD1: tariff.SSD1,
							load10Percent: tariff.load10Percent,
							createDate: new Date()
						},
						function (data) {
							ToastNotificationService('Сохранено');	
							tariff.isEdit = false;
							getPrices();			
						},
						function (error) {
							ToastNotificationService('Не удалось сохранить данные');
							$scope.isHyperVPriceEdit = false;					
					});
				}
			} else ToastNotificationService("Все цены должны быть больше 0");
		};	

	}	
})();
