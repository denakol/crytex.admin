(function () {
    angular.module('machine.virtualMachine')
        .controller('VirtualMachineController', virtualMachineController);

    virtualMachineController.$inject =
        ['$scope', 'WebServices','$stateParams', '$mdSidenav', '$mdDialog', 'TypeTask', 'StatusTask', 'ResourceType', 'BASE_INFO', 'ToastNotificationService', 'monitorTask', 'TypeNotify', 'StatusVm'];

    function virtualMachineController($scope, WebServices, $stateParams, $mdSidenav, $mdDialog, TypeTask, StatusTask, ResourceType, BASE_INFO, ToastNotificationService, monitorTask, TypeNotify, StatusVm) {
        $scope.currentVirtualMachine = null;
        $scope.statusVm = StatusVm;
        $scope.typeTask  = TypeTask;
        $scope.BASE_INFO = BASE_INFO;

        $scope.processingTasks = {};
        $scope.completedTasks = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 10,
        };


        $scope.snapshotVms = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 4,
        };

        $scope.backupVms = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 4,
        };

        $scope.manageMachine = [
            {name: 'Запуск', value: 0, picture: 'play_arrow'},
            {name: 'Остановка', value: 1, picture: 'pause'},
            {name: 'Перезагрузить', value: 2, picture: 'replay'},
            {name: 'Выключить', value: 3, picture: 'stop'}
        ];


        getVirtualMachine();
        getSnapShotVms();
        getEndTasks();
        getVmBackup();

        $scope.updateTaskValue = function(task){
            var value = null;
            if(task.typeTask == TypeTask.UpdateVm){
                value = "CPU:"+ task.arrayOptions.Cpu+", RAM:"+task.arrayOptions.Ram+", HDD:"+task.arrayOptions.Hdd;
            }else{
                value = task.arrayOptions;
            }
            return value;
        }

        $scope.visibleTask = function(task){
            task.show = !task.show;
        }

        var originatorEv;

        $scope.openMenu = function ($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };

        $scope.pageChangeHandler = function(type, num) {
            if(type == 'task'){
                getProcessingTasks();
                getEndTasks();
            }else if(type == 'snapshot'){
                getSnapShotVms();
            }else if(type == 'backup'){
                getSnapShotVms();
            }

        }

        $scope.changeState = function (value) {
            WebServices.AdminTaskV2.save({}, {
                    TypeTask: TypeTask.ChangeStatus,
                    StatusTask: StatusTask.Pending,
                    ResourceType: ResourceType.Vm,
                    ResourceId: $scope.currentVirtualMachine.id,
                    Virtualization: $scope.currentVirtualMachine.typeVirtualization,
                    Options: JSON.stringify({
                        TypeChangeStatus : value,
                        VmId : $scope.currentVirtualMachine.id
                    })
                },
                function success(data) {
                    ToastNotificationService('Статус изменен');
                    getProcessingTasks();
                },
                function err(error) {
                    ToastNotificationService('Не удалось изменить статус');
                });
            $scope.currentVirtualMachine.pendingStatus = true;
        };
        $scope.vmId =$stateParams.id;

        function getVirtualMachine() {
            WebServices.AdminVirtualMachine.get({
                    id: $stateParams.id
                },
                function success(machine) {
                    $scope.currentVirtualMachine = machine;
                    $scope.currentVirtualMachine.pendingStatus = false;
                    getProcessingTasks();
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        };



        function getProcessingTasks() {
            WebServices.AdminTaskV2.getAllPage({
                    pageNumber: 1,
                    pageSize: 20,
                    resourceId: $stateParams.id,
                    statusTasks: [StatusTask.Processing, StatusTask.Start, StatusTask.Pending]
                },
                function success(tasks) {
                    $scope.processingTasks.items = tasks.items;
                    if($scope.processingTasks.items.length != 0 && $scope.currentVirtualMachine) $scope.currentVirtualMachine.pendingStatus = true;
                    angular.forEach($scope.processingTasks.items, function (task) {
                        task.arrayOptions = JSON.parse(task.options);
                    });
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        };

        function getEndTasks() {
            WebServices.AdminTaskV2.getAllPage({
                    pageNumber: $scope.completedTasks.currentPage,
                    pageSize: $scope.completedTasks.pageSize,
                    resourceId: $stateParams.id,
                    statusTasks: [StatusTask.EndWithErrors, StatusTask.End]
                },
                function success(tasks) {
                    $scope.completedTasks.items = tasks.items;
                    $scope.completedTasks.totalPages = tasks.totalPages;
                    $scope.completedTasks.totalRows = tasks.totalRows;
                    angular.forEach($scope.completedTasks.items, function (task) {
                        task.arrayOptions = JSON.parse(task.options);
                    });

                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        };

        function getSnapShotVms() {
            WebServices.AdminSnapShotVmService.getAllPage({
                    pageNumber: $scope.snapshotVms.currentPage,
                    pageSize: $scope.snapshotVms.pageSize,
                    vmId: $stateParams.id
                },
                function success(snapshot) {
                    $scope.snapshotVms.items = snapshot.items;
                    $scope.snapshotVms.totalPages = snapshot.totalPages;
                    $scope.snapshotVms.totalRows = snapshot.totalRows;
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        };

        function getVmBackup() {
            WebServices.AdminVmBackupService.getAllPage({
                    pageNumber: $scope.backupVms.currentPage,
                    pageSize: $scope.backupVms.pageSize,
                    vmId: $stateParams.id
                },
                function success(backup) {
                    $scope.backupVms.items = backup.items;
                    $scope.backupVms.totalPages = backup.totalPages;
                    $scope.backupVms.totalRows = backup.totalRows;
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        };



        $scope.deleteVirtualMachine = function (index) {

        }

        $scope.changeVirtualMachine = function (machine) {
            WebServices.AdminTaskV2.save({}, {
                    TypeTask: TypeTask.UpdateVm,
                    StatusTask: StatusTask.Pending,
                    ResourceType: ResourceType.Vm,
                    ResourceId: machine.id,
                    Virtualization: machine.typeVurtualization,
                    Options: JSON.stringify({
                        Cpu: machine.cpu,
                        Ram: machine.ram,
                        Hdd: machine.hdd,
                        Name: machine.name,
                        VmId: machine.id
                    })
                },
                function success(value) {
                    ToastNotificationService('Добавлено');
                    getProcessingTasks();
                },
                function err(error) {
                    ToastNotificationService('Не удалось добавить');
                });
        };


        $scope.showConfirm = function (index) {
            var confirm = $mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            $mdDialog.show(confirm).then(function () {
                $scope.deleteVirtualMachine(index);
            }, function () {
                // Ну нет так нет
            });
        };

        $scope.showAdvanced = function (index) {
            $mdDialog.show({
                controller: DialogVirtualMachineController,
                templateUrl: 'editVirtualMachineForm.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: index,
                clickOutsideToClose: true,
                locals: {
                    machine: angular.copy($scope.currentVirtualMachine)
                }
            })
                .then(function (answer) {
                    $scope.changeVirtualMachine(answer);
                }, function () {
                });
        };

        var myMonitorTask = new monitorTask({
            callback: function (baseNotify) {
                if (baseNotify.TypeNotify == TypeNotify.EndTask) {
                    ToastNotificationService("Задача завершена");

                    getProcessingTasks();
                    getEndTasks();

                    if(baseNotify.Task.TypeTask == TypeTask.ChangeStatus){
                        $scope.currentVirtualMachine.pendingStatus = false;
                        if(baseNotify.Task.Options.TypeChangeStatus == $scope.manageMachine[0].value ||
                            baseNotify.Task.Options.TypeChangeStatus == $scope.manageMachine[2].value )
                        {
                            $scope.currentVirtualMachine.status = StatusVm.Enable;
                        }else{
                            $scope.currentVirtualMachine.status = StatusVm.Disable;
                        }
                    }
                    if(baseNotify.Task.TypeTask == TypeTask.UpdateVm){
                        getVirtualMachine();
                    }
                }
            }
        });

    };

    function DialogVirtualMachineController($scope, $mdDialog, machine, TypeVirtualization, ToastNotificationService) {

        $scope.machine = machine;
        $scope.TypeVirtualization = TypeVirtualization.arrayItems;
        $scope.machine.cpu = machine.coreCount;
        $scope.machine.ram = machine.ramCount;
        $scope.machine.hdd = machine.hardDriveSize;

        function checkInfo() {
            if ($scope.machine.cpu >= machine.operatingSystem.minCoreCount && $scope.machine.ram >= machine.operatingSystem.minRamCount && $scope.machine.hdd >= machine.hardDriveSize) {
                if($scope.machine.cpu != machine.coreCount || $scope.machine.ram != machine.ramCount || $scope.machine.hdd != machine.hardDriveSize){
                    return true;
                }
                else{
                    ToastNotificationService('Должен быть изменен хотябы один параметр машины');
                    return false;
                }
            } else {
                ToastNotificationService('Необходимо корректно заполнить все поля');
                return false;
            }
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.answer = function () {
            if (checkInfo()) {
                $mdDialog.hide($scope.machine);
            }
        };
    }
})();