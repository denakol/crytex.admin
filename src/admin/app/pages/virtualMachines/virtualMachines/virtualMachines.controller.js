"use strict";

(function () {
    angular.module('machine.virtualMachines')
        .controller('VirtualMachinesController', virtualMachinesController);

    virtualMachinesController.$inject =

        ['$scope', 'WebServices', '$mdDialog', 'TypeTask', 'ResourceType', 'BASE_INFO', 'ToastNotificationService', 'monitorTask', 'TypeNotify', 'StatusTask', 'StatusVm', 'TypeVirtualization'];


    function virtualMachinesController($scope, WebServices, $mdDialog, TypeTask, ResourceType, BASE_INFO, ToastNotificationService, monitorTask, TypeNotify, StatusTask, StatusVm, TypeVirtualization) {
        $scope.defaultVirtualMachines = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 5
        };        
        $scope.searchParam = {
            user: null,
            virtualization: null,
            createDateFrom: null,
            createDateTo: null
        };
        $scope.defaultTasks = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 5
        };
        $scope.manageMachine = [
            {name: 'Запуск', value: 0, picture: 'play_arrow'},
            {name: 'Остановка', value: 1, picture: 'pause'},
            {name: 'Перезагрузить', value: 2, picture: 'replay'},
            {name: 'Выключить', value: 3, picture: 'stop'}
        ];

        $scope.typeTask = TypeTask;
        $scope.statusVm = StatusVm;
        $scope.typeOfVirtualization = TypeVirtualization.arrayItems;

        var originatorEv;

        //////functions
        $scope.applyFilter = applyFilter;
        $scope.clearSearchParams = clearSearchParams;
        $scope.createVirtualMachine = createVirtualMachine;
        $scope.showConfirm = showConfirm;
        $scope.showAdvanced = showAdvanced;


        function clearSearchParams(){
            $scope.searchParam = {
                user: null,
                virtualization: null,
                createDateFrom: null,
                createDateTo: null
            };
            getVirtualMachines();
        };

        function applyFilter(){
            getVirtualMachines();
        };

        function initialize(){
            getVirtualMachines();
            getTasks();
        }
        initialize;

        $scope.openMenu = function ($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };        

        $scope.changeState = function (machine, value) {
            WebServices.AdminTaskV2.save({}, {
                    TypeTask: TypeTask.ChangeStatus,
                    StatusTask: StatusTask.Pending,
                    ResourceType: ResourceType.Vm,
                    ResourceId: machine.id,
                    Virtualization: machine.typeVirtualization,
                    Options: JSON.stringify({
                        TypeChangeStatus : value,
                        VmId : machine.id
                    })
                },
                function success(data) {
                    ToastNotificationService('Статус изменен');
                    getTasks();
                },
                function err(error) {
                    ToastNotificationService('Не удалось изменить статус');
                });
            machine.pendingStatus = true;
        };

        function getVirtualMachines() {
            WebServices.AdminVirtualMachine.getAllPage({
                    pageNumber: $scope.defaultVirtualMachines.currentPage,
                    pageSize: $scope.defaultVirtualMachines.pageSize,
                    userId: $scope.searchParam.user ? $scope.searchParam.user.id : null,
                    virtualization: $scope.searchParam.virtualization,
                    createDateFrom: $scope.searchParam.createDateFrom,
                    createDateTo: $scope.searchParam.createDateTo                
                },
                function success(machines) {
                    $scope.defaultVirtualMachines.items = machines.items;
                    $scope.defaultVirtualMachines.totalPages = machines.totalPages;
                    $scope.defaultVirtualMachines.totalRows = machines.totalRows;
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        };
        getVirtualMachines();


        function getTasks() {
            WebServices.AdminTaskV2.getAllPage({
                    pageNumber: $scope.defaultTasks.currentPage,
                    pageSize: $scope.defaultTasks.pageSize,
                    statusTasks: [StatusTask.Processing, StatusTask.Start, StatusTask.Pending]
                },
                function success(tasks) {
                    $scope.defaultTasks.items = tasks.items;
                    angular.forEach($scope.defaultTasks.items, function (task) {
                        task.arrayOptions = JSON.parse(task.options);
                    });
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        };
        getTasks();


        function deleteVirtualMachine(index) {

        }

        function createVirtualMachine(machine) {
            WebServices.AdminTaskV2.save({}, {
                    TypeTask: TypeTask.CreateVm,
                    StatusTask: StatusTask.Pending,
                    ResourceType: ResourceType.Vm,
                    Virtualization: machine.typeVirtualization,
                    Options: JSON.stringify({
                        Cpu: machine.cpu,
                        Ram: machine.ram,
                        Hdd: machine.hdd,
                        Name: machine.name,
                        OperatingSystemId: machine.os
                    })
                },
                function success(value) {
                    ToastNotificationService('Добавлено');
                    getTasks();
                },
                function err(error) {
                    ToastNotificationService('Не удалось добавить');
                });
        };

        function showConfirm(index) {
            var confirm = $mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            $mdDialog.show(confirm).then(function () {
                $scope.deleteVirtualMachine(index);
            }, function () {
                // Ну нет так нет
            });
        };

        function showAdvanced(index) {
            $mdDialog.show({
                controller: DialogVirtualMachineController,
                templateUrl: 'editVirtualMachineForm.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: index,
                clickOutsideToClose: true,
                locals: {
                    machine: index ? angular.copy($scope.defaultVirtualMachines.items[index]) : null
                }
            })
                .then(function (answer) {
                    $scope.createVirtualMachine(answer);
                }, function () {
                });
        };        

        var myMonitorTask = new monitorTask({
            callback: function (baseNotify) {
                if (baseNotify.TypeNotify == TypeNotify.EndTask) {
                    ToastNotificationService("Задача завершена");
                    $scope.getVirtualMachines();
                    if(baseNotify.Task.TypeTask == TypeTask.CreateVm){
                        getTasks();
                    }
     
                    if(baseNotify.Task.TypeTask == TypeTask.ChangeStatus){
                        var index = $scope.defaultVirtualMachines.items.map(function (e) { return e.id; }).indexOf(baseNotify.Task.ResourceId);
                        $scope.defaultVirtualMachines.items[index].pendingStatus = false;
                        if(baseNotify.Task.Options.TypeChangeStatus == $scope.manageMachine[0].value ||
                            baseNotify.Task.Options.TypeChangeStatus == $scope.manageMachine[2].value )
                        {
                            $scope.defaultVirtualMachines.items[index].status = StatusVm.Enable
                        }else{
                            $scope.defaultVirtualMachines.items[index].status = StatusVm.Disable
                        }
                    }
                }
            }
        });

    };

    function DialogVirtualMachineController($scope, $mdDialog, machine, TypeVirtualization, $filter, ToastNotificationService, WebServices) {
        function checkInfo() {
            if ($scope.machine.name != '' && !isNaN($scope.machine.typeVirtualization) && !isNaN($scope.machine.os) &&
                $scope.machine.cpu >= $scope.os.minCoreCount &&
                $scope.machine.ram >= $scope.os.minRamCount &&
                $scope.machine.hdd >= $scope.os.minHardDriveSize) {
                return true;
            } else {
                return false;
            }
        };

        $scope.machine = machine;
        $scope.TypeVirtualization = TypeVirtualization.arrayItems;
        $scope.OSs = [];
        $scope.os = {};

        $scope.getOS = function () {
            WebServices.AdminOperatingSystem.query({},
                function success(os) {
                    $scope.OSs = os;
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        };
        $scope.getOS();

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.answer = function () {
            if (checkInfo()) {
                $mdDialog.hide($scope.machine);
            } else {
                ToastNotificationService('Необходимо корректно заполнить все поля');
            }
        };

        $scope.$watch('machine.cpu', function () {
            if ($scope.machine && $scope.machine.cpu != '') {
                if (Number($scope.machine.cpu) < $scope.os.minCoreCount) {
                    ToastNotificationService('Значение CPU должно быть > ' + $scope.os.minCoreCount);
                }
            }
        });

        $scope.$watch('machine.ram', function () {
            if ($scope.machine && $scope.machine.ram != '') {
                if (Number($scope.machine.ram) < $scope.os.minRamCount) {
                    ToastNotificationService('Значение RAM должно быть > ' + $scope.os.minRamCount);
                }
            }
        });

        $scope.$watch('machine.hdd', function () {
            if ($scope.machine && $scope.machine.hdd != '') {
                if (Number($scope.machine.hdd) < $scope.os.minHardDriveSize) {
                    ToastNotificationService('Значение HDD должно быть > ' + $scope.os.minHardDriveSize);
                }
            }
        });

        $scope.chooseOS = function () {
            if ($scope.machine && $scope.machine.os) {
                $scope.os = $filter('filter')($scope.OSs, {id: $scope.machine.os})[0];
            }
        };
    }
})();