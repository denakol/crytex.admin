﻿(function () {

    angular.module("crytex.machine", [
        "machine.virtualMachines",
        "machine.virtualMachine",
    ]);

})();