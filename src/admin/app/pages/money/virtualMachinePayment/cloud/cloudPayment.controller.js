﻿(function () {

    angular.module('virtualMachinePayment.cloudPayment')
        .controller('CloudPaymentController', cloudPaymentController);

    cloudPaymentController.$inject = ['$scope', '$filter', 'WebServices', 'ToastNotificationService', 'TypeVirtualization', 'CountingPeriodType', 'UsageSubscriptionPaymentGroupingTypes'];

    function cloudPaymentController($scope, $filter, WebServices, ToastNotificationService, TypeVirtualization, CountingPeriodType, UsageSubscriptionPaymentGroupingTypes) {
        $scope.usageVms = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 5,
            virtualization: null,
            startDate: null,
            endDate: null,
            userId: null,
            subscriptionVmId: null,
            user: null,
            periodType: null,
            groupingType: $filter('filter')(UsageSubscriptionPaymentGroupingTypes.arrayItems, { name: 'GroupByPeriodAndSubscriptionVm' })[0].value
        };

        $scope.isEnableFilter = false;
        $scope.typeVirtualization = TypeVirtualization;
        $scope.periodType = CountingPeriodType.arrayItems;

        ////////////////////////////////////////////////////////////////////////
        $scope.loadUsageVms = loadUsageVms;
        $scope.clearSearchParams = clearSearchParams;
        $scope.applyFilter = applyFilter;
        $scope.visibleUsageVm = visibleUsageVm;
        $scope.pageChangeHandler = pageChangeHandler;


        initialize();
        function initialize() {         
            getUsablePeriods();
            loadUsageVms();
        };

        function getUsablePeriods() {
            angular.forEach($scope.periodType, function (periodType) {
                if (periodType.name == 'Day' || periodType.name == 'Month'){
                    periodType.isShow = true;                    
                } else {
                    periodType.isShow = false;
                }
            });
            
            var init = $filter('filter')($scope.periodType, { name: 'Day' })[0].value;
            $scope.usageVms.periodType = init;
        };

        function loadUsageVms() {
            WebServices.AdminUsageSubscriptionPaymentService.getAllPage({
                    pageNumber: $scope.usageVms.currentPage,
                    pageSize: $scope.usageVms.pageSize,
                    typeVirtualization: $scope.usageVms.virtualization,
                    fromDate: ($scope.usageVms.startDate) ? moment($scope.usageVms.startDate).format('YYYY.MM.DD HH:mm:ss'): null,
                    toDate: ($scope.usageVms.endDate) ? moment($scope.usageVms.endDate).format('YYYY.MM.DD HH:mm:ss'): null,
                    userId: $scope.usageVms.userId,
                    subscriptionVmId: $scope.usageVms.subscriptionVmId,
                    periodType: $scope.usageVms.periodType,
                    groupingType: $scope.usageVms.groupingType
                },
                function (data) {
                    $scope.usageVms.items = data.items;
                    $scope.usageVms.totalRows = data.totalRows;
                    $scope.usageVms.totalPages = data.totalPages;

                     angular.forEach($scope.usageVms.items, function (usageVm) {
                        usageVm.show = false; 

                        angular.forEach(usageVm.subscriptions, function (subscription) {
                            var totalAmount = 0;
                            
                            for (var i = 0; i < subscription.usageSubscriptionPayment.length; i++) {
                                totalAmount += subscription.usageSubscriptionPayment[i].amount;
                            };

                            subscription.usageSubscriptionPayment.totalAmount = totalAmount;
                        });
                    });

                },
                function (error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        };


        function pageChangeHandler(num) {
            $scope.usageVms.currentPage = num;
            loadUsageVms();
        };

        function visibleUsageVm (usageVm){
            usageVm.show = !usageVm.show;
        }

        function clearSearchParams(){
            $scope.usageVms.userId = null;
            $scope.usageVms.startDate = null;
            $scope.usageVms.endDate = null;
            $scope.usageVms.virtualization = null;
            $scope.usageVms.userId = null;
            $scope.usageVms.subscriptionVmId = null;

            $scope.isEnableFilter = false;
            loadUsageVms()
        };

        function applyFilter(){
            $scope.isEnableFilter = true;
            $scope.usageVms.userId = $scope.usageVms.user ? $scope.usageVms.user.id : null;
            loadUsageVms()
        };

    }

})();
