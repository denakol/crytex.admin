﻿(function () {

    angular.module('virtualMachinePayment.vpsPayment')
        .controller('VpsPaymentController', vpsPaymentController);

   vpsPaymentController.$inject = ['$scope', 'WebServices', 'ToastNotificationService', 'TypeVirtualization', 'OSSource'];

    function vpsPaymentController($scope, WebServices, ToastNotificationService, TypeVirtualization, OSSource) {
    	$scope.subscriptionPayments = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 5
        };

        $scope.searchParam = {
            userId:null,
            user:null,
            virtualizationType:null,
            operatingSystem: null,
            dateFrom:null,
            dateTo:null
        };

        $scope.virtualizationType = TypeVirtualization;
        $scope.operatingSystems = OSSource;

        $scope.applyFilter = applyFilter;
        $scope.clearSearchParams = clearSearchParams;

        function initialize(){
            getSubscriptionPayments();
        };

        initialize();

        function getSubscriptionPayments(){
            WebServices.AdminFixedSubscriptionPayment.getAllPage({
                    pageNumber: $scope.subscriptionPayments.currentPage,
                    pageSize: $scope.subscriptionPayments.pageSize,
                    userId: $scope.searchParam.userId,
                    dateFrom: $scope.searchParam.dateFrom,
                    dateTo: $scope.searchParam.dateTo,
                    virtualization: $scope.searchParam.virtualizationType,
                    operatingSystem: $scope.searchParam.operatingSystem
                },
                function success(result) {
                    $scope.subscriptionPayments.items = result.items;
                    $scope.subscriptionPayments.totalRows = result.totalRows;
                    $scope.subscriptionPayments.totalPages = result.totalPages;
                    if (result.items.length === 0) {
                    	ToastNotificationService('Ещё нет данных');
                    	$scope.subscriptionPayments.items = [];
                    }
                },
                function err(error) {
                    $scope.subscriptionPayments.items = [];
                    ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        };

        function clearSearchParams(){
            $scope.searchParam = {
                userId:null,
                user:null,
                virtualizationType:null,
                operatingSystem: null,
                dateFrom:null,
                dateTo:null
            };
            getSubscriptionPayments();
        };    

        function applyFilter(){ 
            $scope.searchParam.userId = $scope.searchParam.user ? $scope.searchParam.user.id : null;
            getSubscriptionPayments();
        };

        $scope.pageChangeHandler = function(num) {
            $scope.subscriptionPayments.currentPage = num;
            getSubscriptionPayments();
        };
    }

})();
