﻿(function () {

    angular.module("money.virtualMachinePayment", [
        "virtualMachinePayment.vpsPayment",
        "virtualMachinePayment.cloudPayment"
    ]);

})();