var admin;
(function (admin) {
    var discountsController = (function () {
        function discountsController() {
        }
        return discountsController;
    }());
    admin.discountsController = discountsController;
    angular.module('money.discounts')
        .controller('DiscountsController', discountsController);
})(admin || (admin = {}));
