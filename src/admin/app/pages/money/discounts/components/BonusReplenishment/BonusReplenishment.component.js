var admin;
(function (admin) {
    var BonusReplenishmentController = (function () {
        function BonusReplenishmentController(WebServices, ToastNotificationService, $filter) {
            this.WebServices = WebServices;
            this.ToastNotificationService = ToastNotificationService;
            this.$filter = $filter;
            this.try = false;
            this.getBonusReplenishments();
        }
        BonusReplenishmentController.prototype.getBonusReplenishments = function () {
            var _this = this;
            this.WebServices.AdminBonusReplenishmentService.getAll({}).$promise
                .then(function (data) {
                _this.bonuses = data;
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        BonusReplenishmentController.prototype.addBonus = function (form) {
            var _this = this;
            if (form.$valid) {
                this.newBonus.disable = true;
                this.WebServices.AdminBonusReplenishmentService.save(this.newBonus).$promise
                    .then(function (data) {
                    _this.newBonus.id = data.id;
                    _this.bonuses.splice(0, 0, _this.newBonus);
                    _this.newBonus = {};
                    _this.try = false;
                    _this.ToastNotificationService('Добавлено');
                })
                    .catch(function (err) {
                    if (!isNaN(err)) {
                        _this.ToastNotificationService('Не удалось выгрузить данные');
                    }
                });
            }
        };
        BonusReplenishmentController.prototype.startEdit = function (bonus) {
            bonus.userReplenishmentSizeEdit = bonus.userReplenishmentSize;
            bonus.bonusSizeEdit = bonus.bonusSize;
            bonus.edit = true;
            bonus.try = true;
        };
        BonusReplenishmentController.prototype.backEdit = function (discount) {
            discount.DiscountSizeEdit = discount.discountSize;
            discount.MonthCountEdit = discount.monthCount;
            discount.edit = false;
            discount.try = false;
        };
        BonusReplenishmentController.prototype.endEdit = function (bonus, form_repeat) {
            if (form_repeat.$valid) {
                bonus.userReplenishmentSize = bonus.userReplenishmentSizeEdit;
                bonus.bonusSize = bonus.bonusSizeEdit;
                this.bonusUpdate(bonus);
                bonus.try = false;
                bonus.edit = false;
            }
        };
        BonusReplenishmentController.prototype.bonusUpdate = function (bonus) {
            var _this = this;
            this.WebServices.AdminBonusReplenishmentService.update(bonus.id, bonus).$promise
                .then(function (data) {
                var myDiscount = _this.$filter('filter')(_this.bonuses, { id: bonus.id })[0];
                var index = _this.bonuses.indexOf(myDiscount);
                _this.bonuses.splice(index, 1, bonus);
                _this.ToastNotificationService('Добавлено');
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        BonusReplenishmentController.prototype.unBlockBonus = function (bonus) {
            bonus.disable = false;
            this.bonusUpdate(bonus);
        };
        BonusReplenishmentController.prototype.blockBonus = function (bonus) {
            bonus.disable = true;
            this.bonusUpdate(bonus);
        };
        BonusReplenishmentController.prototype.removeBonus = function (index) {
            var _this = this;
            this.WebServices.AdminBonusReplenishmentService.delete({
                id: this.bonuses[index].id
            }).$promise
                .then(function (data) {
                _this.bonuses.splice(index, 1);
                _this.ToastNotificationService('Удалено');
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        BonusReplenishmentController.$inject = ["WebServices", "ToastNotificationService",
            "$filter"];
        return BonusReplenishmentController;
    }());
    admin.BonusReplenishmentController = BonusReplenishmentController;
    var options = {
        restrict: "EA",
        bindings: {
            discounts: '<'
        },
        templateUrl: 'app/pages/money/discounts/components/BonusReplenishment/BonusReplenishment.html',
        controller: BonusReplenishmentController,
        controllerAs: 'vm'
    };
    angular.module("money.discounts")
        .component('bonusReplenishment', options);
})(admin || (admin = {}));
