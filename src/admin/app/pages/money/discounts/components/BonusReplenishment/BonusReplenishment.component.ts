module admin {

    import BonusReplenishmentViewModel = core.WebApi.Models.BonusReplenishmentViewModel;

    export class BonusReplenishmentController{
        
        public static $inject = ["WebServices", "ToastNotificationService",
        "$filter"];

        public bonuses: BonusReplenishmentViewModel;

        public newBonus: BonusReplenishmentViewModel;

        public try:boolean = false;
        
        constructor(private WebServices:any, private ToastNotificationService:any,
                    private $filter:any){
            this.getBonusReplenishments();
        }
        
        public getBonusReplenishments(){
            this.WebServices.AdminBonusReplenishmentService.getAll({
            }).$promise
                .then((data:any)=> {
                    this.bonuses = data;
                })
                .catch((err:any)=>{
                    this.ToastNotificationService('Не удалось выгрузить данные')
                })
        }

        public addBonus(form:any){
            if(form.$valid){
                this.newBonus.disable = true;

                this.WebServices.AdminBonusReplenishmentService.save(
                    this.newBonus
                ).$promise
                    .then((data:any)=> {
                        this.newBonus .id = data.id;
                        this.bonuses.splice(0, 0, this.newBonus);
                        this.newBonus = <BonusReplenishmentViewModel>{};
                        this.try = false;
                        this.ToastNotificationService('Добавлено');
                    })
                    .catch((err:any)=> {
                        if (!isNaN(err)) {
                            this.ToastNotificationService('Не удалось выгрузить данные');
                        }
                    })
            }
        }

        public startEdit(bonus:any){
            bonus.userReplenishmentSizeEdit = bonus.userReplenishmentSize;
            bonus.bonusSizeEdit = bonus.bonusSize;
            bonus.edit = true;
            bonus.try = true;
        }

        public backEdit(discount:any){
            discount.DiscountSizeEdit = discount.discountSize;
            discount.MonthCountEdit = discount.monthCount;
            discount.edit = false;
            discount.try = false;
        }

        public endEdit(bonus:any, form_repeat:any){
            if(form_repeat.$valid){
                bonus.userReplenishmentSize = bonus.userReplenishmentSizeEdit;
                bonus.bonusSize = bonus.bonusSizeEdit;
                this.bonusUpdate(bonus);
                bonus.try = false;
                bonus.edit = false;
            }
        }

        public bonusUpdate(bonus:BonusReplenishmentViewModel){
            this.WebServices.AdminBonusReplenishmentService.update(
                bonus.id, bonus
            ).$promise
                .then((data:any)=> {
                    var myDiscount = this.$filter('filter')(this.bonuses, {id: bonus.id})[0];
                    var index:number = this.bonuses.indexOf(myDiscount);
                    this.bonuses.splice(index, 1, bonus);
                    this.ToastNotificationService('Добавлено');
                })
                .catch((err:any)=> {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                });
        }

        public unBlockBonus(bonus:BonusReplenishmentViewModel){
            bonus.disable = false;
            this.bonusUpdate(bonus);
        }

        public blockBonus(bonus:BonusReplenishmentViewModel){
            bonus.disable = true;
            this.bonusUpdate(bonus);
        }

        public removeBonus(index:number){
            this.WebServices.AdminBonusReplenishmentService.delete({
                id: this.bonuses[index].id
            }).$promise
                .then((data:any)=> {
                    this.bonuses.splice(index, 1);
                    this.ToastNotificationService('Удалено');
                })
                .catch((err:any)=> {
                    this.ToastNotificationService('Не удалось выгрузить данные')
                })
        }
    }

    var options:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            discounts: '<'
        },
        templateUrl: 'app/pages/money/discounts/components/BonusReplenishment/BonusReplenishment.html',
        controller: BonusReplenishmentController,
        controllerAs: 'vm'
    };

    angular.module("money.discounts")
        .component('bonusReplenishment', options);
}