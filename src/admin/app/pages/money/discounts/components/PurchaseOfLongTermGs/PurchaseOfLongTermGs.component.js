var admin;
(function (admin) {
    var PurchaseOfLongTermGsController = (function () {
        function PurchaseOfLongTermGsController(WebServices, ToastNotificationService, ResourceType, $filter) {
            this.WebServices = WebServices;
            this.ToastNotificationService = ToastNotificationService;
            this.ResourceType = ResourceType;
            this.$filter = $filter;
            this.try = false;
            this.getDiscount();
        }
        PurchaseOfLongTermGsController.prototype.getDiscount = function () {
            var _this = this;
            this.WebServices.AdminLongTermDiscountService.getAll({}).$promise
                .then(function (data) {
                _this.discounts = data;
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        PurchaseOfLongTermGsController.prototype.addDiscount = function (form) {
            var _this = this;
            if (form.$valid) {
                this.newDiscount.disable = true;
                this.newDiscount.resourceType = this.ResourceType.Gs;
                this.WebServices.AdminLongTermDiscountService.save(this.newDiscount).$promise
                    .then(function (data) {
                    _this.newDiscount.id = data.id;
                    _this.discounts.splice(0, 0, _this.newDiscount);
                    _this.newDiscount = {};
                    _this.try = false;
                    _this.ToastNotificationService('Добавлено');
                })
                    .catch(function (err) {
                    if (!isNaN(err)) {
                        _this.ToastNotificationService('Не удалось выгрузить данные');
                    }
                });
            }
        };
        PurchaseOfLongTermGsController.prototype.startEdit = function (discount) {
            discount.DiscountSizeEdit = discount.discountSize;
            discount.MonthCountEdit = discount.monthCount;
            discount.edit = true;
            discount.try = true;
        };
        PurchaseOfLongTermGsController.prototype.backEdit = function (discount) {
            discount.DiscountSizeEdit = discount.discountSize;
            discount.MonthCountEdit = discount.monthCount;
            discount.edit = false;
            discount.try = false;
        };
        PurchaseOfLongTermGsController.prototype.endEdit = function (discount, form_repeat) {
            if (form_repeat.$valid) {
                discount.discountSize = discount.DiscountSizeEdit;
                discount.monthCount = discount.MonthCountEdit;
                this.discountUpdate(discount);
                discount.try = false;
                discount.edit = false;
            }
        };
        PurchaseOfLongTermGsController.prototype.discountUpdate = function (discount) {
            var _this = this;
            this.WebServices.AdminLongTermDiscountService.update(discount.id, discount).$promise
                .then(function (data) {
                var myDiscount = _this.$filter('filter')(_this.discounts, { id: discount.id })[0];
                var index = _this.discounts.indexOf(myDiscount);
                _this.discounts.splice(index, 1, discount);
                _this.ToastNotificationService('Изменено');
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        PurchaseOfLongTermGsController.prototype.unBlockDiscount = function (discount) {
            discount.disable = false;
            this.discountUpdate(discount);
        };
        PurchaseOfLongTermGsController.prototype.blockDiscount = function (discount) {
            discount.disable = true;
            this.discountUpdate(discount);
        };
        PurchaseOfLongTermGsController.prototype.removeDiscount = function (index) {
            var _this = this;
            this.WebServices.AdminLongTermDiscountService.delete({
                id: this.discounts[index].id
            }).$promise
                .then(function (data) {
                _this.discounts.splice(index, 1);
                _this.ToastNotificationService('Удалено');
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        PurchaseOfLongTermGsController.$inject = ["WebServices", "ToastNotificationService",
            "ResourceType", "$filter"];
        return PurchaseOfLongTermGsController;
    }());
    admin.PurchaseOfLongTermGsController = PurchaseOfLongTermGsController;
    var options = {
        restrict: "EA",
        bindings: {},
        templateUrl: 'app/pages/money/discounts/components/PurchaseOfLongTermGs/PurchaseOfLongTermGs.html',
        controller: PurchaseOfLongTermGsController,
        controllerAs: 'vm'
    };
    angular.module("money.discounts")
        .component('purchaseOfLongTermGs', options);
})(admin || (admin = {}));
