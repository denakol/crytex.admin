module admin {

    import DiscountViewModel = core.WebApi.Models.DiscountViewModel;
    import ResourceType = core.WebApi.Models.Enums.ResourceType;

    export class PurchaseOfLongTermGsController {

        public static $inject = ["WebServices", "ToastNotificationService",
            "ResourceType", "$filter"];

        public discounts:DiscountViewModel;

        public newDiscount:DiscountViewModel;

        public try:boolean = false;

        constructor(private WebServices:any, private ToastNotificationService:any,
                    private ResourceType:any, private $filter:any){
            this.getDiscount();
        }

        public getDiscount(){

            this.WebServices.AdminLongTermDiscountService.getAll({

            }).$promise
                .then((data:any)=>{
                    this.discounts = data;
                })
                .catch((err:any)=>{
                    this.ToastNotificationService('Не удалось выгрузить данные');
                })
        }

        public addDiscount(form:any){
            if(form.$valid){
                this.newDiscount.disable = true;
                this.newDiscount.resourceType = this.ResourceType.Gs;
                this.WebServices.AdminLongTermDiscountService.save(
                    this.newDiscount
                ).$promise
                    .then((data:any)=> {
                        this.newDiscount.id = data.id;
                        this.discounts.splice(0, 0, this.newDiscount);
                        this.newDiscount = <DiscountViewModel>{};
                        this.try = false;
                        this.ToastNotificationService('Добавлено');
                    })
                    .catch((err:any)=> {
                        if (!isNaN(err)) {
                            this.ToastNotificationService('Не удалось выгрузить данные');
                        }
                    })
            }
        }

        public startEdit(discount:any){
            discount.DiscountSizeEdit = discount.discountSize;
            discount.MonthCountEdit = discount.monthCount;
            discount.edit = true;
            discount.try = true;
        }

        public backEdit(discount:any){
            discount.DiscountSizeEdit = discount.discountSize;
            discount.MonthCountEdit = discount.monthCount;
            discount.edit = false;
            discount.try = false;
        }

        public endEdit(discount:any, form_repeat:any){
            if(form_repeat.$valid){
                discount.discountSize = discount.DiscountSizeEdit;
                discount.monthCount = discount.MonthCountEdit;
                this.discountUpdate(discount);
                discount.try = false;
                discount.edit = false;
            }
        }

        public discountUpdate(discount:DiscountViewModel){
            this.WebServices.AdminLongTermDiscountService.update(
                discount.id, discount
            ).$promise
                .then((data:any)=> {
                    var myDiscount = this.$filter('filter')(this.discounts, {id: discount.id})[0];
                    var index:number = this.discounts.indexOf(myDiscount);
                    this.discounts.splice(index, 1, discount);
                    this.ToastNotificationService('Изменено');
                })
                .catch((err:any)=> {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                });
        }

        public unBlockDiscount(discount:DiscountViewModel){
            discount.disable = false;
            this.discountUpdate(discount);
        }

        public blockDiscount(discount:DiscountViewModel){
            discount.disable = true;
            this.discountUpdate(discount);
        }
        
        public removeDiscount(index:number){
            this.WebServices.AdminLongTermDiscountService.delete({
                id: this.discounts[index].id
            }).$promise
                .then((data:any)=> {
                    this.discounts.splice(index, 1);
                    this.ToastNotificationService('Удалено');
                })
                .catch((err:any)=> {
                    this.ToastNotificationService('Не удалось выгрузить данные')
                })
        }
    }

    var options:ng.IComponentOptions = {
        restrict: "EA",
        bindings: {
            
        },
        templateUrl: 'app/pages/money/discounts/components/PurchaseOfLongTermGs/PurchaseOfLongTermGs.html',
        controller: PurchaseOfLongTermGsController,
        controllerAs: 'vm'
    };

    angular.module("money.discounts")
        .component('purchaseOfLongTermGs', options);
}