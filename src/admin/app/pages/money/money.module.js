﻿(function () {

    angular.module("crytex.money", [
        "money.statistic",
        "money.users",
        "money.payment",
        "money.discounts",
        "payment.billingTransaction",
        "money.virtualMachinePayment",
        "money.moneySystem"
    ]);

})();