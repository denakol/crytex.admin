(function () {

    angular.module('payment.billingTransaction')
        .controller('BillingTransactionController', BillingTransactionController);

    BillingTransactionController.$inject = ['$scope', '$filter', 'WebServices', 'ToastNotificationService', 'BillingTransactionType'];

    function BillingTransactionController($scope, $filter, WebServices, ToastNotificationService, BillingTransactionType) {
        $scope.transactions = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 5
        };    

        $scope.searchParam = {
            userId:null,
            user:null,
            transactionType:null,
            dateFrom:null,
            dateTo:null
        };

        $scope.transactionType = BillingTransactionType.arrayItems; 
        $scope.transactionType.push({translate: "Не выбран ", value:$scope.transactionType.length});

        $scope.applyFilter = applyFilter;
        $scope.clearSearchParams = clearSearchParams;
        $scope.isEnableFilter = false;

        function initialize(){
            getTransactions();
        };

        initialize();

        //////////////////////////////////////////////////////////////////////
        function getTransactions(){
            WebServices.AdminBillingTransaction.getAllPage({
                    pageNumber: $scope.transactions.currentPage,
                    pageSize: $scope.transactions.pageSize,
                    userId: $scope.isEnableFilter ? $scope.searchParam.userId : null,
                    dateFrom: $scope.isEnableFilter ? $scope.searchParam.dateFrom : null,
                    dateTo: $scope.isEnableFilter ? $scope.searchParam.dateTo : null,
                    billingTransactionType: $scope.isEnableFilter ? $scope.searchParam.transactionType : null
                },
                function success(result) {
                    $scope.transactions.items = result.items;
                    for (var i = 0; i < $scope.transactions.items.length; i++) {
                        $scope.transactions.items[i].transactionType = 
                            $filter('filter')($scope.transactionType, { value: $scope.transactions.items[i].transactionType })[0].name;
                    };
                    $scope.transactions.totalRows = result.totalRows;
                    $scope.transactions.totalPages = result.totalPages;
                    if (result.items.length === 0) ToastNotificationService('Ещё нет данных');
                },
                function err(error) {
                    $scope.transactions.items = [];
                    ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }

        function clearSearchParams(){
            $scope.searchParam = {
                userId:null,
                user:null,
                transactionType:null,
                dateFrom:null,
                dateTo:null
            };
            $scope.isEnableFilter = false;
            getTransactions();
        };    

        function applyFilter(){ 
            $scope.searchParam.userId = $scope.searchParam.user ? $scope.searchParam.user.id : null;
            $scope.isEnableFilter = true;
            getTransactions();
        };    

        $scope.pageChangeHandler = function(num) {
            $scope.transactions.currentPage = num;
            getTransactions();
        };

    }

})();
