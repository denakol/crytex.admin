module admin{

    import IResourceClass = angular.resource.IResourceClass;
    import PaymentSystemView = core.WebApi.Models.PaymentSystemView;
    import FileType = core.WebApi.Models.Enums.FileType;
    import FileDescriptorViewModel = core.WebApi.Models.FileDescriptorViewModel;

    export interface IActiveState {
        name: string;
        value: boolean;
    }

    export class MoneySystemController {

        public static $inject = ["WebServices", "ToastNotificationService", "BASE_INFO", "Upload",
                                "$filter", "PaymentType", "$mdDialog"];

        public paymentSystemList: Array<PaymentSystemView>;
        public newPaymentSystem: PaymentSystemView;
        public editablePaymentSystem: PaymentSystemView;

        public statusType: Array<IActiveState> = [];
        public isShowNewPaymentSystemForm: boolean;
        public paymentSystemImage: any = {};

        private paymentService: IResourceClass<PaymentSystemView>;
        private tempImageFileDescriptor: FileDescriptorViewModel;

        constructor(private WebServices:any, private toastNotificationService:any,
                    public baseInfo: any, private fileUploader: any,
                    private $filter: any, public paymentType: any,
                    private $mdDialog: any){

            this.paymentService = WebServices.AdminPaymentSystemService;
            this.initializeStatusType();
            this.setDefaultPaymentSystem();
            this.getPaymentSystems();
        }

        public createPaymentSystem () {
            // Создаём новую игру
            this.newPaymentSystem.imageFileDescriptor = null;

            this.paymentService.save( this.newPaymentSystem ).$promise
                .then ( (responseObject: any) => {
                    // Добавляем игру в список
                    this.newPaymentSystem.id = responseObject.id;
                    this.newPaymentSystem.imageFileDescriptor = this.tempImageFileDescriptor;
                    this.paymentSystemList.splice(this.paymentSystemList.length, 0, this.newPaymentSystem);
                    this.setDefaultPaymentSystem();
                })
                .catch ( (response:any) => {
                    if (response.status != -1){
                        if (response.data &&
                            response.data.modelState &&
                            response.data.modelState.validationError){
                            this.toastNotificationService(response.data.modelState.validationError[0]);
                        } else if (response.data &&
                            response.data.message) {
                            this.toastNotificationService(response.data.message);
                        }
                    } else {
                        this.toastNotificationService("Ошибка обновления данных");
                    }
                    return;
                });
        }

        public editPaymentSystem (paymentSystem: PaymentSystemView) {
            if (this.editablePaymentSystem) {
                // Закроем уже открытое редактирование
                var previousEditPaymentSystem = this.$filter('filter')(this.paymentSystemList, { id: this.editablePaymentSystem.id })[0];
                this.cancleEditPaymentSystem(previousEditPaymentSystem);
            }
            paymentSystem.isEdit = true;
            this.editablePaymentSystem = angular.copy(paymentSystem);
        }

        public cancleEditPaymentSystem (paymentSystem: PaymentSystemView) {
            paymentSystem.isEdit = false;
            this.editablePaymentSystem = null;
        }

        public saveChanges () {
            this.paymentService.update(this.editablePaymentSystem)
                .$promise.then(() => {
                    var sourcePaymentSystem = this.$filter('filter')(this.paymentSystemList, { id: this.editablePaymentSystem.id })[0];
                    var index = this.paymentSystemList.indexOf(sourcePaymentSystem);
                    this.paymentSystemList[index] = angular.copy(this.editablePaymentSystem);
                    this.paymentSystemList[index].isEdit = false;
                })
                .catch ( (response:any) => {
                    this.toastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

        // Подтверждение удаления платёжной системы
        public showConfirm (id: string) {
            var confirm = this.$mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');

            this.$mdDialog.show(confirm).then( () => {
                // Удалить
                this.deletePaymentSystem(id);
            });
        }

        private initializeStatusType () {
            var active = <IActiveState>{
                name: "Включено",
                value: true
            }
            var disactive = <IActiveState>{
                name: "Выключено",
                value: false
            }
            this.statusType.push(active);
            this.statusType.push(disactive);
        }

        private deletePaymentSystem (id: string) {
            this.paymentService.delete(
                {
                    id: id
                }).$promise.then(() => {
                    var sourcePaymentSystem = this.$filter('filter')(this.paymentSystemList, { id: id })[0];
                    var index = this.paymentSystemList.indexOf(sourcePaymentSystem);
                    if (index !== -1) {
                        // Обновляем хост
                        this.paymentSystemList.splice(index, 1);
                    }
                })
                .catch ( (response:any) => {
                    this.toastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

        private setDefaultPaymentSystem () {
            this.newPaymentSystem = <PaymentSystemView>{
                paymentType: this.paymentType.arrayItems[0].value,
                isEnabled: true
            };
            this.paymentSystemImage = null;
            this.isShowNewPaymentSystemForm = false;
        }

        private getPaymentSystems (){
            this.paymentSystemList = this.paymentService.query();

            this.paymentSystemList.$promise
                .catch((err:any)=>{
                    this.toastNotificationService('Не удалось выгрузить данные');
                })
        }

        // Send file on server
        private uploadFiles (file:any, what:string) : any {
            // Set params
            var file =  this.fileUploader.upload({
                url: this.baseInfo.URL + this.baseInfo.PORT + this.baseInfo.API_URL + '/Admin/AdminFile/Post',
                file: file,
                data: {
                    fileName: file.name,
                    fileType: FileType.Image
                }
            });

            // Upload
            file.then( (description:any) => {
                    this.tempImageFileDescriptor =  {
                        name: "Image Name",
                        path: description.data.path,
                        type: FileType.Image
                    };
                    if (what == 'new') {
                        this.newPaymentSystem.imageFileDescriptorId = description.data.id;
                    } else if (what == 'edit') {
                        this.editablePaymentSystem.imageFileDescriptorId = description.data.id;
                        this.editablePaymentSystem.imageFileDescriptor = angular.copy(this.tempImageFileDescriptor);
                    }
                },
                (response:any) => {
                    // Умолчим, при конечной отправке выползет сообщение
                }
            );

            return;
        }

    }

    angular.module("money.moneySystem")
        .controller('MoneySystemController', MoneySystemController);
}