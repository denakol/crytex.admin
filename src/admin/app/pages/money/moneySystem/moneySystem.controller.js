var admin;
(function (admin) {
    var FileType = core.WebApi.Models.Enums.FileType;
    var MoneySystemController = (function () {
        function MoneySystemController(WebServices, toastNotificationService, baseInfo, fileUploader, $filter, paymentType, $mdDialog) {
            this.WebServices = WebServices;
            this.toastNotificationService = toastNotificationService;
            this.baseInfo = baseInfo;
            this.fileUploader = fileUploader;
            this.$filter = $filter;
            this.paymentType = paymentType;
            this.$mdDialog = $mdDialog;
            this.statusType = [];
            this.paymentSystemImage = {};
            this.paymentService = WebServices.AdminPaymentSystemService;
            this.initializeStatusType();
            this.setDefaultPaymentSystem();
            this.getPaymentSystems();
        }
        MoneySystemController.prototype.createPaymentSystem = function () {
            var _this = this;
            // Создаём новую игру
            this.newPaymentSystem.imageFileDescriptor = null;
            this.paymentService.save(this.newPaymentSystem).$promise
                .then(function (responseObject) {
                // Добавляем игру в список
                _this.newPaymentSystem.id = responseObject.id;
                _this.newPaymentSystem.imageFileDescriptor = _this.tempImageFileDescriptor;
                _this.paymentSystemList.splice(_this.paymentSystemList.length, 0, _this.newPaymentSystem);
                _this.setDefaultPaymentSystem();
            })
                .catch(function (response) {
                if (response.status != -1) {
                    if (response.data &&
                        response.data.modelState &&
                        response.data.modelState.validationError) {
                        _this.toastNotificationService(response.data.modelState.validationError[0]);
                    }
                    else if (response.data &&
                        response.data.message) {
                        _this.toastNotificationService(response.data.message);
                    }
                }
                else {
                    _this.toastNotificationService("Ошибка обновления данных");
                }
                return;
            });
        };
        MoneySystemController.prototype.editPaymentSystem = function (paymentSystem) {
            if (this.editablePaymentSystem) {
                // Закроем уже открытое редактирование
                var previousEditPaymentSystem = this.$filter('filter')(this.paymentSystemList, { id: this.editablePaymentSystem.id })[0];
                this.cancleEditPaymentSystem(previousEditPaymentSystem);
            }
            paymentSystem.isEdit = true;
            this.editablePaymentSystem = angular.copy(paymentSystem);
        };
        MoneySystemController.prototype.cancleEditPaymentSystem = function (paymentSystem) {
            paymentSystem.isEdit = false;
            this.editablePaymentSystem = null;
        };
        MoneySystemController.prototype.saveChanges = function () {
            var _this = this;
            this.paymentService.update(this.editablePaymentSystem)
                .$promise.then(function () {
                var sourcePaymentSystem = _this.$filter('filter')(_this.paymentSystemList, { id: _this.editablePaymentSystem.id })[0];
                var index = _this.paymentSystemList.indexOf(sourcePaymentSystem);
                _this.paymentSystemList[index] = angular.copy(_this.editablePaymentSystem);
                _this.paymentSystemList[index].isEdit = false;
            })
                .catch(function (response) {
                _this.toastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        // Подтверждение удаления платёжной системы
        MoneySystemController.prototype.showConfirm = function (id) {
            var _this = this;
            var confirm = this.$mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            this.$mdDialog.show(confirm).then(function () {
                // Удалить
                _this.deletePaymentSystem(id);
            });
        };
        MoneySystemController.prototype.initializeStatusType = function () {
            var active = {
                name: "Включено",
                value: true
            };
            var disactive = {
                name: "Выключено",
                value: false
            };
            this.statusType.push(active);
            this.statusType.push(disactive);
        };
        MoneySystemController.prototype.deletePaymentSystem = function (id) {
            var _this = this;
            this.paymentService.delete({
                id: id
            }).$promise.then(function () {
                var sourcePaymentSystem = _this.$filter('filter')(_this.paymentSystemList, { id: id })[0];
                var index = _this.paymentSystemList.indexOf(sourcePaymentSystem);
                if (index !== -1) {
                    // Обновляем хост
                    _this.paymentSystemList.splice(index, 1);
                }
            })
                .catch(function (response) {
                _this.toastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        MoneySystemController.prototype.setDefaultPaymentSystem = function () {
            this.newPaymentSystem = {
                paymentType: this.paymentType.arrayItems[0].value,
                isEnabled: true
            };
            this.paymentSystemImage = null;
            this.isShowNewPaymentSystemForm = false;
        };
        MoneySystemController.prototype.getPaymentSystems = function () {
            var _this = this;
            this.paymentSystemList = this.paymentService.query();
            this.paymentSystemList.$promise
                .catch(function (err) {
                _this.toastNotificationService('Не удалось выгрузить данные');
            });
        };
        // Send file on server
        MoneySystemController.prototype.uploadFiles = function (file, what) {
            var _this = this;
            // Set params
            var file = this.fileUploader.upload({
                url: this.baseInfo.URL + this.baseInfo.PORT + this.baseInfo.API_URL + '/Admin/AdminFile/Post',
                file: file,
                data: {
                    fileName: file.name,
                    fileType: FileType.Image
                }
            });
            // Upload
            file.then(function (description) {
                _this.tempImageFileDescriptor = {
                    name: "Image Name",
                    path: description.data.path,
                    type: FileType.Image
                };
                if (what == 'new') {
                    _this.newPaymentSystem.imageFileDescriptorId = description.data.id;
                }
                else if (what == 'edit') {
                    _this.editablePaymentSystem.imageFileDescriptorId = description.data.id;
                    _this.editablePaymentSystem.imageFileDescriptor = angular.copy(_this.tempImageFileDescriptor);
                }
            }, function (response) {
                // Умолчим, при конечной отправке выползет сообщение
            });
            return;
        };
        MoneySystemController.$inject = ["WebServices", "ToastNotificationService", "BASE_INFO", "Upload",
            "$filter", "PaymentType", "$mdDialog"];
        return MoneySystemController;
    }());
    admin.MoneySystemController = MoneySystemController;
    angular.module("money.moneySystem")
        .controller('MoneySystemController', MoneySystemController);
})(admin || (admin = {}));
