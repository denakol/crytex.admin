﻿(function () {

    angular.module('money.payment')
        .controller('PaymentController', paymentController);

    paymentController.$inject = ['$scope', 'WebServices', 'ToastNotificationService', 'PaymentType'];

    function paymentController($scope, WebServices, ToastNotificationService, PaymentType) {
        $scope.payments = {
            items: [],
            totalPages: 0,
            totalRows: 0,
            currentPage: 1,
            pageSize: 5,
            userId: null,
            user: null,
            success: null,
            dateType: null,
            fromDate: null,
            toDate: null,
            type: null
        };
        $scope.searchParam = {user: null}
        $scope.dateType = [
            {value: 0, name: "Дата отправки"},
            {value: 1, name: "Дата завершения"}
        ];
        $scope.successType = [
            {value: null, name: "Любой"},
            {value: false, name: "Неуспешно"},
            {value: true, name: "Успешно"}
        ];
        $scope.paymentType = PaymentType;
        $scope.getPayments = getPayments;
        $scope.applyFilter = applyFilter;
        $scope.clearSearchParams = clearSearchParams;
        $scope.isEnableFilter = false;

        getPayments();

        //////////////////////////////////////////////////////////////////////
        function getPayments(){
            WebServices.AdminPaymentService.getAllPage({
                    pageNumber: $scope.payments.currentPage,
                    pageSize: $scope.payments.pageSize,
                    success: $scope.payments.success,
                    dateType: $scope.payments.dateType,
                    fromDate: ($scope.payments.fromDate) ? moment($scope.payments.fromDate).format('YYYY.MM.DD HH:mm:ss'): null,
                    toDate: ($scope.payments.toDate) ? moment($scope.payments.toDate).format('YYYY.MM.DD HH:mm:ss'): null,
                    userId: $scope.payments.userId,
                    paymentSystem: $scope.payments.type
                },
                function success(result) {
                    $scope.payments.items = result.items;
                    $scope.payments.totalRows = result.totalRows;
                    $scope.payments.totalPages = result.totalPages;
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }

        $scope.pageChangeHandler = function(num) {
            getPayments();
        };

        function clearSearchParams(){
            $scope.payments.userId = null;
            $scope.payments.user = null;
            $scope.payments.success = null;
            $scope.payments.dateType = null;
            $scope.payments.fromDate = null;
            $scope.payments.toDate = null;

            $scope.isEnableFilter = false;
            getPayments();
        };

        function applyFilter(){
            $scope.payments.userId = $scope.payments.user ? $scope.payments.user.id : null;
            $scope.isEnableFilter = true;
            getPayments();
        };

    }

})();
