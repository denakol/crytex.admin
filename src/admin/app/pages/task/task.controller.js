(function () {

	angular.module('crytex.task')
        .controller('TaskController', TaskController);

	TaskController.$inject = 
	['$scope', 'WebServices', '$mdDialog','ToastNotificationService','TypeTask','StatusTask','TypeVirtualization','ResourceType','TypeDate','$filter'];

	function TaskController($scope, WebServices, $mdDialog,ToastNotificationService,TypeTask,StatusTask,TypeVirtualization,ResourceType,TypeDate,$filter) {
		//Binding functions
		$scope.loadData = loadData;
		$scope.clearSearchParams = clearSearchParams;
		$scope.applyFilter = applyFilter;
		$scope.showConfirm = showConfirm;

		//Variables
		var notSelected = 'Not Selected';
		$scope.pageNumber = 1;
		$scope.pageSize = 5;
		$scope.tasks = [];
		$scope.totalRows = 0;
		$scope.totalPages = 0;		
	    $scope.typeOfTask = TypeTask.arrayItems; $scope.typeOfTask.push({name: notSelected, value:$scope.typeOfTask.length});
	    $scope.statusOfTask = StatusTask.arrayItems; $scope.statusOfTask.push({name: notSelected, value:$scope.statusOfTask.length});
	    $scope.typeOfVirtualization = TypeVirtualization.arrayItems; $scope.typeOfVirtualization.push({name: notSelected, value:$scope.typeOfVirtualization.length});
	    $scope.TypeOfResource = ResourceType.arrayItems; $scope.TypeOfResource.push({name: notSelected, value:$scope.TypeOfResource.length});
	    $scope.typeOfPeriod = TypeDate.arrayItems; $scope.typeOfPeriod.push({name: notSelected, value:$scope.typeOfPeriod.length});	    

		$scope.searchTaskParams = {
			resourceId: null,
			typeTask:null,
			statusTask:null,
			virtualization:null,
			startDate:null,
			endDate:null,
			typeDate:null
		};
		loadData();		

		//Load data
		function loadData() {
			WebServices.AdminTaskV2.getAllPage({
					pageNumber: $scope.pageNumber,
					pageSize: $scope.pageSize,
					resourceId: $scope.searchTaskParams.resourceId,
					typeTask:$scope.searchTaskParams.typeTask,
					statusTask:$scope.searchTaskParams.statusTask,
					virtualization:$scope.searchTaskParams.virtualization,
					startDate:$scope.searchTaskParams.startDate,
					endDate:$scope.searchTaskParams.endDate,
					typeDate:$scope.searchTaskParams.typeDate
				},
				function (data) {
					$scope.tasks = [];
					$scope.totalRows = data.totalRows;
					$scope.totalPages = data.totalPages;
					for (var i = 0; i < data.items.length; i++) {			
						data.items[i].typeTask = $filter('filter')($scope.typeOfTask, { value: data.items[i].typeTask })[0];
						data.items[i].statusTask = $filter('filter')($scope.statusOfTask, { value: data.items[i].statusTask })[0];
						data.items[i].virtualization = $filter('filter')($scope.typeOfVirtualization, { value: data.items[i].virtualization })[0];
						data.items[i].resourceType = $filter('filter')($scope.TypeOfResource, { value: data.items[i].resourceType })[0];
						data.items[i].startedAt = $filter('date')(data.items[i].startedAt, 'medium')
						data.items[i].createdAt = $filter('date')(data.items[i].createdAt, 'medium')
						data.items[i].completedAt = $filter('date')(data.items[i].completedAt, 'medium')
						$scope.tasks.push(data.items[i]);								
					};					
				},
				function (error) {
					if (!isNaN(error)) {
						ToastNotificationService('Не удалось выгрузить данные');
						clearSearchParams();
					}
				}
			);
		};

	    function clearSearchParams(){
	        $scope.searchTaskParams = {
				resourceId: null,
				typeTask:null,
				statusTask:null,
				virtualization:null,
				startDate:null,
				endDate:null,
				typeDate:null
			};
	        loadData();
	    };    

	    function applyFilter(){		    	
	    	$scope.searchTaskParams = {
	    		typeTask: $scope.searchParam.typeTask == notSelected ? '' : $scope.searchParam.typeTask,
	    		statusTask: $scope.searchParam.statusTask == notSelected ? '' : $scope.searchParam.statusTask,
	    		virtualization: $scope.searchParam.virtualization == notSelected ? '' : $scope.searchParam.virtualization,
	    		typeDate: $scope.searchParam.typeDate,
	    		startDate: $scope.searchParam.startDate ? new Date($scope.searchParam.startDate) : null,
	    		endDate: $scope.searchParam.endDate ? new Date($scope.searchParam.endDate) : null
	    	};
	    	loadData();	    	  
	    };    


	    function deleteTask(taskId){
	        WebServices.AdminTaskV2.delete({
	            id: taskId 
	        },
	        function success(){
	            loadData();
	        },
	        function err(error){
	            ToastNotificationService('Не удалось удалить');
	        });
	    };

	    function showConfirm(taskId) {
	        var confirm = $mdDialog.confirm()
	            .title('Вы уверены, что хотите удалить данный элемент?')
	            .ok('Да')
	            .cancel('Отменить');
	        $mdDialog.show(confirm).then(function() {
	            deleteTask(taskId);
	        }, function() {
	          // Ну нет так нет
	        });
	    };

	    $scope.pageChangeHandler = function(num) {
			$scope.pageNumber = num;
			loadData();
		};

	};

})();
