(function () {

    angular.module('crytex.signIn')
        .controller('SignInController', signInController);

    signInController.$inject = ['$scope', '$window', '$mdToast', '$mdDialog', 'AuthService'];

    function signInController($scope, $window, $mdToast, $mdDialog, AuthService) {

        $scope.authentication = AuthService.authentication;

        $scope.user = {
            signInData: {
                userName: '',
                password: '',
                useRefreshToken: false
            }
        };

        $scope.toastPosition = angular.extend({}, { bottom: false, top: true, left: false, right: true});

        $scope.getToastPosition = function () {

            return Object.keys($scope.toastPosition)
                .filter(function(pos) { return $scope.toastPosition[pos]; })
                .join(' ');

        };   
 
        $scope.signOut = function () {

            AuthService.signOut().then(function () {

                $window.location.reload();

            }).catch(function (e) {

                console.error(e);

            });

        };

        $scope.signIn = function (form) {

            if (form.$valid) {
            
                AuthService.signIn($scope.user.signInData).then(function (response) {

                    $window.location.reload();

                }).catch(function (error) {

                    $mdToast.show(
                       $mdToast.simple()
                       .content('Неправильная пара логин/пароль!')
                       .position($scope.getToastPosition())
                       .hideDelay(3000)
                    );

                });

            } else {

                $mdToast.show(
                    $mdToast.simple()
                    .content('Оба поля обязательны для заполнения!')
                    .position($scope.getToastPosition())
                    .hideDelay(3000)
                );

            }
        };

        $scope.displaySignInForm = function (index) {

            $mdDialog.show({
                controller: DialogSignInController,
                templateUrl: 'app/pages/signIn/signIn.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            })
            .then(function (answer) {

            }, function () {

            });
        };

        function DialogSignInController($scope, $mdDialog) {
       
            $scope.cancel = function (need) {

                $mdDialog.cancel();

            };
        }
    }

})();