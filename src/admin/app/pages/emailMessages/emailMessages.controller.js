﻿(function () {

	angular.module('crytex.emailMessages')
        .controller('EmailMessagesController', emailMessagesController);

	emailMessagesController.$inject = ['$scope', '$filter', 'WebServices', '$mdToast', '$mdDialog'];

	function emailMessagesController($scope, $filter, WebServices, $mdToast, $mdDialog) {
		$scope.pageNumber = 1;
		$scope.pageSize = 1;
		$scope.emails = null;
		$scope.totalRows = 0;
		$scope.totalPages = 0;
		$scope.searchEmailParams = {
			emailStatus : null,
			fromDate: null,
			toDate: null,
			isProcessed: null,
			sender : null,
			receiver: null
		};
		$scope.emailResultStatus = [
			{name: "Sent", value: 0, translate: "Отправлено"},
			{name: "Queued", value: 1, translate: "В очереди"},
			{name: "Rejected", value: 2, translate: "Отклонено"},
			{name: "Invalid", value: 3, translate: "Недействительное"},
			{name: "Scheduled", value: 4, translate: "Планируется"},
			{name: "All", value: null, translate: "Все"}
		];
		$scope.isProcessed = [
			{name: "All", value: null, translate: "Все"},
			{name: "True", value: true, translate: "Да"},
			{name: "False", value: false, translate: "Нет"}
		];
		$scope.emailTemplateType = [
			{name: "Registration", value: 0, translate: "Регистрация"},
			{name: "ChangePassword", value: 1, translate: "Смена пароля"},
			{name: "ChangeProfile", value: 2, translate: "Смена профиля"}
		];
		$scope.filters = false;

		loadPage();

		$scope.loadPage = loadPage;

		function loadPage() {
			WebServices.AdminEmailService.getAllPage({
					pageNumber: $scope.pageNumber,
					pageSize: $scope.pageSize,
					emailStatus : $scope.searchEmailParams.emailStatus,
					fromDate : $scope.searchEmailParams.fromDate,
					toDate : $scope.searchEmailParams.toDate,
					isProcessed : $scope.searchEmailParams.isProcessed,
					sender : $scope.searchEmailParams.sender,
					receiver : $scope.searchEmailParams.receiver
				},
				function (data) {
					$scope.emails = data.items;
					$scope.totalRows = data.totalRows;
					$scope.totalPages = data.totalPages;

					angular.forEach($scope.emails, function(email) {
						email.isProcessed = email.isProcessed ? 'Да' : 'Нет';
					});
				},
				function (error) {
					if (!isNaN(error)) {
						showMessage('Не удалось выгрузить данные');
					}
				}
			);
		}

		$scope.pageChangeHandler = function(num) {
			$scope.pageNumber = num;
			$scope.loadPage();
		}

		$scope.textChangeHandler = function() {
			$scope.loadPage();
		}

			function showMessage(message) {
			$mdToast.show(
				$mdToast.simple()
					.content(message)
					.position($scope.getToastPosition())
					.hideDelay(3000));
		}

		var last = { bottom: false, top: true, left: false, right: true };
		$scope.toastPosition = angular.extend({}, last);

		$scope.getToastPosition = function () {
			return Object.keys($scope.toastPosition)
				.filter(function (pos) { return $scope.toastPosition[pos]; })
				.join(' ');
		};

		$scope.deleteEmail = function (index) {
			WebServices.AdminEmailService.delete({ id: $scope.emails[index].id },
				function (ok) {
					// Удаляем из списка
					$scope.emails.splice(index, 1);
					showMessage('Удалено');
				},
				function (error) {
					showMessage('Не удалось выгрузить данные');
				}
			);
		}

		$scope.showConfirm = function (index) {
			var confirm = $mdDialog.confirm()
				.title('Вы уверены, что хотите удалить данный элемент?')
				.ok('Да')
				.cancel('Отменить');
			$mdDialog.show(confirm).then(function () {
				$scope.deleteEmail(index);
			}, function () {
				// Ну нет так нет
			});
		};

		$scope.filtersHandler = function(){
			$scope.filters = !$scope.filters;
		}



	}

})();
