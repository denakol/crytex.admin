module admin {

    import IResourceClass = angular.resource.IResourceClass;
    import HyperVHostViewModel  = core.WebApi.Models.HyperVHostViewModel;
    import PaginationSetting = core.PaginationSetting;
    import PageModel = core.PageModel;
    
    export class hyperVHostController{
        
        public static $inject = ["WebServices", "ToastNotificationService", "$mdDialog", "$filter"];
        
        public hyperVHosts: PageModel<HyperVHostViewModel>;

        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        };
                
        constructor(private WebServices:any,
                    private ToastNotificationService:any,
                    private mdDialog:any,
                    private $filter:any){
            this.getHyperVHosts();
        }

        public getHyperVHosts(){
            this.WebServices.AdminHyperVHostService.getAll({
            }).$promise
                .then((data:any)=>{
                    this.hyperVHosts = data;
                })
                .catch((err:any)=>{
                    this.ToastNotificationService('Не удалось выгрузить данные');
                });
        }

        public showEdit(index:number){
            this.mdDialog.show({
                    controller: "DialogHyperVEditCtrl as dialog",
                    templateUrl: "app/pages/preferences/HyperVHost/editHyperVHostForm.tmpl.html",
                    parent: angular.element(document.body),
                    targetEvent: null,
                    clickOutsideToClose: true,
                    locals: {
                        hyperVHostEdit: (typeof index !== "undefined") ? angular.copy(this.hyperVHosts[index]) : null
                    }
                })
                .then((answer:any) => {
                    this.createOrUpdateHyperVhost(answer);
                }, ((err:any) => {

                }));
        }

        public createOrUpdateHyperVhost(hyperVHost:any){
            if(hyperVHost.id){
                this.WebServices.AdminHyperVHostService.update(hyperVHost)
                    .$promise
                        .then((data:any)=>{
                            var myHyperVhost = this.$filter('filter')(this.hyperVHosts, {id:hyperVHost.id})[0];
                            var index:number = this.hyperVHosts.indexOf(myHyperVhost);
                            this.hyperVHosts.splice(index, 1, hyperVHost);
                            this.ToastNotificationService('Изменено');
                        })
                        .catch((err:any)=>{
                            this.ToastNotificationService('Не удалось выгрузить данные');
                        })
            } else {
                this.WebServices.AdminHyperVHostService.save(hyperVHost)
                    .$promise
                        .then((data:any)=>{
                            hyperVHost.id = data.id;
                            this.hyperVHosts.splice(0, 0, hyperVHost);
                            this.ToastNotificationService('Добавлено');
                        })
                        .catch((err:any)=>{
                            this.ToastNotificationService('Не удалось выгрузить данные');
                        });
            }
        }

        public deleteHyperVHost(index:number){
            this.WebServices.AdminHyperVHostService.delete({
                id: this.hyperVHosts[index].id
            }).$promise
                .then((data:any)=>{
                    this.hyperVHosts.splice(index, 1);
                    this.ToastNotificationService('Удалено');
                })
                .catch((err:any)=>{
                    this.ToastNotificationService('Не удалось выгрузить данные');
                })
        }

        public showConfirm(index:number){
            var confirm = this.mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            this.mdDialog.show(confirm)
                .then(() => {
                    this.deleteHyperVHost(index);
                })
                .catch(() => {

                });
        }
    }

    class dialogHyperVHostEditCtrl{
        
        public static $inject = ["hyperVHostEdit", "$mdDialog"];
        
        constructor(public hyperVHostEdit:any,
                    private $mdDialog:any){
        }

        public answer(){
            this.$mdDialog.hide(this.hyperVHostEdit);
        }

        public cancel(){
            this.$mdDialog.cancel();
        }
    }

    angular.module('preferences.news')
        .controller('HyperVHostController', hyperVHostController)
        .controller('DialogHyperVEditCtrl', dialogHyperVHostEditCtrl);
}