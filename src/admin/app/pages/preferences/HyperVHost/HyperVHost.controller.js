var admin;
(function (admin) {
    var hyperVHostController = (function () {
        function hyperVHostController(WebServices, ToastNotificationService, mdDialog, $filter) {
            this.WebServices = WebServices;
            this.ToastNotificationService = ToastNotificationService;
            this.mdDialog = mdDialog;
            this.$filter = $filter;
            this.paginationSettings = {
                pageNumber: 1,
                pageSize: 5
            };
            this.getHyperVHosts();
        }
        hyperVHostController.prototype.getHyperVHosts = function () {
            var _this = this;
            this.WebServices.AdminHyperVHostService.getAll({}).$promise
                .then(function (data) {
                _this.hyperVHosts = data;
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        hyperVHostController.prototype.showEdit = function (index) {
            var _this = this;
            this.mdDialog.show({
                controller: "DialogHyperVEditCtrl as dialog",
                templateUrl: "app/pages/preferences/HyperVHost/editHyperVHostForm.tmpl.html",
                parent: angular.element(document.body),
                targetEvent: null,
                clickOutsideToClose: true,
                locals: {
                    hyperVHostEdit: (typeof index !== "undefined") ? angular.copy(this.hyperVHosts[index]) : null
                }
            })
                .then(function (answer) {
                _this.createOrUpdateHyperVhost(answer);
            }, (function (err) {
            }));
        };
        hyperVHostController.prototype.createOrUpdateHyperVhost = function (hyperVHost) {
            var _this = this;
            if (hyperVHost.id) {
                this.WebServices.AdminHyperVHostService.update(hyperVHost)
                    .$promise
                    .then(function (data) {
                    var myHyperVhost = _this.$filter('filter')(_this.hyperVHosts, { id: hyperVHost.id })[0];
                    var index = _this.hyperVHosts.indexOf(myHyperVhost);
                    _this.hyperVHosts.splice(index, 1, hyperVHost);
                    _this.ToastNotificationService('Изменено');
                })
                    .catch(function (err) {
                    _this.ToastNotificationService('Не удалось выгрузить данные');
                });
            }
            else {
                this.WebServices.AdminHyperVHostService.save(hyperVHost)
                    .$promise
                    .then(function (data) {
                    hyperVHost.id = data.id;
                    _this.hyperVHosts.splice(0, 0, hyperVHost);
                    _this.ToastNotificationService('Добавлено');
                })
                    .catch(function (err) {
                    _this.ToastNotificationService('Не удалось выгрузить данные');
                });
            }
        };
        hyperVHostController.prototype.deleteHyperVHost = function (index) {
            var _this = this;
            this.WebServices.AdminHyperVHostService.delete({
                id: this.hyperVHosts[index].id
            }).$promise
                .then(function (data) {
                _this.hyperVHosts.splice(index, 1);
                _this.ToastNotificationService('Удалено');
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        hyperVHostController.prototype.showConfirm = function (index) {
            var _this = this;
            var confirm = this.mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            this.mdDialog.show(confirm)
                .then(function () {
                _this.deleteHyperVHost(index);
            })
                .catch(function () {
            });
        };
        hyperVHostController.$inject = ["WebServices", "ToastNotificationService", "$mdDialog", "$filter"];
        return hyperVHostController;
    }());
    admin.hyperVHostController = hyperVHostController;
    var dialogHyperVHostEditCtrl = (function () {
        function dialogHyperVHostEditCtrl(hyperVHostEdit, $mdDialog) {
            this.hyperVHostEdit = hyperVHostEdit;
            this.$mdDialog = $mdDialog;
        }
        dialogHyperVHostEditCtrl.prototype.answer = function () {
            this.$mdDialog.hide(this.hyperVHostEdit);
        };
        dialogHyperVHostEditCtrl.prototype.cancel = function () {
            this.$mdDialog.cancel();
        };
        dialogHyperVHostEditCtrl.$inject = ["hyperVHostEdit", "$mdDialog"];
        return dialogHyperVHostEditCtrl;
    }());
    angular.module('preferences.news')
        .controller('HyperVHostController', hyperVHostController)
        .controller('DialogHyperVEditCtrl', dialogHyperVHostEditCtrl);
})(admin || (admin = {}));
