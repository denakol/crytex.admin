var admin;
(function (admin) {
    var AdminLocationsController = (function () {
        function AdminLocationsController(WebServices, $mdDialog, ToastNotificationService, $filter) {
            this.WebServices = WebServices;
            this.$mdDialog = $mdDialog;
            this.ToastNotificationService = ToastNotificationService;
            this.$filter = $filter;
            this.isShowNewLocationForm = false;
            this.isShowEditLocationForm = false;
            this.getLocations();
        }
        AdminLocationsController.prototype.getLocations = function () {
            var _this = this;
            this.WebServices.AdminLocationService.getAll({}).$promise
                .then(function (data) {
                _this.locations = data;
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminLocationsController.prototype.createLocation = function () {
            var _this = this;
            this.newLocation.createDate = new Date().toISOString();
            this.newLocation.disabled = true;
            this.WebServices.AdminLocationService.save(this.newLocation).$promise
                .then(function (data) {
                _this.newLocation.id = data.id;
                _this.locations.splice(0, 0, _this.newLocation);
                _this.ToastNotificationService('Добавлено');
                _this.setDefaultNewLocation();
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminLocationsController.prototype.showConfirm = function (index) {
            var _this = this;
            var confirm = this.$mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            this.$mdDialog.show(confirm).then(function () {
                _this.deleteLocation(index);
            });
        };
        AdminLocationsController.prototype.deleteLocation = function (index) {
            var _this = this;
            this.WebServices.AdminLocationService.delete({
                id: this.locations[index].id
            }).$promise
                .then(function (data) {
                _this.locations.splice(index, 1);
                _this.ToastNotificationService('Удалено');
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminLocationsController.prototype.editLocation = function (location) {
            if (this.editableLocation) {
                var previousEditPaymentSystem = this.$filter('filter')(this.locations, { id: this.editableLocation.id })[0];
                this.cancelEditLocation(previousEditPaymentSystem);
            }
            location.isEdit = true;
            this.editableLocation = angular.copy(location);
            this.editableLocation.isActive = !this.editableLocation.disabled;
        };
        AdminLocationsController.prototype.cancelEditLocation = function (location) {
            location.isEdit = false;
            this.editableLocation = null;
        };
        AdminLocationsController.prototype.saveChanges = function () {
            var _this = this;
            this.editableLocation.disabled = !this.editableLocation.isActive;
            this.WebServices.AdminLocationService.update(this.editableLocation).$promise
                .then(function (data) {
                _this.ToastNotificationService('Изменено');
                var sourceLocation = _this.$filter('filter')(_this.locations, { id: _this.editableLocation.id })[0];
                var index = _this.locations.indexOf(sourceLocation);
                _this.locations[index] = angular.copy(_this.editableLocation);
                _this.locations[index].isEdit = false;
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        AdminLocationsController.prototype.setDefaultNewLocation = function () {
            this.newLocation = {};
            this.isShowNewLocationForm = false;
        };
        AdminLocationsController.$inject = ["WebServices", "$mdDialog", "ToastNotificationService", "$filter"];
        return AdminLocationsController;
    }());
    admin.AdminLocationsController = AdminLocationsController;
    angular.module("preferences.locations")
        .controller("AdminLocationsController", AdminLocationsController);
})(admin || (admin = {}));
