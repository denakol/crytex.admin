module admin {

    import LocationViewModel = core.WebApi.Models.LocationViewModel;
    import PageModel = core.PageModel;

    export class AdminLocationsController{

        public static $inject = ["WebServices", "$mdDialog", "ToastNotificationService", "$filter"];

        public locations: PageModel<LocationViewModel>;
        
        public newLocation: LocationViewModel;
        public editableLocation: LocationViewModel;
        
        public isShowNewLocationForm:boolean = false;
        public isShowEditLocationForm:boolean = false;

        constructor(private WebServices:any, private $mdDialog:any,
                    private ToastNotificationService:any, private $filter:any){
            this.getLocations();
        }

        public getLocations(){
            this.WebServices.AdminLocationService.getAll({
            }).$promise
                .then((data:any)=> {
                    this.locations = data;
                })
                .catch((err:any)=> {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                });
        }
        
        public createLocation(){
            this.newLocation.createDate = new Date().toISOString();
            this.newLocation.disabled = true;
            
            this.WebServices.AdminLocationService.save(this.newLocation).$promise
                .then((data:any)=> {
                    this.newLocation.id = data.id;
                    this.locations.splice(0, 0, this.newLocation);
                    this.ToastNotificationService('Добавлено');
                    this.setDefaultNewLocation();
                })
                .catch((err:any)=> {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                })
        }

        public showConfirm(index:number){
            var confirm = this.$mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');

            this.$mdDialog.show(confirm).then( () => {
                this.deleteLocation(index);
            });
        }

        public deleteLocation(index:number){
            this.WebServices.AdminLocationService.delete({
                id: this.locations[index].id
            }).$promise
                .then((data:any)=> {
                    this.locations.splice(index, 1);
                    this.ToastNotificationService('Удалено');
                })
                .catch((err:any)=> {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                })
        }

        public editLocation(location:LocationViewModel){
            if(this.editableLocation){
                var previousEditPaymentSystem = this.$filter('filter')(this.locations, { id: this.editableLocation.id })[0];
                this.cancelEditLocation(previousEditPaymentSystem);
            }
            location.isEdit = true;
            this.editableLocation = angular.copy(location);
            this.editableLocation.isActive = !this.editableLocation.disabled;
        }

        public cancelEditLocation(location:LocationViewModel){
            location.isEdit = false;
            this.editableLocation = null;
        }

        public saveChanges(){
            this.editableLocation.disabled = !this.editableLocation.isActive;
            this.WebServices.AdminLocationService.update(this.editableLocation).$promise
                .then((data:any)=> {
                    this.ToastNotificationService('Изменено');
                    var sourceLocation = this.$filter('filter')(this.locations, { id: this.editableLocation.id })[0];
                    var index = this.locations.indexOf(sourceLocation);
                    this.locations[index] = angular.copy(this.editableLocation);
                    this.locations[index].isEdit = false;
                })
                .catch((err:any)=> {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                });
        }

        public setDefaultNewLocation(){
            this.newLocation = <LocationViewModel>{};
            this.isShowNewLocationForm = false;
        }
    }
    
    angular.module("preferences.locations")
        .controller("AdminLocationsController", AdminLocationsController);
}