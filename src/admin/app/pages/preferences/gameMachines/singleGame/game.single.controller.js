var admin;
(function (admin) {
    var FileType = core.WebApi.Models.Enums.FileType;
    var SingleGameController = (function () {
        function SingleGameController(webService, $mdDialog, gameFamily, $stateParams, toastNotificationService, baseInfo, $state, $filter, fileUploader) {
            this.webService = webService;
            this.$mdDialog = $mdDialog;
            this.gameFamily = gameFamily;
            this.$stateParams = $stateParams;
            this.toastNotificationService = toastNotificationService;
            this.baseInfo = baseInfo;
            this.$state = $state;
            this.$filter = $filter;
            this.fileUploader = fileUploader;
            this.isEditGame = false;
            this.gameService = webService.AdminGameService;
            this.tariffService = webService.AdminGameServerTariffService;
            this.getGame();
        }
        // Подтверждение удаления тарифа или игры
        SingleGameController.prototype.showConfirm = function (name, id) {
            var _this = this;
            var confirm = this.$mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            this.$mdDialog.show(confirm).then(function () {
                // Удалить
                if (name === "game") {
                    _this.deleteGame(id);
                }
                else if (name === "tariff") {
                    _this.deleteTariff(id);
                }
            });
        };
        SingleGameController.prototype.cancleEditGame = function () {
            this.game = this.defaultGame;
            this.isEditGame = false;
        };
        SingleGameController.prototype.editGame = function () {
            this.isEditGame = true;
        };
        SingleGameController.prototype.updateGame = function () {
            var _this = this;
            this.gameService.updateGame(this.game).$promise
                .then(function () {
                _this.isEditGame = false;
            })
                .catch(function (response) {
                _this.toastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        SingleGameController.prototype.switchStateGame = function () {
            var _this = this;
            this.game.disabled = !this.game.disabled;
            this.gameService.updateGame(this.game).$promise
                .catch(function (response) {
                _this.toastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        SingleGameController.prototype.switchStateTariff = function (tariff) {
            var _this = this;
            tariff.disabled = !tariff.disabled;
            this.tariffService.updateTariff(tariff).$promise
                .catch(function (response) {
                _this.toastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        SingleGameController.prototype.clearNewTariff = function () {
            this.newTariff = {
                id: 0,
                gameId: this.game.id,
                slot: 0,
                name: '',
                performance: 0,
                createDate: new Date(),
                isEdit: false,
                disabled: false
            };
        };
        SingleGameController.prototype.editTariff = function (originalTariff) {
            // Если есть открытый редактируемый талон, то закрываем его
            var previousEditabletariff = this.$filter('filter')(this.game.gameServerTariffs, { isEdit: true })[0];
            if (previousEditabletariff)
                previousEditabletariff.isEdit = false;
            // Откроем редактируемые поля
            originalTariff.isEdit = true;
            this.editableTariff = angular.copy(originalTariff);
        };
        SingleGameController.prototype.cancleEditTariff = function (originalTariff) {
            originalTariff.isEdit = false;
        };
        SingleGameController.prototype.updateTariff = function () {
            var _this = this;
            this.tariffService.updateTariff(this.editableTariff)
                .$promise.then(function () {
                _this.UpdateTariffList("update", _this.editableTariff.id);
            })
                .catch(function (response) {
                _this.toastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        SingleGameController.prototype.createTariff = function () {
            var _this = this;
            this.tariffService.createTariff(this.newTariff)
                .$promise.then(function (data) {
                _this.UpdateTariffList("add", data.id);
            })
                .catch(function (response) {
                _this.toastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        SingleGameController.prototype.getGame = function () {
            var _this = this;
            this.gameService.getGameById({ id: this.$stateParams.id }).$promise.then(function (data) {
                _this.defaultGame = data;
                _this.game = data;
                _this.clearNewTariff();
                return;
            })
                .catch(function (response) {
                _this.toastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        SingleGameController.prototype.deleteGame = function (id) {
            var _this = this;
            this.gameService.deleteGame({
                id: id
            }).$promise.then(function () {
                _this.$state.go('preferencesGameMachines');
            })
                .catch(function (response) {
                _this.toastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        SingleGameController.prototype.deleteTariff = function (id) {
            var _this = this;
            this.tariffService.deleteTariff({
                id: id
            }).$promise.then(function () {
                _this.UpdateTariffList("delete", id);
            })
                .catch(function (response) {
                _this.toastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        SingleGameController.prototype.UpdateTariffList = function (action, tariffId) {
            if (action === "delete") {
                var deletableTariff = this.$filter('filter')(this.game.gameServerTariffs, { id: tariffId })[0];
                var index = this.game.gameServerTariffs.indexOf(deletableTariff);
                this.game.gameServerTariffs.splice(index, 1);
            }
            else if (action === "update") {
                var updatableTariff = this.$filter('filter')(this.game.gameServerTariffs, { id: tariffId })[0];
                var index = this.game.gameServerTariffs.indexOf(updatableTariff);
                this.game.gameServerTariffs[index] = angular.copy(this.editableTariff);
                this.game.gameServerTariffs[index].isEdit = false;
            }
            else if (action === "add") {
                this.newTariff.id = tariffId;
                this.game.gameServerTariffs.splice(this.game.gameServerTariffs.length, 0, angular.copy(this.newTariff));
                this.clearNewTariff();
            }
        };
        SingleGameController.prototype.uploadFiles = function (file) {
            var _this = this;
            // Set params
            var file = this.fileUploader.upload({
                url: this.baseInfo.URL + this.baseInfo.PORT + this.baseInfo.API_URL + '/Admin/AdminFile/Post',
                file: file,
                data: {
                    fileType: FileType.Image
                }
            });
            // Upload
            file.then(function (description) {
                var newDescription = {
                    name: "Image Name",
                    path: description.data.path,
                    type: FileType.Image
                };
                _this.game.imageFileDescriptor = newDescription;
                _this.game.imageFileDescriptorId = description.data.id;
            }, function (response) {
                // Умолчим, при конечной отправке выползет сообщение
            });
            return;
        };
        SingleGameController.$inject = ["WebServices", "$mdDialog", "GameFamily",
            "$stateParams", "ToastNotificationService", "BASE_INFO",
            "$state", "$filter", "Upload"];
        return SingleGameController;
    }());
    admin.SingleGameController = SingleGameController;
    angular.module('gameMachines.singleGame')
        .controller('SingleGameController', SingleGameController);
})(admin || (admin = {}));
