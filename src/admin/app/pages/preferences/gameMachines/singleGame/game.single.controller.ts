module admin {

    import GameViewModel = core.WebApi.Models.GameViewModel;
    import PageModel = core.PageModel;
    import AdminGameService = core.IAdminGameService;
    import PaginationSetting = core.PaginationSetting;
    import GameFamily = core.WebApi.Models.Enums.GameFamily;
    import IResourceClass = angular.resource.IResourceClass;
    import IAdminGameService = core.IAdminGameService;
    import GameServerTariffView = core.WebApi.Models.GameServerTariffView;
    import IAdminGameTariffService = core.IAdminGameTariffService;
    import FileType = core.WebApi.Models.Enums.FileType;
    import FileDescriptorViewModel = core.WebApi.Models.FileDescriptorViewModel;

    export class SingleGameController {

        static $inject = ["WebServices", "$mdDialog", "GameFamily",
                            "$stateParams", "ToastNotificationService", "BASE_INFO",
                            "$state", "$filter", "Upload"];

        public game: GameViewModel;
        public newTariff: GameServerTariffView;
        public editableTariff: GameServerTariffView;
        public isEditGame: boolean = false;

        private defaultGame: GameViewModel;
        private gameService: IAdminGameService;
        private tariffService: IAdminGameTariffService;

        constructor(private webService: any, private $mdDialog:any,
                    public gameFamily: any, private $stateParams: any,
                    private toastNotificationService: any, public baseInfo: any,
                    private $state: any, private $filter: any,
                    private fileUploader: any) {

            this.gameService = webService.AdminGameService;
            this.tariffService = webService.AdminGameServerTariffService;
            this.getGame();
        }

        // Подтверждение удаления тарифа или игры
        public showConfirm (name: string, id: number) {
            var confirm = this.$mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');

            this.$mdDialog.show(confirm).then( () => {
                // Удалить
                if (name === "game") {
                    this.deleteGame(id);
                } else if (name === "tariff") {
                    this.deleteTariff(id);
                }
            });
        }

        public cancleEditGame() {
            this.game = this.defaultGame;
            this.isEditGame = false;
        }

        public editGame() {
            this.isEditGame = true;
        }

        public updateGame() {
            this.gameService.updateGame(this.game).$promise
                .then(() => {
                    this.isEditGame = false;
                })
                .catch ( (response:any) => {
                    this.toastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

        public switchStateGame() {
            this.game.disabled = !this.game.disabled;

            this.gameService.updateGame(this.game).$promise
                .catch ( (response:any) => {
                    this.toastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

        public switchStateTariff(tariff: GameServerTariffView) {
            tariff.disabled = !tariff.disabled;

            this.tariffService.updateTariff(tariff).$promise
                .catch ( (response:any) => {
                    this.toastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

        public clearNewTariff() {
            this.newTariff = <GameServerTariffView>{
                id: 0,
                gameId: this.game.id,
                slot: 0,
                name: '',
                performance: 0,
                createDate: new Date(),
                isEdit: false,
                disabled: false
            };

        }

        public editTariff(originalTariff: GameServerTariffView) {
            // Если есть открытый редактируемый талон, то закрываем его
            var previousEditabletariff = this.$filter('filter')(this.game.gameServerTariffs, {isEdit: true})[0];
            if (previousEditabletariff) previousEditabletariff.isEdit = false;

            // Откроем редактируемые поля
            originalTariff.isEdit = true;
            this.editableTariff = angular.copy(originalTariff);
        }

        public cancleEditTariff(originalTariff: GameServerTariffView) {
            originalTariff.isEdit = false;
        }

        public updateTariff() {
            this.tariffService.updateTariff(this.editableTariff)
                .$promise.then(() => {
                    this.UpdateTariffList("update", this.editableTariff.id);
                })
                .catch ( (response:any) => {
                    this.toastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

        public createTariff() {
            this.tariffService.createTariff(this.newTariff)
                .$promise.then((data: any) => {
                    this.UpdateTariffList("add", data.id);
                })
                .catch ( (response:any) => {
                    this.toastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

        private getGame() {
            this.gameService.getGameById({id: this.$stateParams.id}).$promise.then((data: GameViewModel) => {
                    this.defaultGame = data;
                    this.game = data;
                    this.clearNewTariff();
                    return;
                })
                .catch ( (response:any) => {
                    this.toastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

        private deleteGame(id: number) {
            this.gameService.deleteGame(
                {
                    id: id
                }).$promise.then(() => {
                    this.$state.go('preferencesGameMachines');
                })
                .catch ( (response:any) => {
                    this.toastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

        private deleteTariff(id: number) {
            this.tariffService.deleteTariff(
                {
                    id: id
                }).$promise.then(() => {
                    this.UpdateTariffList("delete", id);
                })
                .catch ( (response:any) => {
                    this.toastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

        private UpdateTariffList(action: string, tariffId: number) {
            if  (action === "delete") {
                var deletableTariff = this.$filter('filter')(this.game.gameServerTariffs, {id: tariffId})[0];
                var index = this.game.gameServerTariffs.indexOf(deletableTariff);
                this.game.gameServerTariffs.splice(index, 1);
            } else if (action === "update") {
                var updatableTariff = this.$filter('filter')(this.game.gameServerTariffs, {id: tariffId})[0];
                var index = this.game.gameServerTariffs.indexOf(updatableTariff);
                this.game.gameServerTariffs[index] = angular.copy(this.editableTariff);
                this.game.gameServerTariffs[index].isEdit = false;
            } else if (action === "add") {
                this.newTariff.id = tariffId;
                this.game.gameServerTariffs.splice(this.game.gameServerTariffs.length, 0, angular.copy(this.newTariff));
                this.clearNewTariff();
            }
        }

        private uploadFiles (file:any) : any {
            // Set params
            var file =  this.fileUploader.upload({
                url: this.baseInfo.URL + this.baseInfo.PORT + this.baseInfo.API_URL + '/Admin/AdminFile/Post',
                file: file,
                data: {
                    fileType: FileType.Image
                }
            });

            // Upload
            file.then( (description:any) => {
                    var newDescription: FileDescriptorViewModel = {
                        name: "Image Name",
                        path: description.data.path,
                        type: FileType.Image
                    }
                    this.game.imageFileDescriptor = newDescription;
                    this.game.imageFileDescriptorId = description.data.id;
                },
                (response:any) => {
                    // Умолчим, при конечной отправке выползет сообщение
                }
            );

            return;
        }

    }

    angular.module('gameMachines.singleGame')
        .controller('SingleGameController', SingleGameController);
}