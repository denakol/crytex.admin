var admin;
(function (admin) {
    var GameFamily = core.WebApi.Models.Enums.GameFamily;
    var FileType = core.WebApi.Models.Enums.FileType;
    var GameController = (function () {
        function GameController(webService, $mdDialog, gameFamily, $filter, toastNotificationService, baseInfo, fileUploader) {
            this.webService = webService;
            this.$mdDialog = $mdDialog;
            this.gameFamily = gameFamily;
            this.$filter = $filter;
            this.toastNotificationService = toastNotificationService;
            this.baseInfo = baseInfo;
            this.fileUploader = fileUploader;
            this.isShowNewGameForm = false;
            this.paginationSettings = {
                pageNumber: 1,
                pageSize: 5
            };
            this.gameImage = {};
            this.gameService = webService.AdminGameService;
            this.getGames();
            this.setDefaultNewGame();
        }
        GameController.prototype.pageChangeHandler = function (index) {
            this.paginationSettings.pageNumber = index;
            this.getGames();
        };
        GameController.prototype.createGame = function () {
            var _this = this;
            // Создаём новую игру
            this.newGame.imageFileDescriptor = null;
            this.gameService.createGame(this.newGame).$promise
                .then(function (responseObject) {
                // Добавляем игру в список
                _this.newGame.id = responseObject.id;
                _this.newGame.imageFileDescriptor = _this.tempImageFileDescriptor;
                _this.gameList.items.splice(0, 0, _this.newGame);
                _this.gameList.items.splice(_this.paginationSettings.pageSize, 1);
                _this.setDefaultNewGame();
            })
                .catch(function (response) {
                if (response.status != -1) {
                    if (response.data &&
                        response.data.modelState &&
                        response.data.modelState.validationError) {
                        _this.toastNotificationService(response.data.modelState.validationError[0]);
                    }
                    else if (response.data &&
                        response.data.message) {
                        _this.toastNotificationService(response.data.message);
                    }
                }
                else {
                    _this.toastNotificationService("Ошибка обновления данных");
                }
                return;
            });
        };
        GameController.prototype.setDefaultNewGame = function () {
            this.newGame = {};
            this.gameImage = null;
            this.isShowNewGameForm = false;
        };
        // Send file on server
        GameController.prototype.uploadFiles = function (file) {
            var _this = this;
            // Set params
            var file = this.fileUploader.upload({
                url: this.baseInfo.URL + this.baseInfo.PORT + this.baseInfo.API_URL + '/Admin/AdminFile/Post',
                file: file,
                data: {
                    fileName: file.name,
                    fileType: FileType.Image
                }
            });
            // Upload
            file.then(function (description) {
                _this.tempImageFileDescriptor = {
                    name: "Image Name",
                    path: description.data.path,
                    type: FileType.Image
                };
                _this.newGame.imageFileDescriptorId = description.data.id;
            }, function (response) {
                // Умолчим, при конечной отправке выползет сообщение
            });
            return;
        };
        GameController.prototype.getGames = function () {
            var _this = this;
            this.gameList = this.gameService.getAllPage({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize,
                familyGame: GameFamily.Unknown
            });
            this.gameList.$promise
                .catch(function (response) {
                _this.toastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        GameController.$inject = ["WebServices", "$mdDialog", "GameFamily", "$filter",
            "ToastNotificationService", "BASE_INFO", "Upload"];
        return GameController;
    }());
    admin.GameController = GameController;
    angular.module('gameMachines.game')
        .controller('GameController', GameController);
})(admin || (admin = {}));
