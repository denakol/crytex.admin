module admin {

    import GameViewModel = core.WebApi.Models.GameViewModel;
    import PageModel = core.PageModel;
    import AdminGameService = core.IAdminGameService;
    import PaginationSetting = core.PaginationSetting;
    import GameFamily = core.WebApi.Models.Enums.GameFamily;
    import FileType = core.WebApi.Models.Enums.FileType;
    import FileDescriptorViewModel = core.WebApi.Models.FileDescriptorViewModel;

    export class GameController {

        static $inject = ["WebServices", "$mdDialog", "GameFamily", "$filter",
                            "ToastNotificationService", "BASE_INFO", "Upload"];

        public gameList: PageModel<GameViewModel>;
        public newGame: GameViewModel;
        public isShowNewGameForm: boolean = false;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        }
        public gameImage: any = {};
        private tempImageFileDescriptor: FileDescriptorViewModel;

        private gameService: AdminGameService;

        constructor(private webService: any, private $mdDialog:any,
                    public gameFamily: any, private $filter: any,
                    private toastNotificationService: any, public baseInfo: any,
                    private fileUploader: any) {

            this.gameService = webService.AdminGameService;
            this.getGames();
            this.setDefaultNewGame();
        }

        public pageChangeHandler (index: number) {
            this.paginationSettings.pageNumber = index;
            this.getGames();
        }

        public createGame() {
            // Создаём новую игру
            this.newGame.imageFileDescriptor = null;
            this.gameService.createGame( this.newGame ).$promise
                .then ( (responseObject: any) => {
                    // Добавляем игру в список
                    this.newGame.id = responseObject.id;
                    this.newGame.imageFileDescriptor = this.tempImageFileDescriptor;
                    this.gameList.items.splice(0,0, this.newGame);
                    this.gameList.items.splice(this.paginationSettings.pageSize, 1);
                    this.setDefaultNewGame();
                })
                .catch ( (response:any) => {
                    if (response.status != -1){
                        if (response.data &&
                            response.data.modelState &&
                            response.data.modelState.validationError){
                            this.toastNotificationService(response.data.modelState.validationError[0]);
                        } else if (response.data &&
                            response.data.message) {
                            this.toastNotificationService(response.data.message);
                        }
                    } else {
                        this.toastNotificationService("Ошибка обновления данных");
                    }
                    return;
                });
        }

        public setDefaultNewGame() {
            this.newGame = <GameViewModel>{};
            this.gameImage = null;
            this.isShowNewGameForm = false;
        }

        // Send file on server
        private uploadFiles (file:any) : any {
            // Set params
            var file =  this.fileUploader.upload({
                url: this.baseInfo.URL + this.baseInfo.PORT + this.baseInfo.API_URL + '/Admin/AdminFile/Post',
                file: file,
                data: {
                    fileName: file.name,
                    fileType: FileType.Image
                }
            });

            // Upload
            file.then( (description:any) => {
                    this.tempImageFileDescriptor =  {
                        name: "Image Name",
                        path: description.data.path,
                        type: FileType.Image
                    };
                    this.newGame.imageFileDescriptorId = description.data.id;
                },
                (response:any) => {
                    // Умолчим, при конечной отправке выползет сообщение
                }
            );

            return;
        }

        private getGames() {
            this.gameList = this.gameService.getAllPage(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize,
                    familyGame: GameFamily.Unknown
                });

            this.gameList.$promise
                .catch ( (response:any) => {
                    this.toastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

    }

    angular.module('gameMachines.game')
        .controller('GameController', GameController);
}