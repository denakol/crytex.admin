module admin {

    import GameHostViewModel = core.WebApi.Models.GameHostViewModel;
    import AdminGameHostService = core.IAdminGameHostService;
    import GameViewModel = core.WebApi.Models.GameViewModel;
    import AdminGameService = core.IAdminGameService;
    import GameFamily = core.WebApi.Models.Enums.GameFamily;
    import PageModel = core.PageModel;

    export class HostAdvanceWindowController {

        static $inject = ["gameHostService", "$mdDialog", "gameHost", "GameFamily", "WebServices", "ToastNotificationService", "locations"];

        public gameList: PageModel<GameViewModel>;

        private gameService: AdminGameService;

        constructor(private gameHostService: AdminGameHostService, private $mdDialog:any,
                    public gameHost: GameHostViewModel, private GameFamily: any,
                    private webService: any, private toastNotificationService: any,
                    public locations:any) {

            this.gameService = webService.AdminGameService;
            if (!this.gameHost.supportedGames) {
                this.gameHost.supportedGames = [];
            }
            this.getGames();
        }

        public cancel () {
            this.$mdDialog.cancel();
        };

        public answer () {
            if (this.gameHost.id) {
                // Обновляем хост
                this.gameHostService.updateGameHost( this.gameHost ).$promise
                    .then ( () => {
                        // Обновляем данные
                        this.$mdDialog.hide(this.gameHost);
                    })
                    .catch ( (response:any) => {
                        if (response.status != -1){
                            if (response.data &&
                                response.data.modelState &&
                                response.data.modelState.validationError){
                                this.toastNotificationService(response.data.modelState.validationError[0]);
                            } else if (response.data &&
                                response.data.message) {
                                this.toastNotificationService(response.data.message);
                            }
                        } else {
                            this.toastNotificationService("Ошибка обновления данных");
                        }
                        return;
                    });
            }
            else {
                // Создаём новый хост
                this.gameHostService.createGameHost( this.gameHost ).$promise
                    .then ( (responseObject: any) => {
                        // Добавляем игру в список
                        this.gameHost.id = responseObject.id;
                        this.$mdDialog.hide(this.gameHost);
                    })
                    .catch ( (response:any) => {
                        if (response.status != -1){
                            if (response.data &&
                                response.data.modelState &&
                                response.data.modelState.validationError){
                                this.toastNotificationService(response.data.modelState.validationError[0]);
                            } else if (response.data &&
                                response.data.message) {
                                this.toastNotificationService(response.data.message);
                            }
                        } else {
                            this.toastNotificationService("Ошибка обновления данных");
                        }
                        return;
                    });
            }
        };

        public addGame(game: GameViewModel) {
            // Добавим игру в список хоста
            this.gameHost.supportedGames.push(game);
            this.gameHost.supportedGamesIds.push(game.id);

            // Удалим игру из общего списка, чтобы не добавлять два раза
            var index = this.gameList.items.indexOf(game);
            this.gameList.items.splice(index, 1);
        }

        public removeGame(game: GameViewModel) {
            // Добавим игру в общий список
            this.gameList.items.push(game);


            // Удалим игру из списка хоста
            var index = this.gameHost.supportedGames.indexOf(game);
            this.gameHost.supportedGames.splice(index, 1);
            this.gameHost.supportedGamesIds.splice(index, 1);
        }

        private getGames() {
            // Получим все игры что есть
            this.gameList = this.gameService.getAllPage(
                {
                    pageNumber: 1,
                    pageSize: 1000,
                    familyGame: GameFamily.Unknown
                });

            this.gameList.$promise.then( (games: PageModel<GameViewModel>) => {
                    games = this.compareGames(games);
                    return games;
                })
                .catch ( (response:any) => {
                    return;
                });
        }

        private compareGames(games: PageModel<GameViewModel>): PageModel<GameViewModel> {
            if (this.gameHost.supportedGamesIds) {
                var gamesIds = this.gameHost.supportedGamesIds;

                for (var i = 0; i < games.items.length; i++) {
                    for (var j = 0; j < gamesIds.length; j++) {
                        if ( games.items[i].id == gamesIds[j]){
                            games.items.splice(i, 1);
                            break;
                        }
                    }
                }
            }
            return games;
        }

    }

    angular.module('gameMachines.host')
        .controller('HostAdvanceWindowController', HostAdvanceWindowController);
}