var admin;
(function (admin) {
    var GameFamily = core.WebApi.Models.Enums.GameFamily;
    var HostAdvanceWindowController = (function () {
        function HostAdvanceWindowController(gameHostService, $mdDialog, gameHost, GameFamily, webService, toastNotificationService, locations) {
            this.gameHostService = gameHostService;
            this.$mdDialog = $mdDialog;
            this.gameHost = gameHost;
            this.GameFamily = GameFamily;
            this.webService = webService;
            this.toastNotificationService = toastNotificationService;
            this.locations = locations;
            this.gameService = webService.AdminGameService;
            if (!this.gameHost.supportedGames) {
                this.gameHost.supportedGames = [];
            }
            this.getGames();
        }
        HostAdvanceWindowController.prototype.cancel = function () {
            this.$mdDialog.cancel();
        };
        ;
        HostAdvanceWindowController.prototype.answer = function () {
            var _this = this;
            if (this.gameHost.id) {
                // Обновляем хост
                this.gameHostService.updateGameHost(this.gameHost).$promise
                    .then(function () {
                    // Обновляем данные
                    _this.$mdDialog.hide(_this.gameHost);
                })
                    .catch(function (response) {
                    if (response.status != -1) {
                        if (response.data &&
                            response.data.modelState &&
                            response.data.modelState.validationError) {
                            _this.toastNotificationService(response.data.modelState.validationError[0]);
                        }
                        else if (response.data &&
                            response.data.message) {
                            _this.toastNotificationService(response.data.message);
                        }
                    }
                    else {
                        _this.toastNotificationService("Ошибка обновления данных");
                    }
                    return;
                });
            }
            else {
                // Создаём новый хост
                this.gameHostService.createGameHost(this.gameHost).$promise
                    .then(function (responseObject) {
                    // Добавляем игру в список
                    _this.gameHost.id = responseObject.id;
                    _this.$mdDialog.hide(_this.gameHost);
                })
                    .catch(function (response) {
                    if (response.status != -1) {
                        if (response.data &&
                            response.data.modelState &&
                            response.data.modelState.validationError) {
                            _this.toastNotificationService(response.data.modelState.validationError[0]);
                        }
                        else if (response.data &&
                            response.data.message) {
                            _this.toastNotificationService(response.data.message);
                        }
                    }
                    else {
                        _this.toastNotificationService("Ошибка обновления данных");
                    }
                    return;
                });
            }
        };
        ;
        HostAdvanceWindowController.prototype.addGame = function (game) {
            // Добавим игру в список хоста
            this.gameHost.supportedGames.push(game);
            this.gameHost.supportedGamesIds.push(game.id);
            // Удалим игру из общего списка, чтобы не добавлять два раза
            var index = this.gameList.items.indexOf(game);
            this.gameList.items.splice(index, 1);
        };
        HostAdvanceWindowController.prototype.removeGame = function (game) {
            // Добавим игру в общий список
            this.gameList.items.push(game);
            // Удалим игру из списка хоста
            var index = this.gameHost.supportedGames.indexOf(game);
            this.gameHost.supportedGames.splice(index, 1);
            this.gameHost.supportedGamesIds.splice(index, 1);
        };
        HostAdvanceWindowController.prototype.getGames = function () {
            var _this = this;
            // Получим все игры что есть
            this.gameList = this.gameService.getAllPage({
                pageNumber: 1,
                pageSize: 1000,
                familyGame: GameFamily.Unknown
            });
            this.gameList.$promise.then(function (games) {
                games = _this.compareGames(games);
                return games;
            })
                .catch(function (response) {
                return;
            });
        };
        HostAdvanceWindowController.prototype.compareGames = function (games) {
            if (this.gameHost.supportedGamesIds) {
                var gamesIds = this.gameHost.supportedGamesIds;
                for (var i = 0; i < games.items.length; i++) {
                    for (var j = 0; j < gamesIds.length; j++) {
                        if (games.items[i].id == gamesIds[j]) {
                            games.items.splice(i, 1);
                            break;
                        }
                    }
                }
            }
            return games;
        };
        HostAdvanceWindowController.$inject = ["gameHostService", "$mdDialog", "gameHost", "GameFamily", "WebServices", "ToastNotificationService", "locations"];
        return HostAdvanceWindowController;
    }());
    admin.HostAdvanceWindowController = HostAdvanceWindowController;
    angular.module('gameMachines.host')
        .controller('HostAdvanceWindowController', HostAdvanceWindowController);
})(admin || (admin = {}));
