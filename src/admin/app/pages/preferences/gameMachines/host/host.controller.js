var admin;
(function (admin) {
    var GameFamily = core.WebApi.Models.Enums.GameFamily;
    var HostController = (function () {
        function HostController(webService, $mdDialog, $filter, ToastNotificationService, GameFamily) {
            this.webService = webService;
            this.$mdDialog = $mdDialog;
            this.$filter = $filter;
            this.ToastNotificationService = ToastNotificationService;
            this.GameFamily = GameFamily;
            this.paginationSettings = {
                pageNumber: 1,
                pageSize: 5
            };
            this.gameHostService = webService.AdminGameHostService;
            this.gameService = webService.AdminGameService;
            this.getGameHosts();
            this.getLocations();
        }
        HostController.prototype.getLocations = function () {
            var _this = this;
            this.webService.AdminLocationService.getAll({}).$promise
                .then(function (data) {
                _this.locations = data;
            })
                .catch(function (err) {
                _this.ToastNotificationService('Не удалось выгрузить данные');
            });
        };
        HostController.prototype.pageChangeHandler = function (index) {
            this.paginationSettings.pageNumber = index;
            this.getGameHosts();
        };
        HostController.prototype.showConfirm = function (gameHost) {
            var _this = this;
            var confirm = this.$mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            this.$mdDialog.show(confirm).then(function () {
                // Удалить
                _this.deleteGameHost(gameHost);
            });
        };
        HostController.prototype.showAdvanced = function (gameHost) {
            var _this = this;
            this.$mdDialog.show({
                controller: admin.HostAdvanceWindowController,
                controllerAs: 'vm',
                templateUrl: 'gameHostAdvanceWindow.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: {
                    gameHostService: this.gameHostService,
                    gameHost: gameHost ? angular.copy(gameHost) : {},
                    locations: this.locations
                }
            })
                .then(function (gameHost) {
                // Обновляем, либо добавляем
                _this.updateGameHostList(gameHost);
            });
        };
        ;
        HostController.prototype.getGameHosts = function () {
            var _this = this;
            this.gameHostList = this.gameHostService.getAllPage({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize
            });
            this.gameHostList.$promise.then(function (gameHosts) {
                _this.getGames();
                return;
            })
                .catch(function (response) {
                _this.ToastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        HostController.prototype.getGames = function () {
            var _this = this;
            // Получим все игры что есть
            this.gameList = this.gameService.getAllPage({
                pageNumber: 1,
                pageSize: 1000,
                familyGame: GameFamily.Unknown
            });
            this.gameList.$promise.then(function (games) {
                _this.compareGames();
                return games;
            })
                .catch(function (response) {
                _this.ToastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        HostController.prototype.compareGames = function () {
            var allGames = this.gameList.items;
            angular.forEach(this.gameHostList.items, function (gameHost) {
                var currentGames = [];
                angular.forEach(gameHost.supportedGamesIds, function (supportedGameId) {
                    angular.forEach(allGames, function (game) {
                        if (supportedGameId == game.id) {
                            currentGames.push(game);
                        }
                    });
                });
                gameHost.supportedGames = currentGames;
            });
            return;
        };
        HostController.prototype.deleteGameHost = function (gameHost) {
            var _this = this;
            this.gameHostService.deleteGameHost({
                id: gameHost.id
            }).$promise.then(function () {
                // Всё хорошо, удаляем элемент из списка
                var index = _this.gameHostList.items.indexOf(gameHost);
                _this.gameHostList.items.splice(index, 1);
            })
                .catch(function (response) {
                _this.ToastNotificationService("Ошибка работы с сервером");
                return;
            });
        };
        HostController.prototype.updateGameHostList = function (gameHost) {
            var sourceHost = this.$filter('filter')(this.gameHostList.items, { id: gameHost.id })[0];
            var index = this.gameHostList.items.indexOf(sourceHost);
            if (index !== -1) {
                // Обновляем хост
                this.gameHostList.items[index] = gameHost;
            }
            else {
                // Добаляем хост
                this.gameHostList.items.splice(0, 0, gameHost);
                this.gameHostList.items.splice(this.paginationSettings.pageSize, 1);
            }
        };
        HostController.$inject = ["WebServices", "$mdDialog", "$filter", "ToastNotificationService", "GameFamily"];
        return HostController;
    }());
    admin.HostController = HostController;
    angular.module('gameMachines.host')
        .controller('HostController', HostController);
})(admin || (admin = {}));
