module admin {

    import GameHostViewModel = core.WebApi.Models.GameHostViewModel;
    import GameViewModel = core.WebApi.Models.GameViewModel;
    import PageModel = core.PageModel;
    import AdminGameHostService = core.IAdminGameHostService;
    import AdminGameService = core.IAdminGameService;
    import GameFamily = core.WebApi.Models.Enums.GameFamily;
    import PaginationSetting = core.PaginationSetting;
    import LocationViewModel = core.WebApi.Models.LocationViewModel;

    export class HostController {

        static $inject = ["WebServices", "$mdDialog", "$filter", "ToastNotificationService", "GameFamily"];

        public gameHostList: PageModel<GameHostViewModel>;
        public gameList: PageModel<GameViewModel>;
        public locations: PageModel<LocationViewModel>;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        }

        private gameHostService: AdminGameHostService;
        private gameService: AdminGameService;

        constructor(private webService: any, private $mdDialog:any,
                    private $filter: any, private ToastNotificationService: any,
                    private GameFamily: any) {

            this.gameHostService = webService.AdminGameHostService;
            this.gameService = webService.AdminGameService;

            this.getGameHosts();
            this.getLocations();
        }
        
        public getLocations(){
            this.webService.AdminLocationService.getAll({
            }).$promise
                .then((data:any)=> {
                    this.locations = data;
                })
                .catch((err:any)=> {
                    this.ToastNotificationService('Не удалось выгрузить данные');
                });
        }

        public pageChangeHandler (index: number) {
            this.paginationSettings.pageNumber = index;
            this.getGameHosts();
        }

        public showConfirm (gameHost: GameHostViewModel) {
            var confirm = this.$mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');

            this.$mdDialog.show(confirm).then( () => {
                // Удалить
                this.deleteGameHost(gameHost)
            });
        }

        public showAdvanced (gameHost: GameHostViewModel) {
            this.$mdDialog.show({
                    controller: HostAdvanceWindowController,
                    controllerAs: 'vm',
                    templateUrl: 'gameHostAdvanceWindow.tmpl.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    locals: {
                        gameHostService: this.gameHostService,
                        gameHost: gameHost ? angular.copy(gameHost) : {},
                        locations: this.locations
                    }
                })
                .then( (gameHost: GameHostViewModel) => {
                    // Обновляем, либо добавляем
                    this.updateGameHostList(gameHost);
                });
        };

        private getGameHosts() {
            this.gameHostList = this.gameHostService.getAllPage(
                {
                    pageNumber: this.paginationSettings.pageNumber,
                    pageSize: this.paginationSettings.pageSize
                });

            this.gameHostList.$promise.then( (gameHosts: PageModel<GameHostViewModel>) => {
                this.getGames();
                return;
            })
            .catch ( (response:any) => {
                this.ToastNotificationService("Ошибка работы с сервером");
                return;
            });
        }

        private getGames() {
            // Получим все игры что есть
            this.gameList = this.gameService.getAllPage(
            {
                pageNumber: 1,
                pageSize: 1000,
                familyGame: GameFamily.Unknown
            });

            this.gameList.$promise.then( (games: PageModel<GameViewModel>) => {
                this.compareGames();
                return games;
            })
            .catch ( (response:any) => {
                this.ToastNotificationService("Ошибка работы с сервером");
                return;
            });
        }

        private compareGames() {
            var allGames = this.gameList.items;

            angular.forEach(this.gameHostList.items, function(gameHost) {
                var currentGames: Array<GameViewModel> = [];

                angular.forEach(gameHost.supportedGamesIds, function(supportedGameId) {
                    angular.forEach(allGames, function(game) {
                        if (supportedGameId == game.id){
                            currentGames.push(game);
                        }
                    });
                });

                gameHost.supportedGames = currentGames;
            });
            return;
        }

        private deleteGameHost(gameHost: GameHostViewModel) {
            this.gameHostService.deleteGameHost(
                {
                    id: gameHost.id
                }).$promise.then(() => {
                    // Всё хорошо, удаляем элемент из списка
                    var index = this.gameHostList.items.indexOf(gameHost);
                    this.gameHostList.items.splice(index, 1);
                })
                .catch ( (response:any) => {
                    this.ToastNotificationService("Ошибка работы с сервером");
                    return;
                });
        }

        private updateGameHostList(gameHost: GameHostViewModel) {
            var sourceHost = this.$filter('filter')(this.gameHostList.items, { id: gameHost.id })[0];
            var index = this.gameHostList.items.indexOf(sourceHost);
            if (index !== -1) {
                // Обновляем хост
                this.gameHostList.items[index] = gameHost;
            }
            else {
                // Добаляем хост
                this.gameHostList.items.splice(0,0, gameHost);
                this.gameHostList.items.splice(this.paginationSettings.pageSize, 1);
            }
        }
    }

    angular.module('gameMachines.host')
        .controller('HostController', HostController);
}