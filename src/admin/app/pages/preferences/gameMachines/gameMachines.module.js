(function () {

    angular.module("preferences.gameMachines", [
        "gameMachines.game",
        "gameMachines.host",
        "gameMachines.singleGame"
    ]);

})();