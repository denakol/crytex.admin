(function () {

    angular.module('preferences.configurationVmWareCenter')
        .controller('ConfigurationVmWareController', configurationVmWareController);

    configurationVmWareController.$inject = ['$scope', 'WebServices', '$mdDialog', 'ToastNotificationService', '$filter'];

    function configurationVmWareController($scope, WebServices, $mdDialog, ToastNotificationService, $filter) {

        $scope.vmWareCenters = [];
        $scope.isShowAddBtn = true;

        getVmWareCenters();

        ///////////////////////////////////////////////////////////////
        function getVmWareCenters() {
            WebServices.AdminVmWareVCenterService.getAll({},
                function (data) {
                    $scope.vmWareCenters = data;
                },
                function (error) {
                    if (!isNaN(error)) {
                        ToastNotificationService('Не удалось выгрузить данные');
                    }
                }
            );
        };

        $scope.createOrUpdateVmWareCenter = function (vmWareCenter) {

            if (vmWareCenter.serverAddress != vmWareCenter.userName !=
                vmWareCenter.password != vmWareCenter.name != '') {
                if (vmWareCenter.id) {
                    // Обновляем
                    WebServices.AdminVmWareVCenterService.update(vmWareCenter.id, vmWareCenter,
                        function (data) {
                            var myVmWareCenter = $filter('filter')($scope.vmWareCenters, {id:vmWareCenter.id})[0];
                            var index = $scope.vmWareCenters.indexOf(myVmWareCenter);
                            $scope.vmWareCenters.splice(index, 1, vmWareCenter);
                            ToastNotificationService('Добавлено');
                        },
                        function (error) {
                            if (!isNaN(error)) {
                                ToastNotificationService('Не удалось выгрузить данные');
                            }
                        }
                    );
                } else {
                    // Добавляем
                    WebServices.AdminVmWareVCenterService.save(vmWareCenter,
                        function (data) {
                            vmWareCenter.id = data.id;
                            $scope.vmWareCenters.splice(0, 0, vmWareCenter);
                            ToastNotificationService('Добавлено');
                        },
                        function (error) {
                            if (!isNaN(error)) {
                                ToastNotificationService('Не удалось выгрузить данные');
                            }
                        }
                    );
                }
            } else {
                ToastNotificationService('Необходимо заполнить все поля');
            }
        };

        $scope.showAdvanced = function (index) {
            $mdDialog.show({
                controller: DialogVmWareCenterController,
                templateUrl: 'editVmWareCenter.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: null,
                clickOutsideToClose: true,
                locals: {
                    vmWareCenter: (typeof index !== "undefined") ? angular.copy($scope.vmWareCenters[index]) : null
                }
            })
            .then(function (answer) {
                $scope.createOrUpdateVmWareCenter(answer);
            }, function () {

            });
        };

        function DialogVmWareCenterController($scope, $mdDialog, vmWareCenter) {
            $scope.vmWareCenter = vmWareCenter;

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.answer = function () {
                $mdDialog.hide($scope.vmWareCenter);
            };
        }

    }

})();
