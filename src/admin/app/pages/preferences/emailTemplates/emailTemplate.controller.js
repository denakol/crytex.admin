﻿(function () {

    angular.module('preferences.emailTemplates')
        .controller('emailTemplateListController', emailTemplateListController);

    emailTemplateListController.$inject = ['$scope', '$http', 'WebServices', '$mdToast', '$mdDialog'];

    function emailTemplateListController($scope, $http, WebServices, $mdToast, $mdDialog) {
        $scope.templates = null;
        $scope.emailTemplateTypeSource = [
            {type: 'Registration', value: 0},
            {type: 'ChangePassword', value: 1},
            {type: 'ChangeProfile', value: 2},
            {type: 'CreateVm', value: 3},
            {type: 'UpdateVm', value: 4}
        ];


        $scope.loadPage = function () {
            WebServices.AdminEmailTemplateService.getAll({
            },
                function (data) {
                    $scope.templates = data;
                },
                function (error) {
                    if (!isNaN(error)) {
                        //showMessage('Не удалось выгрузить данные');
                    }
                }
            );
        }

        //init
       $scope.loadPage();



        function showMessage(message) {
            $mdToast.show(
                $mdToast.simple()
                    .content(message)
                    .position($scope.getToastPosition())
                    .hideDelay(3000));
        }

        var last = { bottom: false, top: true, left: false, right: true };
        $scope.toastPosition = angular.extend({}, last);

        $scope.getToastPosition = function () {
            return Object.keys($scope.toastPosition)
                .filter(function (pos) { return $scope.toastPosition[pos]; })
                .join(' ');
        };

        $scope.deleteTemplate = function (index) {

            WebServices.AdminEmailTemplateService.delete({ id: $scope.templates[index].id },
                function (ok) {
                    // Удаляем из списка
                    $scope.templates.splice(index, 1);
                    showMessage('Удалено');
                },
                function (error) {
                    showMessage('Не удалось выгрузить данные');
                }
            );
        };

        $scope.createOrUpdateTemplate = function (template) {
            template.parameterNamesList = (template.body + template.subject).match(/[^{}]+(?=\})/g);
            if(!template.parameterNamesList) template.parameterNamesList = [];
            if (template.id) {
                // Обновляем
                WebServices.AdminEmailTemplateService.update(template.id, template,
                    function (data) {
                        var index = $scope.templates.map(function (e) { return e.id; }).indexOf(template.id);
                        $scope.templates[index] = template;
                        showMessage('Добавлено');
                    },
                    function (error) {
                        if (!isNaN(error)) {
                            showMessage('Не удалось выгрузить данные');
                        }
                    }
                );
            } else {
                // Добавляем
                WebServices.AdminEmailTemplateService.save(template,
                    function (data) {
                        template.id = data.id;
                        $scope.templates.splice(0, 0, template);
                        showMessage('Добавлено');
                    },
                    function (error) {
                        if (!isNaN(error)) {
                            showMessage('Не удалось выгрузить данные');
                        }
                    }
                );
            }
        };

        $scope.showConfirm = function (index) {
            var confirm = $mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            $mdDialog.show(confirm).then(function () {
                $scope.deleteTemplate(index);
            }, function () {
                // Ну нет так нет
            });
        };

        $scope.showEdit = function (index) {
            $mdDialog.show({
                controller: DialogTemplateEditCtrl,
                templateUrl: "editTemplateForm.tmpl.html",
                parent: angular.element(document.body),
                targetEvent: index,
                clickOutsideToClose: true,
                locals: {
                    templateEdit: (typeof index !== "undefined") ? angular.copy($scope.templates[index]) : null,
                    emailTemplateTypeSource: $scope.emailTemplateTypeSource
                }
            })
                .then(function (answer) {
                    $scope.createOrUpdateTemplate(answer);
                }, function () {

                });
        };


    }

})();




function DialogTemplateEditCtrl($scope, $mdDialog, templateEdit, emailTemplateTypeSource) {
    $scope.templateEdit = templateEdit;
    if(!$scope.templateEdit){
        $scope.templateEdit = {parameterNamesList:[]}
    }

    $scope.emailTemplateTypeSource = emailTemplateTypeSource;
    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.answer = function () {
        $mdDialog.hide($scope.templateEdit);
    };
}