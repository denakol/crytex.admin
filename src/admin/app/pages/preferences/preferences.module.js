﻿(function () {

    angular.module("crytex.preferences", [
        "preferences.machineTemplates",
        "preferences.gameMachines",
        "preferences.configurationHyperV",
        "preferences.emailTemplates",
        "preferences.configurationVmWareCenter",
        "preferences.news",
        "preferences.hyperVHost",
        "preferences.locations"
    ]);

})();