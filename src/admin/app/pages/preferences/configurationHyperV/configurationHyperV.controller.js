(function () {

    angular.module('preferences.configurationHyperV')
        .controller('ConfigurationHyperVController', configurationHyperVController);

    configurationHyperVController.$inject = ['$scope', 'WebServices', '$mdToast', '$mdDialog', '$filter'];

    function configurationHyperVController($scope, WebServices, $mdToast, $mdDialog, $filter) {

        $scope.defaultSCVMs = [];
        $scope.isShowAddBtn = true;
        $scope.editableSCVM = {};

        getSCVM();

        function showMessage(message) {
            $mdToast.show(
                $mdToast.simple()
                .content(message)
                .position($scope.getToastPosition())
                .hideDelay(3000));
        }

        $scope.tabClick = function (val) {
            $scope.isShowAddBtn = val != 'System Center' ? false : true;
        }

        var last = { bottom: false, top: true, left: false, right: true };

        $scope.toastPosition = angular.extend({}, last);

        $scope.getToastPosition = function () {
            return Object.keys($scope.toastPosition)
                .filter(function (pos) { return $scope.toastPosition[pos]; })
                .join(' ');
        };

        function getSCVM() {
            WebServices.AdminSystemCenterVirtualManagerService.getAll({},
                function (data) {
                    $scope.defaultSCVMs = data
                    console.log(data);
                },
                function (error) {
                    if (!isNaN(error)) {
                        showMessage('Не удалось выгрузить данные');
                    }
                }
            );
        };

        $scope.getSCVM = getSCVM;


        $scope.deleteSCVM = function (index) {
            WebServices.AdminSystemCenterVirtualManagerService.delete({ id: $scope.defaultSCVMs[index].id },
                function (ok) {
                    // Удаляем из списка
                    $scope.defaultSCVMs.splice(index, 1);
                    showMessage('Удалено');
                },
                function (error) {
                    showMessage('Не удалось выгрузить данные');
                }
            );
        }

        $scope.createOrUpdateSCVM = function (scvm) {
            if (scvm.host != scvm.userName !=
                scvm.password != scvm.name != '') {
                if (scvm.id) {
                    // Обновляем
                    $scope.editableSCVM = scvm;
                    WebServices.AdminSystemCenterVirtualManagerService.update(scvm.id, scvm,
                        function (data) {
                            var index = $scope.defaultSCVMs.map(function (e) { return e.id; }).indexOf(scvm.id);
                            $scope.defaultSCVMs[index] = scvm;
                            showMessage('Добавлено');
                        },
                        function (error) {
                            if (!isNaN(error)) {
                                showMessage('Не удалось выгрузить данные');
                            }
                        }
                    );
                } else {
                    // Добавляем
                    WebServices.AdminSystemCenterVirtualManagerService.save(scvm,
                        function (data) {
                            scvm.id = data.id;
                            $scope.defaultSCVMs.splice(0, 0, scvm);
                            showMessage('Добавлено');
                        },
                        function (error) {
                            if (!isNaN(error)) {
                                showMessage('Не удалось выгрузить данные');
                            }
                        }
                    );
                }
            } else {
                showMessage('Необходимо заполнить все поля');
            }
        };

        $scope.showConfirm = function (index) {
            var confirm = $mdDialog.confirm()
                  .title('Вы уверены, что хотите удалить данный элемент?')
                  .ok('Да')
                  .cancel('Отменить');
            $mdDialog.show(confirm).then(function () {
                $scope.deleteSCVM(index);
            }, function () {
                // Ну нет так нет
            });
        };

        $scope.showAdvanced = function (index) {
            $mdDialog.show({
                controller: DialogSCVMController,
                templateUrl: 'editSCVMform.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: index,
                clickOutsideToClose: true,
                locals: {
                    scvm: (typeof index !== "undefined") ? angular.copy($scope.defaultSCVMs[index]) : null
                }
            })
            .then(function (answer) {
                $scope.createOrUpdateSCVM(answer);
            }, function () {

            });
        };

        function DialogSCVMController($scope, $mdDialog, scvm) {
            $scope.scvm = scvm;

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.answer = function () {
                $mdDialog.hide($scope.scvm);
            };
        }

    }

})();
