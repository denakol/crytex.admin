﻿(function () {

    angular.module('machineTemplates.operatingSystem')
        .controller('OperatingSystemController', operatingSystemController);

    operatingSystemController.$inject = ['$scope', '$mdToast', '$mdDialog', '$filter', 'WebServices','BASE_INFO', 'OSSource'];

    function operatingSystemController($scope, $mdToast, $mdDialog, $filter, WebServices, BASE_INFO, OSSource) {

        $scope.defaultOS = [];
        $scope.BASE_INFO = BASE_INFO;
        $scope.osSource = OSSource;

        function showMessage(message) {
            $mdToast.show(
                $mdToast.simple()
                .content(message)
                .position($scope.getToastPosition())
                .hideDelay(3000));
        };

        var last = { bottom: false, top: true, left: false, right: true };
        $scope.toastPosition = angular.extend({}, last);

        $scope.getToastPosition = function () {
            return Object.keys($scope.toastPosition)
                .filter(function (pos) { return $scope.toastPosition[pos]; })
                .join(' ');
        };

        $scope.getOS = function () {
            WebServices.AdminOperatingSystem.query({},
                function success(os) {
                    $scope.defaultOS = os;
                },
                function err(error) {
                    showMessage('Не удалось выгрузить данные');
                });
        };
        $scope.getOS();

        $scope.deleteOS = function (index) {
            var os = $scope.defaultOS[index];
            WebServices.AdminOperatingSystem.delete({ id: os.id },
                function success(ok) {
                    // Удаляем из списка
                    $scope.defaultOS.splice(index, 1);
                    showMessage('Удалено');
                },
                function err(error) {
                    showMessage('Не удалось удалить');
                });
        };

        $scope.showConfirm = function (index) {
            var confirm = $mdDialog.confirm()
                  .title('Вы уверены, что хотите удалить данный элемент?')
                  .ok('Да')
                  .cancel('Отменить');
            $mdDialog.show(confirm).then(function () {
                $scope.deleteOS(index);
            }, function () {
                // Ну нет так нет
            });
        };

        $scope.showAdvanced = function (index) {
            $mdDialog.show({
                controller: DialogOSController,
                templateUrl: 'NewOrEditOS.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: null,
                clickOutsideToClose: true,
                locals: {
                    os: !isNaN(index) ? angular.copy($scope.defaultOS[index]) : {}
                }
            })
            .then(function (os) {
                if(os.updated) {
                    var myOS = $filter('filter')($scope.defaultOS, { id: os.id })[0];
                    var index = $scope.defaultOS.indexOf(myOS);
                    $scope.defaultOS.splice(index, 1, os);
                    showMessage('Обновлено');
                } else {
                    $scope.defaultOS.splice(0, 0, os);
                    showMessage('Добавлено');
                }
            }, function () {
            });
        };

    }

    function DialogOSController($scope, $mdDialog, os, BASE_INFO, Upload, OSSource, WebServices, $mdToast) {
        $scope.OS = os;
        $scope.osSource = OSSource;

        var last = { bottom: false, top: true, left: false, right: true };
        $scope.toastPosition = angular.extend({}, last);


        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.answer = function (OSForm) {

            if(!OSForm.$invalid){
                $scope.createOrUpdateOS($scope.OS); 
            }
        };

        $scope.createOrUpdateOS = function (os) {

            if ( typeof os.imageFileId != 'undefined') {
                if (os.id) {
                    // Обновляем
                    $scope.editableOS = os;

                    WebServices.AdminOperatingSystem.update({ id: os.id },
                        {
                            name: os.name,
                            description: os.description,
                            serverTemplateName: os.serverTemplateName,
                            imageFileId: os.imageFileId,
                            imagePath: os.imagePath,
                            defaultAdminPassword: os.defaultAdminPassword,
                            family: os.family,
                            minCoreCount: os.minCoreCount,
                            minRamCount: os.minRamCount,
                            minHardDriveSize: os.minHardDriveSize,
                        },
                        function success(value) {
                            $scope.editableOS.updated = true;
                            $mdDialog.hide($scope.editableOS);
                        },
                        function err(error) {
                            showMessage('Не удалось обновить');
                        });
                } else {
                    // Добавляем
                    WebServices.AdminOperatingSystem.save({
                            name: os.name,
                            description: os.description,
                            serverTemplateName: os.serverTemplateName,
                            imageFileId: os.imageFileId,
                            imagePath: os.imagePath,
                            defaultAdminPassword: os.defaultAdminPassword,
                            family: os.family,
                            minCoreCount: os.minCoreCount,
                            minRamCount: os.minRamCount,
                            minHardDriveSize: os.minHardDriveSize
                        }, function success(data) {
                            data.updated = false;
                            $mdDialog.hide(data);
                        },
                        function err(error) {
                            showMessage('Не удалось добавить');
                        });
                }
            } else {
                showMessage('Необходимо выбрать изображение');
            }
        };

        function showMessage(message) {
            $mdToast.show(
                $mdToast.simple()
                    .content(message)
                    .position($scope.getToastPosition())
                    .hideDelay(3000));
        };

        $scope.getToastPosition = function () {
            return Object.keys($scope.toastPosition)
                .filter(function (pos) { return $scope.toastPosition[pos]; })
                .join(' ');
        };

        $scope.uploadFiles = function (file, errFiles) {
            $scope.f = file;
            var directoryImagePath = '~/App_Data/Files/Images';
            $scope.errFile = errFiles && errFiles[0];
            if (file) {
                file.upload = Upload.upload({
                    url: BASE_INFO.URL + BASE_INFO.PORT + BASE_INFO.API_URL + '/Admin/AdminFile/Post',
                    file: file,
                    data: {
                        fileName: file.name,
                        directoryPath: directoryImagePath,
                        fileType: 0
                    }
                });

                file.upload.then(function (response) {
                    $scope.OS.imageFileId = response.data.id;
                    $scope.OS.imagePath = response.data.path;
                }, function (response) {
                    alert('Ошибка при загрузке изображения');
                });
            }
        };
    }

})();
