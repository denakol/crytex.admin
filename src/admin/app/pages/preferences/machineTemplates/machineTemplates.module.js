﻿(function () {

    angular.module("preferences.machineTemplates", [
        "machineTemplates.operatingSystem",
        "machineTemplates.serverTemplate"
    ]);

})();