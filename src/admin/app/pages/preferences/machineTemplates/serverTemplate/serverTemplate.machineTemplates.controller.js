(function () {

    angular.module('machineTemplates.serverTemplate')
        .controller('ServerTemplateController', serverTemplateController);

    serverTemplateController.$inject = ['$scope', 'ToastNotificationService', '$mdDialog', '$filter', 'WebServices', 'Upload', 'TypeServerTemplate'];

    function serverTemplateController($scope, ToastNotificationService, $mdDialog, $filter, WebServices, Upload, TypeServerTemplate) {

        $scope.defaultTemplates = [];
        $scope.typeServerTemplate = TypeServerTemplate;

        $scope.getTemplates = function () {
            WebServices.AdminTemplate.query({},
                function success(templates) {
                    $scope.defaultTemplates = templates;
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                });
        };
        $scope.getTemplates();

        $scope.deleteServerTemplate = function (index) {
            var template = $scope.defaultTemplates[index];

            WebServices.AdminTemplate.delete({id: template.id},
                function success(ok) {
                    $scope.defaultTemplates.splice(index, 1);
                    ToastNotificationService('Удалено');
                },
                function err(error) {
                    ToastNotificationService('Не удалось удалить');
                });
        };

        $scope.createOrUpdateServerTemplate = function (template) {

            if (template.id) {
                // Обновляем
                $scope.editableTemplate = template;

                WebServices.AdminTemplate.update({id: template.id},
                    {
                        name: template.name,
                        description: template.description,
                        coreCount: template.coreCount,
                        ramCount: template.ramCount,
                        hardDriveSize: template.hardDriveSize,
                        imageFileId: template.imageFileId,
                        operatingSystemId: template.operatingSystemId,
                        typeServerTemplate: template.typeServerTemplate
                    },
                    function success(value) {
                        var myTemplate = $filter('filter')($scope.defaultTemplates, {id: $scope.editableTemplate.id})[0];
                        var index = $scope.defaultTemplates.indexOf(myTemplate);
                        $scope.defaultTemplates.splice(index, 1, $scope.editableTemplate);
                        ToastNotificationService('Обновлено');
                    },
                    function err(error) {
                        ToastNotificationService('Не удалось обновить');
                    });
            } else {
                // Добавляем
                WebServices.AdminTemplate.save({
                        name: template.name,
                        description: template.description,
                        coreCount: template.coreCount,
                        ramCount: template.ramCount,
                        hardDriveSize: template.hardDriveSize,
                        imageFileId: template.imageFileId,
                        operatingSystemId: template.operatingSystemId,
                        typeServerTemplate: template.typeServerTemplate
                    },
                    function success(data) {
                        template.id = data.id;
                        $scope.defaultTemplates.splice(0, 0, template);
                        ToastNotificationService('Добавлено');
                    },
                    function err(error) {
                        ToastNotificationService('Не удалось добавить');
                    });
            }

        };


        $scope.showConfirm = function (index) {
            var confirm = $mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            $mdDialog.show(confirm).then(function () {
                $scope.deleteServerTemplate(index);
            }, function () {
                // Ну нет так нет
            });
        };

        $scope.showAdvanced = function (index) {
            $mdDialog.show({
                controller: DialogServerTemplateController,
                templateUrl: 'NewOrEditServerTemplate.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: index,
                clickOutsideToClose: true,
                locals: {
                    template: !isNaN(index) ? angular.copy($scope.defaultTemplates[index]) : {}
                }
            })
                .then(function (answer) {
                    $scope.createOrUpdateServerTemplate(answer);
                }, function () {

                });
        };

        function DialogServerTemplateController($scope, $mdDialog, template, BASE_INFO, Upload, WebServices, TypeServerTemplate) {

            $scope.operatingSystems = [];
            $scope.template = template;
            $scope.typeServerTemplate = TypeServerTemplate;

            $scope.getOS = function () {
                WebServices.AdminOperatingSystem.query({},
                    function success(os) {
                        $scope.operatingSystems = os;
                    },
                    function err(error) {
                        showMessage('Не удалось выгрузить данные');
                    });
            };
            $scope.getOS();

            $scope.uploadFiles = function (file, errFiles) {
                $scope.f = file;
                var directoryImagePath = '~/App_Data/Files/Images';
                $scope.errFile = errFiles && errFiles[0];
                if (file) {
                    file.upload = Upload.upload({
                        url: BASE_INFO.URL + BASE_INFO.PORT + BASE_INFO.API_URL + '/Admin/AdminFile/Post',
                        file: file,
                        data: {
                            fileName: file.name,
                            directoryPath: directoryImagePath,
                            fileType: 0
                        }
                    });

                    file.upload.then(function (response) {
                        $scope.template.imageFileId = response.data.id;
                    }, function (response) {
                        ToastNotificationService('Ошибка при загрузке изображения');
                    });
                }
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.answer = function () {
                if ($scope.TemplateForm.$valid) {
                    $mdDialog.hide($scope.template);
                }
            };
        }

    }

})();
