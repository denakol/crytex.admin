var admin;
(function (admin) {
    var AdminNewsController = (function () {
        function AdminNewsController(WebServices, $mdToast, $mdDialog, localStorageService, $filter) {
            this.WebServices = WebServices;
            this.$mdToast = $mdToast;
            this.$mdDialog = $mdDialog;
            this.localStorageService = localStorageService;
            this.$filter = $filter;
            this.paginationSettings = {
                pageNumber: 1,
                pageSize: 5
            };
            this.last = {
                bottom: false,
                top: true,
                left: false,
                right: true
            };
            this.toastPosition = angular.extend({}, this.last);
            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;
            this.NewsService = WebServices.AdminNewsService;
            this.userId = localStorageService.get('authorizationData').userId;
            this.userName = localStorageService.get('authorizationData').userName;
            this.loadPage();
        }
        AdminNewsController.prototype.loadPage = function () {
            var _this = this;
            this.news = this.NewsService.getAllPage({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize
            });
            this.news.$promise
                .then(function (data) {
                if (data.items.length === 0) {
                    _this.news.items = [];
                }
                else {
                    _this.news.items = data.items;
                    _this.news.totalRows = data.totalRows;
                    _this.news.totalPages = data.totalPages;
                }
            })
                .catch(function (err) {
                if (!isNaN(err)) {
                    _this.showMessage('Не удалось выгрузить данные');
                }
            });
        };
        AdminNewsController.prototype.deleteNews = function (index) {
            var _this = this;
            this.NewsService.delete({
                id: this.news.items[index].id
            }, (function (ok) {
                _this.news.items.splice(index, 1);
                _this.showMessage('Удалено');
            }), (function (err) {
                _this.showMessage('Не удалось выгрузить данные');
            }));
        };
        AdminNewsController.prototype.createOrUpdateNews = function (news) {
            var _this = this;
            if (news['id']) {
                this.NewsService.update({ id: news['id'] }, news, (function (data) {
                    var myNews = _this.$filter('filter')(_this.news.items, { id: news.id })[0];
                    var index = _this.news.items.indexOf(myNews);
                    _this.news.items.splice(index, 1, news);
                    _this.showMessage('Добавлено');
                }), (function (err) {
                    if (!isNaN(err)) {
                        _this.showMessage('Не удалось выгрузить данные');
                    }
                }));
            }
            else {
                news['userId'] = this.userId;
                news['userName'] = this.userName;
                news['createTime'] = new Date().toISOString();
                this.NewsService.save(news, (function (data) {
                    news['id'] = data.id;
                    _this.news.items.splice(0, 0, news);
                    _this.showMessage('Добавлено');
                }), (function (err) {
                    if (!isNaN(err)) {
                        _this.showMessage('Не удалось выгрузить данные');
                    }
                }));
            }
        };
        AdminNewsController.prototype.showConfirm = function (index) {
            var _this = this;
            var confirm = this.$mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            this.$mdDialog.show(confirm)
                .then(function () {
                _this.deleteNews(index);
            })
                .catch(function () { });
        };
        AdminNewsController.prototype.showEdit = function (index) {
            var _this = this;
            this.$mdDialog.show({
                controller: "DialogNewsEditCtrl as dialogNew",
                templateUrl: "app/pages/preferences/news/editNewsForm.tmpl.html",
                parent: angular.element(document.body),
                targetEvent: null,
                clickOutsideToClose: true,
                locals: {
                    newsEdit: (typeof index !== "undefined") ? angular.copy(this.news.items[index]) : null
                }
            })
                .then(function (answer) {
                _this.createOrUpdateNews(answer);
            }, function (err) {
            });
        };
        AdminNewsController.prototype.showMessage = function (message) {
            this.$mdToast.show(this.$mdToast.simple()
                .content(message)
                .position(this.getToastPosition())
                .hideDelay(3000));
        };
        AdminNewsController.prototype.getToastPosition = function () {
            var _this = this;
            return Object.keys(this.toastPosition)
                .filter(function (pos) { return _this.toastPosition[pos]; })
                .join(' ');
        };
        AdminNewsController.prototype.pageChangeHandler = function (newPageNumber) {
            this.paginationSettings.pageNumber = newPageNumber;
            this.loadPage();
        };
        AdminNewsController.$inject = ["WebServices", "$mdToast", "$mdDialog", "localStorageService", "$filter"];
        return AdminNewsController;
    }());
    admin.AdminNewsController = AdminNewsController;
    var DialogNewsEditCtrl = (function () {
        function DialogNewsEditCtrl($mdDialog, newsEdit) {
            this.$mdDialog = $mdDialog;
            this.newsEdit = newsEdit;
        }
        DialogNewsEditCtrl.prototype.answer = function () {
            this.$mdDialog.hide(this.newsEdit);
        };
        DialogNewsEditCtrl.prototype.cancel = function () {
            this.$mdDialog.cancel();
        };
        DialogNewsEditCtrl.$inject = ["$mdDialog", "newsEdit"];
        return DialogNewsEditCtrl;
    }());
    angular.module('preferences.news')
        .controller('AdminNewsController', AdminNewsController)
        .controller('DialogNewsEditCtrl', DialogNewsEditCtrl);
})(admin || (admin = {}));
