module admin {
    import IResourceClass = angular.resource.IResourceClass;
    import PageModel = core.PageModel;
    import PaginationSetting = core.PaginationSetting;
    import NewsViewModel = core.WebApi.Models.NewsViewModel;

    interface Last {
        bottom:boolean;
        top:boolean;
        left:boolean;
        right:boolean;
    }

    export class AdminNewsController {
        static $inject = ["WebServices", "$mdToast", "$mdDialog", "localStorageService", "$filter"];

        public news:PageModel<NewsViewModel>;
        public paginationSettings: PaginationSetting = {
            pageNumber: 1,
            pageSize: 5
        };

        public last: Last = {
            bottom:false,
            top:true,
            left:false,
            right:true
        };

        private userId:string;
        private userName:string;

        public toastPosition:any = angular.extend({}, this.last);

        private NewsService: IResourceClass<PageModel<NewsViewModel>>;

        constructor(private WebServices: any,
                    private $mdToast:any,
                    private $mdDialog:any,
                    private localStorageService:any,
                    private $filter:any){
            this.paginationSettings.pageNumber = 1;
            this.paginationSettings.pageSize = 5;
            this.NewsService = WebServices.AdminNewsService;

            this.userId = localStorageService.get('authorizationData').userId;
            this.userName = localStorageService.get('authorizationData').userName;

            this.loadPage();
        }

        public loadPage(){
            this.news = this.NewsService.getAllPage({
                pageNumber: this.paginationSettings.pageNumber,
                pageSize: this.paginationSettings.pageSize
            });

            this.news.$promise
                .then((data:any) => {
                    if(data.items.length ===0 ){
                        this.news.items = [];
                    } else {
                        this.news.items = data.items;

                        this.news.totalRows = data.totalRows;
                        this.news.totalPages = data.totalPages;
                    }
                })
                .catch((err:any) => {
                    if (!isNaN(err)) {
                        this.showMessage('Не удалось выгрузить данные');
                    }
                })
        }

        public deleteNews(index:number){
            this.NewsService.delete({
                id: this.news.items[index].id
            }, (
                    (ok:any) => {
                        this.news.items.splice(index, 1);
                        this.showMessage('Удалено');
                    }
                ),
                (
                    (err:any) => {
                        this.showMessage('Не удалось выгрузить данные');
                    }
                )
            );
        }

        public createOrUpdateNews(news:any){
            if(news['id']){
                this.NewsService.update(
                    {id : news['id'] }, news,
                    (
                        (data:any) => {
                            var myNews:any = this.$filter('filter')(this.news.items, {id:news.id})[0];
                            var index:number = this.news.items.indexOf(myNews);
                            this.news.items.splice(index, 1, news);
                            this.showMessage('Добавлено');
                        }
                    ),
                    (
                        (err:any) => {
                            if (!isNaN(err)) {
                                this.showMessage('Не удалось выгрузить данные');
                            }
                        }
                    )
                );
            } else {
                news['userId'] = this.userId;
                news['userName'] = this.userName;
                news['createTime'] = new Date().toISOString();
                this.NewsService.save(news,
                    (
                        (data:any) => {
                            news['id'] = data.id;
                            this.news.items.splice(0, 0, news);
                            this.showMessage('Добавлено');
                        }
                    ),
                    (
                        (err:any) => {
                            if (!isNaN(err)) {
                                this.showMessage('Не удалось выгрузить данные');
                            }
                        }
                    )
                );
            }
        }

        public showConfirm(index:number){
            var confirm = this.$mdDialog.confirm()
                .title('Вы уверены, что хотите удалить данный элемент?')
                .ok('Да')
                .cancel('Отменить');
            this.$mdDialog.show(confirm)
                .then(() => {
                    this.deleteNews(index);
                })
                .catch(() => {});
        }

        public showEdit(index:number){
            this.$mdDialog.show({
                controller: "DialogNewsEditCtrl as dialogNew",
                templateUrl: "app/pages/preferences/news/editNewsForm.tmpl.html",
                parent: angular.element(document.body),
                targetEvent: null,
                clickOutsideToClose: true,
                locals: {
                    newsEdit: (typeof index !== "undefined") ? angular.copy(this.news.items[index]) : null
                }
            })
                .then((answer:any) => {
                    this.createOrUpdateNews(answer);
                }, (err:any)=> {

                });

        }

        public showMessage(message:string){
            this.$mdToast.show(
                this.$mdToast.simple()
                    .content(message)
                    .position(this.getToastPosition())
                    .hideDelay(3000)
            );
        }

        public getToastPosition(){
            return Object.keys(this.toastPosition)
                .filter((pos) => { return this.toastPosition[pos]; })
                .join(' ');
        }
        
        public pageChangeHandler(newPageNumber:number){
            this.paginationSettings.pageNumber = newPageNumber;
            this.loadPage();
        }
    }

    class DialogNewsEditCtrl {
        static $inject = ["$mdDialog", "newsEdit"];

        constructor(private $mdDialog:any, private newsEdit:any ){
        }

        public answer(){
            this.$mdDialog.hide(this.newsEdit);
        }

        public cancel(){
            this.$mdDialog.cancel();
        }
    }

    angular.module('preferences.news')
        .controller('AdminNewsController', AdminNewsController)
        .controller('DialogNewsEditCtrl', DialogNewsEditCtrl);
}