(function () {

    angular.module('preferences.configurationHyperV')
        .controller('testController', testController);

    testController.$inject = ['$scope', 'WebServices', '$mdToast', '$mdDialog', '$filter'];

    function testController($scope, WebServices, $mdToast, $mdDialog, $filter) {
        $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
        $scope.series = ['Series A', 'Series B'];

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };
    }
})();
