﻿(function () {

    angular.module("crytex.statistic", [
        "statistic.general",
        "statistic.statisticVm"
    ]);

})();