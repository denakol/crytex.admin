﻿(function () {

    angular.module('statistic.statisticVm')
        .controller('StatisticVmController', statisticVmController);

    statisticVmController.$inject = ['$scope', 'WebServices', 'ToastNotificationService'];

    function statisticVmController($scope, WebServices, ToastNotificationService) {
        $scope.statisticSubscriptionVm = null;
        $scope.statisticSubscriptionVmToday = null;

        getStatisticVm(false);
        getStatisticVm(true);


        function getStatisticVm(isToday){
            WebServices.AdminStatisticService.getSubscriptionVm({
                    today: isToday
                },
                function success(statistic) {
                    if(isToday) $scope.statisticSubscriptionVmToday = statistic;
                    else $scope.statisticSubscriptionVm = statistic;
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }
    }

})();
