﻿(function () {

    angular.module('statistic.general')
        .controller('GeneralController', generalController);

    generalController.$inject = ['$scope', 'WebServices', 'ToastNotificationService'];

    function generalController($scope, WebServices, ToastNotificationService) {
        $scope.statistic = null;
        getStatistic();


        function getStatistic(){
            WebServices.AdminStatisticService.getSummary({},
                function success(statistic) {
                    console.log(statistic);
                    $scope.statistic = statistic;
                },
                function err(error) {
                    ToastNotificationService('Не удалось выгрузить данные');
                }
            );
        }
    }

})();
