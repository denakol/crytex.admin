﻿(function () {

    angular.module('crytex.logs')
        .controller('LogsController', logsController);

    logsController.$inject = ['$scope', 'WebServices', 'ToastNotificationService'];

    function logsController($scope, WebServices, ToastNotificationService) {

        $scope.logs = {
            items: [],
            pageNumber: 1,
            pageSize: 10,
            dateFrom: null,
            dateTo: null,
            sourceLog: null,
            totalRows: 0,
            totalPages: 0
        }
        $scope.typeSource = ['API', 'Web', 'ExecutorTask', 'Background'];

        loadPage ();
        ///////////////////////////////////////////////////////////////////////////

        function loadPage (){
            WebServices.AdminLogService.getAllPage({
                pageNumber: $scope.logs.pageNumber,
                pageSize: $scope.logs.pageSize,
                sourceLog: $scope.logs.sourceLog,
                dateFrom: $scope.logs.dateFrom,
                dateTo: $scope.logs.dateTo
            },
                function (data) {
                    $scope.logs.items = data.items;
                    $scope.logs.totalRows = data.totalRows;
                    $scope.logs.totalPages = data.totalPages;
                },
                function (error) {
                    if (!isNaN(error)) {
                        ToastNotificationService('Не удалось выгрузить данные');
                    }
                }
            );
        }

        $scope.pageChangeHandler = function(page){
            loadPage ();
        }

        $scope.loadPage = loadPage;
    }

})();