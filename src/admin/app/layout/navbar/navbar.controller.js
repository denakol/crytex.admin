﻿(function () {

    angular.module("crytex.navbar")
        .controller("NavbarController", navbarController);

    navbarController.$inject = ['$scope', '$timeout', '$mdSidenav', '$mdComponentRegistry'];

    function navbarController($scope, $timeout, $mdSidenav, $mdComponentRegistry) {

        $scope.toggleLeft = function () {
            $mdSidenav('left').toggle();
        };

    };

})();