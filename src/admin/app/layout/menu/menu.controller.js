﻿(function () {

    angular.module("crytex.menu")
        .controller("MenuController", menuController);

    menuController.$inject = ['$scope', '$timeout', '$mdSidenav', '$filter'];

    function menuController($scope, $timeout, $mdSidenav, $filter) {

        $scope.close = function () {
            $mdSidenav('left').close()
                .then(function () {

                });
        };
        $scope.menuItems = [
            {
                name: 'Главное меню',icon:'home', sref: 'home', isShowSubitems: false, subitems: []
            },
            {
                name: 'Подписки', icon:'check_circle', isShowSubitems: false, subitems: [
                { name: 'Виртуальные машины', sref: 'subscriptionVirtualMachines' },
                { name: 'Игровые сервера', sref: 'subscriptionGameServers' }
            ]
            },
            {
                name: 'Пользователи',icon:'face', isShowSubitems: false, subitems: [
                 { name: 'Просмотр пользователей', sref: 'users' },
                 { name: 'Обращения пользователей', sref: 'userHelpDeskRequests' },
                 { name: 'Перезвоните мне', sref: 'userCalls' }
                ]
            },
            {
                name: 'Денежные средства', icon:'credit_card', isShowSubitems: false, subitems: [
                     { name: 'Тарифы виртуальных машин', sref: 'tariffOfVirtualMachine' },
                     { name: 'Пополнения', sref: 'moneyPayment' },
                     { name: 'Транзакции пользователей', sref: 'paymentBillingTransaction'},
                     { name: 'Скидки', sref: 'moneyDiscounts' },
                     { name: 'Виртуальные машины', sref: 'virtualMachinePayment' },
                     { name: 'Денежные системы', sref: 'moneySystem' }
                ]
            }, {

                name: 'Email', icon:'feedback',isShowSubitems: false, subitems: [
                 { name: 'Просмотр сообщений', sref: 'emailMessages' }
                ]
            },
            {
                name: 'Логирование', icon:'new_releases',isShowSubitems: false, subitems: [
                 { name: 'Все логи', sref: 'logs' },
                ]
            },
            {
                name: 'Инфраструктура',icon:'device_hub',isShowSubitems: false, subitems: [
                { name: 'Просмотр машин', sref: 'virtualMachines' },
                { name: 'Задачи', sref: 'task' }
                ]
            },

            {
                name: 'Настройка',icon:'settings_application', isShowSubitems: false, subitems: [

                 { name: 'Игровые машины', sref: 'preferencesGameMachines' },
                 { name: 'Конфигурация Hyper V', sref: 'preferencesConfigurationHyperV' },
                 { name: 'Конфигурация VmWare', sref: 'preferencesConfigurationVmWare' },
                 { name: 'Шаблоны машин', sref: 'preferencesMachineTemplates' },
                 { name: 'Шаблоны email', sref: 'preferencesEmailTemplates' },
                 { name: 'Управления файлами', sref: 'preferencesFileManager' },
                 { name: 'Новости', sref: 'news' },
                 { name: 'Локации', sref: 'preferencesLocations' }
                ]
            },
            {
                name: 'Статистика',icon:'trending_up', isShowSubitems: false, subitems: [
                    { name: 'Общая статистика', sref: 'statisticGeneral' },
                    { name: 'Виртуальные машины', sref: 'statisticVirtualMachine' },
                    { name: 'Пользователи', sref: 'statisticUser' },
                    { name: 'Билинг', sref: 'statisticBilling' },
                    { name: 'Отчеты', sref: 'statisticReport' }
                ]
            }
        ];
    };

})();
