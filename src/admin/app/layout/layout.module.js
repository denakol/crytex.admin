﻿(function () {

    angular.module("crytex.layout", [
        "crytex.menu",
        "crytex.navbar"
    ]);

})();