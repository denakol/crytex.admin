﻿(function () {

    angular.module("crytex").config(config);

    config.$inject = ['$resourceProvider','$provide','$mdIconProvider','$mdThemingProvider','$mdDateLocaleProvider'];

    function config($resourceProvider, $provide,$mdIconProvider,$mdThemingProvider,$mdDateLocaleProvider) {

        $resourceProvider.defaults.stripTrailingSlashes = false;
        $resourceProvider.defaults.actions.update = { method: 'PUT', params: { id: '@id' } }
        $resourceProvider.defaults.actions.getAllPage = { method: 'GET', isArray: false }
        $resourceProvider.defaults.actions.getAll = { method: 'GET', isArray: true }

        $mdDateLocaleProvider.firstDayOfWeek = 1;
        $mdDateLocaleProvider.shortMonths  = ['янв', 'фев', 'март','апр','май','июн','июл','авг','сен','окт','ноя','дек'];
        $mdDateLocaleProvider.shortDays = ['Вс', 'Пн', 'Вт','Ср','Чт','Пт','Сб']
        $mdThemingProvider.theme('default')
            .primaryPalette('blue-grey').
        accentPalette('red');
        $mdIconProvider
            .defaultFontSet( 'material-icons' )

        //$provide.decorator('$httpBackend', angular.mock.e2e.$httpBackendDecorator);

        //also you can set here app themes, loading bar properties and many other
    };

})();