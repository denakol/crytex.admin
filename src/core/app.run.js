(function () {

    angular.module("crytex.core").run(running);

    running.$inject = ['amMoment'];

    function running(amMoment) {
        amMoment.changeLocale('ru');
    };

})();
