(function () {

    angular.module("blocks.services")
        .factory("monitor", Monitor);

    Monitor.$inject = ["$rootScope", 'localStorageService', 'hub','BASE_INFO'];
    function Monitor($rootScope, localStorageService, hub, BASE_INFO) {
        return function (options) {
            var authData = localStorageService.get("authorizationData");
            var token = authData.token;

            var monitorHub = new hub('MonitorHub',
                {
                    listeners: {
                        'recieveVmMessage': function (result) {
                            $rootScope.$apply(function () {
                                if (options.callback) {
                                    options.callback(result);
                                }
                            });
                        }
                    },
                    rootPath:BASE_INFO.URL + BASE_INFO.PORT,
                    methods: ['subscribe', 'unSubscribe'],

                    queryParams: {
                        Authorization: token
                    },

                    errorHandler: function (error) {
                        console.error(error);
                    },

                    stateChanged: function (state) {
                        switch (state.newState) {
                            case $.signalR.connectionState.connecting:

                                break;
                            case $.signalR.connectionState.connected:

                                break;
                            case $.signalR.connectionState.reconnecting:

                                break;
                            case $.signalR.connectionState.disconnected:

                                break;
                        }
                    }
                }
            );


            function subscribe(vmId) {
                monitorHub.subscribe(vmId);
            }

            function unSubscribe(vmId) {
                return monitorHub.unSubscribe(vmId);
            }


            var monitorService = {};
            monitorService.subscribe = subscribe;
            monitorService.unSubscribe = unSubscribe;
            monitorService.$promise = monitorHub.promise;
          return  monitorService;
        };
    };

})();