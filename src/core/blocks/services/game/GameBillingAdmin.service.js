var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * Created by denak on 17.03.2016.
 */
var core;
(function (core) {
    var GameBillingAdmin = (function (_super) {
        __extends(GameBillingAdmin, _super);
        function GameBillingAdmin(WebServices) {
            _super.call(this, WebServices.AdminGameServerService);
        }
        GameBillingAdmin.$inject = ['WebServices'];
        return GameBillingAdmin;
    }(core.GameBilling));
    core.GameBillingAdmin = GameBillingAdmin;
    angular.module("blocks.services")
        .service("GameBillingAdmin", GameBillingAdmin);
})(core || (core = {}));
