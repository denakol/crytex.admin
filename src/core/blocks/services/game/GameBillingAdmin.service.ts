/**
 * Created by denak on 17.03.2016.
 */
module core {
  
    export class GameBillingAdmin extends GameBilling {
        static $inject = ['WebServices']

        constructor(WebServices:IWebServices) {
            super( WebServices.AdminGameServerService);

        }

    }



    angular.module("blocks.services")
        .service("GameBillingAdmin", GameBillingAdmin);


}