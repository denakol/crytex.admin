/**
 * Created by denak on 17.03.2016.
 */
module core {

 
    export class GameBillingUser extends GameBilling {
        static $inject = ['WebServices']

        constructor(WebServices:IWebServices) {
            super( WebServices.UserGameServerService);

        }

    }



    angular.module("blocks.services")
        .service("GameBillingUser", GameBillingUser);


}