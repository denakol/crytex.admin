module core {
    import IPromise = angular.IPromise;
    import IGameServerBuyOptionsViewModel = core.WebApi.Models.IGameServerBuyOptionsViewModel;

    export interface IGameBilling{
        buyServerGame(gameOption:IGameServerBuyOptionsViewModel): IPromise<any>;
        prolongVm():IPromise<any>;
        updateGame():IPromise<any>;
    }
}