module core {

    import GameServerBuyOptionsViewModel = core.WebApi.Models.GameServerBuyOptionsViewModel;
    import IGameServerBuyOptionsViewModel = core.WebApi.Models.IGameServerBuyOptionsViewModel;


    export class GameBilling implements IGameBilling {

        buyServerGame(gameOption: IGameServerBuyOptionsViewModel):angular.IPromise<any> {
            var gameOptions = new GameServerBuyOptionsViewModel;
            gameOptions.gameServerTariffId = gameOption.tariff.id;
            gameOptions.slotCount = gameOption.slotCount;
            gameOptions.countingPeriodType = gameOption.countingPeriodType;
            gameOptions.expirePeriod = gameOption.expirePeriod;
            gameOptions.serverName = gameOption.serverName;
            gameOptions.gameId = gameOption.game.id;
            return  this.GameService.save(gameOptions).$promise;
        }

        prolongVm():angular.IPromise<any> {
            return undefined;
        }

        updateGame():angular.IPromise<any> {
            return undefined;
        }

        private GameService:IGameServerService;
        constructor(service:IGameServerService) {
            this.GameService = service;
        }

    }



}