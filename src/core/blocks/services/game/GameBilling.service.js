var core;
(function (core) {
    var GameServerBuyOptionsViewModel = core.WebApi.Models.GameServerBuyOptionsViewModel;
    var GameBilling = (function () {
        function GameBilling(service) {
            this.GameService = service;
        }
        GameBilling.prototype.buyServerGame = function (gameOption) {
            var gameOptions = new GameServerBuyOptionsViewModel;
            gameOptions.gameServerTariffId = gameOption.tariff.id;
            gameOptions.slotCount = gameOption.slotCount;
            gameOptions.countingPeriodType = gameOption.countingPeriodType;
            gameOptions.expirePeriod = gameOption.expirePeriod;
            gameOptions.serverName = gameOption.serverName;
            gameOptions.gameId = gameOption.game.id;
            return this.GameService.save(gameOptions).$promise;
        };
        GameBilling.prototype.prolongVm = function () {
            return undefined;
        };
        GameBilling.prototype.updateGame = function () {
            return undefined;
        };
        return GameBilling;
    }());
    core.GameBilling = GameBilling;
})(core || (core = {}));
