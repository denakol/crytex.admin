var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * Created by denak on 17.03.2016.
 */
var core;
(function (core) {
    var GameBillingUser = (function (_super) {
        __extends(GameBillingUser, _super);
        function GameBillingUser(WebServices) {
            _super.call(this, WebServices.UserGameServerService);
        }
        GameBillingUser.$inject = ['WebServices'];
        return GameBillingUser;
    }(core.GameBilling));
    core.GameBillingUser = GameBillingUser;
    angular.module("blocks.services")
        .service("GameBillingUser", GameBillingUser);
})(core || (core = {}));
