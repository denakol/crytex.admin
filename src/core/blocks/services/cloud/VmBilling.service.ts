/**
 * Created by denak on 10.02.2016.
 */
module core {
    import IVmBilling = core.IVmBilling;

    import SubscriptionBuyOptionsUserViewModel = core.WebApi.Models.SubscriptionBuyOptionsUserViewModel;
    import MachineConfigUpdateViewModel = core.WebApi.Models.MachineConfigUpdateViewModel;
    import MachinePeriodDaysUpdateViewModel = core.WebApi.Models.MachinePeriodDaysUpdateViewModel;
    export class VmBilling implements IVmBilling {

        buyVm(vmOption:user.VmOption):angular.IPromise<any> {
            var subscriptionVm = new SubscriptionBuyOptionsUserViewModel;
            subscriptionVm.cpu = vmOption.cpu;
            subscriptionVm.ram = vmOption.ram;
            subscriptionVm.hdd = vmOption.hdd;
            subscriptionVm.operatingSystemId = vmOption.userOperatingSystem.id;
            subscriptionVm.name =vmOption.name;
            subscriptionVm.subscriptionType = vmOption.subscriptionType;
            subscriptionVm.dailyBackupStorePeriodDays = 2;//vmOption.backupStoring;
            subscriptionVm.virtualization = vmOption.virtualization;
            subscriptionVm.subscriptionsMonthCount = vmOption.countMonth;
            return  this.UserSubscriptionVirtualMachineService.save(subscriptionVm).$promise;
        }

        prolongVm(option:any):angular.IPromise<any> {
            return undefined;
        }

        updateMachineConfiguration(subscriptionId:string, cpu:number,ram:number,hdd:number):angular.IPromise<any>
        {
            var config = new MachineConfigUpdateViewModel();
            config.subscriptionId =subscriptionId;
            config.cpu =cpu;
            config.ram =ram;
            config.hdd =hdd;
            return this.UserSubscriptionVirtualMachineService.updateMachineConfiguration(config).$promise;
        }

        updateSubscriptionBackupStoragePeriod(subscriptionId:string, backapingStoring:number):angular.IPromise<any>
        {
            var config = new MachinePeriodDaysUpdateViewModel();
            config.subscriptionId = subscriptionId;
            config.newPeriodDays = backapingStoring;
            return this.UserSubscriptionVirtualMachineService.updateSubscriptionBackupStoragePeriod(config).$promise;
        }


        private UserSubscriptionVirtualMachineService:ISubscriptionVirtualMachineService;
        constructor(service:ISubscriptionVirtualMachineService) {
            this.UserSubscriptionVirtualMachineService = service;
        }

    }



}