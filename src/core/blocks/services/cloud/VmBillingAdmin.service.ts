/**
 * Created by denak on 16.03.2016.
 */
/**
 * Created by denak on 10.02.2016.
 */
module core {
    import IVmBilling = core.IVmBilling;
    import IResourceClass = angular.resource.IResourceClass;
    import SubscriptionBuyOptionsUserViewModel = core.WebApi.Models.SubscriptionBuyOptionsUserViewModel;
    import MachineConfigUpdateViewModel = core.WebApi.Models.MachineConfigUpdateViewModel;
    export class VmBillingAdmin extends VmBilling {
        static $inject = ['WebServices']

        constructor(WebServices:IWebServices) {
            super( WebServices.AdminSubscriptionVirtualMachineService);

        }

    }



    angular.module("blocks.services")
        .service("VmBillingAdmin", VmBillingAdmin);


}