var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * Created by denak on 16.03.2016.
 */
/**
 * Created by denak on 10.02.2016.
 */
var core;
(function (core) {
    var VmBillingAdmin = (function (_super) {
        __extends(VmBillingAdmin, _super);
        function VmBillingAdmin(WebServices) {
            _super.call(this, WebServices.AdminSubscriptionVirtualMachineService);
        }
        VmBillingAdmin.$inject = ['WebServices'];
        return VmBillingAdmin;
    }(core.VmBilling));
    core.VmBillingAdmin = VmBillingAdmin;
    angular.module("blocks.services")
        .service("VmBillingAdmin", VmBillingAdmin);
})(core || (core = {}));
