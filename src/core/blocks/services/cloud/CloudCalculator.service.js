/**
 * Created by denak on 04.02.2016.
 */
var core;
(function (core) {
    var SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    var CloudCalculator = (function () {
        function CloudCalculator(WebServices, $q) {
            this.$q = $q;
            this.userPrice = WebServices.UserPriceService.query();
        }
        CloudCalculator.prototype.CalculatePrice = function (vmOption) {
            if (this.userPrice.$resolved && vmOption.userOperatingSystem) {
                var price = 0;
                var tariff = this.userPrice.find(function (elem) {
                    return elem.virtualization == vmOption.virtualization && elem.operatingSystem == vmOption.userOperatingSystem.family;
                });
                if (!tariff) {
                    return new PriceVm();
                }
                if (vmOption.backupStoring > 0) {
                    vmOption.backupStoring -= 1;
                }
                tariff.backupStoringGb = 0;
                var priceCpu = tariff.processor1 * vmOption.cpu;
                var priceRam = tariff.raM512 * vmOption.ram / 512;
                var priceHdd = tariff.hdD1 * vmOption.hdd;
                price =
                    priceCpu + priceRam + priceHdd +
                        tariff.backupStoringGb * vmOption.backupStoring * (vmOption.hdd + vmOption.ssd);
                //refactor need
                var priceResult = new PriceVm(price, priceCpu, priceRam, priceHdd);
                if (vmOption.subscriptionType == SubscriptionType.Usage) {
                    priceResult.price = priceResult.price / (30 * 24);
                }
                priceResult.cpuPrice /= (30 * 24);
                priceResult.ramPrice /= (30 * 24);
                priceResult.hddPrice /= (30 * 24);
                return priceResult;
            }
            return new PriceVm();
        };
        CloudCalculator.prototype.CalculatePriceWithWait = function (vmOption) {
            var _this = this;
            var deferred = this.$q.defer();
            this.userPrice.$promise.then(function () {
                deferred.resolve(_this.CalculatePrice(vmOption));
            });
            return deferred.promise;
        };
        CloudCalculator.$inject = ["WebServices", "$q"];
        return CloudCalculator;
    }());
    core.CloudCalculator = CloudCalculator;
    var PriceVm = (function () {
        function PriceVm(price, cpuPrice, ramPrice, hddPrice) {
            if (price === void 0) { price = 0; }
            if (cpuPrice === void 0) { cpuPrice = 0; }
            if (ramPrice === void 0) { ramPrice = 0; }
            if (hddPrice === void 0) { hddPrice = 0; }
            this.price = price;
            this.cpuPrice = cpuPrice;
            this.ramPrice = ramPrice;
            this.hddPrice = hddPrice;
            var hourlyKoef = 1 / (30 * 24);
            var hourlyDayKoef = 1 / 30;
            this.priceHourly = new Price(cpuPrice * hourlyKoef, ramPrice * hourlyKoef, hddPrice * hourlyKoef);
            this.priceDay = new Price(cpuPrice * hourlyDayKoef, ramPrice * hourlyDayKoef, hddPrice * hourlyDayKoef);
            this.priceMonth = new Price(cpuPrice, ramPrice, hddPrice);
        }
        return PriceVm;
    }());
    core.PriceVm = PriceVm;
    var Price = (function () {
        function Price(cpu, ram, hdd) {
            this.cpu = cpu;
            this.ram = ram;
            this.hdd = hdd;
        }
        Object.defineProperty(Price.prototype, "Sum", {
            get: function () {
                return this.cpu + this.ram + this.hdd;
            },
            enumerable: true,
            configurable: true
        });
        return Price;
    }());
    core.Price = Price;
    angular.module("blocks.services").service("cloudCalculator", CloudCalculator);
})(core || (core = {}));
/**
 * Created by denak on 04.02.2016.
 */
var core;
(function (core) {
    var SubscriptionType = core.WebApi.Models.Enums.SubscriptionType;
    var VmOption = (function () {
        function VmOption() {
            this.ssd = 0;
            this.backupStoring = 1;
            this.subscriptionType = SubscriptionType.Fixed;
        }
        VmOption.GetFromVm = function (userVm, subscriptionType) {
            var vmOption = new VmOption();
            vmOption.virtualization = userVm.virtualizationType;
            vmOption.cpu = userVm.coreCount;
            vmOption.ram = userVm.ramCount;
            vmOption.hdd = userVm.hardDriveSize;
            vmOption.currentCpu = userVm.coreCount;
            vmOption.currentRam = userVm.ramCount;
            vmOption.currenHdd = userVm.hardDriveSize;
            vmOption.userOperatingSystem = userVm.operatingSystem;
            vmOption.subscriptionType = subscriptionType;
            return vmOption;
        };
        return VmOption;
    }());
    core.VmOption = VmOption;
})(core || (core = {}));
