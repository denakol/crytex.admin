/**
 * Created by denak on 10.02.2016.
 */
var core;
(function (core) {
    var SubscriptionBuyOptionsUserViewModel = core.WebApi.Models.SubscriptionBuyOptionsUserViewModel;
    var MachineConfigUpdateViewModel = core.WebApi.Models.MachineConfigUpdateViewModel;
    var MachinePeriodDaysUpdateViewModel = core.WebApi.Models.MachinePeriodDaysUpdateViewModel;
    var VmBilling = (function () {
        function VmBilling(service) {
            this.UserSubscriptionVirtualMachineService = service;
        }
        VmBilling.prototype.buyVm = function (vmOption) {
            var subscriptionVm = new SubscriptionBuyOptionsUserViewModel;
            subscriptionVm.cpu = vmOption.cpu;
            subscriptionVm.ram = vmOption.ram;
            subscriptionVm.hdd = vmOption.hdd;
            subscriptionVm.operatingSystemId = vmOption.userOperatingSystem.id;
            subscriptionVm.name = vmOption.name;
            subscriptionVm.subscriptionType = vmOption.subscriptionType;
            subscriptionVm.dailyBackupStorePeriodDays = 2; //vmOption.backupStoring;
            subscriptionVm.virtualization = vmOption.virtualization;
            subscriptionVm.subscriptionsMonthCount = vmOption.countMonth;
            return this.UserSubscriptionVirtualMachineService.save(subscriptionVm).$promise;
        };
        VmBilling.prototype.prolongVm = function (option) {
            return undefined;
        };
        VmBilling.prototype.updateMachineConfiguration = function (subscriptionId, cpu, ram, hdd) {
            var config = new MachineConfigUpdateViewModel();
            config.subscriptionId = subscriptionId;
            config.cpu = cpu;
            config.ram = ram;
            config.hdd = hdd;
            return this.UserSubscriptionVirtualMachineService.updateMachineConfiguration(config).$promise;
        };
        VmBilling.prototype.updateSubscriptionBackupStoragePeriod = function (subscriptionId, backapingStoring) {
            var config = new MachinePeriodDaysUpdateViewModel();
            config.subscriptionId = subscriptionId;
            config.newPeriodDays = backapingStoring;
            return this.UserSubscriptionVirtualMachineService.updateSubscriptionBackupStoragePeriod(config).$promise;
        };
        return VmBilling;
    }());
    core.VmBilling = VmBilling;
})(core || (core = {}));
