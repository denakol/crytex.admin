/**
 * Created by denak on 10.02.2016.
 */
module core {
    import IPromise = angular.IPromise;
    import VmOption = user.VmOption;
    export interface IVmBilling{

        buyVm(vmOption:VmOption ): IPromise<any>;

        prolongVm(option:any):IPromise<any>;
        updateMachineConfiguration(subscriptionId:string, cpu:number,ram:number,hdd:number):IPromise<any>;
        updateSubscriptionBackupStoragePeriod(subscriptionId:string, backapingStoring:number):IPromise<any>;
    }
}