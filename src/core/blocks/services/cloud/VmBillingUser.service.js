var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * Created by denak on 16.03.2016.
 */
/**
 * Created by denak on 10.02.2016.
 */
var core;
(function (core) {
    var VmBillingUser = (function (_super) {
        __extends(VmBillingUser, _super);
        function VmBillingUser(WebServices) {
            _super.call(this, WebServices.UserSubscriptionVirtualMachineService);
        }
        VmBillingUser.$inject = ['WebServices'];
        return VmBillingUser;
    }(core.VmBilling));
    core.VmBillingUser = VmBillingUser;
    angular.module("blocks.services")
        .service("VmBilling", VmBillingUser);
})(core || (core = {}));
