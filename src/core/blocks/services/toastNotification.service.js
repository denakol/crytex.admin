(function () {

    angular.module("blocks.services")
        .factory("ToastNotificationService", toastNotificationService);

    toastNotificationService.$inject = ["$mdToast"];

    function toastNotificationService($mdToast) {
       
        var last = { bottom: false, top: true, left: false, right: true };

        var toastPosition = angular.extend({}, last);

        var getToastPosition = function () {
            return Object.keys(toastPosition)
                .filter(function (pos) { return toastPosition[pos]; })
                .join(' ');
        };

        function showMessage(message) {
            $mdToast.show(
                $mdToast.simple()
                .content(message)
                .position(getToastPosition())
                .hideDelay(3000));
        };

        return showMessage;

    }

})();
