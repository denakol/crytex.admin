
(function () {

	/*
		Доки по использованию httpBackend: https://docs.angularjs.org/api/ngMock/service/$httpBackend
		Для отключения всего этого дела:
			1 - комментируем строчку $provide.decorator('$httpBackend'...... в файле app.condig.js
			2 - в данном файле (fakeResource.service.js) меняем строку "angular.module("blocks.services").run(run);"
			на "angular.module("blocks.services")//.run(run);"
	*/

    angular.module("blocks.services");//.run(run);

    run.$inject = ['$httpBackend'];

    function run($httpBackend) {  	
        var mockData = new Object();
		mockData.fake = [];

		$httpBackend.whenGET(new RegExp("app/*")).passThrough();
		$httpBackend.whenGET(new RegExp("content/*")).passThrough();
    	$httpBackend.whenGET(new RegExp("/api/Admin/AdminSystemCenterVirtualManager")).respond(200, mockData.fake);
    };    

})();