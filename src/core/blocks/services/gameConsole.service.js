(function () {
    
    angular.module("blocks.services")
        .factory("gameConsole", GameConsole);

    GameConsole.$inject = [];


    function GameConsole() {
        

        function Console(options) {
            var gameConsole = this;
            
            gameConsole.send = function (command) {
                var date = moment(new Date()).format('DD.MM.YYYY');
                options.callback(date + " : "  + command);
            };
        }
        
        return Console;
    }
})();