/*
 defaults actions :
 get: "GET"
 getAllPage: "GET", isArray: false
 getAll, query: "GET", isArray: true
 remove: "DELETE"
 delete: "DELETE"
 save: "POST"
 update: "PUT"
 */

(function () {

    angular.module("blocks.services")
        .factory("WebServices", webServices);

    webServices.$inject = ['$resource', 'BASE_INFO'];

    function webServices($resource, BASE_INFO) {

        var apiUrl = BASE_INFO.URL + BASE_INFO.PORT + BASE_INFO.API_URL;
        var baseUrl = BASE_INFO.URL + BASE_INFO.PORT;

        // Давайте писать в алфавитном порядке
        return {            

            // Сервисы для администратора писать с Admin....

            AdminBillingTransaction: $resource(apiUrl + '/Admin/AdminBillingTransaction', {}, {}),

            AdminBonusReplenishmentService: $resource(apiUrl + '/Admin/AdminBonusReplenishment/:id', {id: '@id'}, {}),

            AdminDiscountService:$resource(apiUrl + '/Admin/AdminDiscount/:id', {id: '@Id'}, {
                blockType: {method: 'POST', url: apiUrl + '/Admin/AdminDiscount/BlockType/:typeDiscount/:disable', params: { typeDiscount: '@typeDiscount', disable: '@disable' }}
            }),

            AdminEmailService: $resource(apiUrl + '/Admin/AdminEmailInfo/:id', {id: '@Id'}, {}),

            AdminGameService: $resource(apiUrl + '/Admin/AdminGame/:id', {id: '@Id'}, {
                getGameById: {method: 'GET'},
                updateGame: {method: 'PUT'},
                createGame: {method: 'POST'},
                deleteGame: {method: 'DELETE'}
            }),

            AdminGameHostService: $resource(apiUrl + '/Admin/AdminGameHost/:id', {id: '@Id'}, {
                updateGameHost: {method: 'PUT'},
                createGameHost: {method: 'POST'},
                deleteGameHost: {method: 'DELETE'}
            }),

            AdminGameServerService: $resource(apiUrl + '/Admin/AdminGameServer/:id', {id: '@Id'}, {

                updateGameServerStatus:{method:'POST', url: apiUrl + '/Admin/AdminGameServer/UpdateServerStatus'},
                updateGameServerConfiguration:{method:'POST', url: apiUrl + '/Admin/AdminGameServer/UpdateGameServerConfiguration'},
                updateServerStatus:{method:'POST', url: apiUrl + '/Admin/AdminGameServer/UpdateServerStatus'},
                extendGameServer: {method: 'PUT', url: apiUrl + '/Admin/AdminGameServer'}

            }),

            AdminGameServerTariffService: $resource(apiUrl + '/Admin/AdminGameServerTariff/:id', {id: '@Id'}, {
                updateTariff: {method: 'PUT'},
                createTariff: {method: 'POST'},
                deleteTariff: {method: 'DELETE'}
            }),

            AdminNewsService: $resource(apiUrl + '/Admin/AdminNews/:method', {}, {
                save: {method: 'POST', params: {method: 'POST'}},
                update: {method: 'PUT'},
                delete: {method: 'DELETE'}
            }),
            
            AdminEmailTemplateService: $resource(apiUrl + '/Admin/AdminEmailTemplate/:id', {id: '@Id'}, {}),

            AdminFixedSubscriptionPayment: $resource(apiUrl + '/Admin/AdminFixedSubscriptionPayment/:id', {id: '@Id'}, {}),

            AdminHelpDeskCommentService: $resource(apiUrl + '/Admin/AdminHelpDeskRequestComment/:id', {id: '@Id'}, {
                post: {method: 'POST', params: {id: '@id', userId: '@userId'}}
            }),

            AdminHelpDeskService: $resource(apiUrl + '/Admin/AdminHelpDeskRequest/:id', {id: '@Id'}, {}),

            AdminHyperVHostService: $resource(apiUrl + '/Admin/AdminHyperVHost/:id', {id: '@Id'}, {}),

            AdminLogService: $resource(apiUrl + '/Admin/AdminLog/:id', {id: '@Id'}, {}),

            AdminLocationService: $resource(apiUrl + '/Admin/AdminLocation/:id', {id: '@id'}, {}),

            AdminLongTermDiscountService: $resource(apiUrl + '/Admin/AdminLongTermDiscount/:id', {id: '@id'}, {}),
            
            AdminOperatingSystem: $resource(apiUrl + '/Admin/AdminOperatingSystem/:method', {}, {
                save: {method: 'POST', params: {method: 'POST'}},
                update: {method: 'PUT'}
            }),

            AdminPaymentGameServerService: $resource(apiUrl + '/Admin/AdminPaymentGameServer', {id: '@id'}, {}),

            AdminPaymentService: $resource(apiUrl + '/Admin/AdminPayment/:id', {id: '@id'}, {}),

            AdminPaymentSystemService: $resource(apiUrl + '/Admin/AdminPaymentSystem/:id', {id: '@id'}, {}),

            AdminPhoneCallService: $resource(apiUrl + '/Admin/AdminPhoneCallRequest/:id', {id: '@Id'}, {}),
            
            AdminPriceService: $resource(apiUrl + '/Admin/AdminTariff', {}, {
                update: {method: 'PUT'}
            }),
            
            AdminSnapShotVmService: $resource(apiUrl + '/Admin/AdminSnapShotVm', {}, {}),
           
            AdminStateMachineService: $resource(apiUrl + '/Admin/AdminStateMachine/:id', {id: '@Id'}, {}),

            AdminStatisticService: $resource(apiUrl + '/Admin/AdminStatistic/:id', {id: '@Id'}, {
                getSummary: {method: 'GET', url: apiUrl + '/Admin/AdminStatistic/method/summary', isArray: false},
                getSubscriptionVm: {method: 'GET', url: apiUrl + '/Admin/AdminStatistic/method/subscriptionVm', isArray: false}
            }),

            AdminSystemCenterVirtualManagerService: $resource(apiUrl + '/Admin/AdminSystemCenterVirtualManager/:id', {id: '@Id'}, {}),

            AdminSubscriptionVirtualMachineService: $resource(apiUrl + '/Admin/AdminSubscriptionVm/:id', {id: '@Id'}, {
                updateData: {method: 'POST', url: apiUrl + '/Admin/AdminSubscriptionVm/UpdateSubscription'},
                updateMachineStatus: {method: 'POST', url: apiUrl + '/Admin/AdminSubscriptionVm/UpdateMachineStatus'},
                updateMachineConfiguration: {method: 'POST', url: apiUrl + '/Admin/AdminSubscriptionVm/UpdateSubscriptionConfiguration'},
                extendSubscription: {method: 'PUT', url: apiUrl + '/Admin/AdminSubscriptionVm/:id'},
                updateSubscriptionBackupStoragePeriod:{method: 'POST', url: apiUrl + '/Admin/AdminSubscriptionVm/UpdateSubscriptionBackupStoragePeriod'}
            }),

            AdminTaskV2: $resource(apiUrl + '/Admin/AdminTaskV2/:method', {}, {
                save: {method: 'POST', params: {method: 'POST'}},
            }), 

            AdminTemplate: $resource(apiUrl + '/Admin/AdminTemplate/:method', {}, {
                save: {method: 'POST', params: {method: 'POST'}},
                update: {method: 'PUT'}
            }),

            AdminUsageSubscriptionPaymentService: $resource(apiUrl + '/Admin/AdminUsageSubscriptionPayment', {}, {}),

            AdminUserSearch: $resource(apiUrl + '/Admin/UserSearch', {}, {}),

            AdminUserService: $resource(apiUrl + '/Admin/AdminUser/:id', {id: '@Id'}, {
                updateState: {method: 'POST', url: apiUrl + '/Admin/AdminUser/UpdateState'},
                updateBalance: {method: 'POST', url: apiUrl + '/Admin/AdminUser/UpdateBalance'}
            }),
            
            AdminVirtualMachine: $resource(apiUrl + '/Admin/AdminUserVm', {}, {}),

            AdminVmBackupService: $resource(apiUrl + '/Admin/AdminVmBackup/:id', {id: '@Id'}, {}),

            AdminVmWareVCenterService: $resource(apiUrl + '/Admin/AdminVmWareVCenter/:id', {id: '@Id'}, {}),

            // Сервисы для пользователей писать с User....
            User: $resource(apiUrl + '/User/User', {}, {}),

            UserBillingTransaction: $resource(apiUrl + '/User/BillingTransaction/:id', {id: '@Id'}, {}),

            UserFixedSubscriptionPayment: $resource(apiUrl + '/User/FixedSubscriptionPayment', {}, {}),

            UserGameService: $resource(apiUrl + '/User/Game/:id', {id: '@Id'}, {}),

            UserGameServerService: $resource(apiUrl + '/User/GameServer/:id', {id: '@Id'}, {
                updateGameServerConfiguration:{method:'POST', url: apiUrl + '/User/GameServer/UpdateGameServerConfiguration'},
                updateServerStatus:{method:'POST', url: apiUrl + '/User/GameServer/UpdateServerStatus'},
                extendGameServer: {method: 'PUT', url: apiUrl + '/Admin/AdminGameServer'}
            }),

            UserHelpDeskRequest: $resource(apiUrl + '/User/HelpDeskRequest/:id', {id: '@Id'}, {
                updateRequest: {method: 'PUT', url: apiUrl + '/User/HelpDeskRequest/:id'},
            }),

            UserHelpDeskRequestComment: $resource(apiUrl + '/User/HelpDeskRequestComment/:id', {id: '@Id'}, {
                createComment: {method: 'POST', url: apiUrl + '/User/HelpDeskRequestComment/:id'}
            }),

            UserLocationService: $resource(apiUrl + '/User/Location', {id: '@Id'}, {}),

            UserLongTermDiscountService: $resource(apiUrl + '/User/LongTermDiscount/:id', {id: '@id'}, {}),

            UserNews: $resource(apiUrl + '/User/News/:id', {id: '@Id'}, {}),

            UserOperatingSystem: $resource(apiUrl + '/Admin/OperatingSystem/:method', {}, {}),

            UserPayment: $resource(apiUrl + '/User/Payment/:id', {id: '@Id'}, {
                billingTransactionInfo: {method: 'GET', url: apiUrl + '/User/Payment/method/billingTransactionInfo'}
            }),

            UserPaymentForecast: $resource(apiUrl + '/User/PaymentForecast/:id', {}, {}),

            UserPaymentGameServerService: $resource(apiUrl + '/User/PaymentGameServer/:id', {}, {}),

            UserPaymentSystemService: $resource(apiUrl + '/User/UserPaymentSystem/:id', {id: '@id'}, {}),

            UserPhoneCallRequest: $resource(apiUrl + '/User/PhoneCallRequest', {}, {}),

            UserPriceService: $resource(apiUrl + '/User/Tariff', {}, {}),

            UserStateMachineService: $resource(apiUrl + '/User/StateMachine/:id', {id: '@Id'}, {
                getLastState: {method: 'GET', url: apiUrl + '/User/StateMachine/method/lastState'},
            }),

            UserSubscriptionVirtualMachineService: $resource(apiUrl + '/User/SubscriptionVm/:id', {id: '@Id'}, {
                updateData: {method: 'POST', url: apiUrl + '/User/SubscriptionVm/UpdateSubscription'},
                updateMachineStatus: {method: 'POST', url: apiUrl + '/User/SubscriptionVm/UpdateMachineStatus'},
                updateMachineConfiguration: {method: 'POST', url: apiUrl + '/User/SubscriptionVm/UpdateSubscriptionConfiguration'},
                updateSubscription: {method: 'POST', url: apiUrl + '/User/SubscriptionVm/UpdateSubscription'},
                addTestPeriod: {method: 'POST', url: apiUrl + '/User/SubscriptionVm/AddTestPeriod'},
                extendSubscription: {method: 'PUT', url: apiUrl + '/User/SubscriptionVm/:id'},
                updateSubscriptionBackupStoragePeriod:{method: 'POST', url: apiUrl + '/User/SubscriptionVm/UpdateSubscriptionBackupStoragePeriod'}
            }),

            UserTaskV2Service: $resource(apiUrl + '/User/TaskV2/:id', {id: '@Id'}, {}),

            UserUsageSubscriptionPayment: $resource(apiUrl + '/User/UsageSubscriptionPayment', {}, {}),
        };
    }
})();
