/**
 * Created by denak on 08.02.2016.
 */
module core {
    import IResourceClass = angular.resource.IResourceClass;
    import UserPaymentForecastViewModel = core.UserPaymentForecastViewModel;
    import BillingViewModel = core.WebApi.Models.BillingViewModel;
    import TaskV2ViewModel = core.WebApi.Models.TaskV2ViewModel;
    import PaymentView = core.WebApi.Models.PaymentView;
    import NewsViewModel = core.WebApi.Models.NewsViewModel;
    import StateMachine = core.WebApi.Models.StateMachine;
    import FixedSubscriptionPaymentViewModel = core.WebApi.Models.FixedSubscriptionPaymentViewModel;
    import UsageSubscriptionPaymentView = core.WebApi.Models.UsageSubscriptionPaymentView;
    import SubscriptionVmViewModel = core.WebApi.Models.SubscriptionVmViewModel;
    import BillingTransactionInfoViewModel = core.WebApi.Models.BillingTransactionInfoViewModel;
    import HelpDeskRequestCommentViewModel = core.WebApi.Models.HelpDeskRequestCommentViewModel;
    import HelpDeskRequestViewModel = core.WebApi.Models.HelpDeskRequestViewModel;
    import GameViewModel = core.WebApi.Models.GameViewModel;
    import GameHostViewModel = core.WebApi.Models.GameHostViewModel;
    import GameServerTariffView = core.WebApi.Models.GameServerTariffView;
    import GameServerViewModel = core.WebApi.Models.GameServerViewModel;
    import PaymentGameServerViewModel = core.WebApi.Models.PaymentGameServerViewModel;
    import PaymentSystemView = core.WebApi.Models.PaymentSystemView;

    export interface IWebServices {
        AdminBillingTransaction:IResourceClass<IResourceClass<any>>;

        AdminBonusReplenishmentService:IResourceClass<IResourceClass<any>>;

        AdminPhoneCallService:IResourceClass<IResourceClass<any>>;

        AdminDiscountService:IResourceClass<IResourceClass<any>>;

        AdminEmailService:IResourceClass<IResourceClass<any>>;

        AdminEmailTemplateService:IResourceClass<any>;

        AdminFixedSubscriptionPayment: IResourceClass<any>;

        AdminGameHostService: IAdminGameHostService;

        AdminGameService: IAdminGameService;

        AdminGameServerService: IAdminGameServerService;

        AdminGameServerTariffService: IAdminGameTariffService;

        AdminHelpDeskCommentService:IResourceClass<any>;

        AdminHelpDeskService: IResourceClass<any>,

        AdminLogService:IResourceClass<any>,

        AdminLocationService: IResourceClass<any>,

        AdminLongTermDiscountService: IResourceClass<any>,

        AdminOperatingSystem: IResourceClass<any>,

        AdminPaymentService: IResourceClass<PaymentView>,

        AdminPaymentSystemService: IResourceClass<PaymentSystemView>,

        AdminPriceService: IResourceClass<any>,

        AdminSnapShotVmService: IResourceClass<any>,

        AdminStateMachineService: IResourceClass<any>,

        AdminStatisticService: IResourceClass<any>,

        AdminSystemCenterVirtualManagerService: IResourceClass<any>,

        AdminSubscriptionVirtualMachineService: ISubscriptionVirtualMachineService,
        AdminTaskV2: IResourceClass<any>;

        AdminTemplate:IResourceClass<any>;
        AdminUsageSubscriptionPaymentService: IResourceClass<any>;

        AdminUserSearch: IResourceClass<any>;

        AdminUserService: IResourceClass<any>;
        AdminVirtualMachine:IResourceClass<any>;

        AdminVmBackupService: IResourceClass<any>;

        AdminVmWareVCenterService: IResourceClass<any>;

        // Сервисы для пользователей писать с User....
        User: IResourceClass<any>;

        UserBillingTransaction: IResourceClass<BillingViewModel>;

        UserFixedSubscriptionPayment: IResourceClass<FixedSubscriptionPaymentViewModel>;

        UserGameService: IResourceClass<PageModel<GameViewModel>>;

        UserGameServerService : IUserGameServerService;

        UserHelpDeskRequestService: IUserHelpDeskRequestService;

        UserHelpDeskRequestComment: IUserHelpDeskRequestCommentService;

        UserLocationService: IResourceClass<any>;

        UserLongTermDiscountService: IResourceClass<any>;

        UserNews: IResourceClass<NewsViewModel>;

        UserOperatingSystem: IResourceClass<any>;

        UserPayment: IResourceClass<PaymentView>;

        UserPaymentForecast: IResourceClass<UserPaymentForecastViewModel>;

        UserPaymentGameServerService: IResourceClass<PaymentGameServerViewModel>;

        UserPaymentSystemService: IResourceClass<PaymentSystemView>,

        UserPhoneCallRequest:IResourceClass<any>;

        UserPriceService: IResourceClass<any>;
        UserStateMachine:IUserStateMachineService;
        UserSubscriptionVirtualMachineService :IUserSubscriptionVirtualMachineService;

        UserTaskV2Service: IResourceClass<TaskV2ViewModel>;

        UserUsageSubscriptionPayment: IResourceClass<UsageSubscriptionPaymentView>;
    }

    export interface ISubscriptionVirtualMachineService extends IResourceClass<SubscriptionVmViewModel> {
        updateMachineConfiguration(data: Object): SubscriptionVmViewModel;
        updateMachineStatus(data: Object):SubscriptionVmViewModel;
        updateSubscription(data: Object):SubscriptionVmViewModel;
        updateSubscriptionBackupStoragePeriod(data: Object):SubscriptionVmViewModel;
    }
    
    
    export interface IUserSubscriptionVirtualMachineService extends ISubscriptionVirtualMachineService{
        updateData(data: Object):SubscriptionVmViewModel;
        addTestPeriod(data: Object):SubscriptionVmViewModel;
        extendSubscription(data: Object):SubscriptionVmViewModel;
        updateSubscriptionBackupStoragePeriod(data: Object):SubscriptionVmViewModel;
    }

    export interface IUserHelpDeskRequestService extends IResourceClass<HelpDeskRequestViewModel> {

        updateRequest(id: Object, data: Object): any;
    }

    export interface IUserHelpDeskRequestCommentService extends IResourceClass<HelpDeskRequestCommentViewModel> {

        createComment(id: Object, data: Object): any;
    }

    export interface IUserStateMachineService  extends IResourceClass<StateMachine> {

        getLastState(data: Object): StateMachine;
    }

    export interface IUserPayment  extends IResourceClass<PaymentView> {

        billingTransactionInfo(data: Object): PageModel<BillingTransactionInfoViewModel>;
    }

    export interface IAdminGameService extends IResourceClass<PageModel<GameViewModel>> {

        getGameById(data: Object): any;
        createGame(data: Object): any;
        updateGame(data: Object): any;
        deleteGame(data: Object): any;
    }

    export interface IAdminGameTariffService extends IResourceClass<GameServerTariffView> {

        createTariff(data: Object): any;
        updateTariff(data: Object): any;
        deleteTariff(data: Object): any;
    }

    export interface IAdminGameHostService extends IResourceClass<PageModel<GameHostViewModel>> {

        createGameHost(data: Object): any;
        updateGameHost(data: Object): any;
        deleteGameHost(data: Object): any;
    }


    export interface IGameServerService extends IResourceClass<GameServerViewModel> {

        updateGameServerConfiguration(data: Object): any;
        updateServerStatus(data: Object): any;
        extendGameServer(data: Object): any;
    }
  
    export interface IAdminGameServerService extends IGameServerService{
        

    }

    export interface IUserGameServerService extends IGameServerService {

    
    }

}