var core;
(function (core) {
    var WebApi;
    (function (WebApi) {
        var Models;
        (function (Models) {
            var Enums;
            (function (Enums) {
                (function (PhysicalServerType) {
                    PhysicalServerType[PhysicalServerType["ReadyServer"] = 1] = "ReadyServer";
                    PhysicalServerType[PhysicalServerType["AviableServer"] = 2] = "AviableServer";
                })(Enums.PhysicalServerType || (Enums.PhysicalServerType = {}));
                var PhysicalServerType = Enums.PhysicalServerType;
                (function (TypeUser) {
                    TypeUser[TypeUser["JuridicalPerson"] = 0] = "JuridicalPerson";
                    TypeUser[TypeUser["PhysicalPerson"] = 1] = "PhysicalPerson";
                })(Enums.TypeUser || (Enums.TypeUser = {}));
                var TypeUser = Enums.TypeUser;
                (function (FileType) {
                    FileType[FileType["Image"] = 0] = "Image";
                    FileType[FileType["Loader"] = 1] = "Loader";
                    FileType[FileType["Document"] = 2] = "Document";
                    FileType[FileType["HelpDeskRequestAttachment"] = 3] = "HelpDeskRequestAttachment";
                })(Enums.FileType || (Enums.FileType = {}));
                var FileType = Enums.FileType;
                (function (GameFamily) {
                    GameFamily[GameFamily["Unknown"] = 0] = "Unknown";
                    GameFamily[GameFamily["Ark"] = 1] = "Ark";
                    GameFamily[GameFamily["Arma3"] = 2] = "Arma3";
                    GameFamily[GameFamily["Cs"] = 3] = "Cs";
                    GameFamily[GameFamily["Css"] = 4] = "Css";
                    GameFamily[GameFamily["CsGo"] = 5] = "CsGo";
                    GameFamily[GameFamily["Cure"] = 6] = "Cure";
                    GameFamily[GameFamily["Dods"] = 7] = "Dods";
                    GameFamily[GameFamily["GMod"] = 8] = "GMod";
                    GameFamily[GameFamily["L4D"] = 9] = "L4D";
                    GameFamily[GameFamily["L4D2"] = 10] = "L4D2";
                    GameFamily[GameFamily["Minecraft"] = 11] = "Minecraft";
                    GameFamily[GameFamily["SaMp"] = 12] = "SaMp";
                    GameFamily[GameFamily["T2F"] = 13] = "T2F";
                    GameFamily[GameFamily["Bmdm"] = 14] = "Bmdm";
                    GameFamily[GameFamily["Cscz"] = 15] = "Cscz";
                    GameFamily[GameFamily["Insurgency"] = 16] = "Insurgency";
                    GameFamily[GameFamily["JustCause2"] = 17] = "JustCause2";
                })(Enums.GameFamily || (Enums.GameFamily = {}));
                var GameFamily = Enums.GameFamily;
                (function (RequestStatus) {
                    RequestStatus[RequestStatus["New"] = 0] = "New";
                    RequestStatus[RequestStatus["InProcessing"] = 1] = "InProcessing";
                    RequestStatus[RequestStatus["Completed"] = 2] = "Completed";
                })(Enums.RequestStatus || (Enums.RequestStatus = {}));
                var RequestStatus = Enums.RequestStatus;
                (function (OperatingSystemFamily) {
                    OperatingSystemFamily[OperatingSystemFamily["Windows2012"] = 0] = "Windows2012";
                    OperatingSystemFamily[OperatingSystemFamily["Ubuntu"] = 1] = "Ubuntu";
                })(Enums.OperatingSystemFamily || (Enums.OperatingSystemFamily = {}));
                var OperatingSystemFamily = Enums.OperatingSystemFamily;
                (function (TypeServer) {
                    TypeServer[TypeServer["GameServer"] = 0] = "GameServer";
                    TypeServer[TypeServer["SystemServer"] = 1] = "SystemServer";
                    TypeServer[TypeServer["UserServer"] = 2] = "UserServer";
                })(Enums.TypeServer || (Enums.TypeServer = {}));
                var TypeServer = Enums.TypeServer;
                (function (TypeVirtualization) {
                    TypeVirtualization[TypeVirtualization["HyperV"] = 0] = "HyperV";
                    TypeVirtualization[TypeVirtualization["VmWare"] = 1] = "VmWare";
                })(Enums.TypeVirtualization || (Enums.TypeVirtualization = {}));
                var TypeVirtualization = Enums.TypeVirtualization;
                (function (HostResourceType) {
                    HostResourceType[HostResourceType["Hdd"] = 0] = "Hdd";
                })(Enums.HostResourceType || (Enums.HostResourceType = {}));
                var HostResourceType = Enums.HostResourceType;
                (function (SnapshotStatus) {
                    SnapshotStatus[SnapshotStatus["Creating"] = 0] = "Creating";
                    SnapshotStatus[SnapshotStatus["Active"] = 1] = "Active";
                    SnapshotStatus[SnapshotStatus["WaitForDeletion"] = 2] = "WaitForDeletion";
                    SnapshotStatus[SnapshotStatus["Deleted"] = 3] = "Deleted";
                })(Enums.SnapshotStatus || (Enums.SnapshotStatus = {}));
                var SnapshotStatus = Enums.SnapshotStatus;
                (function (StatusVM) {
                    StatusVM[StatusVM["Enable"] = 0] = "Enable";
                    StatusVM[StatusVM["Disable"] = 1] = "Disable";
                    StatusVM[StatusVM["Error"] = 2] = "Error";
                    StatusVM[StatusVM["Creating"] = 3] = "Creating";
                    StatusVM[StatusVM["Deleted"] = 4] = "Deleted";
                    StatusVM[StatusVM["UpdatingConf"] = 5] = "UpdatingConf";
                    StatusVM[StatusVM["UpdatingStatus"] = 6] = "UpdatingStatus";
                })(Enums.StatusVM || (Enums.StatusVM = {}));
                var StatusVM = Enums.StatusVM;
                (function (PaymentViewModelType) {
                    PaymentViewModelType[PaymentViewModelType["WebHosting"] = 0] = "WebHosting";
                    PaymentViewModelType[PaymentViewModelType["FixedSubscription"] = 1] = "FixedSubscription";
                    PaymentViewModelType[PaymentViewModelType["UsageSubscription"] = 2] = "UsageSubscription";
                    PaymentViewModelType[PaymentViewModelType["SubscriptionBackup"] = 3] = "SubscriptionBackup";
                    PaymentViewModelType[PaymentViewModelType["PhysicalServer"] = 4] = "PhysicalServer";
                    PaymentViewModelType[PaymentViewModelType["GameServer"] = 5] = "GameServer";
                })(Enums.PaymentViewModelType || (Enums.PaymentViewModelType = {}));
                var PaymentViewModelType = Enums.PaymentViewModelType;
                (function (ServerPaymentType) {
                    ServerPaymentType[ServerPaymentType["Slot"] = 0] = "Slot";
                    ServerPaymentType[ServerPaymentType["Configuration"] = 1] = "Configuration";
                })(Enums.ServerPaymentType || (Enums.ServerPaymentType = {}));
                var ServerPaymentType = Enums.ServerPaymentType;
                (function (TypeDiscount) {
                    TypeDiscount[TypeDiscount["BigPurchase"] = 0] = "BigPurchase";
                    TypeDiscount[TypeDiscount["BonusReplenishment"] = 1] = "BonusReplenishment";
                    TypeDiscount[TypeDiscount["PurchaseOfLongTerm"] = 2] = "PurchaseOfLongTerm";
                })(Enums.TypeDiscount || (Enums.TypeDiscount = {}));
                var TypeDiscount = Enums.TypeDiscount;
                (function (ResourceType) {
                    ResourceType[ResourceType["Vm"] = 0] = "Vm";
                    ResourceType[ResourceType["Gs"] = 1] = "Gs";
                })(Enums.ResourceType || (Enums.ResourceType = {}));
                var ResourceType = Enums.ResourceType;
                (function (TypeChangeStatus) {
                    TypeChangeStatus[TypeChangeStatus["Start"] = 0] = "Start";
                    TypeChangeStatus[TypeChangeStatus["Stop"] = 1] = "Stop";
                    TypeChangeStatus[TypeChangeStatus["Reload"] = 2] = "Reload";
                    TypeChangeStatus[TypeChangeStatus["PowerOff"] = 3] = "PowerOff";
                })(Enums.TypeChangeStatus || (Enums.TypeChangeStatus = {}));
                var TypeChangeStatus = Enums.TypeChangeStatus;
                (function (StatusTask) {
                    StatusTask[StatusTask["Start"] = 0] = "Start";
                    StatusTask[StatusTask["Pending"] = 1] = "Pending";
                    StatusTask[StatusTask["Processing"] = 2] = "Processing";
                    StatusTask[StatusTask["End"] = 3] = "End";
                    StatusTask[StatusTask["EndWithErrors"] = 4] = "EndWithErrors";
                    StatusTask[StatusTask["Queued"] = 5] = "Queued";
                })(Enums.StatusTask || (Enums.StatusTask = {}));
                var StatusTask = Enums.StatusTask;
                (function (TypeTask) {
                    TypeTask[TypeTask["CreateVm"] = 0] = "CreateVm";
                    TypeTask[TypeTask["UpdateVm"] = 1] = "UpdateVm";
                    TypeTask[TypeTask["ChangeStatus"] = 2] = "ChangeStatus";
                    TypeTask[TypeTask["RemoveVm"] = 3] = "RemoveVm";
                    TypeTask[TypeTask["Backup"] = 4] = "Backup";
                    TypeTask[TypeTask["DeleteBackup"] = 5] = "DeleteBackup";
                    TypeTask[TypeTask["CreateSnapshot"] = 6] = "CreateSnapshot";
                    TypeTask[TypeTask["DeleteSnapshot"] = 7] = "DeleteSnapshot";
                    TypeTask[TypeTask["LoadSnapshot"] = 8] = "LoadSnapshot";
                    TypeTask[TypeTask["CreateWebHosting"] = 9] = "CreateWebHosting";
                    TypeTask[TypeTask["StartWebApp"] = 10] = "StartWebApp";
                    TypeTask[TypeTask["StopWebApp"] = 11] = "StopWebApp";
                    TypeTask[TypeTask["RestartWebApp"] = 12] = "RestartWebApp";
                    TypeTask[TypeTask["DisableWebHosting"] = 13] = "DisableWebHosting";
                    TypeTask[TypeTask["DeleteHosting"] = 14] = "DeleteHosting";
                    TypeTask[TypeTask["CreateGameServer"] = 15] = "CreateGameServer";
                    TypeTask[TypeTask["DeleteGameServer"] = 16] = "DeleteGameServer";
                    TypeTask[TypeTask["GameServerChangeStatus"] = 17] = "GameServerChangeStatus";
                    TypeTask[TypeTask["UpdateGameServer"] = 18] = "UpdateGameServer";
                    TypeTask[TypeTask["Test"] = 99] = "Test";
                })(Enums.TypeTask || (Enums.TypeTask = {}));
                var TypeTask = Enums.TypeTask;
                (function (TriggerType) {
                    TriggerType[TriggerType["EndTask"] = 0] = "EndTask";
                })(Enums.TriggerType || (Enums.TriggerType = {}));
                var TriggerType = Enums.TriggerType;
                (function (BillingTransactionType) {
                    BillingTransactionType[BillingTransactionType["BalanceReplenishment"] = 0] = "BalanceReplenishment";
                    BillingTransactionType[BillingTransactionType["WebHostingPayment"] = 1] = "WebHostingPayment";
                    BillingTransactionType[BillingTransactionType["FixedSubscriptionVmPayment"] = 2] = "FixedSubscriptionVmPayment";
                    BillingTransactionType[BillingTransactionType["UsageSubscriptionVmPayment"] = 3] = "UsageSubscriptionVmPayment";
                    BillingTransactionType[BillingTransactionType["PhysicalServerPayment"] = 5] = "PhysicalServerPayment";
                    BillingTransactionType[BillingTransactionType["GameServer"] = 6] = "GameServer";
                    BillingTransactionType[BillingTransactionType["TestPeriod"] = 7] = "TestPeriod";
                    BillingTransactionType[BillingTransactionType["ReturnMoneyForDeletedService"] = 8] = "ReturnMoneyForDeletedService";
                })(Enums.BillingTransactionType || (Enums.BillingTransactionType = {}));
                var BillingTransactionType = Enums.BillingTransactionType;
                (function (SubscriptionType) {
                    SubscriptionType[SubscriptionType["Usage"] = 0] = "Usage";
                    SubscriptionType[SubscriptionType["Fixed"] = 1] = "Fixed";
                })(Enums.SubscriptionType || (Enums.SubscriptionType = {}));
                var SubscriptionType = Enums.SubscriptionType;
                (function (SubscriptionVmStatus) {
                    SubscriptionVmStatus[SubscriptionVmStatus["WaitForPayment"] = 0] = "WaitForPayment";
                    SubscriptionVmStatus[SubscriptionVmStatus["Active"] = 1] = "Active";
                    SubscriptionVmStatus[SubscriptionVmStatus["WaitForDeletion"] = 2] = "WaitForDeletion";
                    SubscriptionVmStatus[SubscriptionVmStatus["Deleted"] = 3] = "Deleted";
                })(Enums.SubscriptionVmStatus || (Enums.SubscriptionVmStatus = {}));
                var SubscriptionVmStatus = Enums.SubscriptionVmStatus;
                (function (PaymentStatus) {
                    PaymentStatus[PaymentStatus["Success"] = 0] = "Success";
                    PaymentStatus[PaymentStatus["Created"] = 1] = "Created";
                    PaymentStatus[PaymentStatus["Failed"] = 2] = "Failed";
                })(Enums.PaymentStatus || (Enums.PaymentStatus = {}));
                var PaymentStatus = Enums.PaymentStatus;
                (function (UrgencyLevel) {
                    UrgencyLevel[UrgencyLevel["Low"] = 0] = "Low";
                    UrgencyLevel[UrgencyLevel["Normal"] = 1] = "Normal";
                    UrgencyLevel[UrgencyLevel["High"] = 2] = "High";
                    UrgencyLevel[UrgencyLevel["Critical"] = 3] = "Critical";
                })(Enums.UrgencyLevel || (Enums.UrgencyLevel = {}));
                var UrgencyLevel = Enums.UrgencyLevel;
                (function (PhysicalServerOptionType) {
                    PhysicalServerOptionType[PhysicalServerOptionType["Ram"] = 0] = "Ram";
                    PhysicalServerOptionType[PhysicalServerOptionType["Hdd"] = 1] = "Hdd";
                    PhysicalServerOptionType[PhysicalServerOptionType["OperationSystem"] = 2] = "OperationSystem";
                    PhysicalServerOptionType[PhysicalServerOptionType["Traffic"] = 3] = "Traffic";
                    PhysicalServerOptionType[PhysicalServerOptionType["IpAdress"] = 4] = "IpAdress";
                    PhysicalServerOptionType[PhysicalServerOptionType["Admistration"] = 5] = "Admistration";
                })(Enums.PhysicalServerOptionType || (Enums.PhysicalServerOptionType = {}));
                var PhysicalServerOptionType = Enums.PhysicalServerOptionType;
                (function (BoughtPhysicalServerStatus) {
                    BoughtPhysicalServerStatus[BoughtPhysicalServerStatus["New"] = 0] = "New";
                    BoughtPhysicalServerStatus[BoughtPhysicalServerStatus["Created"] = 1] = "Created";
                    BoughtPhysicalServerStatus[BoughtPhysicalServerStatus["Active"] = 2] = "Active";
                    BoughtPhysicalServerStatus[BoughtPhysicalServerStatus["WaitPayment"] = 3] = "WaitPayment";
                    BoughtPhysicalServerStatus[BoughtPhysicalServerStatus["WaitForDeletion"] = 4] = "WaitForDeletion";
                    BoughtPhysicalServerStatus[BoughtPhysicalServerStatus["Deleted"] = 5] = "Deleted";
                    BoughtPhysicalServerStatus[BoughtPhysicalServerStatus["DontCreate"] = 6] = "DontCreate";
                })(Enums.BoughtPhysicalServerStatus || (Enums.BoughtPhysicalServerStatus = {}));
                var BoughtPhysicalServerStatus = Enums.BoughtPhysicalServerStatus;
                (function (EmailTemplateType) {
                    EmailTemplateType[EmailTemplateType["Registration"] = 0] = "Registration";
                    EmailTemplateType[EmailTemplateType["ChangePassword"] = 1] = "ChangePassword";
                    EmailTemplateType[EmailTemplateType["ChangeProfile"] = 2] = "ChangeProfile";
                    EmailTemplateType[EmailTemplateType["CreateVm"] = 3] = "CreateVm";
                    EmailTemplateType[EmailTemplateType["UpdateVm"] = 4] = "UpdateVm";
                    EmailTemplateType[EmailTemplateType["SubscriptionNeedsPayment"] = 5] = "SubscriptionNeedsPayment";
                    EmailTemplateType[EmailTemplateType["SubscriptionEndWarning"] = 6] = "SubscriptionEndWarning";
                    EmailTemplateType[EmailTemplateType["SubscriptionDeletionWarning"] = 7] = "SubscriptionDeletionWarning";
                    EmailTemplateType[EmailTemplateType["CreateVmCredentials"] = 8] = "CreateVmCredentials";
                    EmailTemplateType[EmailTemplateType["GameServerNeedsPayment"] = 9] = "GameServerNeedsPayment";
                    EmailTemplateType[EmailTemplateType["GameServerEndWarning"] = 10] = "GameServerEndWarning";
                    EmailTemplateType[EmailTemplateType["GameServerDeletionWarning"] = 11] = "GameServerDeletionWarning";
                    EmailTemplateType[EmailTemplateType["ResetPassword"] = 12] = "ResetPassword";
                    EmailTemplateType[EmailTemplateType["WebHostingWasDisabled"] = 13] = "WebHostingWasDisabled";
                    EmailTemplateType[EmailTemplateType["WebHostingEndWaring"] = 14] = "WebHostingEndWaring";
                    EmailTemplateType[EmailTemplateType["WebHostingDeletionWarning"] = 15] = "WebHostingDeletionWarning";
                    EmailTemplateType[EmailTemplateType["PhysicalServerNeedsPayment"] = 16] = "PhysicalServerNeedsPayment";
                    EmailTemplateType[EmailTemplateType["PhysicalServerEndWarning"] = 17] = "PhysicalServerEndWarning";
                    EmailTemplateType[EmailTemplateType["PhysicalServerDeletionWarning"] = 18] = "PhysicalServerDeletionWarning";
                    EmailTemplateType[EmailTemplateType["PhysicalServerCreated"] = 19] = "PhysicalServerCreated";
                    EmailTemplateType[EmailTemplateType["PhysicalServerReady"] = 20] = "PhysicalServerReady";
                    EmailTemplateType[EmailTemplateType["PhysicalServerDontCreate"] = 21] = "PhysicalServerDontCreate";
                })(Enums.EmailTemplateType || (Enums.EmailTemplateType = {}));
                var EmailTemplateType = Enums.EmailTemplateType;
                (function (EmailResultStatus) {
                    EmailResultStatus[EmailResultStatus["Sent"] = 0] = "Sent";
                    EmailResultStatus[EmailResultStatus["Queued"] = 1] = "Queued";
                    EmailResultStatus[EmailResultStatus["Rejected"] = 2] = "Rejected";
                    EmailResultStatus[EmailResultStatus["Invalid"] = 3] = "Invalid";
                    EmailResultStatus[EmailResultStatus["Scheduled"] = 4] = "Scheduled";
                })(Enums.EmailResultStatus || (Enums.EmailResultStatus = {}));
                var EmailResultStatus = Enums.EmailResultStatus;
                (function (GameServerUpdateType) {
                    GameServerUpdateType[GameServerUpdateType["UpdateSettings"] = 0] = "UpdateSettings";
                    GameServerUpdateType[GameServerUpdateType["Prolongation"] = 1] = "Prolongation";
                    GameServerUpdateType[GameServerUpdateType["UpdateSlotCount"] = 2] = "UpdateSlotCount";
                })(Enums.GameServerUpdateType || (Enums.GameServerUpdateType = {}));
                var GameServerUpdateType = Enums.GameServerUpdateType;
                (function (TypeDate) {
                    TypeDate[TypeDate["StartedAt"] = 0] = "StartedAt";
                    TypeDate[TypeDate["CompletedAt"] = 1] = "CompletedAt";
                })(Enums.TypeDate || (Enums.TypeDate = {}));
                var TypeDate = Enums.TypeDate;
                (function (TypeNotify) {
                    TypeNotify[TypeNotify["EndTask"] = 0] = "EndTask";
                })(Enums.TypeNotify || (Enums.TypeNotify = {}));
                var TypeNotify = Enums.TypeNotify;
                (function (GameServerState) {
                    GameServerState[GameServerState["Creating"] = 0] = "Creating";
                    GameServerState[GameServerState["Enable"] = 1] = "Enable";
                    GameServerState[GameServerState["Disable"] = 2] = "Disable";
                    GameServerState[GameServerState["UpdatingConf"] = 3] = "UpdatingConf";
                    GameServerState[GameServerState["UpdatingStatus"] = 4] = "UpdatingStatus";
                    GameServerState[GameServerState["Error"] = 5] = "Error";
                })(Enums.GameServerState || (Enums.GameServerState = {}));
                var GameServerState = Enums.GameServerState;
                (function (CountingPeriodType) {
                    CountingPeriodType[CountingPeriodType["Day"] = 0] = "Day";
                    CountingPeriodType[CountingPeriodType["Week"] = 1] = "Week";
                    CountingPeriodType[CountingPeriodType["Month"] = 2] = "Month";
                    CountingPeriodType[CountingPeriodType["Year"] = 3] = "Year";
                })(Enums.CountingPeriodType || (Enums.CountingPeriodType = {}));
                var CountingPeriodType = Enums.CountingPeriodType;
                (function (PaymentSystemType) {
                    PaymentSystemType[PaymentSystemType["Onpay"] = 0] = "Onpay";
                    PaymentSystemType[PaymentSystemType["Sprypay"] = 1] = "Sprypay";
                    PaymentSystemType[PaymentSystemType["Interkassa"] = 2] = "Interkassa";
                    PaymentSystemType[PaymentSystemType["PayPal"] = 3] = "PayPal";
                    PaymentSystemType[PaymentSystemType["WebMoney"] = 4] = "WebMoney";
                    PaymentSystemType[PaymentSystemType["YandexMoney"] = 5] = "YandexMoney";
                    PaymentSystemType[PaymentSystemType["TestSystem"] = 6] = "TestSystem";
                })(Enums.PaymentSystemType || (Enums.PaymentSystemType = {}));
                var PaymentSystemType = Enums.PaymentSystemType;
            })(Enums = Models.Enums || (Models.Enums = {}));
        })(Models = WebApi.Models || (WebApi.Models = {}));
    })(WebApi = core.WebApi || (core.WebApi = {}));
})(core || (core = {}));
