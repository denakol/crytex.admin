/**
 * Created by denak on 11.02.2016.
 */
module core {
    export enum ServerTypesResult
    {
        ServerError =0,
        UserExist =1,
        IncorrectPassword = 2,
        UserBlocked =3,
        NotValidateEmail=4,
        NotEnoughMoney=5
    }
}