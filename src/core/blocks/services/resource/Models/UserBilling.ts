module user {

    export interface BillingViewModel {
        userId: string;
        userName: string;
        transactionType: string;
        cashAmount: number;
        description: string;
        subscriptionVmId: string;
    }
}