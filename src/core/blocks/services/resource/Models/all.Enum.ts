
module core.WebApi.Models.Enums {
    export  enum PhysicalServerType {
        ReadyServer = 1,
        AviableServer = 2
    }

    export  enum TypeUser {
        JuridicalPerson = 0,
        PhysicalPerson = 1
    }
    export  enum FileType {
        Image = 0,
        Loader = 1,
        Document = 2,
        HelpDeskRequestAttachment = 3
    }
    export  enum GameFamily {
        Unknown = 0,
        Ark,
        Arma3,
        Cs,
        Css,
        CsGo,
        Cure,
        Dods,
        GMod,
        L4D,
        L4D2,
        Minecraft,
        SaMp,
        T2F,
        Bmdm,
        Cscz,
        Insurgency,
        JustCause2
    }
    export  enum RequestStatus {
        New = 0,
        InProcessing = 1,
        Completed = 2
    }
    export  enum OperatingSystemFamily {
        Windows2012 = 0,
        Ubuntu = 1
    }
    export  enum TypeServer {
        GameServer = 0,
        SystemServer = 1,
        UserServer = 2
    }
    export  enum TypeVirtualization {
        HyperV = 0,
        VmWare = 1
    }
    export  enum HostResourceType {
        Hdd = 0
    }
    export  enum SnapshotStatus {
        Creating = 0,
        Active = 1,
        WaitForDeletion = 2,
        Deleted = 3
    }
    export  enum StatusVM {
        Enable = 0,
        Disable = 1,
        Error = 2,
        Creating = 3,
        Deleted = 4,
        UpdatingConf,
        UpdatingStatus
    }
    export enum PaymentViewModelType {
        WebHosting = 0,
        FixedSubscription = 1,
        UsageSubscription = 2,
        SubscriptionBackup = 3,
        PhysicalServer = 4,
        GameServer = 5
    }
    export  enum ServerPaymentType {
        Slot = 0,
        Configuration = 1
    }
    export  enum TypeDiscount {
        BigPurchase = 0,
        BonusReplenishment = 1,
        PurchaseOfLongTerm = 2
    }
    export  enum ResourceType {
        Vm = 0,
        Gs = 1
    }
    export  enum TypeChangeStatus {
        Start = 0,
        Stop = 1,
        Reload = 2,
        PowerOff = 3
    }
    export  enum StatusTask {
        Start = 0,
        Pending = 1,
        Processing = 2,
        End = 3,
        EndWithErrors = 4,
        Queued = 5
    }
    export  enum TypeTask {
        CreateVm = 0,
        UpdateVm = 1,
        ChangeStatus = 2,
        RemoveVm = 3,
        Backup = 4,
        DeleteBackup = 5,
        CreateSnapshot = 6,
        DeleteSnapshot = 7,
        LoadSnapshot = 8,
        CreateWebHosting = 9,
        StartWebApp = 10,
        StopWebApp = 11,
        RestartWebApp = 12,
        DisableWebHosting = 13,
        DeleteHosting = 14,
        CreateGameServer = 15,
        DeleteGameServer = 16,
        GameServerChangeStatus = 17,
        UpdateGameServer = 18,
        Test = 99
    }
    export  enum TriggerType {
        EndTask = 0
    }

    export  enum BillingTransactionType {
        BalanceReplenishment = 0,
        WebHostingPayment = 1,
        FixedSubscriptionVmPayment = 2,
        UsageSubscriptionVmPayment = 3,
        PhysicalServerPayment = 5,
        GameServer = 6,
        TestPeriod = 7,
        ReturnMoneyForDeletedService = 8
    }
    export  enum SubscriptionType {
        Usage = 0,
        Fixed = 1
    }
    export  enum SubscriptionVmStatus {
        WaitForPayment = 0,
        Active = 1,
        WaitForDeletion = 2,
        Deleted = 3
    }
    export  enum PaymentStatus {
        Success = 0,
        Created = 1,
        Failed = 2
    }

    export  enum UrgencyLevel {
        Low = 0,
        Normal = 1,
        High = 2,
        Critical = 3
    }
    export  enum PhysicalServerOptionType {
        Ram = 0,
        Hdd = 1,
        OperationSystem = 2,
        Traffic = 3,
        IpAdress = 4,
        Admistration = 5
    }
    export  enum BoughtPhysicalServerStatus {
        New = 0,
        Created = 1,
        Active = 2,
        WaitPayment = 3,
        WaitForDeletion = 4,
        Deleted = 5,
        DontCreate = 6
    }
    export  enum EmailTemplateType {
        Registration = 0,
        ChangePassword = 1,
        ChangeProfile = 2,
        CreateVm = 3,
        UpdateVm = 4,
        SubscriptionNeedsPayment = 5,
        SubscriptionEndWarning = 6,
        SubscriptionDeletionWarning = 7,
        CreateVmCredentials = 8,
        GameServerNeedsPayment = 9,
        GameServerEndWarning = 10,
        GameServerDeletionWarning = 11,
        ResetPassword = 12,
        WebHostingWasDisabled = 13,
        WebHostingEndWaring = 14,
        WebHostingDeletionWarning = 15,
        PhysicalServerNeedsPayment = 16,
        PhysicalServerEndWarning = 17,
        PhysicalServerDeletionWarning = 18,
        PhysicalServerCreated = 19,
        PhysicalServerReady = 20,
        PhysicalServerDontCreate = 21
    }
    export  enum EmailResultStatus {
        Sent = 0,
        Queued = 1,
        Rejected = 2,
        Invalid = 3,
        Scheduled = 4
    }
    export  enum GameServerUpdateType {
        UpdateSettings = 0,
        Prolongation = 1,
        UpdateSlotCount = 2
    }

    export  enum TypeDate {
        StartedAt = 0,
        CompletedAt = 1
    }

    export enum TypeNotify {
        EndTask = 0
    }

    export enum GameServerState
    {
        Creating = 0,
        Enable = 1,
        Disable = 2,
        UpdatingConf,
        UpdatingStatus,
        Error
    }

    export enum CountingPeriodType
    {
        Day = 0,
        Week = 1,
        Month = 2,
        Year = 3
    }

    export enum PaymentSystemType
    {
        Onpay = 0,
        Sprypay = 1,
        Interkassa = 2,
        PayPal = 3,
        WebMoney = 4,
        YandexMoney = 5,
        TestSystem
    }
}

