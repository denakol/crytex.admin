module core.WebApi.Models {
    import IResource = angular.resource.IResource;
    import TypeChangeStatus = core.WebApi.Models.Enums.TypeChangeStatus;
    import GameFamily = core.WebApi.Models.Enums.GameFamily;
    import PaymentViewModelType = core.WebApi.Models.Enums.PaymentViewModelType;
    import CountingPeriodType = core.WebApi.Models.Enums.CountingPeriodType;
    import PaymentSystemType = core.WebApi.Models.Enums.PaymentSystemType;

    export interface ApplicationUserSearchParamsViewModel {
        name:string;
        lastname:string;
        patronymic:string;
        email:string;
    }
    export interface AdminApplicationUserSearchParamsViewModel extends ApplicationUserSearchParamsViewModel {
        typeOfUser:core.WebApi.Models.Enums.TypeUser;
        userName:string;
        registerDateFrom:Date;
        registerDateTo:Date;
    }
    export interface ApplicationUserViewModel extends IResource<ApplicationUserViewModel> {
        id:string;
        userName:string;
        email:string;
        password:string;
        changePassword:boolean;
        phoneNumber:string;
        name:string;
        lastName:string;
        patronymic:string;
        city:string;
        country:string;
        address:string;
        codePhrase:string;
        userType:core.WebApi.Models.Enums.TypeUser;
        payer:string;
        contactPerson:string;
        balance:number;
        isBlocked:boolean;
        companyName:string;
        inn:string;
        kpp:string;
        juridicalAddress:string;
        mailAddress:string;
    }
    export interface BillingSearchParamsViewModel {
        userId?:string;
        dateFrom?:Date;
        dateTo?:Date;
        billingTransactionType?:core.WebApi.Models.Enums.BillingTransactionType;
    }
    export interface AdminBillingSearchParamsViewModel extends BillingSearchParamsViewModel {
        subscriptionVmId:string;
        subscriptionGameServerId:string;
    }
    export interface BillingViewModel {
        userId:string;
        userName:string;
        transactionType:core.WebApi.Models.Enums.BillingTransactionType;
        cashAmount:number;
        date:Date;
        description:string;
        subscriptionVmId:string;
    }
    export interface BillingTransactionInfoViewModel {
        billingTransactionId:string;
        transactionType:core.WebApi.Models.Enums.BillingTransactionType;
        transactionCashAmount:number;
        payments:Array<PaymentViewModelBase>;
    }
    export interface PaymentViewModelBase {
        Id:string;
        Date:Date;
        Amount:number;
        BillingTransactionId:string;
        ReturnedToUser:boolean;
        ReturnDate:Date;
        PaymentModelType:core.WebApi.Models.Enums.PaymentViewModelType;
    }
    export interface BoughtPhysicalServerViewModel {
        id:string;
        physicalServerId:string;
        userId:string;
        createDate:Date;
        dateEnd:Date;
        countMonth:number;
        discountPrice:number;
        status:core.WebApi.Models.Enums.BoughtPhysicalServerStatus;
        config:string;
        autoProlongation:boolean;
        server:PhysicalServerViewModel;
        options:PhysicalServerOptionViewModel[];
    }
    export interface PhysicalServerViewModel {
        id:string;
        processorName:string;
        description:string;
        price:number;
        config:string;
        options:PhysicalServerOptionViewModel[];
    }
    export interface PhysicalServerOptionViewModel {
        id:string;
        name:string;
        description:string;
        price:number;
        type:core.WebApi.Models.Enums.PhysicalServerOptionType;
        isDefault:boolean;
    }
    export interface BuyWebHostingParamsModel {
        webHostingTariffId:string;
        name:string;
        monthCount:number;
        autoProlongation:boolean;
    }
    export interface ChangePhysicalServerViewModel {
        serverId:string;
        status:number;
        message:string;
        autoProlongation:boolean;
    }
    export interface DiscountViewModel {
        id:number;
        monthCount:number;
        discountSize:number;
        disable:boolean;
        resourceType:core.WebApi.Models.Enums.ResourceType;
    }

    export interface BonusReplenishmentViewModel {
        id: number;
        userReplenishmentSize: number;
        bonusSize:number;
        disable:boolean;
    }
    export interface EmailInfoesViewModel {
        id:number;
        dateSending:Date;
        from:string;
        to:string;
        subjectParams:string;
        bodyParams:string;
        emailTemplateType:core.WebApi.Models.Enums.EmailTemplateType;
        isProcessed:boolean;
        emailResultStatus:core.WebApi.Models.Enums.EmailResultStatus;
        reason:string;
    }
    export interface FixedSubscriptionPaymentSearchParamViewModel {
        dateFrom:Date;
        dateTo:Date;
        subscriptionVmId:string;
    }
    export interface AdminFixedSubscriptionPaymentSearchParamViewModel extends FixedSubscriptionPaymentSearchParamViewModel {
        userId:string;
        virtualization:core.WebApi.Models.Enums.TypeVirtualization;
        operatingSystem:core.WebApi.Models.Enums.OperatingSystemFamily;
    }
    export interface FixedSubscriptionPaymentViewModel {
        id:string;
        subscriptionVmId:string;
        userVmName:string;
        userVmId:string;
        date:Date;
        amount:number;
        monthCount:number;
        coreCount:number;
        ramCount:number;
        hardDriveSize:number;
        tariffId:number;
        userId:string;
        userName:string;
        virtualization:core.WebApi.Models.Enums.TypeVirtualization;
        operatingSystem:core.WebApi.Models.Enums.OperatingSystemFamily;
    }
    export interface GameServerConfigurationView {
        id:number;
        gameName:string;
        serverTemplateId:number;
        processor1:number;
        rAM512:number;
        slot:number;
    }
    export interface GameServerConfigViewModel {
        serverId:string;
        serverName:string;
        autoProlongation:boolean;
        monthCount:number;
        updateType:core.WebApi.Models.Enums.GameServerUpdateType;
        status:core.WebApi.Models.Enums.TypeChangeStatus;
    }
    export interface GameServerMachineConfigUpdateViewModel {
        gameServerId:string;
        cpu:number;
        ram:number;
    }

    export interface PaymentForecastViewModel {
        currentBalance:number;
        usageSubscriptionOneDayForecasts:SubscriptionPaymentForecastViewModel[];
        usageSubscriptionsMonthForecasts:SubscriptionPaymentForecastViewModel[];
        fixedSubscriptionsMonthForecasts:SubscriptionPaymentForecastViewModel[];
        gameServerPaymentForecasts:GameServerPaymentForecastViewModel[];
        webHostingPaymentForecasts:WebHostingPaymentForecastViewModel[];
    }
    export interface SubscriptionPaymentForecastViewModel extends PaymentForecastViewModelBase {
        subscriptionVmId:string;
    }
    export interface PaymentForecastViewModelBase {
        paymentForecast:number;
    }
    export interface GameServerPaymentForecastViewModel extends PaymentForecastViewModelBase {
        gameServerId:string;
    }
    export interface WebHostingPaymentForecastViewModel extends PaymentForecastViewModelBase {
        webHostingId:string;
    }
    export interface PhysicalServerChangeOptionsAviable {
        serverId:string;
        options:PhysicalServerOptionViewModel[];
        replaceAll:boolean;
    }
    export interface ProlongateGameServerViewModel {
        serverId:string;
        monthCount:number;
    }
    export interface PaymentSystemView extends IResource<PaymentSystemView>{
        id:string;
        name:string;
        isEnabled:boolean;
        paymentType: PaymentSystemType;
        imageFileDescriptorId:number;
        imageFileDescriptor: FileDescriptorViewModel;

        // Только на клиенте
        isEdit: boolean;
        isSelected: boolean;
    }
    export interface PhysicalServerGetOption {
        id:string;
        type:core.WebApi.Models.Enums.PhysicalServerType;
    }
    export interface WebHostingProlongateOptionsModel {
        webHostingId:string;
        monthCount:number;
    }
    export class MachineConfigUpdateViewModel {
        subscriptionId:string;
        cpu:number;
        ram:number;
        hdd:number;
    }
    export class MachinePeriodDaysUpdateViewModel {
        subscriptionId:string;
        newPeriodDays:number;
    }

    export interface NewsViewModel {
        id:string;
        title:string;
        createTime:Date;
        body:string;
        userId:string;
        userName:string;
    }
    export interface PaymentGameServerViewModel {
        gameServerId:string;
        gameServerName:string;
        coreCount:number;
        ramCount:number;
        slotCount:number;
        monthCount:number;
        paymentModelType: PaymentViewModelType;
    }
    export interface SearchPaymentGameServerParams {
        fromDate: Date;
        toDate: Date;
        userId: string;
        serverId: string;
    }
    export interface PhoneCallRequestViewModel {
        phoneNumber:string;
        creationDate:Date;
        isRead:boolean;
        id:number;
    }
    export interface PhoneCallRequestEditViewModel {
        isRead:boolean;
    }
    export interface RegionViewModel {
        id:number;
        name:string;
        area:string;
        enable:boolean;
    }
    export interface SnapshotVmViewModel {
        id:string;
        name:string;
        date:Date;
        vmId:string;
        validation:boolean;
    }
    export class SubscriptionBuyOptionsUserViewModel {
        public cpu:number;
        public operatingSystemId:number;
        public name:string;
        public ram:number;
        public sSD:number;
        public hdd:number;
        public subscriptionsMonthCount:number;
        public autoProlongation:boolean;
        public subscriptionType:core.WebApi.Models.Enums.SubscriptionType;
        public virtualization:core.WebApi.Models.Enums.TypeVirtualization;
        public dailyBackupStorePeriodDays:number;
    }
    export interface SubscriptionBuyOptionsAdminViewModel extends SubscriptionBuyOptionsUserViewModel {
        userId:string;
    }
    export interface SubscriptionProlongateOptionsViewModel {
        subscriptionId:string;
        monthCount:number;
    }
    export interface SubscriptionVmViewModel extends IResource<SubscriptionVmViewModel> {
        id:string;
        dateCreate:Date;
        dateEnd:Date;
        tariffId:number;
        userId:string;
        name:string;
        userName:string;
        subscriptionType:core.WebApi.Models.Enums.SubscriptionType;
        autoProlongation:boolean;
        operatingSystemId:number;
        userVm:UserVmViewModel;
        dailyBackupStorePeriodDays:number;
    }
    export interface UserVmViewModel {
        id:string;
        coreCount:number;
        ramCount:number;
        hardDriveSize:number;
        status:core.WebApi.Models.Enums.StatusVM;
        virtualizationType:core.WebApi.Models.Enums.TypeVirtualization;
        name:string;
        userId:string;
        userName:string;
        osImageFilePath:string;
        osName:string;
        operatingSystem:OperatingSystemViewModel;
        operatingSystemPassword:string;
        tasks:TaskV2ViewModel[];
    }
    export interface StateMachine extends BaseEntity {
        cpuLoad:number;
        ramLoad:number;
        upTime:string;
        date:Date;
        vmId:string;
    }

    export interface GameHostViewModel extends IResource<GameHostViewModel> {
        id:number;
        serverAddress:string;
        port:number;
        username:string;
        password:string;
        gameServersMaxCount:number;
        supportedGamesIds:Array<number>;
        supportedGames:Array<GameViewModel>;
    }
    export class GameServerBuyOptionsViewModel {
        gameServerTariffId:number;
        slotCount:number;
        expirePeriod:number;
        countingPeriodType: CountingPeriodType;
        serverName:string;
        userId:string;
        gameId:number;
        autoProlongation:boolean;
    }
    export interface IGameServerBuyOptionsViewModel extends GameServerBuyOptionsViewModel {
        // Только на клиенте
        game:GameViewModel;
        tariff:GameServerTariffSimpleView;
        totalPrice:number;
        discount:number;
    }
    export interface BaseEntity extends IResource<BaseEntity> {
        id:number;
    }
    export interface OperatingSystemViewModel extends UserOperatingSystemViewModel {
        imagePath:string;
        imageFileId:number;
        serverTemplateName:string;
        defaultAdminPassword:string;
    }
    export interface UserOperatingSystemViewModel {
        id:number;
        name:string;
        description:string;
        imageSrc:string;
        family:core.WebApi.Models.Enums.OperatingSystemFamily;
        minCoreCount:number;
        minHardDriveSize:number;
        minRamCount:number;
    }
    export interface TariffViewModel {
        id:number;
        operatingSystem:core.WebApi.Models.Enums.OperatingSystemFamily;
        virtualization:core.WebApi.Models.Enums.TypeVirtualization;
        processor1:number;
        raM512:number;
        hdD1:number;
        ssD1:number;
        load10Percent:number;
        createDate:Date;
        updateDate:Date;
        backupStoringGb:number;
    }
    export interface TaskV2SearchParamsViewModel {
        resourceId?:string;
        userId?:string;
        pageNumber?:number;
        pageSize?:number;
        typeTask?:core.WebApi.Models.Enums.TypeTask;
        statusTasks?:core.WebApi.Models.Enums.StatusTask[];
        virtualization?:core.WebApi.Models.Enums.TypeVirtualization;
    }
    export interface AdminTaskV2SearchParamsViewModel extends TaskV2SearchParamsViewModel {
        startDate:Date;
        endDate:Date;
        typeDate:core.WebApi.Models.Enums.TypeDate;
    }
    export interface TaskV2ViewModel {
        id:string;
        resourceType:core.WebApi.Models.Enums.ResourceType;
        resourceId:string;
        typeTask:core.WebApi.Models.Enums.TypeTask;
        statusTask:core.WebApi.Models.Enums.StatusTask;
        options:string;
        userId:string;
        errorMessage:string;
        virtualization:core.WebApi.Models.Enums.TypeVirtualization;
        createdAt:Date;
        startedAt:Date;
        completedAt:Date;
    }
    export interface HyperVHostResourceViewModel {
        id:string;
        resourceType:core.WebApi.Models.Enums.HostResourceType;
        value:string;
        valid:boolean;
        updateDate:Date;
        hyperVHostId:string;
        deleted:boolean;
    }
    export interface HyperVHostViewModel {
        id:string;
        host:string;
        coreNumber:number;
        ramSize:number;
        userName:string;
        password:string;
        valid:boolean;
        dateAdded:Date;
        systemCenterVirtualManagerId:string;
        deleted:boolean;
        disabled:boolean;
        createdManual:boolean;
        resources:HyperVHostResourceViewModel[];
    }
    export interface SystemCenterVirtualManagerViewModel {
        id:string;
        host:string;
        userName:string;
        password:string;
        name:string;
        hyperVHosts:HyperVHostViewModel[];
    }
    export interface TestPeriodViewModel {
        userId:string;
        countDay:number;
        cashAmount:number;
    }
    export interface TriggerViewModel {
        userId:string;
        type:core.WebApi.Models.Enums.TriggerType;
        thresholdValue:number;
        thresholdValueSecond:number;
    }
    export interface UpdateEmailTemplateViewModel {
        id:number;
        subject:string;
        body:string;
        parameterNamesList:string[];
    }
    export interface PaymentView extends IResource<PaymentView> {
        id:string;
        cashAmount:number;
        paymentSystemId:string;
        date:Date;
        dateEnd:Date;
        userId:string;
        success:boolean;
        userName:string;
    }
    export interface EmailTemplateViewModel {
        id:number;
        subject:string;
        body:string;
        emailTemplateType:core.WebApi.Models.Enums.EmailTemplateType;
        parameterNamesList:string[];
    }
    export interface    FileDescriptorViewModel {
        name:string;
        path:string;
        type:core.WebApi.Models.Enums.FileType;
    }
    export interface HelpDeskRequestCommentViewModel {
        id?:number;
        comment:string;
        userId?:string;
        userName?:string;
        userEmail?:string;
        creationDate:Date;
        isUserComment:boolean;
        isRead:boolean;
    }
    export interface HelpDeskRequestViewModel {
        summary:string;
        details:string;
        status:core.WebApi.Models.Enums.RequestStatus;
        creationDate:Date;
        read:boolean;
        id:number;
        userId:string;
        userName:string;
        email:string;
        urgency:core.WebApi.Models.Enums.UrgencyLevel;
        fileDescriptorParams:FileDescriptorParam[];
    }
    export interface FileDescriptorParam {
        id:number;
        path:string;
        name:string;
    }
    export interface MessageViewModel {
        body:string;
    }
    export interface OperatingSystemEditViewModel {
        name:string;
        description:string;
        serverTemplateName:string;
        imageFileId:number;
        family:core.WebApi.Models.Enums.OperatingSystemFamily;
        imagePath:string;
        defaultAdminPassword:string;
        minCoreCount:number;
        minHardDriveSize:number;
        minRamCount:number;
    }
    export interface PageModel<T> {
        items:T[];
        totalPages:number;
        totalRows:number;
    }
    export interface ServerTemplateEditViewModel {
        name:string;
        description:string;
        coreCount:number;
        ramCount:number;
        hardDriveSize:number;
        imageFileId:number;
        operatingSystemId:number;
        userId:string;
        typeServerTemplate:core.WebApi.Models.Enums.TypeServer;
    }
    export interface ServerTemplateViewModel {
        id:number;
        name:string;
        description:string;
        coreCount:number;
        ramCount:number;
        hardDriveSize:number;
        imageFileId:number;
        operatingSystemId:number;
        imageSrc:string;
        userId:string;
        typeServerTemplate:core.WebApi.Models.Enums.TypeServer;
    }
    export interface UpdateGameServerOptions {
        serverId:string;
        status:core.WebApi.Models.Enums.TypeChangeStatus;
    }
    export interface UpdateMachineStatusOptions {
        status?:core.WebApi.Models.Enums.TypeChangeStatus;
        subscriptionId:string;
    }
    export interface UsageSubscriptionPaymentByPeriodView {
        month:string;
        date:Date;
        usageSubscriptionPayment:UsageSubscriptionPaymentView[];
    }
    export interface UsageSubscriptionPaymentView {
        id:string;
        subscriptionVmId:string;
        tariffId:number;
        userId:string;
        userVmId:string;
        userVmName:string;
        userName:string;
        paid:boolean;
        date:Date;
        coreCount:number;
        ramCount:number;
        hardDriveSize:number;
        amount:number;
    }
    export interface UsageSubscriptionPaymentGroupByVmView {
        name:string;
        subscriptions:UsageSubscriptionPaymentByPeriodView[];
    }
    export interface SubscriptionUpdateOptions {
        id:string;
        name:string;
        autoProlongation?:boolean;
    }
    export interface UserLoginLogEntryModel {
        userId:string;
        loginDate:Date;
        ipAddress:string;
        withDataSaving:boolean;
    }
    export interface UserVmSearchParamsViewModel {
    }
    export interface AdminUserVmSearchParamsViewModel extends UserVmSearchParamsViewModel {
        userId:string;
        virtualization:core.WebApi.Models.Enums.TypeVirtualization;
        createDateFrom:Date;
        createDateTo:Date;
    }
    export interface VmBackupViewModel {
        id:string;
        vmId:string;
        name:string;
        dateCreated:Date;
    }
    export interface VmWareVCenterViewModel {
        name:string;
        userName:string;
        password:string;
        serverAddress:string;
        id:string;
    }
    export interface WebHostingPaymentViewModel {
        date:Date;
        amount:number;
        webHostingName:string;
        webHostingId:string;
    }
    export interface WebHostingTariffViewModel {
        id:string;
        name:string;
        storageSizeGB:number;
        domainCount:number;
        ftpAccountCount:number;
        databaseCount:number;
        price:number;
        createDate:Date;
        lastUpdateDate:Date;
    }
    export interface WebHostingUpdateModel {
        name:string;
        autoProlongation:boolean;
    }
    export interface WebHostingViewModel {
        id:string;
        tariffName:string;
    }
    export interface SignUpModel {
        userName:string;
        password:string;
        confirmPassword:string;
    }


    export interface TaskEndNotify extends BaseNotify {
        task:TaskV2ViewModel;
        success:boolean;
        pypeError:TypeError;
        error:string;
    }

    export interface BaseNotify {
        userId:string;
        test:string;
        typeNotify:core.WebApi.Models.Enums.TypeNotify;
    }


    export class BaseOptions {

    }


    export class ConfigVmOptions extends BaseOptions {
        cpu:number;
        ram:number;
        hddGB:number;
        name:String;
    }

    export class CreateVmOptions extends ConfigVmOptions {
        operatingSystemId:number;
        userVmId:string;
    }

    export class UpdateVmOptions extends ConfigVmOptions {
        vmId:string;
    }

    export class RemoveVmOptions extends BaseOptions {
        vmId:string;
    }

    export class ChangeStatusOptions extends BaseOptions {
        typeChangeStatus:TypeChangeStatus;
        vmId:string;
    }

    export class BackupOptions extends BaseOptions {
        backupName:string;
        vmId:string;
        vmBackupId:string;
    }

    export class CreateSnapshotOptions extends BaseOptions {
        snapshotId:string;
        vmId:string;
        name:string;
    }

    export class DeleteSnapshotOptions extends BaseOptions {
        snapshotId:string;
        deleteWithChildrens:boolean;
        vmId:string;
    }

    export class LoadSnapshotOptions extends BaseOptions {
        snapshotId:string;
        vmId:string;
    }

    export interface UpdateSubscriptionBackupStoragePeriodModel {
        subscriptionId:string;
        newPeriodDays:number;
    }


    export class BaseGameServerOptions extends BaseOptions {
        public gameServerId:string;
    }

    export class CreateGameServerOptions extends BaseGameServerOptions {
        public slotCount:number;
        public gameServerFirstPortInRange:number;
    }


    export class DeleteGameServerOptions extends BaseGameServerOptions {

    }


    export class ChangeGameServerStatusOptions extends BaseGameServerOptions {
        public gameServerPassword:string;
        public typeChangeStatus:TypeChangeStatus
        public gameServerPort:number;
    }

    export interface LocationViewModel extends IResource<LocationViewModel> {
        id:string;
        name:string;
        description:string;
        createDate:Date;
        disabled:boolean;
    }
}


