module core.WebApi.Models {
    import GameFamily = core.WebApi.Models.Enums.GameFamily;
    import IResource = angular.resource.IResource;
    import GameServerState = core.WebApi.Models.Enums.GameServerState;
    import TypeChangeStatus = core.WebApi.Models.Enums.TypeChangeStatus;
    export class GameSimpleViewModel {
        /*[Required]*/
        public id:number;
        /*[Required]*/
        public name:string;
        /*[Required]*/
        public family:GameFamily;
        /*[Required]*/
        public version:string;
        public versionCode:string;
        /*[Required]*/
        public imageFileDescriptorId:number;
        public description:string;
        public disabled:boolean;
        public imageFileDescriptor:FileDescriptorViewModel;
    }

    export class GameServerTariffSimpleView {
        public id:number;
        public gameId:number;
        public slot:number;
        public name:string;
        public performance:number;
        public createDate:Date;
        public disabled:boolean;
    }
    export class GameServerTariffView extends GameServerTariffSimpleView {
        public game:GameSimpleViewModel;
    }
    export interface GameViewModel extends IResource<GameViewModel> {
        id:number;
        name:string;
        family:GameFamily;
        version:string;
        versionCode:string;
        imageFileDescriptorId:number;
        imageFileDescriptor:FileDescriptorViewModel;
        description:string;
        disabled:boolean;
        gameServerTariffs:Array<GameServerTariffSimpleView>;
    }

    export class GameServerViewModel {
        public id:string;
        /*[Required]*/
        public name:string;
        /*[Required]*/
        public gameServerTariffId:number;
        public gameHostId:number;
        /*[Required]*/
        public slotCount:number;
        public userId:string;
        public userName:string;
        public expireMonthCount:number;
        public gameId:number;

        public dateExpire:Date;

        public serverState:GameServerState;
        public autoProlongation:boolean;
        public firstPortInRange:number;
        public portRangeSize:number;
        public password:string;
        public gameServerTariff:GameServerTariffView;
        tasks:TaskV2ViewModel[];
    }


    export class GameServerChangeStatusViewModel {

        public serverId:string;
        public changeStatusType:TypeChangeStatus;
    }
}