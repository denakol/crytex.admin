module core {

    import IResource = angular.resource.IResource;
    export interface UserPaymentForecastViewModel extends IResource<UserPaymentForecastViewModel>{
        currentBalance: number;
        usageSubscriptionOneDayForecasts?: Array<SubscriptionPaymentForecastViewModel>
        usageSubscriptionsMonthForecasts?: Array<SubscriptionPaymentForecastViewModel>;
        fixedSubscriptionsMonthForecasts?: Array<SubscriptionPaymentForecastViewModel>;
        gameServerPaymentForecasts?: Array<GameServerPaymentForecastViewModel>;
        webHostingPaymentForecasts?: Array<WebHostingPaymentForecastViewModel>;
    }

    export interface PaymentForecastViewModelBase {
        paymentForecast: number;
    }

    export interface SubscriptionPaymentForecastViewModel extends PaymentForecastViewModelBase {
        subscriptionVmId: string;
    }

    export interface GameServerPaymentForecastViewModel extends PaymentForecastViewModelBase {
        gameServerId: string;
    }

    export interface WebHostingPaymentForecastViewModel extends PaymentForecastViewModelBase {
        webHostingId: string;
    }
}