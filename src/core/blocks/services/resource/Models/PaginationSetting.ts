module core {

    export interface PaginationSetting {
        pageNumber: number;
        pageSize: number;
    }
}

