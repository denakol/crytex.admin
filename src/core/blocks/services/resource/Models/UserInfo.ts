/**
 * Created by denak on 03.02.2016.
 */
module core {
    import IResource = angular.resource.IResource;
     import TypeUser = core.WebApi.Models.Enums.TypeUser;
    export interface UserInfo  extends IResource<UserInfo> {
         Id:string;
         userName:string;
         email:string;
         password:string;
         changePassword:boolean;
         phoneNumber:string;
         name:string;
         lastName:string;
         patronymic:string;
         city:string;
         country:string;
         address:string;
         codePhrase:string;
         userType:TypeUser;
         payer:string;
         contactPerson:string;
         balance:number;
         isBlocked:boolean;
    }
}