var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var core;
(function (core) {
    var WebApi;
    (function (WebApi) {
        var Models;
        (function (Models) {
            var MachineConfigUpdateViewModel = (function () {
                function MachineConfigUpdateViewModel() {
                }
                return MachineConfigUpdateViewModel;
            }());
            Models.MachineConfigUpdateViewModel = MachineConfigUpdateViewModel;
            var MachinePeriodDaysUpdateViewModel = (function () {
                function MachinePeriodDaysUpdateViewModel() {
                }
                return MachinePeriodDaysUpdateViewModel;
            }());
            Models.MachinePeriodDaysUpdateViewModel = MachinePeriodDaysUpdateViewModel;
            var SubscriptionBuyOptionsUserViewModel = (function () {
                function SubscriptionBuyOptionsUserViewModel() {
                }
                return SubscriptionBuyOptionsUserViewModel;
            }());
            Models.SubscriptionBuyOptionsUserViewModel = SubscriptionBuyOptionsUserViewModel;
            var GameServerBuyOptionsViewModel = (function () {
                function GameServerBuyOptionsViewModel() {
                }
                return GameServerBuyOptionsViewModel;
            }());
            Models.GameServerBuyOptionsViewModel = GameServerBuyOptionsViewModel;
            var BaseOptions = (function () {
                function BaseOptions() {
                }
                return BaseOptions;
            }());
            Models.BaseOptions = BaseOptions;
            var ConfigVmOptions = (function (_super) {
                __extends(ConfigVmOptions, _super);
                function ConfigVmOptions() {
                    _super.apply(this, arguments);
                }
                return ConfigVmOptions;
            }(BaseOptions));
            Models.ConfigVmOptions = ConfigVmOptions;
            var CreateVmOptions = (function (_super) {
                __extends(CreateVmOptions, _super);
                function CreateVmOptions() {
                    _super.apply(this, arguments);
                }
                return CreateVmOptions;
            }(ConfigVmOptions));
            Models.CreateVmOptions = CreateVmOptions;
            var UpdateVmOptions = (function (_super) {
                __extends(UpdateVmOptions, _super);
                function UpdateVmOptions() {
                    _super.apply(this, arguments);
                }
                return UpdateVmOptions;
            }(ConfigVmOptions));
            Models.UpdateVmOptions = UpdateVmOptions;
            var RemoveVmOptions = (function (_super) {
                __extends(RemoveVmOptions, _super);
                function RemoveVmOptions() {
                    _super.apply(this, arguments);
                }
                return RemoveVmOptions;
            }(BaseOptions));
            Models.RemoveVmOptions = RemoveVmOptions;
            var ChangeStatusOptions = (function (_super) {
                __extends(ChangeStatusOptions, _super);
                function ChangeStatusOptions() {
                    _super.apply(this, arguments);
                }
                return ChangeStatusOptions;
            }(BaseOptions));
            Models.ChangeStatusOptions = ChangeStatusOptions;
            var BackupOptions = (function (_super) {
                __extends(BackupOptions, _super);
                function BackupOptions() {
                    _super.apply(this, arguments);
                }
                return BackupOptions;
            }(BaseOptions));
            Models.BackupOptions = BackupOptions;
            var CreateSnapshotOptions = (function (_super) {
                __extends(CreateSnapshotOptions, _super);
                function CreateSnapshotOptions() {
                    _super.apply(this, arguments);
                }
                return CreateSnapshotOptions;
            }(BaseOptions));
            Models.CreateSnapshotOptions = CreateSnapshotOptions;
            var DeleteSnapshotOptions = (function (_super) {
                __extends(DeleteSnapshotOptions, _super);
                function DeleteSnapshotOptions() {
                    _super.apply(this, arguments);
                }
                return DeleteSnapshotOptions;
            }(BaseOptions));
            Models.DeleteSnapshotOptions = DeleteSnapshotOptions;
            var LoadSnapshotOptions = (function (_super) {
                __extends(LoadSnapshotOptions, _super);
                function LoadSnapshotOptions() {
                    _super.apply(this, arguments);
                }
                return LoadSnapshotOptions;
            }(BaseOptions));
            Models.LoadSnapshotOptions = LoadSnapshotOptions;
            var BaseGameServerOptions = (function (_super) {
                __extends(BaseGameServerOptions, _super);
                function BaseGameServerOptions() {
                    _super.apply(this, arguments);
                }
                return BaseGameServerOptions;
            }(BaseOptions));
            Models.BaseGameServerOptions = BaseGameServerOptions;
            var CreateGameServerOptions = (function (_super) {
                __extends(CreateGameServerOptions, _super);
                function CreateGameServerOptions() {
                    _super.apply(this, arguments);
                }
                return CreateGameServerOptions;
            }(BaseGameServerOptions));
            Models.CreateGameServerOptions = CreateGameServerOptions;
            var DeleteGameServerOptions = (function (_super) {
                __extends(DeleteGameServerOptions, _super);
                function DeleteGameServerOptions() {
                    _super.apply(this, arguments);
                }
                return DeleteGameServerOptions;
            }(BaseGameServerOptions));
            Models.DeleteGameServerOptions = DeleteGameServerOptions;
            var ChangeGameServerStatusOptions = (function (_super) {
                __extends(ChangeGameServerStatusOptions, _super);
                function ChangeGameServerStatusOptions() {
                    _super.apply(this, arguments);
                }
                return ChangeGameServerStatusOptions;
            }(BaseGameServerOptions));
            Models.ChangeGameServerStatusOptions = ChangeGameServerStatusOptions;
        })(Models = WebApi.Models || (WebApi.Models = {}));
    })(WebApi = core.WebApi || (core.WebApi = {}));
})(core || (core = {}));
