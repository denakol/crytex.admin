var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var core;
(function (core) {
    var WebApi;
    (function (WebApi) {
        var Models;
        (function (Models) {
            var GameSimpleViewModel = (function () {
                function GameSimpleViewModel() {
                }
                return GameSimpleViewModel;
            }());
            Models.GameSimpleViewModel = GameSimpleViewModel;
            var GameServerTariffSimpleView = (function () {
                function GameServerTariffSimpleView() {
                }
                return GameServerTariffSimpleView;
            }());
            Models.GameServerTariffSimpleView = GameServerTariffSimpleView;
            var GameServerTariffView = (function (_super) {
                __extends(GameServerTariffView, _super);
                function GameServerTariffView() {
                    _super.apply(this, arguments);
                }
                return GameServerTariffView;
            }(GameServerTariffSimpleView));
            Models.GameServerTariffView = GameServerTariffView;
            var GameServerViewModel = (function () {
                function GameServerViewModel() {
                }
                return GameServerViewModel;
            }());
            Models.GameServerViewModel = GameServerViewModel;
            var GameServerChangeStatusViewModel = (function () {
                function GameServerChangeStatusViewModel() {
                }
                return GameServerChangeStatusViewModel;
            }());
            Models.GameServerChangeStatusViewModel = GameServerChangeStatusViewModel;
        })(Models = WebApi.Models || (WebApi.Models = {}));
    })(WebApi = core.WebApi || (core.WebApi = {}));
})(core || (core = {}));
