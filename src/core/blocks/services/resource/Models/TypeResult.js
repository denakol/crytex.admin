/**
 * Created by denak on 11.02.2016.
 */
var core;
(function (core) {
    (function (ServerTypesResult) {
        ServerTypesResult[ServerTypesResult["ServerError"] = 0] = "ServerError";
        ServerTypesResult[ServerTypesResult["UserExist"] = 1] = "UserExist";
        ServerTypesResult[ServerTypesResult["IncorrectPassword"] = 2] = "IncorrectPassword";
        ServerTypesResult[ServerTypesResult["UserBlocked"] = 3] = "UserBlocked";
        ServerTypesResult[ServerTypesResult["NotValidateEmail"] = 4] = "NotValidateEmail";
        ServerTypesResult[ServerTypesResult["NotEnoughMoney"] = 5] = "NotEnoughMoney";
    })(core.ServerTypesResult || (core.ServerTypesResult = {}));
    var ServerTypesResult = core.ServerTypesResult;
})(core || (core = {}));
