/**
 * Created by denak on 03.02.2016.
 */
module core {

    import IResource = angular.resource.IResource;

    export interface PageModel<T> extends IResource<PageModel<T>> {
        items:T[];
        totalPages:number;
        totalRows:number;
    }
}

