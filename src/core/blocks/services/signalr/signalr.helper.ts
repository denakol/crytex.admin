/**
 * Created by denak on 09.03.2016.
 */
module core.services.signalr {


    export class JsonHelper {


        public static ToSmallCase(obj:any) {

            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    if (prop.substring(0, 1).toLowerCase() != prop.substring(0, 1)) {
                        if (typeof obj[prop] == "object") {
                            this.ToSmallCase(obj[prop]);
                        }
                        obj[prop.substring(0, 1).toLowerCase() + prop.substring(1)] = obj[prop];
                        delete obj[prop];
                    }
                }
            }

        }
    }
}