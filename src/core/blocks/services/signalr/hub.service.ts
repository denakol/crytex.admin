/**
 * Created by denak on 09.03.2016.
 */
    module core.services.signalr {




        var Hub =function Hub() {
            var globalConnections:any[] = [];

            function initNewConnection(options:any) {
                var connection:any = null;
                if (options && options.rootPath) {
                    connection = $.hubConnection(options.rootPath);
                } else {
                    connection = $.hubConnection();
                }

                connection.logging = (options && options.logging ? true : false);
                return connection;
            }

            function getConnection(options:any) {
                var useSharedConnection = !(options && options.useSharedConnection === false);
                if (useSharedConnection) {
                    return typeof globalConnections[options.rootPath] === 'undefined' ?
                        globalConnections[options.rootPath] = initNewConnection(options) :
                        globalConnections[options.rootPath];
                }
                else {
                    return initNewConnection(options);
                }
            }

            return function (hubName:any, options:any) {
                var Hub = this;

                Hub.connection = getConnection(options);
                Hub.proxy = Hub.connection.createHubProxy(hubName);

                Hub.on = function (event:any, fn:any) {
                    Hub.proxy.on(event, fn);
                };
                Hub.invoke = function (method:any, args:any) {
                    return Hub.proxy.invoke.apply(Hub.proxy, arguments);
                };
                Hub.disconnect = function () {
                    Hub.connection.stop();
                };
                Hub.connect = function () {
                    return Hub.connection.start(options.transport ? {transport: options.transport} : null);
                };

                if (options && options.listeners) {
                    Object.getOwnPropertyNames(options.listeners)
                        .filter(function (propName) {
                            return typeof options.listeners[propName] === 'function';
                        })
                        .forEach(function (propName) {
                            var callback =options.listeners[propName];
                            var f= function (obj:any) {
                                JsonHelper.ToSmallCase(obj);
                                callback(obj);
                            }
                            Hub.on(propName, f);
                        });
                }
                if (options && options.methods) {
                    angular.forEach(options.methods, function (method) {
                        Hub[method] = function () {
                            var args = $.makeArray(arguments);
                            args.unshift(method);
                            return Hub.invoke.apply(Hub, args);
                        };
                    });
                }
                if (options && options.queryParams) {
                    Hub.connection.qs = options.queryParams;
                }
                if (options && options.errorHandler) {
                    Hub.connection.error(options.errorHandler);
                }
                //DEPRECATED
                //Allow for the user of the hub to easily implement actions upon disconnected.
                //e.g. : Laptop/PC sleep and reopen, one might want to automatically reconnect
                //by using the disconnected event on the connection as the starting point.
                if (options && options.hubDisconnected) {
                    Hub.connection.disconnected(options.hubDisconnected);
                }
                if (options && options.stateChanged) {
                    Hub.connection.stateChanged(options.stateChanged);
                }

                //Adding additional property of promise allows to access it in rest of the application.
                Hub.promise = Hub.connect();
                return Hub;
            };
        };

        angular.module("blocks.services")
            .factory("hub", Hub);

    }




