/**
 * Created by denak on 09.03.2016.
 */
var core;
(function (core) {
    var services;
    (function (services) {
        var signalr;
        (function (signalr) {
            var JsonHelper = (function () {
                function JsonHelper() {
                }
                JsonHelper.ToSmallCase = function (obj) {
                    for (var prop in obj) {
                        if (obj.hasOwnProperty(prop)) {
                            if (prop.substring(0, 1).toLowerCase() != prop.substring(0, 1)) {
                                if (typeof obj[prop] == "object") {
                                    this.ToSmallCase(obj[prop]);
                                }
                                obj[prop.substring(0, 1).toLowerCase() + prop.substring(1)] = obj[prop];
                                delete obj[prop];
                            }
                        }
                    }
                };
                return JsonHelper;
            }());
            signalr.JsonHelper = JsonHelper;
        })(signalr = services.signalr || (services.signalr = {}));
    })(services = core.services || (core.services = {}));
})(core || (core = {}));
