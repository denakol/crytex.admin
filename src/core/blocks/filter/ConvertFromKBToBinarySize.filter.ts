/**
 * Created by denak on 02.03.2016.
 */
module core {
    
    
    var filterFromMb = ()=>{
        return (val:any)=> {
            if(val==undefined)
            {
                return "";
            }
            if (val > 1000)
                return (val / 1000).toFixed(0) + " Gb";
            else

                return (val ).toFixed(0) + " Mb";

        }; 
    };
    var filterfromGb = ()=>{
        return (val:any)=> {
            if(val==undefined)
            {
                return "";
            }
            if (val > 1000)
                return (val / 1000).toFixed(0) + " TB";
            else

                return (val ).toFixed(0) + " Gb";

        };
    };
    angular.module("crytex.blocks").filter('convertFromMb', filterFromMb);
    angular.module("crytex.blocks").filter('convertFromGb', filterfromGb);
}