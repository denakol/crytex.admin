/**
 * Created by denak on 02.03.2016.
 */
var core;
(function (core) {
    var filterFromMb = function () {
        return function (val) {
            if (val == undefined) {
                return "";
            }
            if (val > 1000)
                return (val / 1000).toFixed(0) + " Gb";
            else
                return (val).toFixed(0) + " Mb";
        };
    };
    var filterfromGb = function () {
        return function (val) {
            if (val == undefined) {
                return "";
            }
            if (val > 1000)
                return (val / 1000).toFixed(0) + " TB";
            else
                return (val).toFixed(0) + " Gb";
        };
    };
    angular.module("crytex.blocks").filter('convertFromMb', filterFromMb);
    angular.module("crytex.blocks").filter('convertFromGb', filterfromGb);
})(core || (core = {}));
