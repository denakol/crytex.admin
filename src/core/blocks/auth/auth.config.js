﻿(function () {

    angular.module("blocks.auth").config(config).run(run);

    config.$inject = ['$httpProvider'];

    function config($httpProvider) {

        $httpProvider.interceptors.push('AuthInterceptorService');

    };

    run.$inject = ['AuthService'];

    function run(authService) {

        authService.fillAuthData();

    };

})();