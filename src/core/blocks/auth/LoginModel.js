/**
 * Created by denak on 27.01.2016.
 */
var core;
(function (core) {
    var LoginModel = (function () {
        function LoginModel() {
            this.useRefreshToken = true;
        }
        return LoginModel;
    }());
    core.LoginModel = LoginModel;
})(core || (core = {}));
