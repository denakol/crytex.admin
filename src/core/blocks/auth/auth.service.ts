/**
 * Created by denak on 26.01.2016.
 */
/// <reference path="../../_core.ts" />
module core {
    import IHttpService = angular.IHttpService;
    import IQService = angular.IQService;
    import ILocaleService = angular.ILocaleService;
    import IPromise = angular.IPromise;
    import INamespacedStoreService = angular.a0.storage.INamespacedStoreService;
    import AuthData = core.AuthData;
    import LoginModel = core.LoginModel;
    import ConfirmEmailModel = core.ConfirmEmailModel;
    import IAauthService = core.IAauthService;

    "use strict";
    import ApplicationUserViewModel = core.WebApi.Models.ApplicationUserViewModel;
    import IDeferred = angular.IDeferred;


    class authService implements IAauthService {

        private gettingRefreshToken:boolean = false;
        private pendingGettingRequest:IDeferred<boolean>[] = [];

        changePassword(oldPassword:string, newPassword:string):angular.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/ChangePassword", {
                oldPassword: oldPassword,
                newPassword: newPassword
            }).success((response)=> {

                deferred.resolve(response);

            }).catch((error) => {

                deferred.reject(error.data);

            });
            return deferred.promise;
        }

        changeNumberRequest(number:string) {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/AddPhoneNumber", {number: number}).success((response)=> {

                deferred.resolve(response);

            }).catch((error) => {

                deferred.reject(error.data);

            });
            return deferred.promise;

        }

        changeNumber(number:ChangeNumber) {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/VerifyPhoneNumber", number).success((response)=> {

                deferred.resolve(response);

            }).catch((error) => {

                deferred.reject(error.data);

            });
            return deferred.promise;
        }


        fillRegisterData(user:core.FullRegistrationUser):angular.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/UpdateUserInfo?userId=" + this.authentication.userId, user).success((response)=> {
                var i = this.authentication.roles.indexOf('FirstStepRegister');
                this.authentication.roles.splice(i, 1);
                this.authentication.roles.push('User');
                deferred.resolve(response);

            }).catch((error) => {

                deferred.reject(error.data);

            });
            return deferred.promise;
        }

        updateUser(user:ApplicationUserViewModel):angular.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/EditUser?userId=" + this.authentication.userId, user).success((response)=> {
                deferred.resolve(response);
            }).catch((error) => {
                deferred.reject(error.data);
            });
            return deferred.promise;
        }

        resetPassword(user:core.ResetPassword):angular.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/CreateNewPassword", user).success((response)=> {

                deferred.resolve(response);

            }).catch((error) => {

                deferred.reject(error.data);

            });
            return deferred.promise;
        }

        resetPasswordRequest(email:string):angular.IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/ResetPassword", {email: email}).success((response)=> {

                deferred.resolve(response);

            }).catch((error) => {

                deferred.reject(error.data);

            });
            return deferred.promise;
        }

        signUp(user:LoginModel):IPromise<any> {

            var deferred = this.$q.defer();

            this.signOut().then(()=> {

                this.$http.post(this.serviceBase + "api/account/register", user).then((response)=> {

                    deferred.resolve(response);

                }).catch((error) => {

                    deferred.reject(error);
                });

            }).catch((error)=> {

                deferred.reject(error);
            });

            return deferred.promise;
        }


        signIn(user:LoginModel):IPromise<any> {
            var deferred = this.$q.defer();

            this.signOut().then(() => {

                var data =
                    "grant_type=password" +
                    "&username=" + user.userName +
                    "&password=" + user.password;

                if (user.useRefreshToken) {
                    data = data + "&client_id=" + this.clientId;
                }

                this.$http.post(this.serviceBase + "token", data, {headers: {"Content-Type": "application/x-www-form-urlencoded"}}).success((response:any)=> {


                    var authData:AuthData =
                    {
                        token: response.access_token,
                        userName: response.userName,
                        userId: response.userId,
                        refreshToken: user.useRefreshToken ? response.refresh_token : "",
                        useRefreshToken: user.useRefreshToken,
                        roles: response.roles.split(","),
                        isAuth: true,
                        isAvailableVmBilling: false,
                        isRequestTestPeriod: false
                    }
                    this.localStorageService.set("authorizationData", authData);

                    this.fillAuthData();

                    deferred.resolve(response);

                }).catch((error)=> {

                    deferred.reject(error.data);

                });

            }).catch((error)=> {

                deferred.reject(error);

            });

            return deferred.promise;
        }

        removeAuthorizationData() {
            this.localStorageService.remove("authorizationData");
            this.authentication.isAuth = false;
            this.authentication.userName = "";
            this.authentication.useRefreshToken = false;
            this.authentication.userId = "";
            this.authentication.roles = [];
        }

        signOut():IPromise<any> {


            var deferred = this.$q.defer();

            var authData = this.localStorageService.get("authorizationData");
            if (authData) {

                if (authData.useRefreshToken) {

                    var params = {
                        userName: authData.userName,
                        refreshToken: authData.refreshToken
                    };

                    this.$http.post(this.serviceBase + "api/User/account/removeRefreshToken", params).then((response)=> {

                        this.removeAuthorizationData();
                        deferred.resolve();

                    }).catch((error)=> {

                        deferred.reject(error);

                    });

                } else {

                    this.removeAuthorizationData();
                    deferred.resolve();
                }

            } else {

                deferred.resolve();

            }

            return deferred.promise;
        }

        verify(confirmEmail:core.ConfirmEmailModel):IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/ConfirmEmail", confirmEmail).success((response)=> {

                deferred.resolve(response);

            }).catch((error) => {

                deferred.reject(error.data);

            });
            return deferred.promise;
        }

        refreshToken():IPromise<any> {
            var deferred:IDeferred<boolean> = this.$q.defer();

            var authData = this.localStorageService.get("authorizationData");

            if (this.gettingRefreshToken) {
                this.pendingGettingRequest.push(deferred);
            } else if (authData && authData.useRefreshToken) {
                this.gettingRefreshToken = true;
                var data =
                    "grant_type=refresh_token" +
                    "&refresh_token=" + authData.refreshToken +
                    "&client_id=" + this.clientId;

                this.localStorageService.remove("authorizationData");

                this.$http.post(this.serviceBase + "token", data, {headers: {"Content-Type": "application/x-www-form-urlencoded"}}).success((response:any)=> {
                    this.gettingRefreshToken = false;
                    this.localStorageService.set("authorizationData", {
                        token: response.access_token,
                        userName: response.userName,
                        refreshToken: response.refresh_token,
                        useRefreshToken: true,
                        roles: response.roles.split(',')
                    });

                    deferred.resolve(true);

                    for (var def of this.pendingGettingRequest) {
                        def.resolve(false);
                    }
                }).error((err) => {
                    this.gettingRefreshToken = false;

                    var error = ()=> {
                        deferred.reject(true);
                        for (var def of this.pendingGettingRequest) {
                            def.reject(false);
                        }
                    }

                    this.signOut().then(error).catch(error);

                });

            } else {

                deferred.reject();

            }

            return deferred.promise;
        }

        isInRole(role:string):boolean {
            if (this.authentication.roles.indexOf(role) > -1) {
                return true;
            }
            return false;
        }

        registerFirstStep(user:core.LoginModel):IPromise<any> {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/Register", user).success((response) => {

                deferred.resolve(response);

            }).catch((error) => {

                deferred.reject(error.data);

            });
            return deferred.promise;
        }


        static $inject = ['$http', '$q', 'localStorageService', 'BASE_INFO'];
        public authentication:AuthData = new core.AuthData();


        private serviceBase:string;
        private clientId:string;

        private  fillAuthData() {
            var authData = this.localStorageService.get("authorizationData");
            if (authData) {
                this.authentication.isAuth = true;
                this.authentication.userId = authData.userId;
                this.authentication.userName = authData.userName;
                this.authentication.useRefreshToken = authData.useRefreshToken;
                this.authentication.roles = authData.roles;
            }

        };

        constructor(private $http:IHttpService, private $q:IQService, private localStorageService:INamespacedStoreService, BASE_INFO:any) {

            this.fillAuthData();

            this.serviceBase = BASE_INFO.URL + BASE_INFO.PORT + "/";
            this.clientId = BASE_INFO.CLIENT_ID;


        }
    }
    angular.module("blocks.auth")
        .service('AuthService', authService);

}
