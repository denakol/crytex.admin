/**
 * Created by denak on 02.02.2016.
 */
declare module core {
    class ChangeNumber {
        code: string;
        phoneNumber: string;
        constructor(code: string, phoneNumber: string);
    }
}
