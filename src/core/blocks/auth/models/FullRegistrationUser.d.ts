declare module core {
    class FullRegistrationUser {
        name: string;
        userType: UserType;
        lastName: string;
        patronymic: string;
        country: string;
        city: string;
        region: string;
        address: string;
        codePhrase: string;
        constructor();
    }
 
}
