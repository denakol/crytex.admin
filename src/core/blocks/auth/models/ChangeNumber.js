/**
 * Created by denak on 02.02.2016.
 */
var core;
(function (core) {
    var ChangeNumber = (function () {
        function ChangeNumber(code, phoneNumber) {
            this.code = code;
            this.phoneNumber = phoneNumber;
        }
        return ChangeNumber;
    }());
    core.ChangeNumber = ChangeNumber;
})(core || (core = {}));
