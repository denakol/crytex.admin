module core {
  import TypeUser = core.WebApi.Models.Enums.TypeUser;
  export class FullRegistrationUser {

    public name: string;
    public userType:TypeUser;
    public lastName: string;
    public patronymic: string;
    public country: string;
    public city: string;
    public region: string;
    public address: string;
    public codePhrase: string;
    constructor() {

    }
  }

}
