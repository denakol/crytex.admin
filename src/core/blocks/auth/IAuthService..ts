
/**
 * Created by denak on 26.01.2016.
 */
module core {
    import IPromise = angular.IPromise;
    import ApplicationUserViewModel = core.WebApi.Models.ApplicationUserViewModel;
    export interface IAauthService {
        signUp (user:LoginModel):IPromise<any>;
        signIn(user:LoginModel):IPromise<any>;
        signOut():IPromise<any>;
        verify(confirmEmail:ConfirmEmailModel):IPromise<any>;
        refreshToken():IPromise<any>;
        isInRole(role:string):boolean;
        registerFirstStep(user:LoginModel):IPromise<any>;
        authentication:AuthData;
        resetPassword(user:core.ResetPassword):IPromise<any>;
        resetPasswordRequest(email:string):IPromise<any>;
        fillRegisterData (user:FullRegistrationUser):IPromise<any>;
        updateUser(user:ApplicationUserViewModel):angular.IPromise<any>;
        changeNumberRequest (number:string):IPromise<any>;
        changeNumber (change:ChangeNumber):IPromise<any>;
        changePassword(oldPassword:string,newPassword:string):IPromise<any>;
    }
}
