var core;
(function (core) {
    var ResetPassword = (function () {
        function ResetPassword(userId, code, password) {
            this.userId = userId;
            this.code = code;
            this.password = password;
        }
        return ResetPassword;
    }());
    core.ResetPassword = ResetPassword;
})(core || (core = {}));
