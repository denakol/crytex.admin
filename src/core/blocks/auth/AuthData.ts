/**
 * Created by denak on 27.01.2016.
 */
module core {
    export class AuthData {
        isAuth:boolean = false;
        isAvailableVmBilling: boolean = false;
        isRequestTestPeriod: boolean = false;
        userName:string = "";
        useRefreshToken:boolean = false;
        userId:string = "";
        roles:string[]=[];
        token:string="";
        refreshToken:string="";
    }


}