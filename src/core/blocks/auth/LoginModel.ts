/**
 * Created by denak on 27.01.2016.
 */
module core{
    export class LoginModel{
        userName:string;
        password:string;
        useRefreshToken:boolean=true;
    }
}