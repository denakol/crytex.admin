/**
 * Created by denak on 27.01.2016.
 */
var core;
(function (core) {
    var AuthData = (function () {
        function AuthData() {
            this.isAuth = false;
            this.isAvailableVmBilling = false;
            this.isRequestTestPeriod = false;
            this.userName = "";
            this.useRefreshToken = false;
            this.userId = "";
            this.roles = [];
            this.token = "";
            this.refreshToken = "";
        }
        return AuthData;
    }());
    core.AuthData = AuthData;
})(core || (core = {}));
