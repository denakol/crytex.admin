(function() {

    angular.module("blocks.auth")
        .factory('AuthInterceptorService', authInterceptorService);

    authInterceptorService.$inject = ['$q', '$injector'];

    function authInterceptorService($q, $injector) {

        var $http;
        var $state;
        var authService;
        var localStorageService;

        var request = function (config) {

            config.headers = config.headers || {};
                      config.headers["X-Requested-With"] = "XMLHttpRequest";

            localStorageService = localStorageService || $injector.get("localStorageService");
            var authData = localStorageService.get("authorizationData");

            if (authData) {

                config.headers.Authorization = "Bearer " + authData.token;
            }

            return config;
        }

        var retryHttpRequest = function (config, deferred) {

            $http = $http || $injector.get("$http");

            $http(config).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });

        }

        var responseError = function (rejection) {

            var deferred = $q.defer();

            if (rejection.status === 401) {
                authService = authService || $injector.get("AuthService");
                authService.refreshToken().then(function (response) {

                    retryHttpRequest(rejection.config, deferred);

                }, function (result) {

                    if(result){
                        authService.signOut().then(function () {

                            $state = $state || $injector.get("$state");
                            $state.go("home", null, { reload: true });

                        }).finally(function () {
                            deferred.reject(rejection);
                        });
                    }
                    else {
                        deferred.reject(rejection);
                    }


                });

            } else {

                deferred.reject(rejection);
            }

            return deferred.promise;
        }

        var authInterceptorServiceFactory = {};

        authInterceptorServiceFactory.request = request;
        authInterceptorServiceFactory.responseError = responseError;

        return authInterceptorServiceFactory;
    }

})();