/**
 * Created by denak on 26.01.2016.
 */
/// <reference path="../../_core.ts" />
var core;
(function (core) {
    "use strict";
    var authService = (function () {
        function authService($http, $q, localStorageService, BASE_INFO) {
            this.$http = $http;
            this.$q = $q;
            this.localStorageService = localStorageService;
            this.gettingRefreshToken = false;
            this.pendingGettingRequest = [];
            this.authentication = new core.AuthData();
            this.fillAuthData();
            this.serviceBase = BASE_INFO.URL + BASE_INFO.PORT + "/";
            this.clientId = BASE_INFO.CLIENT_ID;
        }
        authService.prototype.changePassword = function (oldPassword, newPassword) {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/ChangePassword", {
                oldPassword: oldPassword,
                newPassword: newPassword
            }).success(function (response) {
                deferred.resolve(response);
            }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
        authService.prototype.changeNumberRequest = function (number) {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/AddPhoneNumber", { number: number }).success(function (response) {
                deferred.resolve(response);
            }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
        authService.prototype.changeNumber = function (number) {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/VerifyPhoneNumber", number).success(function (response) {
                deferred.resolve(response);
            }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
        authService.prototype.fillRegisterData = function (user) {
            var _this = this;
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/UpdateUserInfo?userId=" + this.authentication.userId, user).success(function (response) {
                var i = _this.authentication.roles.indexOf('FirstStepRegister');
                _this.authentication.roles.splice(i, 1);
                _this.authentication.roles.push('User');
                deferred.resolve(response);
            }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
        authService.prototype.updateUser = function (user) {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/EditUser?userId=" + this.authentication.userId, user).success(function (response) {
                deferred.resolve(response);
            }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
        authService.prototype.resetPassword = function (user) {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/CreateNewPassword", user).success(function (response) {
                deferred.resolve(response);
            }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
        authService.prototype.resetPasswordRequest = function (email) {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/ResetPassword", { email: email }).success(function (response) {
                deferred.resolve(response);
            }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
        authService.prototype.signUp = function (user) {
            var _this = this;
            var deferred = this.$q.defer();
            this.signOut().then(function () {
                _this.$http.post(_this.serviceBase + "api/account/register", user).then(function (response) {
                    deferred.resolve(response);
                }).catch(function (error) {
                    deferred.reject(error);
                });
            }).catch(function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
        authService.prototype.signIn = function (user) {
            var _this = this;
            var deferred = this.$q.defer();
            this.signOut().then(function () {
                var data = "grant_type=password" +
                    "&username=" + user.userName +
                    "&password=" + user.password;
                if (user.useRefreshToken) {
                    data = data + "&client_id=" + _this.clientId;
                }
                _this.$http.post(_this.serviceBase + "token", data, { headers: { "Content-Type": "application/x-www-form-urlencoded" } }).success(function (response) {
                    var authData = {
                        token: response.access_token,
                        userName: response.userName,
                        userId: response.userId,
                        refreshToken: user.useRefreshToken ? response.refresh_token : "",
                        useRefreshToken: user.useRefreshToken,
                        roles: response.roles.split(","),
                        isAuth: true,
                        isAvailableVmBilling: false,
                        isRequestTestPeriod: false
                    };
                    _this.localStorageService.set("authorizationData", authData);
                    _this.fillAuthData();
                    deferred.resolve(response);
                }).catch(function (error) {
                    deferred.reject(error.data);
                });
            }).catch(function (error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
        authService.prototype.removeAuthorizationData = function () {
            this.localStorageService.remove("authorizationData");
            this.authentication.isAuth = false;
            this.authentication.userName = "";
            this.authentication.useRefreshToken = false;
            this.authentication.userId = "";
            this.authentication.roles = [];
        };
        authService.prototype.signOut = function () {
            var _this = this;
            var deferred = this.$q.defer();
            var authData = this.localStorageService.get("authorizationData");
            if (authData) {
                if (authData.useRefreshToken) {
                    var params = {
                        userName: authData.userName,
                        refreshToken: authData.refreshToken
                    };
                    this.$http.post(this.serviceBase + "api/User/account/removeRefreshToken", params).then(function (response) {
                        _this.removeAuthorizationData();
                        deferred.resolve();
                    }).catch(function (error) {
                        deferred.reject(error);
                    });
                }
                else {
                    this.removeAuthorizationData();
                    deferred.resolve();
                }
            }
            else {
                deferred.resolve();
            }
            return deferred.promise;
        };
        authService.prototype.verify = function (confirmEmail) {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/ConfirmEmail", confirmEmail).success(function (response) {
                deferred.resolve(response);
            }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
        authService.prototype.refreshToken = function () {
            var _this = this;
            var deferred = this.$q.defer();
            var authData = this.localStorageService.get("authorizationData");
            if (this.gettingRefreshToken) {
                this.pendingGettingRequest.push(deferred);
            }
            else if (authData && authData.useRefreshToken) {
                this.gettingRefreshToken = true;
                var data = "grant_type=refresh_token" +
                    "&refresh_token=" + authData.refreshToken +
                    "&client_id=" + this.clientId;
                this.localStorageService.remove("authorizationData");
                this.$http.post(this.serviceBase + "token", data, { headers: { "Content-Type": "application/x-www-form-urlencoded" } }).success(function (response) {
                    _this.gettingRefreshToken = false;
                    _this.localStorageService.set("authorizationData", {
                        token: response.access_token,
                        userName: response.userName,
                        refreshToken: response.refresh_token,
                        useRefreshToken: true,
                        roles: response.roles.split(',')
                    });
                    deferred.resolve(true);
                    for (var _i = 0, _a = _this.pendingGettingRequest; _i < _a.length; _i++) {
                        var def = _a[_i];
                        def.resolve(false);
                    }
                }).error(function (err) {
                    _this.gettingRefreshToken = false;
                    var error = function () {
                        deferred.reject(true);
                        for (var _i = 0, _a = _this.pendingGettingRequest; _i < _a.length; _i++) {
                            var def = _a[_i];
                            def.reject(false);
                        }
                    };
                    _this.signOut().then(error).catch(error);
                });
            }
            else {
                deferred.reject();
            }
            return deferred.promise;
        };
        authService.prototype.isInRole = function (role) {
            if (this.authentication.roles.indexOf(role) > -1) {
                return true;
            }
            return false;
        };
        authService.prototype.registerFirstStep = function (user) {
            var deferred = this.$q.defer();
            this.$http.post(this.serviceBase + "api/User/Account/Register", user).success(function (response) {
                deferred.resolve(response);
            }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
        authService.prototype.fillAuthData = function () {
            var authData = this.localStorageService.get("authorizationData");
            if (authData) {
                this.authentication.isAuth = true;
                this.authentication.userId = authData.userId;
                this.authentication.userName = authData.userName;
                this.authentication.useRefreshToken = authData.useRefreshToken;
                this.authentication.roles = authData.roles;
            }
        };
        ;
        authService.$inject = ['$http', '$q', 'localStorageService', 'BASE_INFO'];
        return authService;
    }());
    angular.module("blocks.auth")
        .service('AuthService', authService);
})(core || (core = {}));
