﻿﻿(function () {
    // Давайте писать в алфавитном порядке, за исключением певрого
    angular.module("blocks.constantsService")
        .constant('BASE_INFO', {
            "URL": "http://51.254.55.137", //"http://localhost",  http://51.254.55.137
            "PORT": ":5555", //3054  5555
            "API_URL": "/api",
            "CLIENT_ID": "CrytexAngularApp"
        })        

        //.constant("BillingTransactionType", {
        //    "AutomaticDebiting": 0,
        //    "Crediting": 1,
        //    "OneTimeDebiting": 2,
        //    "SystemBonus": 3,
        //    "ReplenishmentFromAdmin": 4,
        //    "WithdrawByAdmin": 5,
        //    "TestPeriod": 6,
        //    "Refill": 7,
        //    arrayItems: [
        //        { name: "Automatic Debiting", value: 0 , translate: "Автоматическое"},
        //        { name: "Crediting", value: 1 , translate: "Кредит"},
        //        { name: "One Time Debiting", value: 2 , translate: "Покупка"},
        //        { name: "System Bonus", value: 3 , translate: "Бонус"},
        //        { name: "ReplenishmentFromAdmin", value: 4 , translate: "Пополнение от администратора"},
        //        { name: "WithdrawByAdmin", value: 5 , translate: "Снято администратором"},
        //        { name: "TestPeriod", value: 6 , translate: "Тестовый период"},
        //        { name: "Refill", value: 7 , translate: "Пополнение"}
        //    ]
        //})

        .constant("BillingTransactionType", {
            "BalanceReplenishment": 0,
            "WebHostingPayment": 1,
            "FixedSubscriptionVmPayment": 2,
            "UsageSubscriptionVmPayment": 3,
            "PhysicalServerPayment": 5,
            "GameServer": 6,
            "TestPeriod": 7,
            "ReturnMoneyForDeletedService": 8,
            arrayItems: [
                { name: "BalanceReplenishment", value: 0 , translate: "Пополнение баланса"},
                { name: "WebHostingPayment", value: 1 , translate: "Оплата веб хостинга"},
                { name: "FixedSubscriptionVmPayment", value: 2 , translate: "Оплата VPS сервера"},
                { name: "UsageSubscriptionVmPayment", value: 3 , translate: "Оплата облачного сервера"},
                { name: "PhysicalServerPayment", value: 4 , translate: "Оплата физического сервера"},
                { name: "GameServer", value: 5 , translate: "Игровой сервер"},
                { name: "TestPeriod", value: 6 , translate: "Тестовый период"},
                { name: "ReturnMoneyForDeletedService", value: 7 , translate: "Вовзрат денег за удалённый сервис"}
            ]
        })

        .constant("CountingPeriodType", {
            "Day": 0,
            "Week": 1,
            "Month": 2,
            "Year": 3,
            arrayItems: [
                { name: "Day", value: 0 , translate: "День"},
                { name: "Week", value: 1 , translate: "Неделя"},
                { name: "Month", value: 2 , translate: "Месяц"},
                { name: "Year", value: 3 , translate: "Год"}
            ]
        })

        .constant("FileType", {
            "Image": 0,
            "Loader": 1,
            "Document": 2,
            "HelpDeskRequestAttachment": 3,
            arrayItems: [
                { name: "Image", value: 0 , translate: "Изображение"},
                { name: "Loader", value: 1 , translate: "Загрузка"},
                { name: "Document", value: 2 , translate: "Документ"},
                { name: "HelpDeskRequestAttachment", value: 3 , translate: "Документ к запросу"}
            ]
        })

        .constant("GameFamily", {
            Unknown: 0,
            Ark: 1,
            Arma3: 2,
            Cs: 3,
            Css: 4,
            CsGo: 5,
            Cure: 6,
            Dods: 7,
            GMod: 8,
            L4D: 9,
            L4D2: 10,
            Minecraft: 11,
            SaMp: 12,
            T2F: 13,
            Bmdm: 14,
            Cscz: 15,
            Insurgency: 16,
            JustCause2: 17,
            arrayItems: [
                { name: "Unknown", value: 0, translate: ""},
                { name: "Ark", value: 1, translate: ""},
                { name: "Arma3", value: 2, translate: ""},
                { name: "Cs", value: 3, translate: ""},
                { name: "Css", value: 4, translate: ""},
                { name: "CsGo", value: 5, translate: ""},
                { name: "Cure", value: 6, translate: ""},
                { name: "Dods", value: 7, translate: ""},
                { name: "GMod", value: 8, translate: ""},
                { name: "L4D", value: 9, translate: ""},
                { name: "L4D2", value: 10, translate: ""},
                { name: "Minecraft", value: 11, translate: ""},
                { name: "SaMp", value: 12, translate: ""},
                { name: "T2F", value: 13, translate: ""},
                { name: "Bmdm", value: 14, translate: ""},
                { name: "Cscz", value: 15, translate: ""},
                { name: "Insurgency", value: 16, translate: ""},
                { name: "JustCause2", value: 17, translate: ""},
            ]
        })

        .constant("OSSource", {
            "Windows2012":0,
            "Ubuntu":1,
            arrayItems:[
                {name: "Windows2012", value: 0},
                {name: "Ubuntu", value: 1},
            ]
        })

        .constant("PaymentType", {
            "Onpay": 0,
            "Sprypay": 1,
            "Interkassa": 2,
            "PayPal": 3,
            "WebMoney": 4,
            "YandexMoney": 5,
            "TestSystem": 6,
            arrayItems: [
                { name: "Onpay", value: 0},
                { name: "Sprypay", value: 1},
                { name: "Interkassa", value: 2},
                { name: "PayPal", value: 3},
                { name: "WebMoney", value: 4},
                { name: "YandexMoney", value: 5},
                { name: "TestSystem", value: 5}
            ]
        })

        .constant("ReadType", {
            "Unread": 0,
            "Read": 1,
            arrayItems: [
                { name: "Unread", value: 0, translate: "Непрочитано" },
                { name: "Read", value: 1, translate: "Прочитано"  }
            ]
        })

        .constant("ResourceType", {
            "Vm": 0,
            "Gs": 1,
            arrayItems: [
                { name: "Vm", value: 0 },
                { name: "Gs", value: 1 }
            ]
        })

        .constant("StatusTask", {
            "Start": 0,
            "Pending": 1,
            "Processing": 2,
            "End": 3,
            "EndWithErrors": 4,
            "Queued": 5,
            arrayItems: [
                { name: "Start", value: 0, translate: "Ждет начала"},
                { name: "Pending", value: 1, translate: "В ожидании" },
                { name: "Processing", value: 2, translate: "В процессе" },
                { name: "End", value: 3, translate: "Выполнена" },
                { name: "EndWithErrors", value: 4, translate: "Не выполнена" },
                { name: "Queued", value: 5, translate: "В очереди" }
            ]
        })

        .constant("StatusVm", {
            "Enable":0,
            "Disable":1,
            "Error":2,
            "Creating":3,
            arrayItems:[
                {name: "Enable", value: 0, translate: "Работает"},
                {name: "Disable", value: 1, translate: "Выключена"},
                {name: "Error", value: 2, translate: "Ошибочное"},
                {name: "Creating", value: 3, translate: "Создание"}
            ]
        })

        .constant("ManageGameServer", {
            arrayItems:[
                {name: 'Start', value: 0, translate: 'Включить'},
                {name: 'Stop', value: 1, translate: 'Выключить'}
            ]
        })

        .constant("ManageMachine", {
            arrayItems:[
                {name: 'Запуск', value: 0, picture: 'play_arrow'},
                {name: 'Остановка', value: 1, picture: 'pause'},
                {name: 'Перезагрузить', value: 2, picture: 'replay'},
                {name: 'Выключить', value: 3, picture: 'stop'}
            ]
        })

        .constant("SubscriptionType", {
            "Usage":0,
            "Fixed":1,
            arrayItems:[
                {name: "Usage", value: 0, translate: "Облачная"},
                {name: "Fixed", value: 1, translate: "Vps"}
            ]
        })

        .constant("TransactionType", {
            "AutomaticDebiting": 0,
            "Crediting": 1,
            "OneTimeDebiting": 2,
            "SystemBonus": 3,
            arrayItems: [
                { name: "AutomaticDebiting", value: 0, translate: "Автоматическое списание"},
                { name: "Crediting", value: 1, translate: "Кредитование"},
                { name: "OneTimeDebiting", value: 2, translate: "Одноразовое списание"},
                { name: "SystemBonus", value: 3, translate: "Системный бонус"},
            ]
        })

        .constant("TypeChangeStatus", {
            "Start": 0,
            "Stop":1,
            "Reload":2,
            "PowerOff":3,
            arrayItems:[
                {name: "Start", value: 0, translate: "Включить"},
                {name: "Stop", value: 1, translate: "Выключить"},
                {name: "Reload", value: 2, translate: "Перезагрузить"},
                {name: "PowerOff", value: 3, translate: "Откл. питание"}
            ]
        })
        
        .constant("TypeDate", {
            "StartedAt": 0,
            "CompletedAt":1,
            "CreatedAt":2,
            arrayItems:[
                {name: "StartedAt", value: 0, translate: "Запущена"},
                {name: "CompletedAt", value: 1, translate: "Выполнена"},
                {name: "CreatedAt", value: 2, translate: "Создана"}
            ]
        })
        
		.constant("TypeDiscount", {
            "BigPurchase": 0,
            "BonusReplenishment": 1,
            "PurchaseOfLongTerm": 2,
            arrayItems: [
                { name: "BigPurchase", value: 0, translate: "Большая покупка", resourceType: 0, symbol: "$" },
                { name: "BonusReplenishment", value: 1, translate: "Бонус при пополнении", symbol: "$"  },
                { name: "PurchaseOfLongTerm", value: 2, translate: "Скидка за покупку на длительный срок", symbol: "месяцев" }
            ]
        })

        .constant("TypeNotify", {
            "EndTask": 0,
            arrayItems:[
                {name: "EndTask", value: 0}
            ]
        })

        .constant("TypeOfOperatinSystem", {
            "Windows": 0,
            "Ubuntu": 1,            
            arrayItems: [
                { name: "Windows", value: 0},
                { name: "Ubuntu", value: 1}
            ]
        })

        .constant("TypeServerPayment", {
            "Slot": 0,
            "Configuration": 1, 
            arrayItems:[
                {name: "Slot", value: 0, translate: "По слотам"},
                {name: "Configuration", value: 1, translate: "По конфигурации"}
            ]
        })

        .constant("TypeTask", {
            "CreateVm": 0,
            "UpdateVm": 1,
            "ChangeStatus": 2,
            "RemoveVm": 3,
            "Backup": 4,
            "DeleteBackup": 5,
            "CreateSnapshot": 6,
            "DeleteSnapshot": 7,
            "LoadSnapshot": 8,
            "CreateWebHosting": 9,
            "StartWebApp": 10,
            "StopWebApp": 11,
            "RestartWebApp": 12,
            "DisableWebHosting": 13,
            "DeleteHosting": 14,
            "CreateGameServer": 15,
            "DeleteGameServer": 16,
            "GameServerChangeStatus": 17,
            "UpdateGameServer": 18,
            "Test": 99,
            arrayItems: [
                { name: "CreateVm", value: 0, translate: "Создание машины" },
                { name: "UpdateVm", value: 1, translate: "Обновление машины" },
                { name: "ChangeStatus", value: 2, translate: "Изменение статуса машины" },
                { name: "RemoveVm", value: 3, translate: "Удаление машины" },
                { name: "Backup", value: 4, translate: "Бэкап" },
                { name: "DeleteBackup", value: 5, translate: "Удаление бэкапа" },
                { name: "CreateSnapshot", value: 6, translate: "Создание снимка" },
                { name: "DeleteSnapshot", value: 7, translate: "Удаление снимка" },
                { name: "LoadSnapshot", value: 8, translate: "Загрузка снимка" },
                { name: "CreateWebHosting", value: 9, translate: "Создание web хостинга" },
                { name: "StartWebApp", value: 10, translate: "Стар web хостинга" },
                { name: "StopWebApp", value: 11, translate: "Остановка web хостинга" },
                { name: "RestartWebApp", value: 12, translate: "Перезагрузка web хостинга" },
                { name: "DisableWebHosting", value: 13, translate: "Отключение web хостинга" },
                { name: "DeleteHosting", value: 14, translate: "Удаление хостинга" },
                { name: "CreateGameServer", value: 15, translate: "Создание сервера" },
                { name: "DeleteGameServer", value: 16, translate: "Удаление сервера" },
                { name: "GameServerChangeStatus", value: 17, translate: "Изменение статуса сервера" },
                { name: "UpdateGameServer", value: 17, translate: "Обновление сервера" },
                { name: "Test", value: 99, translate: "Тест" }
            ]
        })

        .constant("TypeVirtualization", {
            "HyperV": 0,
            "WmWare": 1,
            arrayItems: [
                { name: "HyperV", value: 0 },
                { name: "WmWare", value: 1 }
            ]
        })

        .constant("UrgencyLevel", {
            "Low": 0,
            "Normal": 1,
            "High": 2,
            "Critical": 3,
            arrayItems: [
                { name: "Low", value: 0, translate: "Низкий"},
                { name: "Normal", value: 1, translate: "Нормальный"},
                { name: "High", value: 2, translate: "Высокий"},
                { name: "Critical", value: 3, translate: "Критический"}
            ]
        })

        .constant("UsageSubscriptionPaymentGroupingTypes", {
            "GroupByPeriod": 0,
            "GroupByPeriodAndSubscriptionVm": 1,
            "GroupNone": 2,
            arrayItems: [
                { name: "GroupByPeriod", value: 0, translate: "По периоду"},
                { name: "GroupByPeriodAndSubscriptionVm", value: 1, translate: "По подпискам и периоду"},
                { name: "GroupNone", value: 2, translate: "Без группировки"}
            ]
        })

        .constant("UserType", {
            "JuridicalPerson": 0,
            "PhysicalPerson": 1,
            arrayItems: [
                { name: "JuridicalPerson", value: 0, translate: "Юридическое лицо" },
                { name: "PhysicalPerson", value: 1, translate: "Физическое лицо"  }
            ]
        })
        .constant("TypeServerTemplate", {
            "GameServer": 0,
            "SystemServer": 1,
            "UserServer": 2,
            arrayItems: [
                { name: "GameServer", value: 0, translate: "Игровой сервер"},
                { name: "SystemServer", value: 1, translate: "Системный сервер"},
                { name: "UserServer", value: 2, translate: "Пользовательский сервер"},
            ]
        })
        .constant("TypeRequest", {


            "New": 0,
            "InProcessing": 1,
            "Completed": 2,
            arrayItems: [
                { value: 0, translate: "Новое" },
                { value: 1, translate: "В процессе" },
                { value: 2, translate: "Выполнено" }
            ]
        })

})();
