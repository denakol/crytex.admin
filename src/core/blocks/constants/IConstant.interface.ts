module core {

    export interface IConstant {
        value: number;
        name: string;
        translate: string;
    }
}
