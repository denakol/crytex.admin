/**
 * Created by denak on 28.02.2016.
 */


module core {


   
    import JsonHelper = core.services.signalr.JsonHelper;
    export class SerializationHelper {
        static toInstance<T>(obj: any, json: string,smallCase:boolean=false) : T {


            var jsonObj = JSON.parse(json);

            if (typeof obj["fromJSON"] === "function") {
                obj["fromJSON"](jsonObj);
            }
            else {
                for (var propName in jsonObj) {
                    obj[propName] = jsonObj[propName]
                }
            }

            if(smallCase)
            {
                JsonHelper.ToSmallCase(obj);
            }
            return obj;
        }







    }
}