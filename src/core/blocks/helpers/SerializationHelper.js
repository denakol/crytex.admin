/**
 * Created by denak on 28.02.2016.
 */
var core;
(function (core) {
    var JsonHelper = core.services.signalr.JsonHelper;
    var SerializationHelper = (function () {
        function SerializationHelper() {
        }
        SerializationHelper.toInstance = function (obj, json, smallCase) {
            if (smallCase === void 0) { smallCase = false; }
            var jsonObj = JSON.parse(json);
            if (typeof obj["fromJSON"] === "function") {
                obj["fromJSON"](jsonObj);
            }
            else {
                for (var propName in jsonObj) {
                    obj[propName] = jsonObj[propName];
                }
            }
            if (smallCase) {
                JsonHelper.ToSmallCase(obj);
            }
            return obj;
        };
        return SerializationHelper;
    }());
    core.SerializationHelper = SerializationHelper;
})(core || (core = {}));
