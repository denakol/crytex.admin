﻿(function () {

    angular.module('crytex.library', [
        // Angular modules
        'ngAnimate',
        'ngResource',
        'ngMessages',   
        'ui.router.title',       
        // Cross-app modules
        "crytex.blocks",
        // 3d-party libraries
    	'ngMaterial',
        'ngScrollbar',
    	'ngMenuSidenav',
    	'LocalStorageModule',
    	'ngFileUpload',
		'angularUtils.directives.dirPagination',
        'monospaced.elastic',
        'angularMoment',
        'chart.js',
        'permission',
        'rzModule',
        'ui.mask',
        'ngDropdowns',
        'md.data.table',
        'angular-ladda'
    ]);



})();

