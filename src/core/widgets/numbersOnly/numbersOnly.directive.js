﻿(function () {

    angular.module("crytex.numbersOnly")
        .directive("numbersOnly", numbersOnly);

    numbersOnly.$inject = ["$document"];

    function numbersOnly($document) {

        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue) {
                        var transformedInput = inputValue.replace(/[^0-9]/g, '');
                        if (transformedInput != inputValue) {
                            modelCtrl.$setViewValue(transformedInput);
                            modelCtrl.$render();
                        }
                        return transformedInput;
                    } else {
                        return '';
                    }
                });
            }
        };

    }

})();