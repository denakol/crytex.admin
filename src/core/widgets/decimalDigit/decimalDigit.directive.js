(function () {

    angular.module("crytex.decimalDigit")
        .directive("decimalDigit", decimalDigit);

    decimalDigit.$inject = ["$document"];

    function decimalDigit($document) {

        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue) {
                        var transformedInput = inputValue.replace(/[^0-9\.]/g, '');
                        if (transformedInput != inputValue) {
                            modelCtrl.$setViewValue(transformedInput);
                            modelCtrl.$render();
                        }
                        return transformedInput;
                    } else {
                        return '';
                    }
                });
            }
        };

    }

})();