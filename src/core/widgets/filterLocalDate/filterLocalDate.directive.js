﻿(function () {

    angular.module("crytex.filterLocalDate")
        .filter('FormatToStandartDate', formatToStandartDate);

    formatToStandartDate.$inject = ['$filter'];
    function formatToStandartDate($filter) {
        // Create the return function and set the required parameter name to **input**
        return function(date) {
            var outDate = $filter('amDateFormat')($filter('amLocal')($filter('amUtc')(date)), 'LLL');
            return outDate;
        }

    };




})();

//amParse | amDateFormat:'LLL'