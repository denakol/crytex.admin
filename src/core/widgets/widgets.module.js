﻿(function () {
    angular.module("crytex.widgets", [
         "crytex.numbersOnly",
         "crytex.confirmPassword",
         "crytex.monitoringChart",
         "crytex.filterLocalDate",
         "crytex.decimalDigit"
    ]);
})();