﻿(function () {

    angular.module("crytex.confirmPassword")
        .directive("compareTo", confirmPassword);

    confirmPassword.$inject = ["$document"];

    function confirmPassword($document) {

        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function(scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function(modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function() {
                    ngModel.$validate();
                });
            }
        };

    }

})();