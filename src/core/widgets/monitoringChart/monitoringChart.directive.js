﻿(function () {

    angular.module("crytex.monitoringChart")
        .directive("mdMonitoringChart", monitoringChart);

    monitoringChart.$inject = ['WebServices' ,'ToastNotificationService', 'monitor'];

    function monitoringChart(WebServices, ToastNotificationService, monitor) {

        return {
            scope: {
                'vmId' : '=vmId'
            },
            link: function(scope, element, attrs) {
                scope.stateData = null;
                scope.labelsList = [];
                scope.dataRamLoadList = [[]];
                scope.dataCpuLoadList = [[]];

                WebServices.AdminStateMachineService.getAll({
                    vmId:scope.vmId,
                    diffTime: 5
                }).$promise
                .then(function(data){
                    scope.stateData = data;
                    return true;
                })
                .then(renderingData)
                .then(listenData)
                .catch(function (error) {
                    if (!isNaN(error)) {
                        ToastNotificationService.showMessage('Не удалось выгрузить данные');
                    }
                });


                //////////////////////////////////////////////////////////////
                function renderingData(){
                    scope.labelsList = scope.stateData.map(function(stateData){
                        return moment(stateData.date).format('YYYY.MM.DD HH:mm:ss');
                    });
                    scope.dataRamLoadList[0] = scope.stateData.map(function(stateData){
                        return stateData.ramLoad;
                    });
                    scope.dataCpuLoadList[0] = scope.stateData.map(function(stateData){
                        return stateData.cpuLoad;
                    });
                    return true;
                }

                function listenData(){
                    var monitoring = new monitor({
                        callback: function (stateMachine) {
                            if(scope.labelsList.length > 10) scope.labelsList.shift();

                            scope.labelsList.push(moment(stateMachine.Date).format('YYYY.MM.DD HH:mm:ss'));

                            if(scope.dataRamLoadList[0].length > 10) scope.dataRamLoadList[0].shift();
                            scope.dataRamLoadList[0].push(stateMachine.RamLoad);

                            if(scope.dataCpuLoadList[0].length > 10) scope.dataCpuLoadList[0].shift();
                            scope.dataCpuLoadList[0].push(stateMachine.CpuLoad);
                        }
                    });
                    monitoring.$promise.then(function()
                    {
                        monitoring.subscribe(scope.vmId);
                    });
                }

            },
            restrict: "E",
            template: "<canvas id='line' class='chart chart-line' " +
                "chart-data='dataRamLoadList' " +
                "chart-labels='labelsList' " +
                "chart-legend='true'" +
                "chart-options='{animation: false, scaleOverride: true, scaleSteps: 32, scaleStepWidth: 512, scaleStartValue: 0}' >" +
            "</canvas>" +
            "<canvas id='line' class='chart chart-line' " +
                "chart-data='dataCpuLoadList' " +
                "chart-labels='labelsList' " +
                "chart-legend='true'" +
                "chart-options='{animation: false, scaleOverride: true, scaleSteps: 10, scaleStepWidth: 10, scaleStartValue: 0}' >" +
            "</canvas>"
        };

    }
})();
