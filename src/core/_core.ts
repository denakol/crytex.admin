/**
 * Created by denak on 26.01.2016.
 */
/// <reference path="typings/angularjs/angular.d.ts" />
/// <reference path="typings/angular-material/angular-material.d.ts" />
/// <reference path="typings/angular-ui-router/angular-ui-router.d.ts" />

module core{

}

interface Array<T> {
    find(predicate: (search: T) => boolean) : T;
}