var gulp = require('gulp'),
    open = require('gulp-open'),
    connect = require('gulp-connect'),
    inject = require('gulp-inject'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    gutil = require('gulp-util'),
    bowerFiles = require('main-bower-files'),
    gulpFilter = require('gulp-filter'),
    templateCache = require('gulp-angular-templatecache'),
    obfuscate = require('gulp-obfuscate'),
    minifyCss = require('gulp-minify-css'),
    ngAnnotate = require('gulp-ng-annotate'),
    flatten = require('gulp-flatten'),
    runSequence = require('run-sequence'),

    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    babel = require('gulp-babel'),
    sourcemaps = require('gulp-sourcemaps');
var constant = {
    admin_url: './.tmp',
    admin_url_name: '/.tmp',
    user_url: './.tmpUser',
    user_url_name: '/.tmpUser',
    dev_admin_port: '8888'
}

var fontFilter = gulpFilter(['**/*.eot', '**/*.woff', '**/*.woff2', '**/*.ttf', '**/*.otf']);

var filterJS = gulpFilter('**/*.js', {
    restore: true
});
var cssFilter = gulpFilter('**/*.css', {
    restore: true
});


gulp.task('webserver', function () {

    connect.server({
        root: constant.admin_url,
        livereload: true,
        port: 8888
    });

});

gulp.task('build', function () {


    gulp.src(['src/admin/**/*.svg', 'src/admin/**/*.png'])
        .pipe(gulp.dest(constant.admin_url + '/client'));
    return runSequence(['ClientJsTask', 'VendorTask', 'CssTask', 'TemplatesTask'], 'FontTask',
        'BabelAdmin', 'InjectTask'
    );
});

gulp.task('BabelAdmin', function () {
        // return gulp.src(constant.admin_url + '/client/**/*.js')
        //        .pipe(babel({
        //             presets: ['es2015'],
        //             compact: false,
        //             sourceMaps: 'both'
        //         }))
        //         .pipe(gulp.dest(constant.admin_url + '/client/'));

        return true;
    }
);

function ClientJs() {
    return gulp.src([
            'src/core/**/*.module.js',
            'src/core/**/*.Enum.js',
            'src/core/**/*.model.js',
            'src/core/**/*.helper.js',
            'src/core/**/*.js',
            'src/admin/**/*.module.js',
            'src/admin/**/*.Enum.js',
            'src/admin/**/*.model.js',
            'src/admin/**/*.helper.js',
            'src/admin/**/*.js'
        ], {
            read: true
        })

        .pipe(sourcemaps.init())
        .pipe(concat('all.js').on('error', gutil.log))
        .pipe(sourcemaps.write())

        //      .pipe(uglify().on('error', gutil.log))
        .pipe(gulp.dest(constant.admin_url + '/client'));
}

gulp.task('ClientJsTask', function () {
    ClientJs();
});
gulp.task('ClientJs', function () {

    return runSequence('ClientJsTask', 'BabelAdmin',
        'InjectTask'
    );

});

function Vendor() {
    var sourceBower = bowerFiles();
    sourceBower.push("components/moment/locale/ru.js");
    sourceBower.push("components/ladda/dist/ladda-themeless.min.css");
    sourceBower.push('!components/ladda/dist/ladda.min.css');

    return gulp.src(sourceBower)
        .pipe(filterJS)
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.js'))
        .pipe(sourcemaps.write())
        // .pipe(uglify())
        .pipe(filterJS.restore)
        .pipe(cssFilter)
        //   .pipe(minifyCss())
        .pipe(concat('vendor.css'))
        .pipe(cssFilter.restore)
        // grab vendor font files from bower_components and push in /public
        .pipe(gulp.dest(constant.admin_url + '/vendor'));
}
gulp.task('VendorTask', function () {
    return Vendor();
});
gulp.task('Vendor', function () {
    return runSequence('VendorTask',
        'InjectTask'
    );
});

function Css() {
    return gulp.src(['src/admin/**/*.css'])
        .pipe(sourcemaps.init())
        .pipe(concat('all.css').on('error', gutil.log))
        .pipe(sourcemaps.write())
        // .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(gulp.dest(constant.admin_url + '/client'));

}


gulp.task('CssTask', function () {
    return Css();
});

gulp.task('Css', function () {
    return runSequence('CssTask',
        'InjectTask'
    );

});


function Font() {
    gulp.src('src/admin/**/*.*')
        .pipe(fontFilter)
        .pipe(gulp.dest(constant.admin_url + '/client'));

}
gulp.task('FontTask', function () {
    return Font();
});

function Templates() {
    return gulp.src(['src/admin/**/*.html', 'scr/admin/content/pagination/**/*.html'])
        .pipe(templateCache({
            standalone: true
        }))
        .pipe(gulp.dest(constant.admin_url + '/templates'));
}
gulp.task('TemplatesTask', function () {
    return Templates();
});
gulp.task('Templates', function () {
    return runSequence('TemplatesTask',
        'InjectTask'
    );
});
gulp.task('InjectTask', function () {
    return Inject();
});

function Inject() {
    var sourcesVendorjs = gulp.src([constant.admin_url + '/vendor/*.js']);
    var sourcesClient = gulp.src([

        constant.admin_url + '/client/**/*.js'
    ], {
        read: true
    });
    var target = gulp.src('./src/admin/index.html');
    var sourcesCss = gulp.src(
        [
            constant.admin_url + '/vendor/**/*.css',
            constant.admin_url + '/client/**/*.css'
        ]
    );
    var templates = gulp.src([constant.admin_url + '/templates/**/*.js']);

    return target
        .pipe(inject(templates, {
            name: 'templates',
            ignorePath: constant.admin_url_name
        }).on('error', gutil.log))
        .pipe(inject(sourcesVendorjs, {
            name: 'vendor',
            ignorePath: constant.admin_url_name
        }).on('error', gutil.log))
        .pipe(inject(sourcesClient, {
            ignorePath: constant.admin_url_name
        }).on('error', gutil.log)).on('error', gutil.log)
        .pipe(inject(sourcesCss, {
            ignorePath: constant.admin_url_name
        }).on('error', gutil.log))
        .pipe(gulp.dest(constant.admin_url));
}
gulp.task('watch', function () {
    gulp.watch(['src/admin/**/*.js', 'src/core/**/*.js'], ['ClientJs']);
    gulp.watch(['bower.json'], ['Vendor']);
    gulp.watch(['src/admin/**/*.css'], ['Css']);
    gulp.watch(['src/admin/**/*.html'], ['Templates']);
});


gulp.task('open', function () {

    var options = {
        uri: 'http://localhost:8888/index.html',
        app: 'chrome'
    };

    gulp.src(__filename).pipe(open(options));
});

gulp.task('default', ['webserver', 'build', 'watch']);

gulp.task('start', ['webserver', 'build', 'open', 'watch']);


/*-----------------------User js------------------------------------------------------*/
/*-----------------------User js------------------------------------------------------*/
/*-----------------------User js------------------------------------------------------*/
/*-----------------------User js------------------------------------------------------*/


gulp.task('webserverUser', function () {

    connect.server({
        root: constant.user_url,
        livereload: true,
        port: 8889
    });

});

gulp.task('buildUser', function () {
    gulp.src(['src/user/**/*.svg', 'src/user/**/*.png', 'src/user/**/*.jpg'])
        .pipe(gulp.dest(constant.user_url + '/client'));
    return runSequence(['ClientJsTaskUser', 'VendorTaskUser', 'CssTaskUser', 'ScssTask', 'TemplatesTaskUser'], 'FontTaskUser',
        'BabelClient', 'InjectTaskUser'
    );
});

gulp.task('BabelClient', function () {
        // return gulp.src(constant.user_url + '/client/**/*.js')
        //     .pipe(babel({
        //         presets: ['es2015'],
        //         compact: false,
        //         sourceMaps: 'both'
        //     }).on('error', gutil.log))
        //     .pipe(gulp.dest(constant.user_url + '/client/'));
        return true;
    }
);

function ClientJsUser() {
    return gulp.src([
        /*здесь можно вписать что брать с админки
         */


        'src/core/**/*.module.js',
        'src/core/**/*.Enum.js',
        'src/core/**/*.model.js',
        'src/core/**/*.helper.js',
        'src/core/**/*.js',
        'src/user/**/*.module.js',
        'src/user/**/*.Enum.js',
        'src/user/**/*.model.js',
        'src/user/**/*.helper.js',
        'src/user/**/*.js',


    ], {
        read: true
    }).pipe(sourcemaps.init())
        .pipe(concat('all.js').on('error', gutil.log))
        //      .pipe(uglify().on('error', gutil.log))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(constant.user_url + '/client'));
}

gulp.task('ClientJsTaskUser', function () {
    return ClientJsUser();
});
gulp.task('ClientJsUser', function () {

    return runSequence('ClientJsTaskUser', 'BabelClient',
        'InjectTaskUser'
    );

});

function VendorUser() {
    var sourceBower = bowerFiles();
    sourceBower.push("components/moment/locale/ru.js");
    sourceBower.push("components/ladda/dist/ladda-themeless.min.css");
    sourceBower.push('!components/ladda/dist/ladda.min.css');

    return gulp.src(sourceBower)
        .pipe(filterJS)
        .pipe(concat('vendor.js'))

        // .pipe(uglify())
        .pipe(filterJS.restore)
        .pipe(cssFilter)
        //   .pipe(minifyCss())
        .pipe(concat('vendor.css'))
        .pipe(cssFilter.restore)
        // grab vendor font files from bower_components and push in /public
        .pipe(gulp.dest(constant.user_url + '/vendor'));
}
gulp.task('VendorTaskUser', function () {
    return VendorUser();
});
gulp.task('VendorUser', function () {
    return runSequence('VendorTaskUser',
        'InjectTaskUser'
    );
});

function CssUser() {
    return gulp.src(['src/user/**/*.css'])
        .pipe(sourcemaps.init())
        .pipe(concat('all.css').on('error', gutil.log))
        .pipe(sourcemaps.write())
        // .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(gulp.dest(constant.user_url + '/client'));

}

function ScssConvert() {
    return gulp.src(['src/user/**/*.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(concat('all.sass.css').on('error', gutil.log))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(constant.user_url + '/client'));
}
gulp.task('ScssTask', function () {
    return ScssConvert();
});

gulp.task('CssTaskUser', function () {
    return CssUser();
});
gulp.task('CssUser', function () {
    return runSequence('CssTaskUser', 'ScssTask',
        'InjectTaskUser'
    );

});

function FontUser() {
    gulp.src('src/user/**/*.*')
        .pipe(fontFilter)
        .pipe(gulp.dest(constant.user_url + '/client'));

}
gulp.task('FontTaskUser', function () {
    return FontUser();
});

function TemplatesUser() {
    return gulp.src(['src/core/**/*.html', 'src/user/**/*.html', 'scr/user/content/pagination/**/*.html'])
        .pipe(templateCache({
            standalone: true
        }))
        .pipe(gulp.dest(constant.user_url + '/templates'));
}
gulp.task('TemplatesTaskUser', function () {
    return TemplatesUser();
});
gulp.task('TemplatesUser', function () {
    return runSequence('TemplatesTaskUser',
        'InjectTaskUser'
    );
});
gulp.task('InjectTaskUser', function () {
    return InjectUser();
});

function InjectUser() {
    var sourcesVendorjs = gulp.src([constant.user_url + '/vendor/*.js']);
    var sourcesClient = gulp.src([
        constant.user_url + '/client/**/*.js'
    ]);
    var target = gulp.src('./src/user/index.html');
    var sourcesCss = gulp.src(
        [
            constant.user_url + '/vendor/**/*.css',
            constant.user_url + '/client/**/*.css'
        ]
    );
    var templates = gulp.src([constant.user_url + '/templates/**/*.js']);

    return target
        .pipe(inject(templates, {
            name: 'templates',
            ignorePath: constant.user_url_name
        }).on('error', gutil.log))
        .pipe(inject(sourcesVendorjs, {
            name: 'vendor',
            ignorePath: constant.user_url_name
        }).on('error', gutil.log))
        .pipe(inject(sourcesClient, {
            ignorePath: constant.user_url_name
        }).on('error', gutil.log)).on('error', gutil.log)
        .pipe(inject(sourcesCss, {
            ignorePath: constant.user_url_name
        }).on('error', gutil.log))
        .pipe(gulp.dest(constant.user_url));
}


gulp.task('watchUser', function () {
    gulp.watch(['src/user/**/*.js', 'src/core/**/*.js'], ['ClientJsUser']);
    gulp.watch(['bower.json'], ['VendorUser']);
    gulp.watch(['src/user/**/*.css', 'src/user/**/*.scss'], ['CssUser']);
    gulp.watch(['src/user/**/*.html'], ['TemplatesUser']);
});


gulp.task('openUser', function () {

    var options = {
        uri: 'http://localhost:8889/index.html',
        app: 'chrome'
    };

    gulp.src(__filename).pipe(open(options));
});
gulp.task('TypeScript', function () {
    var tsProject = ts.createProject('tsconfig.json');
    var tsResult = tsProject.src() // instead of gulp.src(...)
        .pipe(ts(tsProject));

    return tsResult.js.pipe(rename({
            suffix: '.generated'
        }))
        .pipe(gulp.dest(""));


});

gulp.task('watchTypeScript', function () {
    gulp.watch(['src/**/*.ts'], ['TypeScript']);

});


gulp.task('defaultUser', ['webserverUser', 'buildUser', 'watchUser']);

gulp.task('startUser', ['webserverUser', 'buildUser', 'openUser', 'watchUser', 'watchTypeScript']);

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//__________________________________________________________________________
